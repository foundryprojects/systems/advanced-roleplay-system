import * as utilitiesManager from '../utilities.js';
import * as debug from '../debug.js';

/**
 *
 * DISABLED for now, v10 issues, rumor has it they are adding drag/drop from header :)
 *
 * We extend this so we can manipulate the template and add the drag-link
 *
 */
export class ARSJournalSheet extends JournalSheet {
    /**
     * Prompt the user with a Dialog for creation of a new JournalEntryPage
     *
     * override so we can put the new page entry below where we are currently selected
     *
     */
    async createPage() {
        const bounds = this.element[0].getBoundingClientRect();
        const options = { parent: this.object, width: 320, top: bounds.bottom - 200, left: bounds.left + 10 };
        let sort;
        let title = { show: true, level: 1 };
        if (this._pages && typeof this.pageIndex === 'number') {
            const currentSort = this._pages[this?.pageIndex]?.sort ?? 0;
            // add it to right after current index, sort order is 10 above it
            sort = currentSort ? currentSort + CONST.SORT_INTEGER_DENSITY / 10000 : currentSort + CONST.SORT_INTEGER_DENSITY;
            title = this._pages[this?.pageIndex]?.title ?? { show: true, level: 1 };
            console.log(`Adding journal entry at ${sort} and current sort is ${currentSort}`);
        } else {
            // Default sort value if _pages or pageIndex is missing
            sort = 0 + CONST.SORT_INTEGER_DENSITY;
        }
        const entry = await JournalEntryPage.implementation.createDialog({ sort }, options);

        if (entry) {
            console.log(`Added journal entry ${entry.name} at ${entry.sort}`);
            entry.update({ title });
        }
    }

    // TESTing
    // _canDragStart(event) {
    //     console.log('ARSJournalSheet _canDragStart');
    //     return super._canDragStart(event);
    // }
    // _onDragStart(event) {
    //     console.log('ARSJournalSheetJournal sheet dragstart');
    //     super._onDragStart(event);
    //     // if (ui.context) ui.context.close({ animate: false });
    //     // const target = event.currentTarget;
    //     // const pageId = target.closest('[data-page-id]').dataset.pageId;
    //     // const anchor = target.closest('[data-anchor]')?.dataset.anchor;
    //     // const page = this.object.pages.get(pageId);
    //     // const dragData = {
    //     //     ...page.toDragData(),
    //     //     anchor: { slug: anchor, name: target.innerText },
    //     // };
    //     // console.log('ARSJournalSheet Journal event drag start', dragData);
    //     // event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
    // }

    /** @inheritdoc */
    //     get template() {
    //         if (this._sheetMode === 'image') return ImagePopout.defaultOptions.template;
    //         return 'systems/ars/templates/journal/journal-sheet.hbs';
    //     }

    //     /** @override */
    //     activateListeners(html) {
    //         super.activateListeners(html);

    //         let _dragLinkHandler = (event) => {
    //             event.dataTransfer.setData(
    //                 'text/plain',
    //                 JSON.stringify({
    //                     type: this.object.documentName,
    //                     pack: this.object.pack,
    //                     id: this.object.id,
    //                     uuid: this.object.uuid,
    //                 })
    //             );
    //         };
    //         html.find('#drag-link').each((i, draglink) => {
    //             draglink.setAttribute('draggable', true);
    //             draglink.addEventListener('dragstart', _dragLinkHandler, false);
    //         });
    //     }
}
