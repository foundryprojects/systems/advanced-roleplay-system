import { ARS } from '../config.js';
import { ARSDice } from '../dice/dice.js';
import * as effectManager from '../effect/effects.js';
import * as utilitiesManager from '../utilities.js';
import { ARSCardPopout } from '../apps/cardPopout.js';
import { ARSAction, ARSActionGroup } from '../action/action.js';
import * as debug from '../debug.js';
import { CombatManager } from '../combat/combat.js';

/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class ARSItem extends Item {
    // /** @inheritDoc */
    // static defineSchema() {
    //     const fields = foundry.data.fields;
    //     return foundry.utils.mergeObject(super.defineSchema(), {
    //         ...itemDescriptionSchema.defineSchema(),
    //         ...itemDetailsSchema.defineSchema(),
    //         itemARSTEST: new fields.StringField({ initial: 'Bob123' }),
    //         alias: new fields.StringField(),
    //         name: new fields.StringField(),
    //         type: new fields.StringField(),
    //     });
    // }

    chatTemplate = {
        // "armor": "systems/ars/templates/chat/parts/chatCard-armor.hbs",
        skill: 'systems/ars/templates/chat/parts/chatCard-skill.hbs',
        weapon: 'systems/ars/templates/chat/parts/chatCard-weapon.hbs',
        spell: 'systems/ars/templates/chat/parts/chatCard-spell.hbs',
        action: 'systems/ars/templates/chat/parts/chatCard-actionV2.hbs',
        potion: 'systems/ars/templates/chat/parts/chatCard-actionV2.hbs',
        power: 'systems/ars/templates/chat/parts/chatCard-power.hbs',
    };

    /**@override */
    async _onUpdate(data, options, id) {
        // console.log('item.js _onUpdate', { data, options, id });
        await super._onUpdate(data, options, id);
        if (game.user.id !== id) return;

        if (game.user.isDM) {
            // console.log('hooks.js updateItem', { item, context, diff, id });
            if (
                this.actor &&
                (data?.system?.attributes?.hasOwnProperty('identified') || data?.system?.hasOwnProperty('location'))
            ) {
                // console.warn("changes include identified state");
                const token = this.actor.getToken();
                if (token) {
                    await token.object.updateAuras();
                    await token.checkForAuras();
                }
            }
        }
    }

    /**@override to allow view of merchant items */
    testUserPermission(user, permission, options) {
        if (game.user.isGM) {
            return super.testUserPermission(user, permission, options);
        } else if (this?.actor?.type == 'merchant') {
            if ([CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED, 'LIMITED'].includes(permission) && !options) {
                return this.permission >= CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED;
            }
        }
        return super.testUserPermission(user, permission, options);
    }

    /**
     * Augment the basic Item data model with additional dynamic data.
     */
    prepareData() {
        super.prepareData();
        // console.log("item.js prepareData ITEM ====> this", this);
        // const data = this.data;
    }

    /** @override */
    prepareDerivedData() {
        new Promise((resolve, reject) => {
            super.prepareDerivedData();
            resolve();
        }).then(async () => {
            // for v10
            this.name = this.getName();

            // todo: utility -- unless foundry has something like this?
            // detect actual description by scrubbing html/whitespace
            const node = document.createElement('div');
            node.innerHTML = this.system.description;
            const desc = (node.textContent || '').replace(/\s/g, '');
            this.hasDescription = false;
            if (desc != '') {
                this.hasDescription = true;
            }

            if (
                this.isOwned &&
                this.system.quantity > 0 &&
                !game.ars.config.nonInventoryTypes.includes(this.type) &&
                (this.system.location.state === 'equipped' || this.system.location.state === 'carried')
            ) {
                this.system.carriedweight = parseFloat((this.system.quantity * this.system.weight).toFixed(2));
            }
            if (this.isOwned) {
                if (this.system?.attack?.type && this.system.attack.type !== 'melee') {
                    this.isRanged = true;
                }
                this.ammo = await this.getAmmo();
                if (this.ammo) {
                    this.ammoCount = parseInt(this.ammo.system.quantity) || 0;
                }
            }
            await this._prepareContains();
        });

        //TODO: work on the new actions functionality
        // this.actionGroups = ARSAction.convertFromActionBundle(this, this.system.actions);
        if (!this.actionGroups) this.actionGroups = ARSActionGroup.loadAll(this);
        ARSActionGroup.prepareDerivedData(this.parent, this);
    }

    /**
     * prepare contans list for contained items
     */
    async _prepareContains() {
        // console.log('item.js _prepareContains', this);
        let contains = [];
        let removedItems = [];
        let containedNames = [];
        let capacityCarried = 0;

        if (this.containedIn) {
            if (!this.containedIn?.system?.itemList?.some((item) => item.id === this.id)) {
                // console.warn(
                //     `${this.name} is not found in ${this.containedIn.name}'s itemList. Setting containedIn to undefined.`
                // );
                this.containedIn = undefined;
            }
        }
        if (this.system?.itemList?.length) {
            for (const carried of this.system.itemList) {
                try {
                    if (!this.actor && !carried.uuid) {
                        // ui.notifications.warn(`Item ${this.name} contains ${carried.name} and it does not have actor/owner and is missing uuid.`);
                        console.log(
                            `Item ${this.name} contains ${carried.name} and it does not have actor/owner and is missing uuid.`,
                            { carried }
                        );
                    }

                    let subItem = undefined;
                    if (this.actor) {
                        subItem = this.actor.items.get(carried.id);
                    }
                    if (!subItem && carried.uuid) {
                        subItem = await fromUuid(carried.uuid);
                        if (!subItem) {
                            const id = carried.uuid.split('.').pop();
                            subItem = await utilitiesManager.getItem(id);
                        }
                    }
                    if (!subItem) subItem = game.items.get(carried.id);

                    if (subItem) {
                        contains.push(subItem);
                        containedNames.push(subItem.name);
                        subItem.containedIn = this;
                        const count = parseFloat(subItem.system?.quantity) || 0;
                        const weight = parseFloat(subItem.system?.weight) || 0;
                        capacityCarried += count * weight;
                        //if subItem is also container...
                        if (subItem.contains.length && subItem?.capacity?.value) {
                            capacityCarried += parseFloat(subItem.capacity.value) || 0.0;
                        }
                    } else {
                        // for now we ignore any compendium entries
                        let packName = this?.actor?.pack ?? null;
                        if (!packName) packName = this?.pack;
                        if (packName) {
                            // Do nothing to anything in a compendium
                        } else {
                            console.warn(
                                `Dropped ${carried.name}(${carried.uuid}/${carried.id}) from ${this?.actor?.name} in ${this.name}, could not find it.`
                            );
                            removedItems.push(carried.id);
                        }
                    }
                } catch (err) {
                    // error, this happens due to sequencing. game.items and
                    // this.actor.items don't exist during certain phases but eventually do.
                }
            }
            if (removedItems.length && game.user.isGM) {
                let bundle = foundry.utils.duplicate(this.system.itemList); // Create a duplicate to avoid direct mutation
                bundle = bundle.filter((item) => !removedItems.includes(item.id)); // Filter out removed items
                await this.update({ 'system.itemList': bundle }); // Correct update structure
            }

            const roundedCarryWeight = parseFloat(capacityCarried).toFixed(2) || 0;

            let overloaded = false;
            if (this.system?.capacity?.max > 0) {
                if (roundedCarryWeight > this.system.capacity.max) {
                    overloaded = true;
                }
            }
            this.capacity = {
                value: roundedCarryWeight,
                names: containedNames.join('+'),
                overloaded,
            };
        } else {
            this.capacity = {
                value: 0,
                names: '',
                overloaded: false,
            };
        }

        this.contains = contains;
    }

    //getter to return item alias
    get alias() {
        return this.system.alias;
    }

    /** getter to get profs that are applied to this item */
    get appliedProfs() {
        let profs = [];
        if (this.isOwned) {
            // get the modifiers for hit from proficiencies
            for (const profItem of this.parent?.proficiencies) {
                for (const weapon of Object.values(profItem.system.appliedto)) {
                    if (weapon.id === this.id) {
                        profs.push(profItem);
                    }
                }
            } // end profs check
        } else {
            profs = null;
        }

        // console.log("item.js getter appliedProfs", { profs })
        return profs;
    }

    /**getter to return weapon speed adjusted for profs */
    get speed() {
        if (this.type == 'weapon') {
            const profs = this.appliedProfs;
            let weaponSpeed = this.system?.attack?.speed || 0;
            if (profs?.length) {
                for (const prof of profs) {
                    const speed = Number(prof.system.speed);
                    if (!isNaN(speed)) {
                        weaponSpeed = weaponSpeed + speed;
                    }
                }
            }
            return weaponSpeed;
        }
        return undefined;
    }

    /** Getter of conditionals from item */
    get conditionals() {
        let conditionals = this?.system?.conditionals || [];
        const profs = this.appliedProfs;
        if (profs && profs.length) {
            for (const prof of profs) {
                if (prof.system?.attributes?.conditionals)
                    for (const cond of Object.values(prof.system.attributes.conditionals)) {
                        conditionals.push(foundry.utils.mergeObject(cond, { name: prof.name }));
                    }
            }
        }

        // console.log("item.js getter conditionals", { conditionals })
        return conditionals;
    }

    /**getter to determine if item is identified  */
    get isIdentified() {
        if (game.user.isGM || ARS.nonInventoryTypes.includes(this.type)) {
            return true;
        } else {
            const identified = this.system?.attributes ? this.system.attributes.identified : true;
            return identified;
        }
    }
    get isIdentifiedRaw() {
        const identified = this.system?.attributes ? this.system.attributes.identified : true;
        return identified;
    }
    get notIdentified() {
        return !this.isIdentified;
    }
    get isMagic() {
        return this.system?.attributes ? this.system.attributes.magic : false;
    }
    get notMagic() {
        return this.system?.attributes ? !this.system.attributes.magic : true;
    }
    get isArmor() {
        return this.type === 'armor' && this.system?.protection?.type.toLowerCase() === 'armor';
    }
    get isShield() {
        return this.type === 'armor' && this.system?.protection?.type.toLowerCase() === 'shield';
    }
    get isWornArmor() {
        return this.isArmor && ['equipped'].includes(this.system?.location?.state);
    }
    get isWornShield() {
        return this.isShield && ['equipped'].includes(this.system?.location?.state);
    }
    get isWornArmorOrShield() {
        return (this.isShield || this.isArmor) && ['equipped'].includes(this.system?.location?.state);
    }
    get isScroll() {
        return this.type === 'spell' && this.system?.attributes?.type?.toLowerCase() === 'scroll';
    }
    get isSpell() {
        return this.type === 'spell' && this.system?.attributes?.type?.toLowerCase() !== 'scroll';
    }

    /** getter to return best attack rate with weapon */
    get bestAttackRate() {
        let bestRate = this.system?.attack?.perRound || '1/1';
        if (this.actor) {
            bestRate = this.actor.getBestAttackRate(this);
        }
        return bestRate;
    }

    // /** @override */
    // get name() {
    //   if (!game.ars.config.settings.identificationItem)
    //     return super.name();

    //   if (game.user.isGM)
    //     return `${super.name}`;
    //   if (['item', 'armor', 'container', 'potion', 'spell', 'weapon'].includes(this.type) && !this.isIdentified) {
    //     return this.alias ? this.alias : `${game.i18n.localize("ARS.unknown")} ${this.type}`;
    //   }
    //   return super.name;
    // }

    /**
     *
     * Wrapper for old getting for item.name
     *
     * @returns
     */
    getName() {
        if (!game.ars.config.settings.identificationItem) return this.name;

        if (game.user.isGM) return `${this.name}`;
        if (['item', 'armor', 'container', 'potion', 'spell', 'weapon'].includes(this.type) && !this.isIdentified) {
            return this.alias ? this.alias : `${game.i18n.localize('ARS.unknown')} ${this.type}`;
        }
        return this.name;
    }

    get nameRaw() {
        return this.name;
    }

    /**
     * inContainer getter
     */
    get inContainer() {
        if (this.containedIn) return true;

        return false;
    }

    //getter to return if this is ammo
    get isAmmo() {
        if (
            ['ammunition'].includes(this.system?.attributes?.type.toLowerCase()) ||
            ['ammunition'].includes(this.system?.attributes?.subtype.toLowerCase())
        ) {
            return true;
        }
        return false;
    }
    get notAmmo() {
        return !this.isAmmo;
    }

    //getter for css used for items
    get css() {
        return [
            !this.isIdentifiedRaw && game.user.isGM ? 'gm-unidentified' : '',
            this.isMagic && this.isIdentified ? 'magic' : '',
        ].filterJoin(' ');
    }
    /**
     * is this item equpped?
     */
    get isEquipped() {
        return this.isOwned && this.system.location?.state === 'equipped';
    }

    /** getter to return "retail" price (with margin) or item */
    get retail() {
        const margin = game.settings.get('ars', 'itemBrowserMargin');
        let itemAtCost = parseInt(this.system?.cost?.value) ?? 0;
        if (isNaN(itemAtCost)) itemAtCost = 0;
        // convert -100 to 100 to percent and get value markup/down value
        return itemAtCost ? Math.ceil(itemAtCost + itemAtCost * (margin * 0.01)) : 0;
    }

    /** getter to return boolean if this item can be looted */
    get lootable() {
        return this.system?.attributes?.type !== 'No-Drop' && this.system?.attributes?.subtype !== 'No-Drop';
    }
    get notLootable() {
        return !this.lootable;
    }

    /** getter to return the level of this class item, otherwise 0 */
    get classLevel() {
        if (this.isOwned && this?.type === 'class') {
            let advancementBundle = Object.values(foundry.utils.deepClone(this.system.advancement));
            return advancementBundle.length || 0;
        }
        return 0;
    }

    async _preCreate(data, options, user) {
        // do stuff to data, options, etc
        // console.log("item.js _preCreate", { data, options, user })
        await super._preCreate(data, options, user);
    }

    // _onCreate(data, options, userId) {
    //   console.log("item.js _onCreate", { data, options, userId });
    //   const item = super._onCreate(data, options, userId);
    //   console.log("item.js _onCreate", { item });
    // }

    /**
     * Send data from chat click roll to a chatCard template
     *
     * @param {*} data
     */
    // async _chatRoll(data = {}) {
    //     console.log('item.js _chatRoll', { data });

    //     const sourceKeyTag = this.uuid;
    //     let chatData = {
    //         author: game.user.id,
    //         speaker: ChatMessage.getSpeaker({ actor: data.sourceActor }),
    //         flags: { 'ars.chatCard.sourceId': sourceKeyTag },
    //     };

    //     // console.log("item.js _chatRoll", { data });

    //     const rAG = this.system.actionList[this.system.actions[0]?.name];
    //     let cardData = {
    //         config: ARS,
    //         ...this,
    //         item: data.item,
    //         sourceActor: data.sourceActor,
    //         sourceToken: data.sourceToken,
    //         owner: data.sourceActor.id,
    //         actions: data.actions,
    //         actionGroupData: rAG,
    //         actionGroups: this.system.actionList,
    //         ...data,
    //     };

    //     // make the popout card for them to view (this is the window card, not the card in chat)
    //     const popoutCard = new ARSCardPopout(cardData).render(true);

    //     // check the last message in chat to see if it matches this item, dont reprint if so.
    //     let lastMsg = game.messages.contents.length - 1;
    //     let lastEntry = game.messages.contents[lastMsg];
    //     const lastMsgSourceId = lastEntry?.flags?.ars?.chatCard?.sourceId;
    //     if (lastMsgSourceId === sourceKeyTag) {
    //         console.warn(`${this.name} is already in the chat output as last message.`);
    //         return null;
    //     }
    //     const templateType = data.type ? data.type : this.type;
    //     chatData.content = await renderTemplate(this.chatTemplate[templateType], cardData);
    //     return ChatMessage.create(chatData);
    // }

    async _chatRoll(context = {}) {
        console.log('item.js _chatRoll', { context });

        const sourceKeyTag = this.uuid;
        let chatData = {
            author: game.user.id,
            speaker: ChatMessage.getSpeaker({ actor: context.sourceActor }),
            flags: { 'ars.chatCard.sourceId': sourceKeyTag },
        };

        let cardData = {
            config: ARS,
            ...this,
            item: context.item,
            sourceActor: context.sourceActor,
            sourceToken: context.sourceToken,
            owner: context.sourceActor.id,
            actionGroup: context.actionGroup,
            actionGroups: this.actionGroups,
            ...context,
        };

        // make the popout card for them to view (this is the window card, not the card in chat)
        const popoutCard = new ARSCardPopout(cardData).render(true);

        // check the last message in chat to see if it matches this item, dont reprint if so.
        let lastMsg = game.messages.contents.length - 1;
        let lastEntry = game.messages.contents[lastMsg];
        const lastMsgSourceId = lastEntry?.flags?.ars?.chatCard?.sourceId;
        if (lastMsgSourceId === sourceKeyTag) {
            console.warn(`${this.name} is already in the chat output as last message.`);
            return null;
        }
        const templateType = context.type ? context.type : this.type;
        chatData.content = await renderTemplate(this.chatTemplate[templateType], cardData);
        return ChatMessage.create(chatData);
    }
    /**
     *
     * This runs a item in macro bar, looking for actions, skill item or memslots
     *
     * @param {*} actor
     */
    async runMacro(macroData = {}) {
        const actor = this.actor;

        console.log('item.js runMacro', { actor, macroData }, this);

        switch (this.type) {
            case 'skill':
                const actorToken = actor.getToken();
                const skillCheckRoll = await new ARSDice(actorToken, this).makeSkillRoll();
                break;

            case 'weapon':
                actor._makeAttackWithItem(null, this);
                break;

            case 'spell':
            case 'memorization':
                // actor might not exist for a spell since it can use
                // spells from World or Compendium directly
                // so it has to exist in the macroData
                const data = {
                    item: this,
                    sourceActor: macroData.actor,
                    actions: this.system.actions,
                    slotIndex: macroData.index,
                    slotLevel: macroData.level,
                    slotType: macroData.type,
                };
                // data.slotIndex = macroData.index;
                // data.slotLevel = macroData.level;
                // data.slotType = macroData.type;
                this._chatRoll(data);
                break;

            default:
                ui.notifications.error(`Unknown macro type ${this.type}`);
                break;
        }
    }

    /**
     *
     * Some after create things to adjust for specific items.
     *
     */
    postCreateItem() {
        let updates = {};
        // console.log("item.js postCreateItem")
        if (this.isOwned) {
            // set quantity to 1 if it's set to 0
            // console.log("item.js postCreateItem", this);
            if (!this.system.quantity) {
                updates['system.quantity'] = 1;
            }
            // look for inventory items, set default equip state
            if (!game.ars.config.nonInventoryTypes.includes(this.type)) {
                switch (this.type) {
                    case 'armor':
                    case 'weapon':
                        updates['system.location.state'] = game.ars.library.const.location.EQUIPPED;
                        break;
                    default:
                        updates['system.location.state'] = game.ars.library.const.location.CARRIED;
                        break;
                }
            }

            // if (['background', 'race'].includes(this.type)) {
            //     this.actor.sheet._reconfigureAcademics(this, this.actor.getMaxLevel(), false);
            // }
            console.log(`${this.actor.name} added ${this.name} to inventory.`);
        }

        // set a defailt icon when first made
        if (!this.img || this.img === 'icons/svg/item-bag.svg') updates['img'] = CONFIG.ARS.icons.general.items[this.type];
        // console.log("item.js postCreateItem ", { updates });
        this.update(updates);
    }

    /**
     *
     * Return ammo
     *
     * @param {Boolean} (optional) If set, only return ammo if it's a valid weapon type
     *
     * @returns Item
     */
    async getAmmo(ammoIsWeapon = false) {
        let ammo = null;
        if (this.isOwned && this.actor && this.type === 'weapon') {
            const ammoId = this.system?.resource?.itemId;
            if (ammoId) {
                ammo = await this.actor.getEmbeddedDocument('Item', ammoId);
                // only return ammo if it's a weapon if ammoIsWeapon
                if (ammo && ammo.type !== 'weapon' && ammoIsWeapon) ammo = null;
            }
        }
        return ammo;
    }

    /**
     * Creates a small text block (html) for this item, typically weapons
     *
     * @returns
     */
    async getStatBlock() {
        // todo: work on this - remmember, they could bypass this dialog
        let statblock = `<div class="lbl_attack_dialog" style="text-align:center;"><b>${this.name}</b>`;
        // ARS.has one range/dmg for ammo so we use weapon otherwise we use ammo for variants
        const ammo = game.ars.config.settings.systemVariant == '0' ? this : await this.getAmmo(true);
        const item = game.ars.config.settings.systemVariant == '0' ? this : ammo ? ammo : this;
        if (item?.system?.attack?.type && item.isIdentified) {
            // #attacks tracking - diplay #attacks if applicable
            const strRemainingAttacks = await CombatManager.attacksPerRoundTracker().displayRemaining(this.actor);

            statblock += ` #ATK <b>${item.bestAttackRate}</b> SPD <b>${this.speed ?? 0}</b>${strRemainingAttacks}`;

            switch (item.system.attack.type) {
                case 'ranged':
                    const ranged = await this.getRange();
                    statblock += ` RNG <b>${ranged.short}/${ranged.medium}/${ranged.long}</b>`;
                    break;

                default:
                    break;
            }
        }

        statblock += '</div>';
        return statblock;
    }

    /**
     * IF this is a ranged weapon return it's range.
     *
     */
    async getRange() {
        const ammo = await this.getAmmo(true);
        let ranged = null;

        switch (this.system?.attack?.type) {
            case 'thrown':
            case 'ranged': {
                // Define weapon range as fallback
                const weaponRange = {
                    short: this.system?.attack?.range?.short || 0,
                    medium: this.system?.attack?.range?.medium || 0,
                    long: this.system?.attack?.range?.long || 0,
                };

                // If ammo exists, use ammo range or fallback to weapon range
                ranged = {
                    short: ammo?.system?.attack?.range?.short ?? weaponRange.short,
                    medium: ammo?.system?.attack?.range?.medium ?? weaponRange.medium,
                    long: ammo?.system?.attack?.range?.long ?? weaponRange.long,
                };

                // Ensure values are integers
                ranged.short = parseInt(ranged.short, 10);
                ranged.medium = parseInt(ranged.medium, 10);
                ranged.long = parseInt(ranged.long, 10);
                break;
            }

            default:
                break;
        }

        return ranged;
    }

    /**
     *
     * Returns enables/!suppressed but NOT transfered effects
     *
     * We ONLY want effects that are marked "!transfer to actor" meaning
     * they only work when the weapon is used during an attack/damage roll
     *
     * @param {*} sourceItem Item this is from? (or not)
     * @returns {Array}
     */
    getItemUseOnlyEffects() {
        // console.log("item.js getItemUseOnlyEffects", this)
        const activeEffects = this.effects.filter((effect) => !effect.disabled && !effect.isSuppressed && !effect.transfer);
        return activeEffects;
    }

    /**
     * Method to play audio.
     * Utilizes the foundry.audio.AudioHelper.play() function.
     */
    playAudio() {
        // Use optional chaining to ensure that system, audio, and file properties exist
        // If they don't exist, the code execution inside the if statement will be skipped
        const audioFile = this.system?.audio?.file;

        if (audioFile) {
            // If audio file is present, construct the configuration object for the audio playback
            const audioConfig = {
                // Source of the audio file
                src: audioFile,
                // Volume of the audio file
                // Parse system.audio.volume into a float, but if it doesn't exist, default to 0.5
                volume: parseFloat(this.system.audio?.volume) || 0.5,
            };

            // Play the audio file using foundry.audio.AudioHelper.play()
            // The second parameter is a boolean for autoplay. Setting it as false to manually control the audio playback
            foundry.audio.AudioHelper.play(audioConfig, false);
        }
    }

    // Function that plays an audio clip based on roll success or failure
    playCheck(rollMode = 'publicroll', success = false, crit = false) {
        console.log('playCheck', { rollMode, success, crit });
        // Ternary operator to select appropriate audio file based on success or failure of roll
        const audioFile = success ? this.system.audio.success : this.system.audio.failure;

        // If there is an audio file, proceed
        if (audioFile) {
            // Array of valid roll modes
            const validRollModes = ['publicroll', 'selfroll', 'gmroll'];

            // Checks if roll mode is valid
            if (validRollModes.includes(rollMode)) {
                // Sets timeout to delay audio playing, 3D dice animation requires longer delay
                const delay = game.dice3d ? 1500 : 0;

                // Promise to play audio after a delay
                utilitiesManager.delayExecution(() => {
                    // Prepare audio parameters
                    const audioParams = {
                        src: audioFile,
                        // Use volume if it exists, else default to 0.5
                        volume: parseFloat(this.system.audio?.volume) || 0.5,
                    };

                    // If roll is public, audio is played to all users
                    const isPublicRoll = rollMode === 'publicroll';

                    // Call foundry.audio.AudioHelper to play audio
                    foundry.audio.AudioHelper.play(audioParams, isPublicRoll);
                }, delay);
            }
        }
    }

    /**
     *
     * This will remove existing effects applied by this item
     * and refresh them.
     *
     * Typically used when a item's effects are directly edited
     *
     */
    refreshEffects() {
        const actor = this.parent;
        console.log('item.js refreshEffects', { actor }, this);
        const removeIds = actor.effects.filter((effect) => effect?.origin?.endsWith(this.uuid)).map((effect) => effect.id);
        if (removeIds.length) actor.deleteEmbeddedDocuments('ActiveEffect', removeIds);
        const effectCreateData = [];
        for (let e of this.effects) {
            if (!e.transfer) continue;
            const effectData = e.toJSON();
            effectData.origin = this.uuid;
            effectCreateData.push(effectData);
        }
        actor.createEmbeddedDocuments('ActiveEffect', effectCreateData);
    }

    /**
     * Remove a contained item from container
     *
     * @param {*} item
     */
    async removeFromContainer() {
        if (this.inContainer && this.containedIn) {
            const containedIn = this.containedIn;
            const newBundle = containedIn.system.itemList.filter((sub) => {
                return sub.id != this.id;
            });
            delete this.containedIn;
            await containedIn.update({ 'system.itemList': newBundle });
        }
    }

    // determine if the item is a rogue skill
    get getIsRogueSkill() {
        if (this.type === 'skill' && this.system?.groups) {
            const groupsArray = this.system.groups.split(',').map((group) => group.toLowerCase().trim());
            // Check if any of the groups match 'rogue' or 'thief'
            const hasRogueTag = ['rogue', 'thief', 'bard'].some((group) => groupsArray.includes(group));
            const rolls1d100 = this.system.features.formula === '1d100';
            return hasRogueTag && rolls1d100;
        }

        return false;
    }

    /**
     *
     * Return list of proficiency objects that are applied to this item
     *
     * @returns {Array} proficiencyList
     */
    getProficiencyList() {
        let profList = [];
        if (this.actor?.proficiencies) {
            for (const prof of this.actor?.proficiencies) {
                for (const applied of prof?.system?.appliedto) {
                    if (applied.id == this.id) profList.push(prof);
                }
            }
        }
        return profList;
    }

    /**
     * Return attack per round property or blank string
     */
    async getAttackPerRound() {
        return await this.system?.attack?.perRound;
    }

    /**
     *
     * Roll item save... move to ARSRoll?
     *
     * @param {*} againstType
     * @returns
     */
    async itemRollSave(againstType, showRoll = false) {
        const variant = game.ars.config.settings.systemVariant;
        const saveTarget = ARS.itemSaves[variant][this.system.attributes.material][againstType];
        const saveType = ARS.itemSaveTypes[variant][againstType];

        let modifier = this.system.attributes.magic ? 1 : 0;
        if (this.system?.protection?.modifier) modifier = this.system?.protection?.modifier || 0;
        if (this.type == 'weapon') modifier = this.system?.attack?.magicPotency || 0;
        if ((variant == 0 || variant == 1) && this.system?.attack?.magicPotency) modifier += 1;
        const formula = `1d20${modifier ? '+' : ''}${modifier ? modifier : ''}`;
        const saveResult = await utilitiesManager.evaluateFormulaValue(formula, this.getRollData(), {
            showRoll,
        });
        const success = saveResult >= saveTarget;

        return { success, saveResult, saveTarget, saveType, formula };
    }
    /**
     *
     * Initiate roll and display item save results
     *
     * @param {Number} againstType index of the type to save against
     *
     *
     */
    async itemSave(againstType) {
        const listSaved = [];
        const listFailed = [];

        const { success, saveResult, saveTarget, saveType, formula } = await this.itemRollSave(againstType, true);

        if (success) {
            listSaved.push({ item: this, rolled: saveResult, saveTarget, formula });
        } else {
            listFailed.push({ item: this, rolled: saveResult, saveTarget, formula });
        }

        const dice = new ARSDice(this);
        dice._playAudioCheck(success);

        const content = await renderTemplate('systems/ars/templates/chat/chatCard-itemSaves.hbs', {
            actor: this.actor,
            listFailed,
            listSaved,
            saveType,
        });

        let chatData = {
            title: 'Item Saves',
            content: content,
            author: game.user.id,
            // rolls: [roll],
            rollMode: game.settings.get('core', 'rollMode'),
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            style: CONST.CHAT_MESSAGE_STYLES.IC,
        };
        // use user current setting? game.settings.get("core", "rollMode")
        ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
        ChatMessage.create(chatData);
    }
} // end ARSitem
