import { ARS } from './config.js';
import * as debug from './debug.js';
import { ARSSettingsAudio } from './apps/settings-audio.js';
import { ARSSettingsCombat } from './apps/settings-combat.js';
import { ARSSettingsMisc } from './apps/settings-misc.js';
import { SocketManager } from './sockets.js';
import * as utilitiesManager from './utilities.js';

export const registerSystemSettings = function () {
    //@override Left-Click Deselection
    game.settings.register('core', 'leftClickRelease', {
        name: 'SETTINGS.LClickReleaseN',
        hint: 'SETTINGS.LClickReleaseL',
        scope: 'client',
        config: true,
        type: new foundry.data.fields.BooleanField({ initial: true }),
    });

    game.settings.registerMenu('ars', 'arsAudio', {
        name: 'Audio',
        label: 'Audio Options', // The text label used in the button
        hint: 'Configuration options for ruleset specific audio',
        icon: 'fas fa-cog', // A Font Awesome icon used in the submenu button
        type: ARSSettingsAudio, // A FormApplication subclass which should be created
        restricted: false, // Restrict this submenu to gamemaster only?
    });

    game.settings.registerMenu('ars', 'arsCombat', {
        name: 'Combat',
        label: 'Combat Options',
        hint: 'Configuration options for ruleset specific combat',
        icon: 'fas fa-cog',
        type: ARSSettingsCombat,
        restricted: true,
    });

    game.settings.registerMenu('ars', 'arsMisc', {
        name: 'Misc',
        label: 'Misc Options',
        hint: 'Configuration various misc. options for the ruleset',
        icon: 'fas fa-cog',
        type: ARSSettingsMisc,
        restricted: true,
    });

    // Internal System Migration Version tracking
    game.settings.register('ars', 'systemCurrentVersion', {
        name: 'System Version',
        scope: 'world',
        config: false,
        type: String,
        default: '',
    });

    // Internal System Changelog Version tracking
    game.settings.register('ars', 'systemChangeLogVersion', {
        name: 'System Changelog Version',
        scope: 'world',
        config: false,
        type: String,
        default: '',
    });

    /**
     *
     * Settings used internally for Party-Tracker
     *
     */
    game.settings.register('ars', 'partyMembers', {
        name: 'System Party Tracker - Members',
        scope: 'world',
        config: false,
        type: Array,
        default: [],
    });
    game.settings.register('ars', 'partyAwards', {
        name: 'System Party Tracker - Awards',
        scope: 'world',
        config: false,
        type: Array,
        default: [],
    });
    game.settings.register('ars', 'partyLogs', {
        name: 'System Party Tracker - Logs',
        requiresReload: false,
        scope: 'world',
        config: false,
        type: Array,
        default: [],
    });
    // end Party-Tracker settings

    //const systemVariant = game.settings.get("ars", "systemVariant");
    game.settings.register('ars', 'systemVariant', {
        name: 'SETTINGS.systemVariantLabel',
        hint: 'SETTINGS.systemVariantTT',
        scope: 'world',
        requiresReload: true,
        config: true,
        type: String,
        choices: {
            0: 'SETTINGS.systemVariant.0',
            1: 'SETTINGS.systemVariant.1',
            2: 'SETTINGS.systemVariant.2',
        },
        default: '0',
        onChange: (systemVariant) => {
            CONFIG.ARS.settings.systemVariant = systemVariant;
            // change initiative based on variant
            switch (systemVariant) {
                case '0':
                    game.settings.set('ars', 'initiativeFormula', '1d6');
                    break;
                case '1':
                    game.settings.set('ars', 'initiativeFormula', '1d6');
                    break;
                case '2':
                    game.settings.set('ars', 'initiativeFormula', '1d10');
                    break;
                default:
                    game.settings.set('ars', 'initiativeFormula', '1d6');
                    break;
            }

            const useAscendingAC = game.settings.get('ars', 'useAscendingAC');
            if (systemVariant !== '2' && useAscendingAC) {
                game.settings.set('ars', 'useAscendingAC', false);
            }
        },
    });

    //const initiativeUseSpeed = game.settings.get("ars", "initiativeUseSpeed");
    game.settings.register('ars', 'initiativeUseSpeed', {
        name: 'SETTINGS.initiativeUseSpeed',
        hint: 'SETTINGS.initiativeUseSpeedTT',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        type: Boolean,
        onChange: (initiativeUseSpeed) => (CONFIG.ARS.settings.initiativeUseSpeed = initiativeUseSpeed),
    });

    //const initiativeFormula = game.settings.get("ars", "initiativeFormula");
    game.settings.register('ars', 'initiativeFormula', {
        name: 'SETTINGS.initiativeFormula',
        hint: 'SETTINGS.initiativeFormulaTT',
        scope: 'world',
        config: false,
        default: '1d6',
        restricted: true,
        type: String,
        onChange: (initiativeFormula) => (CONFIG.Combat.initiative.formula = initiativeFormula),
    });

    //const ascendingInitiative = game.settings.get("ars", "InitiativeAscending");
    game.settings.register('ars', 'InitiativeAscending', {
        name: 'SETTINGS.InitiativeAscending',
        hint: 'SETTINGS.InitiativeAscendingTT',
        scope: 'world',
        config: false,
        default: true,
        restricted: true,
        type: Boolean,
    });

    //const initSideVSide = game.settings.get("ars", "initSideVSide");
    game.settings.register('ars', 'initSideVSide', {
        name: 'SETTINGS.initSideVSide',
        hint: 'SETTINGS.initSideVSideTT',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        type: Boolean,
    });

    //const initManualRolls = game.settings.get("ars", "initManualRolls");
    game.settings.register('ars', 'initManualRolls', {
        name: 'SETTINGS.initManualRolls',
        hint: 'SETTINGS.initManualRollsTT',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        type: Boolean,
    });

    //const useArmorDamage = game.settings.get("ars", "useArmorDamage");
    game.settings.register('ars', 'useArmorDamage', {
        name: 'SETTINGS.useArmorDamage',
        hint: 'SETTINGS.useArmorDamageTT',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        type: Boolean,
        onChange: (useArmorDamage) => (CONFIG.ARS.settings.useArmorDamage = useArmorDamage),
    });

    //const optionNegativeHP = game.settings.get("ars", "optionNegativeHP");
    game.settings.register('ars', 'optionNegativeHP', {
        name: 'SETTINGS.optionNegativeHP',
        hint: 'SETTINGS.optionNegativeHPTT',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        type: Boolean,
        onChange: (optionNegativeHP) => (CONFIG.ARS.settings.optionNegativeHP = optionNegativeHP),
    });

    //const useAutoHitFailDice = game.settings.get("ars", "useAutoHitFailDice");
    game.settings.register('ars', 'useAutoHitFailDice', {
        name: 'SETTINGS.useAutoHitFailDice',
        hint: 'SETTINGS.useAutoHitFailDiceTT',
        scope: 'world',
        config: false,
        default: true,
        restricted: true,
        type: Boolean,
        onChange: (useAutoHitFailDice) => (CONFIG.ARS.settings.autohitfail = useAutoHitFailDice),
    });

    //const autoDamage = game.settings.get("ars", "autoDamage");
    game.settings.register('ars', 'autoDamage', {
        name: 'SETTINGS.autoDamage',
        hint: 'SETTINGS.autoDamageTT',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        type: Boolean,
    });

    //const autoCheck = game.settings.get("ars", "autoCheck");
    game.settings.register('ars', 'autoCheck', {
        name: 'SETTINGS.autoCheck',
        hint: 'SETTINGS.autoCheckTT',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        type: Boolean,
    });

    //const weaponVarmor = game.settings.get("ars", "weaponVarmor");
    game.settings.register('ars', 'weaponVarmor', {
        name: 'SETTINGS.weaponVarmor',
        hint: 'SETTINGS.weaponVarmorTT',
        scope: 'world',
        config: false,
        default: true,
        restricted: true,
        type: Boolean,
    });

    //const rollInitEachRound = game.settings.get("ars", "rollInitEachRound");
    game.settings.register('ars', 'rollInitEachRound', {
        name: 'SETTINGS.rollInitEachRound',
        hint: 'SETTINGS.rollInitEachRoundTT',
        scope: 'world',
        config: false,
        default: true,
        restricted: true,
        type: Boolean,
    });

    //const automateLighting = game.settings.get("ars", "automateLighting");
    game.settings.register('ars', 'automateLighting', {
        name: 'SETTINGS.automateLighting',
        hint: 'SETTINGS.automateLightingTT',
        scope: 'world',
        config: true,
        default: true,
        restricted: true,
        type: Boolean,
        onChange: (automateLighting) => {
            CONFIG.ARS.settings.automateLighting = automateLighting;
        },
    });

    //const automateVision = game.settings.get("ars", "automateVision");
    game.settings.register('ars', 'automateVision', {
        name: 'SETTINGS.automateVision',
        hint: 'SETTINGS.automateVisionTT',
        scope: 'world',
        config: true,
        default: true,
        restricted: true,
        type: Boolean,
        onChange: (automateVision) => {
            CONFIG.ARS.settings.automateVision = automateVision;
        },
    });

    // //const consumeFoodWater = game.settings.get("ars", "consumeFoodWater");
    // game.settings.register('ars', 'consumeFoodWater', {
    //     name: 'SETTINGS.consumeFoodWater',
    //     hint: 'SETTINGS.consumeFoodWaterTT',
    //     scope: 'world',
    //     config: true,
    //     default: false,
    //     type: Boolean,
    //     onChange: (consumeFoodWater) => {
    //         CONFIG.ARS.settings.consumeFoodWater = consumeFoodWater;
    //         // force all clents to refresh actor sheets.
    //         SocketManager.notify(game.user.id, game.user.id, 'reRenderActorSheets', {});
    //         // refresh this GMs actor sheets
    //         utilitiesManager.refreshAllActorSheetsOpened();
    //     },
    // });

    //const automateEncumbrance = game.settings.get("ars", "automateEncumbrance");
    game.settings.register('ars', 'automateEncumbrance', {
        name: 'SETTINGS.automateEncumbrance',
        hint: 'SETTINGS.automateEncumbranceTT',
        scope: 'world',
        config: true,
        default: false,
        type: Boolean,
        onChange: async (automateEncumbrance) => {
            // if (game.user.isGM) {
            // trigger encumbrance check or remove all
            // encumbrance statuses
            // const disable = !automateEncumbrance;
            // CONFIG.ARS.settings.automateEncumbrance = automateEncumbrance;
            // const allPCs = canvas.tokens.placeables.filter((token) => token?.document?.actor?.type === 'character');
            // for (const token of allPCs) {
            //     await token.document.updateEncumbranceStatus(disable);
            // }
            // }
        },
    });

    //const startCombatSound = game.settings.get("ars", "startCombatSound");
    game.settings.register('ars', 'startCombatSound', {
        name: 'SETTINGS.startCombatSound',
        hint: 'SETTINGS.startCombatSoundTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.initiative.startCombatSound,
        type: String,
    });
    //const playStartCombatSound = game.settings.get("ars", "playStartCombatSound");
    game.settings.register('ars', 'playStartCombatSound', {
        name: 'SETTINGS.playStartCombatSound',
        hint: 'SETTINGS.playStartCombatSoundTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: false,
        type: Boolean,
    });

    //const initRoundSound = game.settings.get("ars", "initRoundSound");
    game.settings.register('ars', 'initRoundSound', {
        name: 'SETTINGS.initRoundSound',
        hint: 'SETTINGS.initRoundSoundTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.initiative.start,
        type: String,
    });
    //const playRoundSound = game.settings.get("ars", "playRoundSound");
    game.settings.register('ars', 'playRoundSound', {
        name: 'SETTINGS.initRoundSound',
        hint: 'SETTINGS.initRoundSoundTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: true,
        type: Boolean,
    });

    //const initTurnSound = game.settings.get("ars", "initTurnSound");
    game.settings.register('ars', 'initTurnSound', {
        name: 'SETTINGS.initTurnSound',
        hint: 'SETTINGS.initTurnSoundTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.initiative.turn,
        type: String,
    });
    //const playTurnSound = game.settings.get("ars", "playTurnSound");
    game.settings.register('ars', 'playTurnSound', {
        name: 'SETTINGS.initTurnSound',
        hint: 'SETTINGS.initTurnSoundTT',
        scope: 'world',
        config: false,
        default: true,
        requiresReload: false,
        type: Boolean,
    });

    //const initVolumeSound = game.settings.get("ars", "initVolumeSound");
    game.settings.register('ars', 'initVolumeSound', {
        name: 'SETTINGS.initVolumeSound',
        hint: 'SETTINGS.initVolumeSoundTT',
        scope: 'client',
        config: false,
        requiresReload: false,
        range: {
            min: 0,
            max: 1,
            step: 0.1,
        },
        default: 0.5,
        type: Number,
    });

    //const audioPlayTriggers = game.settings.get("ars", "audioPlayTriggers");
    game.settings.register('ars', 'audioPlayTriggers', {
        name: 'SETTINGS.audioPlayTriggers',
        hint: 'SETTINGS.audioPlayTriggersTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: true,
        type: Boolean,
    });

    //const audioTriggersVolume = game.settings.get("ars", "audioTriggersVolume");
    game.settings.register('ars', 'audioTriggersVolume', {
        name: 'SETTINGS.audioTriggersVolume',
        hint: 'SETTINGS.audioTriggersVolumeTT',
        scope: 'client',
        config: false,
        requiresReload: false,
        range: {
            min: 0,
            max: 1,
            step: 0.1,
        },
        default: 0.5,
        type: Number,
    });

    //const audioTriggerCheckSuccess = game.settings.get("ars", "audioTriggerCheckSuccess");
    game.settings.register('ars', 'audioTriggerCheckSuccess', {
        name: 'SETTINGS.audioTriggerCheckSuccess',
        hint: 'SETTINGS.audioTriggerCheckSuccessTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.save.success,
        onChange: (audioTriggerCheckSuccess) => {
            CONFIG.ARS.sounds.save.success = audioTriggerCheckSuccess;
        },
        type: String,
    });
    //const audioTriggerCheckFail = game.settings.get("ars", "audioTriggerCheckFail");
    game.settings.register('ars', 'audioTriggerCheckFail', {
        name: 'SETTINGS.audioTriggerCheckFail',
        hint: 'SETTINGS.audioTriggerCheckFailTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.save.failure,
        onChange: (audioTriggerCheckFail) => {
            CONFIG.ARS.sounds.save.failure = audioTriggerCheckFail;
        },
        type: String,
    });
    //const audioTriggerMeleeHit = game.settings.get("ars", "audioTriggerMeleeHit");
    game.settings.register('ars', 'audioTriggerMeleeHit', {
        name: 'SETTINGS.audioTriggerMeleeHit',
        hint: 'SETTINGS.audioTriggerMeleeHitTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['melee-hit'],
        onChange: (audioTriggerMeleeHit) => {
            CONFIG.ARS.sounds.combat['melee-hit'] = audioTriggerMeleeHit;
        },
        type: String,
    });
    //const audioTriggerMeleeMiss = game.settings.get("ars", "audioTriggerMeleeMiss");
    game.settings.register('ars', 'audioTriggerMeleeMiss', {
        name: 'SETTINGS.audioTriggerMeleeMiss',
        hint: 'SETTINGS.audioTriggerMeleeMissTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['melee-miss'],
        onChange: (audioTriggerMeleeMiss) => {
            CONFIG.ARS.sounds.combat['melee-miss'] = audioTriggerMeleeMiss;
        },
        type: String,
    });
    //const audioTriggerMeleeCrit = game.settings.get("ars", "audioTriggerMeleeCrit");
    game.settings.register('ars', 'audioTriggerMeleeCrit', {
        name: 'SETTINGS.audioTriggerMeleeCrit',
        hint: 'SETTINGS.audioTriggerMeleeCritTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['melee-hit-crit'],
        onChange: (audioTriggerMeleeCrit) => {
            CONFIG.ARS.sounds.combat['melee-hit-crit'] = audioTriggerMeleeCrit;
        },
        type: String,
    });
    //const audioTriggerRangeHit = game.settings.get("ars", "audioTriggerRangeHit");
    game.settings.register('ars', 'audioTriggerRangeHit', {
        name: 'SETTINGS.audioTriggerRangeHit',
        hint: 'SETTINGS.audioTriggerRangeHitTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['missile-hit'],
        onChange: (audioTriggerRangeHit) => {
            CONFIG.ARS.sounds.combat['missile-hit'] = audioTriggerRangeHit;
        },
        type: String,
    });
    //const audioTriggerRangeMiss = game.settings.get("ars", "audioTriggerRangeMiss");
    game.settings.register('ars', 'audioTriggerRangeMiss', {
        name: 'SETTINGS.audioTriggerRangeMiss',
        hint: 'SETTINGS.audioTriggerRangeMissTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['missile-miss'],
        onChange: (audioTriggerRangeMiss) => {
            CONFIG.ARS.sounds.combat['missile-miss'] = audioTriggerRangeMiss;
        },
        type: String,
    });
    //const audioTriggerRangeCrit = game.settings.get("ars", "audioTriggerRangeCrit");
    game.settings.register('ars', 'audioTriggerRangeCrit', {
        name: 'SETTINGS.audioTriggerRangeCrit',
        hint: 'SETTINGS.audioTriggerRangeCritTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['missile-hit-crit'],
        onChange: (audioTriggerRangeCrit) => {
            CONFIG.ARS.sounds.combat['missile-hit-crit'] = audioTriggerRangeCrit;
        },
        type: String,
    });

    //const audioTriggerCastInterrupted = game.settings.get("ars", "audioTriggerCastInterrupted");
    game.settings.register('ars', 'audioTriggerCastInterrupted', {
        name: 'SETTINGS.audioTriggerCastInterrupted',
        hint: 'SETTINGS.audioTriggerCastInterruptedTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['special-cast-interrupt'],
        onChange: (audioTriggerCastInterrupted) => {
            CONFIG.ARS.sounds.combat['special-cast-interrupt'] = audioTriggerCastInterrupted;
        },
        type: String,
    });

    //const audioTriggerDisplacement = game.settings.get("ars", "audioTriggerDisplacement");
    game.settings.register('ars', 'audioTriggerDisplacement', {
        name: 'SETTINGS.audioTriggerDisplacement',
        hint: 'SETTINGS.audioTriggerDisplacementTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['special-displacement'],
        onChange: (audioTriggerDisplacement) => {
            CONFIG.ARS.sounds.combat['special-displacement'] = audioTriggerDisplacement;
        },
        type: String,
    });

    //const audioTriggerMirrorImage = game.settings.get("ars", "audioTriggerMirrorImage");
    game.settings.register('ars', 'audioTriggerMirrorImage', {
        name: 'SETTINGS.audioTriggerMirrorImage',
        hint: 'SETTINGS.audioTriggerMirrorImageTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['special-mirror-image'],
        onChange: (audioTriggerMirrorImage) => {
            CONFIG.ARS.sounds.combat['special-mirror-image'] = audioTriggerMirrorImage;
        },
        type: String,
    });

    //const audioTriggerStoneSkin = game.settings.get("ars", "audioTriggerStoneSkin");
    game.settings.register('ars', 'audioTriggerStoneSkin', {
        name: 'SETTINGS.audioTriggerStoneSkin',
        hint: 'SETTINGS.audioTriggerStoneSkinTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['special-stone-skin'],
        onChange: (audioTriggerStoneSkin) => {
            CONFIG.ARS.sounds.combat['special-stone-skin'] = audioTriggerStoneSkin;
        },
        type: String,
    });

    //const audioTriggerCast = game.settings.get("ars", "audioTriggerCast");
    game.settings.register('ars', 'audioTriggerCast', {
        name: 'SETTINGS.audioTriggerCast',
        hint: 'SETTINGS.audioTriggerCastTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat['cast-spell'],
        onChange: (audioTriggerCast) => {
            CONFIG.ARS.sounds.combat['cast-spell'] = audioTriggerCast;
        },
        type: String,
    });
    //const audioTriggerDeath = game.settings.get("ars", "audioTriggerDeath");
    game.settings.register('ars', 'audioTriggerDeath', {
        name: 'SETTINGS.audioTriggerDeath',
        hint: 'SETTINGS.audioTriggerDeathTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat.death,
        onChange: (audioTriggerDeath) => {
            CONFIG.ARS.sounds.combat.death = audioTriggerDeath;
        },
        type: String,
    });
    //const audioTriggerUnconscious = game.settings.get("ars", "audioTriggerUnconscious");
    game.settings.register('ars', 'audioTriggerUnconscious', {
        name: 'SETTINGS.audioTriggerUnconscious',
        hint: 'SETTINGS.audioTriggerUnconsciousTT',
        scope: 'world',
        config: false,
        requiresReload: false,
        default: ARS.sounds.combat.unconscious,
        onChange: (audioTriggerUnconscious) => {
            CONFIG.ARS.sounds.combat.unconscious = audioTriggerUnconscious;
        },
        type: String,
    });

    //const ctShowOnlyVisible = game.settings.get("ars", "ctShowOnlyVisible");
    game.settings.register('ars', 'ctShowOnlyVisible', {
        name: 'SETTINGS.ctShowOnlyVisible',
        hint: 'SETTINGS.ctShowOnlyVisibleTT',
        scope: 'world',
        config: false,
        default: true,
        type: Boolean,
        onChange: (ctShowOnlyVisible) => {
            CONFIG.ARS.settings.ctShowOnlyVisible = ctShowOnlyVisible;
            ui.combat.render();
        },
    });

    //const combatAutomateRangeMods = game.settings.get("ars", "combatAutomateRangeMods");
    game.settings.register('ars', 'combatAutomateRangeMods', {
        name: 'SETTINGS.combatAutomateRangeMods',
        hint: 'SETTINGS.combatAutomateRangeModsTT',
        scope: 'world',
        config: false,
        default: true,
        type: Boolean,
    });

    //const floatingHudStaticPosition = game.settings.get("ars", "floatingHudStaticPosition");
    game.settings.register('ars', 'floatingHudStaticPosition', {
        name: 'SETTINGS.floatingHudStaticPosition',
        hint: 'SETTINGS.floatingHudStaticPositionTT',
        scope: 'world',
        config: true,
        default: true,
        type: Boolean,
    });

    //const floatingHudAutoClose = game.settings.get("ars", "floatingHudAutoClose");
    game.settings.register('ars', 'floatingHudAutoClose', {
        name: 'SETTINGS.floatingHudAutoClose',
        hint: 'SETTINGS.floatingHudAutoCloseTT',
        scope: 'client',
        config: true,
        default: true,
        type: Boolean,
    });

    //const floatingHudAllowPlayer = game.settings.get("ars", "floatingHudAllowPlayer");
    game.settings.register('ars', 'floatingHudAllowPlayer', {
        name: 'SETTINGS.floatingHudAllowPlayer',
        hint: 'SETTINGS.floatingHudAllowPlayerTT',
        scope: 'client',
        config: true,
        default: true,
        restricted: true,
        type: Boolean,
        onChange: (floatingHudAllowPlayer) => {
            const scene = game.scenes.active;
            // controlled means they have the token clicked on, shift-click will do multiple
            const controlledTokens = scene.tokens.filter((token) => token.object.controlled);
            controlledTokens.forEach((token) => token.object.release());
            if (!floatingHudAllowPlayer) {
                // Now controlledTokens is an array of Token objects controlled by the user
                controlledTokens.forEach((token) => token.object?.combatHud?.close());
            }
            // re-control the tokens we released. this will bring up hud if we're turning it on
            controlledTokens.forEach((token) => token.object?.control({ releaseOthers: false }));
        },
    });

    //const encumbranceIncludeCoin = game.settings.get("ars", "encumbranceIncludeCoin");
    game.settings.register('ars', 'encumbranceIncludeCoin', {
        name: 'SETTINGS.encumbranceIncludeCoin',
        hint: 'SETTINGS.encumbranceIncludeCoinTT',
        scope: 'world',
        config: true,
        default: true,
        restricted: true,
        type: Boolean,
        onChange: (encumbranceIncludeCoin) => {
            CONFIG.ARS.settings.encumbranceIncludeCoin = encumbranceIncludeCoin;
        },
    });

    //const identificationActor = game.settings.get("ars", "identificationActor");
    game.settings.register('ars', 'identificationActor', {
        name: 'SETTINGS.identificationActor',
        hint: 'SETTINGS.identificationActorTT',
        scope: 'world',
        config: true,
        default: true,
        type: Boolean,
        onChange: (identificationActor) => {
            CONFIG.ARS.settings.identificationActor = identificationActor;
        },
    });

    //const identificationItem = game.settings.get("ars", "identificationItem");
    game.settings.register('ars', 'identificationItem', {
        name: 'SETTINGS.identificationItem',
        hint: 'SETTINGS.identificationItemTT',
        scope: 'world',
        config: true,
        default: true,
        type: Boolean,
        onChange: (identificationItem) => {
            CONFIG.ARS.settings.identificationItem = identificationItem;
        },
    });

    //const panOnInitiative = game.settings.get("ars", "panOnInitiative");
    game.settings.register('ars', 'panOnInitiative', {
        name: 'SETTINGS.panOnInitiative',
        hint: 'SETTINGS.panOnInitiativeTT',
        scope: 'client',
        config: true,
        default: true,
        restricted: true,
        type: Boolean,
    });

    //const npcNumberedNames = game.settings.get("ars", "npcNumberedNames");
    game.settings.register('ars', 'npcNumberedNames', {
        name: 'SETTINGS.nPCNumberedName',
        hint: 'SETTINGS.nPCNumberedNameTT',
        scope: 'world',
        config: true,
        default: true,
        restricted: true,
        type: Boolean,
    });

    //const npcLootable = game.settings.get("ars", "npcLootable");
    game.settings.register('ars', 'npcLootable', {
        name: 'SETTINGS.npcLootable',
        hint: 'SETTINGS.npcLootableTT',
        scope: 'world',
        config: true,
        default: true,
        type: Boolean,
        onChange: (npcLootable) => (CONFIG.ARS.settings.npcLootable = npcLootable),
    });

    //const itemBrowserMargin = game.settings.get("ars", "itemBrowserMargin");
    game.settings.register('ars', 'itemBrowserMargin', {
        name: 'SETTINGS.itemBrowserMargin',
        hint: 'SETTINGS.itemBrowserMarginTT',
        scope: 'world',
        config: true,
        restricted: true,
        range: {
            min: -100,
            max: 100,
            step: 1,
        },
        default: 0,
        type: Number,
        onChange: (itemBrowserMargin) => {
            if (game.ars.ui?.itembrowser && game.ars.ui.itembrowser.rendered) game.ars.ui.itembrowser.render(true);
        },
    });

    // #psionic
    game.settings.register('ars', 'usePsionics', {
        name: 'Psionics',
        hint: 'Enable Psionics system core',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        requiresReload: true,
        type: Boolean,
        onChange: (usePsionics) => {
            CONFIG.ARS.settings.usePsionics = usePsionics;
            document.body.classList.toggle('psionics-enabled', usePsionics);
            if (!usePsionics) game.settings.set('ars', 'usePsionicsV1', false);
        },
    });

    game.settings.register('ars', 'usePsionicsV1', {
        name: 'Psionics v1 Behavior',
        hint: 'Enable v1/0 style mechanics/automation for core Psionics',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        requiresReload: true,
        type: Boolean,
        onChange: (usePsionicsV1) => {
            CONFIG.ARS.settings.usePsionicsV1 = usePsionicsV1;
            document.body.classList.toggle('psionics-enabled-v1', usePsionicsV1);
            const usePsionics = game.settings.get('ars', 'usePsionics');
            if (!usePsionics) game.settings.set('ars', 'usePsionics', true);
        },
    });

    // enable ars info helper
    game.settings.register('ars', 'useInfoHelper', {
        name: 'SETTINGS.useInfoHelper',
        hint: 'SETTINGS.useInfoHelperTT',
        scope: 'world',
        config: false,
        default: false,
        type: Boolean,
        onChange: (useInfoHelper) => {
            CONFIG.ARS.settings.useInfoHelper = useInfoHelper;
        },
    });

    // combine inventory/stack mechanics
    game.settings.register('ars', 'combineStack', {
        name: 'SETTINGS.combineStack',
        hint: 'SETTINGS.combineStackTT',
        scope: 'world',
        config: false,
        default: true,
        type: Boolean,
        onChange: (combineStack) => {
            CONFIG.ARS.settings.combineStack = combineStack;
        },
    });

    // macros - clone on drop
    game.settings.register('ars', 'macroDontCloneOnDrop', {
        name: 'SETTINGS.macroDontCloneOnDrop',
        hint: 'SETTINGS.macroDontCloneOnDropTT',
        scope: 'world',
        config: false,
        default: false,
        type: Boolean,
        onChange: (macroDontCloneOnDrop) => {
            CONFIG.ARS.settings.macroDontCloneOnDrop = macroDontCloneOnDrop;
        },
    });

    // experimental settings - subject to remove/change at any time - this is for general global stuff - there is more niche settings below for granular control
    game.settings.register('ars', 'useExperimental', {
        name: 'Enable Experimental',
        hint: 'Enable experimental features (various) as they become available - subject rapid change.',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        requiresReload: true,
        type: Boolean,
        onChange: (useExperimental) => {
            CONFIG.ARS.settings.useExperimental = useExperimental;
            document.body.classList.toggle('experimental-enabled', useExperimental);
        },
    });

    game.settings.register('ars', 'useAscendingAC', {
        name: 'Enable Ascending AC',
        hint: 'Enable experimental Ascending AC for variant 2.',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        requiresReload: true,
        type: Boolean,
        onChange: (useAscendingAC) => {
            CONFIG.ARS.settings.useAscendingAC = useAscendingAC;
        },
    });

    game.settings.register('ars', 'useComeliness', {
        name: 'Enable Comeliness',
        hint: 'Enable additional Comeliness Ability - using rules from UA.',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        requiresReload: true,
        type: Boolean,
        onChange: (useComeliness) => {
            CONFIG.ARS.settings.useComeliness = useComeliness;
        },
    });

    game.settings.register('ars', 'useHonor', {
        name: 'Enable Honor',
        hint: 'Enable additional Honor Ability - using rules from OA.',
        scope: 'world',
        config: false,
        default: false,
        restricted: true,
        requiresReload: true,
        type: Boolean,
        onChange: (useHonor) => {
            CONFIG.ARS.settings.useHonor = useHonor;
        },
    });

    // hidden setting - s.g.
    game.settings.register('ars', 'useHudToggleKey', {
        name: 'Augmented HUD Launch',
        hint: 'Hold Shift and Select to open the minihud.',
        scope: 'world',
        config: false,
        default: false,
        type: Boolean,
        default: false,
        onChange: (value) => {
        },
    });

    //const debugMode = game.settings.get("ars", "debugMode");
    game.settings.register('ars', 'debugMode', {
        name: 'SETTINGS.debugMode',
        hint: 'SETTINGS.debugModeTT',
        scope: 'world',
        config: true,
        default: false,
        type: Boolean,
        onChange: (debugMode) => {
            CONFIG.debug.hooks = debugMode;
            CONFIG.ARS.settings.debugMode = debugMode;
        },
    });
};
