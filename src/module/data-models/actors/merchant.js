import { actorDataSchema } from './actor.js';
const fields = foundry.data.fields;

/**
 * Schema for Lootables
 */
export default class ARSActorMerchantData extends actorDataSchema {
    /** @inheritdoc */
    static migrateData(source) {
        super.migrateData(source);
        // console.log('ARSActorLootable migrateData', { source }, source.attributes.movement.value);
    }

    /** @inheritDoc */
    static defineSchema() {
        return foundry.utils.mergeObject(super.defineSchema(), {
            markup: new fields.SchemaField({
                buy: new fields.NumberField({ integer: true, require: true, initial: -20, default: -20 }),
                sell: new fields.NumberField({ integer: true, require: true, initial: 20, default: 20 }),
            }),
            market: new fields.SchemaField({
                canSell: new fields.BooleanField({ initial: true, default: true }),
                canBuy: new fields.BooleanField({ initial: true, default: true }),
            }),
        });
    }
}
