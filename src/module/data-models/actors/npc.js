import { actorDataSchema } from './actor.js';
const fields = foundry.data.fields;
/**
 * Schema for NPC
 */
export default class ARSActorNPC extends actorDataSchema {
    /** @inheritdoc */
    static migrateData(source) {
        super.migrateData(source);
        if (source.hitediceNotes) {
            source.hitdiceNotes = source.hitediceNotes;
            delete source.hitediceNotes;
            source.migrate = true;
        }
        // console.log('ARSActorNPC migrateData', { source }, source.attributes.movement.value);
        // if (!Number.isFinite(source.attributes.movement.value)) {
        //     console.log('ARSActorNPC migrateData 1', { source }, source.attributes.movement.value);
        //     source.attributes.movement.value = Number(source.attributes.movement.value) || 0;
        //     console.log('ARSActorNPC migrateData 1', { source }, source.attributes.movement.value);
        // }
    }

    /** @inheritDoc */
    static defineSchema() {
        const npcSchema = foundry.utils.mergeObject(super.defineSchema(), {
            climate: new fields.StringField({ trim: true, default: '' }),
            frequency: new fields.StringField({ trim: true, default: '' }),
            organization: new fields.StringField({ trim: true, default: '' }),
            diet: new fields.StringField({ trim: true, default: '' }),
            activity: new fields.StringField({ trim: true, default: '' }),
            intelligence: new fields.StringField({ trim: true, default: '' }),
            inlair: new fields.StringField({ trim: true, default: '' }),
            treasureType: new fields.StringField({ trim: true, default: '' }),
            numberAppearing: new fields.StringField({ trim: true, default: '' }),
            movement: new fields.StringField({ trim: true, initial: '12', default: '' }),
            hitdice: new fields.StringField({ trim: true, initial: '1', default: '1' }),
            //TODO: remove this in v13 (it is a typoe)
            hitediceNotes: new fields.StringField({ trim: true, default: '' }),
            //
            hitdiceNotes: new fields.StringField({ trim: true, default: '' }),
            hpCalculation: new fields.StringField({ trim: true, default: '' }),
            numberAttacks: new fields.StringField({ trim: true, default: '' }),
            damage: new fields.StringField({ trim: true, initial: '1d6', default: '1d6' }),
            specialAttacks: new fields.StringField({ trim: true, default: '' }),
            specialDefenses: new fields.StringField({ trim: true, default: '' }),
            sizenote: new fields.StringField({ trim: true, default: '' }),
            acNote: new fields.StringField({ trim: true, default: '' }),
            magicresist: new fields.StringField({ trim: true, default: '' }),
            abilityNotes: new fields.ArrayField(new fields.ObjectField()),
            resistances: new fields.SchemaField({
                weapon: new fields.SchemaField({
                    magicpotency: new fields.NumberField({ default: 0 }),
                    metals: new fields.ArrayField(
                        new fields.SchemaField({
                            type: new fields.StringField({ trim: true, initial: 'silver', default: 'silver' }),
                            protection: new fields.StringField({ trim: true, initial: 'halve', default: 'halve' }),
                        })
                    ),
                }),
            }),
            xp: new fields.SchemaField({
                value: new fields.StringField({ trim: true, default: '' }),
            }),
            morale: new fields.StringField({ trim: true, default: '' }),
            disablelooting: new fields.BooleanField({ default: false }),
            matrixTable: new fields.StringField({ trim: true, initial: 'fighter', default: '' }),
            opened: new fields.StringField({ default: '' }),
        });

        // npc specific details properties
        const npcDetails = {
            type: new fields.StringField({ trim: true, default: '' }),
            source: new fields.StringField({ trim: true, default: '' }),
        };
        // append in npc details
        npcSchema.details.fields = {
            ...npcSchema.details.fields,
            ...npcDetails,
        };

        return npcSchema;
    }
}
