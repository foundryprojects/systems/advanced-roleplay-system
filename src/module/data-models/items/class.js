import { itemDataSchema, itemSoundSchema } from './item.js';
export default class ARSItemClass extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            active: new fields.BooleanField({ required: true, initial: true, default: true }),
            xp: new fields.NumberField({ required: true, initial: 0, default: 0 }),
            xpbonus: new fields.NumberField({ required: true, initial: 0, default: 0 }),
            advancement: new fields.ArrayField(new fields.ObjectField({ default: '' })),
            features: new fields.SchemaField({
                acDexFormula: new fields.StringField({
                    required: true,
                    initial: '@abilities.dex.defensive',
                    default: '@abilities.dex.defensive',
                }),
                hpConFormula: new fields.StringField({ initial: '', default: '' }),
                lasthitdice: new fields.NumberField({ required: true, initial: 9, default: 9 }),
                bonuscon: new fields.BooleanField({ initial: false, required: true, default: false }),
                wisSpellBonusDisabled: new fields.BooleanField({ initial: false, required: true, default: false }),
                // spellFreecast: new fields.BooleanField({ required: true, default: false }),
                focus: new fields.SchemaField({
                    major: new fields.StringField({ default: '' }),
                    minor: new fields.StringField({ default: '' }),
                }),
            }),
            proficiencies: new fields.SchemaField({
                penalty: new fields.NumberField({ required: true, initial: -4, default: -4 }),
                weapon: new fields.SchemaField({
                    starting: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                    earnLevel: new fields.NumberField({ required: true, initial: 3, default: 3 }),
                }),
                skill: new fields.SchemaField({
                    starting: new fields.NumberField({ required: true, initial: 0, default: 0 }),
                    earnLevel: new fields.NumberField({ required: true, initial: 3, default: 3 }),
                }),
            }),
            type: new fields.StringField({ initial: '', required: true, trim: true, default: '' }),
            isPsionic: new fields.BooleanField({ required: false, initial: false, default: false }),            
            matrixTable: new fields.StringField({ default: '' }),
            ranks: new fields.ArrayField(new fields.ObjectField()),
        });
    }
}
