import { itemDataSchema, itemSoundSchema, itemCostSchema } from './item.js';
export default class ARSItemSpell extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemSoundSchema.defineSchema(),
            ...itemCostSchema.defineSchema(),

            type: new fields.StringField({ initial: 'Arcane', default: 'Arcane', required: true, nullable: false }),
            level: new fields.NumberField({ required: true, default: 1, initial: 1 }),
            learned: new fields.BooleanField({ default: false }),
            school: new fields.StringField({ default: '', nullable: true, required: false }),
            sphere: new fields.StringField({ default: '', nullable: true, required: false }),
            range: new fields.StringField({ default: '' }),
            components: new fields.SchemaField({
                verbal: new fields.BooleanField({ initial: true, default: true }),
                somatic: new fields.BooleanField({ initial: true, default: true }),
                material: new fields.BooleanField({ initial: true, default: true }),
            }),
            durationText: new fields.StringField({ initial: '', default: '', nullable: true }),
            castingTime: new fields.StringField({ initial: '', default: '' }),
            areaOfEffect: new fields.StringField({ initial: '', default: '' }),
            save: new fields.StringField({ initial: '', default: '' }),
        });
    }
}
