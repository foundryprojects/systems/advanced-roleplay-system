import { itemDataSchema, itemCostSchema } from './item.js';
export default class ARSItemArmor extends itemDataSchema {
    static migrateData(source) {
        // console.log('ARSItemArmor migrateData', { source });
        //TODO: move armorstyle to attack.armorstyle
        // if (source.armorstyle) {
        //     console.log('ARSItemArmor migrateData source.attack.armorstyle', { source }, source.armorstyle);
        //     source.attack.armorstyle = source.armorstyle;
        //     delete source.armorstyle;
        //     source.migrate = true;
        // }
    }
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            ...itemCostSchema.defineSchema(),
            protection: new fields.SchemaField({
                type: new fields.StringField({ initial: '', default: '' }),
                ac: new fields.NumberField({ initial: 10, required: true, default: 10 }),
                modifier: new fields.NumberField({ initial: 0, default: 0 }),
                bulk: new fields.StringField({ initial: '', default: '' }),
                points: new fields.SchemaField({
                    min: new fields.NumberField({ initial: 0, default: 0 }),
                    max: new fields.NumberField({ initial: 0, default: 0 }),
                    value: new fields.NumberField({ initial: 0, default: 0 }),
                }),
            }),

            armorstyle: new fields.StringField({ initial: '', default: '' }),
        });
    }
}
