import { itemDataSchema, itemSoundSchema } from './item.js';

// #psionic - power data model - subject to change/extension

export default class ARSItemPower extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        const schema = foundry.utils.mergeObject(super.defineSchema(), {
            ...itemSoundSchema.defineSchema(),

            groups: new fields.StringField({ default: '' }),
            discipline: new fields.StringField({ initial: 'Clairsentience', default: 'Clairsentience' }),
            powerCategory: new fields.StringField({ initial: 'Science', default: 'Science' }),
            powercost: new fields.StringField({ required: true, initial: '0', default: '0' }),
            powerCostAttack: new fields.StringField({ required: true, initial: '0', default: '0' }),
            powerCostDefense: new fields.StringField({ required: true, initial: '0', default: '0' }),
            maintenance: new fields.StringField({ required: true, initial: '0', default: '0' }),
            range: new fields.StringField({ default: '' }),
            prepTime: new fields.StringField({ initial: '0', default: '0' }),
            areaOfEffect: new fields.StringField({ initial: 'personal', default: 'personal' }),
            prerequisites: new fields.StringField({ initial: 'none', default: 'none' }),
            ability: new fields.StringField({ required: true, initial: 'wis', default: 'wis' }),
            abilityMod: new fields.StringField({ required: true, initial: '0', default: '0' }),
            type: new fields.StringField({ required: true, initial: 'descending', default: 'descending' }),
            formula: new fields.StringField({ required: true, initial: '1d20', default: '1d20' }),
            isDefenseMode: new fields.BooleanField({ initial: false }),
            isAttackMode: new fields.BooleanField({ initial: false }),
            atkDefType: new fields.StringField({ initial: 'none', default: 'none' }),
            modifiers: new fields.SchemaField({
                class: new fields.NumberField({ default: 0 }),
                background: new fields.NumberField({ default: 0 }),
                ability: new fields.NumberField({ default: 0 }),
                armor: new fields.NumberField({ default: 0 }),
                item: new fields.NumberField({ default: 0 }),
                race: new fields.NumberField({ default: 0 }),
                other: new fields.NumberField({ default: 0 }),
            }),
        });

        // console.log(`==================== ARSItemPower Schema ======================: ${schema}`);

        return schema;
    }
}
