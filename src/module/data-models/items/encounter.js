import { itemDataSchema } from './item.js';
import * as utilitiesManager from '../../utilities.js';
export default class ARSItemEncounter extends itemDataSchema {
    /** @inheritDoc */
    static defineSchema() {
        const fields = foundry.data.fields;
        return foundry.utils.mergeObject(super.defineSchema(), {
            proficiencies: new fields.SchemaField({
                weapon: new fields.StringField({ required: true }),
                skill: new fields.StringField({ required: true }),
            }),
            // name: new fields.StringField(),
            // type: new fields.StringField(),

            npcList: new fields.ArrayField(
                new fields.SchemaField({
                    id: new fields.StringField({ required: true }),
                    uuid: new fields.StringField({ required: true }),
                    name: new fields.StringField({ required: true }),
                    img: new fields.StringField({ required: true }),
                    count: new fields.StringField({ initial: '1', default: '1' }),
                    pack: new fields.StringField({ nullable: true }),
                    hp: new fields.NumberField({ integer: true, default: 0 }),
                    xp: new fields.NumberField({ integer: true, default: 0 }),
                })
            ),
        });
    }

    /** @inheritdoc */
    static migrateData(source) {
        // console.log('ARSItemEncounter migratePropertiesData', { source });
    }
}

/**
 *
 * When encounter dropped on scene add contents
 *
 * @param {*} item item object (encounter item)
 * @param {*} dropData drop data that includes x/y
 */
import { isControlActive, isAltActive, isShiftActive } from '../../utilities.js';
export async function encounterDrop(item, dropData) {
    console.log('encounter.js encounterDrop', { item, dropData });
    if (item) {
        for await (const npcr of item.system.npcList) {
            let actor = npcr.uuid ? await fromUuid(npcr.uuid) : await game.actors.get(npcr.id);
            if (!actor && npcr.pack) {
                actor = await game.packs.get(npcr.pack).getDocument(npcr.id);
            }
            if (actor) {
                if (actor.compendium) {
                    const actorData = game.actors.fromCompendium(actor);
                    // create local actor, use same id so we can use it instead if we drop this again
                    actor = await Actor.implementation.create({ ...actorData, id: npcr.id, _id: npcr.id }, { keepId: true });

                    // place this actor into a dump folder
                    const dumpfolder = await utilitiesManager.getFolder('Encounter Drops', 'Actor');
                    actor.update({ folder: dumpfolder.id });
                }

                const countFor = (await utilitiesManager.evaluateFormulaValue(npcr.count)) || 1;
                console.log(`encounter.js EncounterDrop() spawning ${countFor} ${actor.name}`);

                // now we have count and actor object, lets drop them.
                const scene = canvas.scene;
                const spacing = canvas.grid.size; // Ensure placement honors grid size
                let positions = [];

                // Get the snapped drop position
                let { x: px, y: py } = canvas.grid.getSnappedPosition(dropData.x, dropData.y, 1);
                positions.push({ x: px, y: py });

                // Spiral movement logic
                const directions = [
                    { dx: 1, dy: 0 }, // Right
                    { dx: 0, dy: -1 }, // Up
                    { dx: -1, dy: 0 }, // Left
                    { dx: 0, dy: 1 }, // Down
                ];

                let step = 1,
                    dirIndex = 0;

                while (positions.length < countFor) {
                    for (let i = 0; i < 2; i++) {
                        // Each step size is repeated twice (square spiral pattern)
                        for (let j = 0; j < step; j++) {
                            if (positions.length >= countFor) break;
                            px += directions[dirIndex].dx * spacing;
                            py += directions[dirIndex].dy * spacing;

                            // Snap the position to the grid
                            let snapped = canvas.grid.getSnappedPosition(px, py, 1);
                            positions.push({ x: snapped.x, y: snapped.y });
                        }
                        dirIndex = (dirIndex + 1) % 4; // Cycle through directions
                    }
                    step++; // Increase step size every full cycle
                }

                // Create token data using actor.getTokenDocument()
                let tokenDataArray = [];
                for (let pos of positions) {
                    const tokenData = await actor.getTokenDocument({ x: pos.x, y: pos.y, hidden: isAltActive() });
                    tokenDataArray.push(tokenData.toObject()); // Convert to plain object for document creation
                }

                // Create Token Documents
                await scene.createEmbeddedDocuments('Token', tokenDataArray);
            }
        }
    }
}
