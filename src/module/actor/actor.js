import { ARS } from '../config.js';
import { ARSDice } from '../dice/dice.js';
import * as utilitiesManager from '../utilities.js';
import { ARSCardPopout } from '../apps/cardPopout.js';
import * as effectManager from '../effect/effects.js';
import * as dialogManager from '../dialog.js';
import * as debug from '../debug.js';
import { ARSAction, ARSActionGroup } from '../action/action.js';
import { ARSPsionics } from '../psionics/psionics.js';
// import { ARSRoll, ARSRollAttack, ARSRollBase, ARSRollCombat, ARSRollDamage, ARSRollInitiative } from '../dice/rolls.js';

/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class ARSActor extends Actor {
    chatTemplate = {
        action: 'systems/ars/templates/chat/parts/chatCard-actionV2.hbs',
    };

    //getter to return actor alias
    get alias() {
        return this.system.alias;
    }

    get isLarge() {
        return !game.ars.config.notLargeCreature.includes(this.system.attributes.size);
    }

    get isNPC() {
        return this.type === 'npc';
    }
    get isPC() {
        return this.type === 'character';
    }
    /**
     *
     * Returns name, wrapper for old "getter name" for v10
     *
     * @returns
     */
    getName() {
        if (!game.ars.config.settings.identificationActor) return this.name;

        if (['npc', 'lootable'].includes(this.type)) {
            if (!game.user.isGM && !this.isIdentified) {
                return this.alias ? this.alias : game.i18n.localize('ARS.unknownActor');
            }
        }

        return this.name;
    }

    get nameRaw() {
        return this.name;
    }
    /** getting for identified */
    get isIdentified() {
        return this.system.attributes.identified || this.isOwner;
    }

    get needsInitiative() {
        const combatant = this.getCombatant();
        if (combatant) {
            return combatant.initiative === null || combatant.initiative === undefined;
        }
        return false;
    }

    /**
     * get any status initiative modifers
     */
    get initiativeModifier() {
        let mod = 0;
        if (this.hasStatusEffect('restrain')) {
            mod += 3;
        }
        if (this.hasStatusEffect('blind')) {
            mod += 2;
        }

        return mod;
    }

    /** getter to return movement adjusted for encumbrance status effects */
    get movement() {
        const currentMove = this.type === 'npc' ? this.system.movement : this.system.attributes.movement.value;
        let move = parseInt(currentMove);
        const variant = parseInt(game.ars.config.settings.systemVariant);

        switch (variant) {
            case 0:
            case 1:
                {
                    //v0/v1 move adjustments?
                    if (this.hasStatusEffect('encumbrance-severe')) {
                        if (currentMove > 30) move = 30;
                    } else if (this.hasStatusEffect('encumbrance-heavy')) {
                        if (currentMove > 30) move = 30;
                    } else if (this.hasStatusEffect('encumbrance-moderate')) {
                        if (currentMove > 60) move = 60;
                    } else if (this.hasStatusEffect('encumbrance-light')) {
                        if (currentMove > 90) move = 90;
                    }
                }
                break;

            case 2:
                {
                    if (this.hasStatusEffect('encumbrance-severe') || this.hasStatusEffect('encumbrance-max')) {
                        move = 1;
                    } else if (this.hasStatusEffect('encumbrance-heavy')) {
                        move = move - Math.floor(move * (2 / 3));
                    } else if (this.hasStatusEffect('encumbrance-moderate')) {
                        move = move - Math.floor(move * (1 / 2));
                    } else if (this.hasStatusEffect('encumbrance-light') || this.hasStatusEffect('blind')) {
                        move = move - Math.floor(move * (1 / 3));
                    }
                }
                break;

            default:
                {
                }
                break;
        }

        return move;
    }

    get combatant() {
        return this.getCombatant();
    }
    get combat() {
        return this.getCombatant()?.combat;
    }

    /** getter to return the current carried currency on actor */
    get carriedCurrencyItems() {
        return this.items.filter(function (item) {
            return item.system?.location?.state === 'carried' && item.type === 'currency' && item.system?.quantity > 0;
        });
    }
    /**
     * Is this actor currently the actor with initiative?
     */
    get initiativeActive() {
        const turn = this?.combat?.turn;
        const active = this?.combat && this?.combat?.turns[turn] == this?.combatant;
        return active;
    }

    /**
     * returns true if the actor is wearing armor.
     */
    get wearingArmor() {
        const nonArmorAttributes = ['ring', 'bracer', 'cloak'];

        // Filter items to find equipped armors that are not rings, bracers, or cloaks
        const armors = this.armors.filter((item) => {
            const baseItemType = item?.system?.alias?.toLowerCase() || '';
            const itemType = item.system?.attributes?.type?.toLowerCase();
            const itemSubtype = item.system?.attributes?.subtype?.toLowerCase();
            return (
                item.system?.location?.state === 'equipped' &&
                !nonArmorAttributes.includes(baseItemType) &&
                !nonArmorAttributes.includes(itemType) &&
                !nonArmorAttributes.includes(itemSubtype)
            );
        });

        // Return true if any matching armor is found
        return armors.length > 0;
    }

    // Property to get the encumbrance value
    get encumbrance() {
        // const automateEncumbrance = game.settings.get('ars', 'automateEncumbrance');
        // if (!automateEncumbrance) return '';
        // Parse system variant configuration setting to an integer
        const systemVariant = parseInt(game.ars.config.settings.systemVariant);

        // Compute strength value based on ability
        const strengthValue = this.computedStrengthValue;

        switch (systemVariant) {
            /**
             * Encumbrance calculation for variant 2
             */
            case 2:
                {
                    // In v2, magic armor doesn't count towards encumbrance
                    // Calculate total weight of magic armors
                    const magicArmorWeight = this.armors
                        .filter((armor) => armor.isMagic)
                        .reduce(
                            (totalWeight, armor) =>
                                totalWeight + parseInt(armor.system.weight) * parseInt(armor.system.quantity) || 0,
                            0
                        );

                    // Compute actual carried weight after discounting magic armor weight
                    const actualCarriedWeight = parseInt(Number(this.carriedweight).toFixed(0)) - magicArmorWeight;

                    // Get strength data from the strength table
                    const strengthData = ARS.strengthTable[systemVariant][strengthValue];

                    // Define mapping between encumbrance levels and their indices
                    const encumbranceLevelIndices = {
                        unencumbered: 6,
                        light: 7,
                        moderate: 8,
                        heavy: 9,
                        severe: 10,
                        max: 11,
                    };

                    // Find and return the first encumbrance level where the actual carried weight is less than or equal to the corresponding strength value
                    for (let index = 6; index < strengthData.length; index++) {
                        let encumbranceLevel = Object.keys(encumbranceLevelIndices).find(
                            (key) => encumbranceLevelIndices[key] === index
                        );
                        if (actualCarriedWeight <= strengthData[index]) {
                            return encumbranceLevel;
                        }
                    }

                    // If no encumbrance level found, return 'max'
                    return 'max';
                } // end of variant 2
                break;

            /**
             * Encumbrance for standard/variant 1
             */
            default:
                {
                    // variant 0 and 1
                    const weight = parseInt(Number(this.carriedweight).toFixed(0)) || 0;
                    const strMod = parseInt(ARS.strengthTable[systemVariant][strengthValue][2]) || 0;
                    let encumbranceLevel = '';
                    if (weight <= strMod + 35) {
                        // move 12
                        encumbranceLevel = 'unencumbered';
                    } else if (weight <= strMod + 70) {
                        // move 9
                        encumbranceLevel = 'light';
                    } else if (weight <= strMod + 105) {
                        // move 6
                        encumbranceLevel = 'moderate';
                    } else if (weight <= strMod + 150) {
                        // move 3
                        encumbranceLevel = 'heavy';
                    } else {
                        // can't move?
                        encumbranceLevel = 'severe';
                    }
                    return encumbranceLevel;
                }
                break;
        }

        // return an empty string otherwise
        return '';
    }

    get carriedweight() {
        function getFinalWeightReduction(item) {
            let currentItem = item.containedIn;
            while (currentItem) {
                if (!currentItem.containedIn) {
                    return currentItem?.system?.capacity?.weightreduction;
                }
                currentItem = currentItem?.containedIn || undefined;
            }
            return 0;
        }

        // calculate weight carried/encumbrance
        const weightItems = this.items.filter((item) => {
            return ['equipped', 'carried'].includes(item.system?.location?.state);
        });

        let carriedweight = 0;
        weightItems.forEach((item) => {
            const count = parseFloat(item.system?.quantity) || 0;
            const weight = parseFloat(item.system?.weight) || 0;
            const itemWeight = count * weight;
            const weightReduction = getFinalWeightReduction(item);

            if (weightReduction) {
                carriedweight += (itemWeight * (100 - weightReduction)) / 100;
            } else {
                carriedweight += itemWeight;
            }
        });

        return Number(carriedweight).toFixed(2);
    }
    /**
     * get the variant 2 style optional natural weapon initiative modifiers for size
     */
    get initiativeModifierForSize() {
        let mod = 0;
        switch (this.system.attributes.size) {
            // case "tiny":

            case 'small':
            case 'medium':
                mod += 3;
                break;

            case 'large':
                mod += 6;
                break;

            case 'huge':
                mod += 9;
                break;

            case 'gargantuan':
                mod += 12;
                break;

            default:
                break;
        }
        return mod;
    }

    /**
     *
     * returns valid index for
     * ARS.strengthTable[systemVariant][XXXXX]
     * that adjust for percentile str
     *
     * @returns index
     */
    get computedStrengthValue() {
        const str = this.system.abilities.str;
        // If the value isn't 18 or there's no percent, simply return the value
        if (str.value !== 18 || str.percent <= 0) return str.value;

        // Map ability percent to equivalent strength value
        if (str.percent >= 100) return 100;
        if (str.percent >= 91) return 99;
        if (str.percent >= 76) return 90;
        if (str.percent >= 51) return 75;
        // Default strength value for ability.percent < 51
        return 50;
    }

    get warriorAttacksPerRoundTier() {
        let attackTier = 0;
        if (!this.isPC) return 0;
        for (const c of this.classes) {
            const level = this.getClassLevel(c);
            if (this.isWarrior) {
                if (level >= 13) return 3;
                if (level >= 7 && attackTier < 2) attackTier = 2;
                if (level >= 1 && attackTier < 1) attackTier = 1;
            }
        }
        return attackTier;
    }

    get isWarrior() {
        if (this.isPC) {
            return this.classes.some((c) => {
                const className = c.name.toLowerCase();
                return className === 'fighter' || className === 'ranger' || className === 'paladin';
            });
        }
        return false;
    }

    get canSpecialize() {
        if (this.isPC) {
            const singleClassFighter = this.classes.length === 1 && this.classes[0].name.toLowerCase() === 'fighter';
            return singleClassFighter;
        }
        return false;
    }

    // #psionic

    // get psionic state
    get isPsionic() {
        return this.system.psionics.isPsionic;
    }

    // set psionic state
    set isPsionic(val) {
        this.update({ 'system.psionics.isPsionic': val });
    }

    // total psionic strench points
    get totalPsionicStrength() {
        const usePsionicsV1 = game.settings.get('ars', 'usePsionicsV1');
        const psp = this.system.psionics.psp.value;
        const pspattack = this.system.psionics.pspattack.value;
        const pspdefense = this.system.psionics.pspdefense.value;

        let totalPsionicStrength;
        if (usePsionicsV1) {
            totalPsionicStrength = pspattack + pspdefense;
        } else {
            totalPsionicStrength = psp;
        }

        // Ensure the minimum value is 1
        return totalPsionicStrength;
    }

    get totalPsionicAttackStrength() {
        return this.system.psionics.pspattack.value;
    }

    get totalPsionicDefenseStrength() {
        return this.system.psionics.pspdefense.value;
    }

    // sort psionics powers by discipline, then alphabetically, then sort powers in each discipline by science and devotions
    get sortPowers() {
        const powers = this.powers;

        // dont even bother with no powers in inventory
        if (!powers?.length) return;

        const disciplinesMapping = ARS.psionics.disciplines;

        // First, group the items by their discipline
        const groupedItems = powers.reduce((acc, power) => {
            let disciplineKey = power.system.discipline.toLowerCase();
            if (disciplinesMapping.hasOwnProperty(disciplineKey)) {
                if (!acc[disciplineKey]) {
                    acc[disciplineKey] = [];
                }
                acc[disciplineKey].push(power);
            }
            return acc;
        }, {});

        // Then, sort the keys (disciplines) alphabetically
        const sortedDisciplines = Object.keys(groupedItems).sort();

        // Return a new object with the disciplines sorted alphabetically and powers sorted by category
        const sorted = sortedDisciplines.reduce((sortedAcc, discipline) => {
            // Sort the powers within each discipline by science vs. devotion
            sortedAcc[discipline] = groupedItems[discipline].sort((a, b) => {
                if (a.system.powerCategory === b.system.powerCategory) {
                    return 0; // If both are the same category, maintain original order
                } else if (a.system.powerCategory === 'science') {
                    return -1; // Science comes before devotion
                } else {
                    return 1; // Devotion comes after science
                }
            });
            return sortedAcc;
        }, {});

        return sorted;
    }

    // summary of disciplines, sciences and devotions - used to total and validate (v2) powers/abilities for psionic characters
    get powerSummary() {
        const usePsionicsV2 = game.settings.get('ars', 'usePsionicsV1') === false;
        const powers = this.powers;
        const summary = {
            totalDisciplines: 0,
            totalSciences: 0,
            totalDevotions: 0,
            totalDefenseModes: 0, // Track the number of defense modes
        };

        const uniqueDisciplines = new Set();

        // Track counts of defense modes in each category
        let defenseModeSciences = 0;
        let defenseModeDevotions = 0;

        powers.forEach((power) => {
            // Check if the power is a defense mode
            if (power.system.isDefenseMode && usePsionicsV2) {
                summary.totalDefenseModes += 1;
                if (power.system.powerCategory === 'science') {
                    defenseModeSciences += 1;
                } else if (power.system.powerCategory === 'devotion') {
                    defenseModeDevotions += 1;
                }
                return; // Skip adding to sciences or devotions if it's a defense mode
            }

            // Count unique disciplines
            uniqueDisciplines.add(power.system.discipline.toLowerCase());

            // Count sciences and devotions only if not a defense mode
            if (power.system.powerCategory === 'science') {
                summary.totalSciences += 1;
            } else if (power.system.powerCategory === 'devotion') {
                summary.totalDevotions += 1;
            }
        });

        // Set the total number of unique disciplines
        summary.totalDisciplines = uniqueDisciplines.size;

        // Get the actor's current level values for disciplines, sciences, devotions, and defense modes
        const actorLimits = {
            disciplines: this.system.psionics.disciplines,
            sciences: this.system.psionics.sciences,
            devotions: this.system.psionics.devotions,
            defenses: this.system.psionics.defenseModes,
        };

        // Adjust the count of sciences and devotions based on defense modes
        if (summary.totalDefenseModes > actorLimits.defenses && usePsionicsV2) {
            // Calculate the excess defense modes over the allowed limit
            const excessDefenseModes = summary.totalDefenseModes - actorLimits.defenses;

            // Adjust sciences and devotions, ensuring they don't go below zero
            summary.totalSciences = Math.max(0, summary.totalSciences + defenseModeSciences - excessDefenseModes);
            summary.totalDevotions = Math.max(0, summary.totalDevotions + defenseModeDevotions - excessDefenseModes);
        }

        // Compare the summary totals with the actor's level limits
        const invalid = {
            disciplines: summary.totalDisciplines > actorLimits.disciplines,
            sciences: summary.totalSciences > actorLimits.sciences,
            devotions: summary.totalDevotions > actorLimits.devotions,
            defenses: summary.totalDefenseModes > actorLimits.defenses,
        };

        // Return both the summary and the comparison result
        return {
            summary: summary,
            invalid: invalid,
        };
    }

    // Helper function to find the highest key in an object
    findMatrixHighestLevel(obj) {
        // Get the highest key by converting keys to numbers and finding the maximum value
        return Math.max(...Object.keys(obj).map(Number));
    }

    //helper to get the single slice of the array specific to this combatlevel
    getMatrixSliceByLevel(matrixAs, combatLevel) {
        // Get the matrix variant from the game configuration
        const variant = Number(game.ars.config.settings.systemVariant);
        // Access the desired matrix using the variant and matrixAs
        const matrix = game.ars.config.matrix[variant][matrixAs];
        // Get the maximum combat level for the current matrix
        const matrixMaxLevel = this.findMatrixHighestLevel(matrix);
        // Clamp the input combatLevel to be within the valid range for the matrix
        const clampCombatLevel = Math.max(0, Math.min(combatLevel, matrixMaxLevel));
        // Return the matrix slice for the clamp combat level
        return matrix[clampCombatLevel];
    }

    /** getter to return current "best" attack matrix for highest current level */
    get matrixSlice() {
        const bestIndexCheck = 10;
        // best matrix slice for class(es)
        let bestmSlice = undefined;
        // Set the default matrix table as 'fighter' if not defined in the system
        const matrixAs = this.system.matrixTable ?? 'fighter';

        // Determine combat level based on the type of the character (npc or player)
        let combatLevel = this.type === 'npc' ? this.effectiveLevel : this.getMaxLevel();
        // Get the initial matrix slice using the default matrix table and combat level
        let mSlice = this.getMatrixSliceByLevel(matrixAs, combatLevel);

        // Get the active classes for the character
        // const activeClasses = this.getActiveClasses();
        // this will get usable for dual class or active for multi-class
        const activeClasses = this.getUsableClasses();
        // Check if there are any active classes
        if (activeClasses.length) {
            // Iterate over each active class
            activeClasses.forEach((cl) => {
                // Check if the class has a matrix table defined in the system
                if (cl.system.matrixTable) {
                    // Determine the safe combat level for the class, defaulting to 1 if not defined
                    const safeCombatLevel = Math.max(0, cl.classDetails?.level ?? 1);
                    // Get the matrix slice for the current class and safe combat level
                    const cSlice = this.getMatrixSliceByLevel(cl.system.matrixTable, safeCombatLevel);
                    // Update the matrix slice if it is not set or if the current slice is better
                    if (!bestmSlice || cSlice[bestIndexCheck] < bestmSlice[bestIndexCheck]) {
                        bestmSlice = cSlice;
                    }
                }
            });
        }
        if (!bestmSlice && activeClasses.length) {
            ui.notifications.warn(
                `No attack matrix found in class list for ${this.name}, defaulting to fighter. Required for Standard and Variant 1 settings.`
            );
        } else if (activeClasses.length) {
            mSlice = bestmSlice;
        }
        // Return the final matrix slice
        return mSlice;
    }

    /** getter that returns the "types" from class/race for trigger.type evaluation  */
    get types() {
        // Combine classes and races arrays into one array
        const typesItems = [...this.classes, ...this.races]; // Use spread operator instead of Array.concat
        let types = [];

        // Iterate over each item in the combined array
        for (const item of typesItems) {
            // Check if the item has a 'type' property in its system object
            if (item.system.type) {
                // Split the type string by commas and add to the types array
                const splits = item.system.type.split(',').map((type) => type.trim().toLowerCase());
                types = types.concat(splits);
            }
        }

        // Return the unique types
        return [...new Set(types)]; // Ensure types are unique
        // return types;
    }

    /**
     *
     * called before actor created
     *
     * @param {*} data
     * @param {*} options
     * @param {*} user
     */
    async _preCreate(data, options, user) {
        // do stuff to data, options, etc
        // console.log("actor.js _preCreate", { data, options, user })
        await super._preCreate(data, options, user);
        if (!data.hasOwnProperty('prototypeToken')) await this._createTokenPrototype(data);
    }

    async _onCreate(data, options, userId) {
        // console.log("actor.js _onCreate", { data, options, userId })
        await super._onCreate(data, options, userId);
        await this._postCreate(data, options, userId);
    }

    /** @override onDelete to fiddle with game.party */
    async _onDelete(options, id) {
        if (game.user.isDM) {
            if (game.party) {
                game.party.render();
            }
        }
    }

    // /**@override */
    async _onUpdate(data, options, id) {
        console.log('actor.js _onUpdate', { data, options, id });

        await super._onUpdate(data, options, id);
        // if (game.user.id !== id) return;
        if (this?.type !== 'character' && data?.system?.attributes?.hasOwnProperty('identified')) {
            // refresh the combat-tracker with new display modes
            ui.combat.render();
            // flip through messages and update the relevant ones
            game.messages.forEach((msg) => {
                const tokenId = msg.speaker.token;
                if (this.token?.id === tokenId) {
                    //triggers renderChatMessage which updates for display modes
                    ui.chat.updateMessage(msg);
                }
            });

            if (!game.user.isDM && this.token?.object) {
                this.token.object.draw();
            }
        }

        if (!this.isOwner) {
            return;
        }
        await this.updateEncumbrance();
        const hpData = this._getClassHPData();
        if (hpData) await this.update(hpData);

        //hookData?.system?.attributes?.hp
        if (data?.system?.attributes?.hasOwnProperty('hp') && this.system.attributes.hp.max) {
            const optionNegativeHP = game.settings.get('ars', 'optionNegativeHP');
            const dead =
                optionNegativeHP && this.type == 'character'
                    ? data.system.attributes.hp.value <= this.system.attributes.hp.min
                    : data.system.attributes.hp.value <= 0;
            const unconscious = !dead && optionNegativeHP ? data.system.attributes.hp.value <= 0 : false;
            if (!this.hasStatusEffect('dead') && dead) {
                utilitiesManager.playAudioDeath();
                if (this.type === 'npc' && game.party?.getMembers().length) {
                    game.party.addDefeatedAward(this.getToken());
                }
            } else if (!this.hasStatusEffect('dying') && unconscious) {
                utilitiesManager.playAudioUnconscious();
            }
            if (game.user.isDM) utilitiesManager.setHealthStatusMarkers(this);
        }
    }

    /**
     * process encumbrance changes
     *
     */
    async updateEncumbrance() {
        // if (this.isOwner) {
        const automateEncumbrance = game.settings.get('ars', 'automateEncumbrance');

        const statuses = [];
        // current encumbrance status
        const encumbranceState = this.encumbrance;
        // we use a static ID for the effect since you
        // only have a single status effect state
        const staticid = 'ygJJfLRw90gy03dF';
        // the id of the status effect in config
        const encId = `encumbrance-${encumbranceState}`;
        // is the actor encumbered?
        const hasState = encumbranceState && encumbranceState !== 'unencumbered';
        if (hasState) statuses.push(encId);

        // get current status effect if any
        const effect = this.effects.get(staticid);
        // if no encumbrance nuke the effect
        if (!statuses.length || !automateEncumbrance) {
            return effect?.delete();
            // setTimeout(async () => {
            //     await effect?.delete();
            // }, 1500);
            // return;
        }

        // build status effect
        const encData = CONFIG.statusEffects.find((effect) => effect.id === encId);
        encData.label = game.i18n.localize(encData.label);
        const effectData = { ...encData, statuses };

        // if we already have an effect, update
        if (effect) {
            // const hasEffect = effect.statuses.get(endId);
            return effect.update(effectData);
            // setTimeout(async () => {
            //     await effect.update(effectData);
            // }, 1500);
            // return;
        }

        // create status effect
        return ActiveEffect.implementation.create({ _id: staticid, ...effectData }, { parent: this, keepId: true });
        // setTimeout(async () => {
        //     await ActiveEffect.implementation.create({ _id: staticid, ...effectData }, { parent: this, keepId: true });
        // }, 1500);
        // }
    }

    /** do somethings after creating actor */
    async _postCreate(data, options, userId) {
        // console.log("actor.js _postCreate", { data, options, userId })
        if (!CONFIG.ARS.icons?.general?.actors) return;

        const normalDefaultIcon = 'icons/svg/mystery-man.svg';

        const defaultActorIcon = CONFIG.ARS.icons.general.actors[data.type];
        if (defaultActorIcon && this.img === normalDefaultIcon) {
            // We have to check if the current image is the normal "mystery man" icon because otherwise this replaces
            // icons during duplication. It's a hack but I don't see a way to detect duplication.
            await this.update({ img: defaultActorIcon });
        }
    }

    /**@override */
    async _preUpdate(changed, options, user) {
        // console.trace('actor.js _preUpdate()', { changed, options, user });
        let updateData = undefined;
        if (this.isOwner) {
            updateData = this._getClassHPData();
            // combine new data but use changed as source of truth
            if (updateData) changed = foundry.utils.mergeObject(updateData, changed);
        }
        super._preUpdate(changed, options, user);
    }

    /**@override so that we can make sure that items updated such as class/abilities hp calcs will be managed */
    async _onUpdateDescendantDocuments(parent, collection, documents, changes, options, userId) {
        // console.log('actor.js _onUpdateDescendantDocuments', { parent, collection, documents, changes, options, userId });

        super._onUpdateDescendantDocuments(parent, collection, documents, changes, options, userId);

        // if (userId === game.userId && collection === 'items') {
        if (this.isOwner && ['items', 'effects'].includes(collection)) {
            await this.updateEncumbrance(options);
        }

        if (!this.isOwner) return;
        // const promise = new Promise((resolve) => {
        //     resolve(this._getClassHPData());
        // });
        // promise.then(async (classHPData) => {
        //     if (classHPData) await this.update(classHPData);
        //     await this.getToken()?.updateLight();
        //     await this.getToken()?.updateSight();
        // });
        const hpData = this._getClassHPData();
        if (hpData) await this.update(hpData);
        await this.getToken()?.updateLight();
        await this.getToken()?.updateSight();
        await this._resetIsPsionic();
    }

    /** @inheritDoc */
    async _onCreateDescendantDocuments(parent, collection, documents, data, options, userId) {
        // console.log('actor.js _onCreateDescendantDocuments', { parent, collection, documents, data, options, userId });
        super._onCreateDescendantDocuments(parent, collection, documents, data, options, userId);
        // if (userId === game.userId && collection === 'items') {
        if (this.isOwner && ['items', 'effects'].includes(collection)) {
            await this.updateEncumbrance(options);
            const hpData = this._getClassHPData();
            if (hpData) await this.update(hpData);
        }
    }

    /* -------------------------------------------- */

    /** @inheritDoc */
    async _onDeleteDescendantDocuments(parent, collection, documents, ids, options, userId) {
        // console.log('actor.js _onDeleteDescendantDocuments', { parent, collection, documents, ids, options, userId });
        // if (userId === game.userId) {
        super._onDeleteDescendantDocuments(parent, collection, documents, ids, options, userId);
        if (this.isOwner && ['items', 'effects'].includes(collection)) {
            await this.updateEncumbrance(options);
            const hpData = this._getClassHPData();
            if (hpData) await this.update(hpData);
            await this.getToken()?.updateLight();
            await this.getToken()?.updateSight();
            await this._resetIsPsionic();
        }
    }

    /**
     * Augment the basic actor data with additional dynamic data.
     */
    prepareData() {
        // console.log("actor.js prepareData", this)

        this.system.mods = {}; // do this or the mods values from ActiveEffects increment during super.prepareData();
        super.prepareData();

        const data = this.system;
        // data.config = ARS;

        // the order of the following functions matter, keep that in mind

        const isPC = this.type === 'character';

        // @system.abilities.*, ability fields like opendoors, spell failure
        this._buildAbilityFields(data);

        // action.prepareDerivedData(this);

        // this._prepareCharacterItems(data);

        this._prepareCharacterItems(this);

        this._calculateAvailableCurrency(this);

        // Prepare PC specific
        if (isPC) {
            this._prepareClassProficiencySlots(data);
        } else {
            // NPC specific
        }

        // _prepareCasterLevels needs to run after _prepareCharacterItems() so we have a list of actor.classes
        // @rank.levels.{arcane|divine} TOTAL (base+mods)
        this._prepareCasterLevels(data);

        this._prepareCharacterData(data);

        //-- build selection lists for

        // setup Armor values based on worn/etc
        this._prepareArmorClass(data);

        // this._prepareSpellsByLevel(data);
        this._prepareMemorizationSlotData(data);

        // #psionic
        this._preparePsionicData(data);

        // this._prepareMemorization(data); // moved to actor-sheet.js for await
    }

    /**
     * Prepare Character type specific data
     */
    _prepareCharacterData(data) {
        if (this.isOwner) {
            this._prepareClassData();
            // Placing this here so movement (adjusted for encumbrance/etc) can be referenced as formula
            this.system.attributes.movement.current = this.movement;
        }
    }

    /** @override */
    prepareDerivedData() {
        // console.log("actor.js prepareDerivedData", this, this.overrides)
        super.prepareDerivedData();

        // for v10
        this.name = this.getName();

        // const data = this.system;
        const isPC = this.type === 'character';

        // add some data that can be referenced in formulas.
        this.system.initStatusMod = this.initiativeModifier;
        this.system.initSizeMod = this.initiativeModifierForSize;

        const maxHp = this.system.attributes.hp?.max || 0;
        const currentHp = this.system.attributes.hp?.value || 0;
        this.system.attributes.hp.percent = Math.round((currentHp / maxHp) * 100);

        //TODO: work on the new actions functionality
        // this.actionGroups = ARSAction.convertFromActionBundle(this, this.system.actions);
        if (!this.actionGroups) this.actionGroups = ARSActionGroup.loadAll(this);
        ARSActionGroup.prepareDerivedData(this, this);
    }

    /**
     *
     * We use this to add values that can easily be accessed from
     * handlebars to generate specific output in the spell/memorization
     * section
     *
     * @param {*} data
     */
    _prepareMemorizationSlotData(data) {
        const systemVariant = ARS.settings.systemVariant;

        // console.log("actor.js _prepareMemorizationSlotData 1", foundry.utils.duplicate(data), this)

        // data.spellInfo.slots.arcane.1 ADD #
        data.memorizations = {};
        let hasSpellSlots = false;
        let slotsCount = {};
        for (const spellType in data.spellInfo.slots) {
            slotsCount[spellType] = 0;
            let bTypeHasSlots = false;
            // console.log("actor.js _prepareMemorizationSlotData type", { spellType });
            Object.values(data.spellInfo.slots[spellType].value).forEach((slotCount, index) => {
                if (slotCount > 0) {
                    hasSpellSlots = true;
                    bTypeHasSlots = true;

                    if (spellType === 'divine') {
                        const wis = data.abilities.wis.value;
                        if (wis > 25) wis = 25;
                        if (wis < 1) wis = 1;
                        const wisBonusSlots = game.ars.config.wisdomBonusSlots[systemVariant][wis][index];
                        const disableWisBonus = this.classes.find((entry) => entry.system.features.wisSpellBonusDisabled);
                        // is disableWisBonus then we dont include high wis spell slot bonuses
                        slotCount += disableWisBonus ? 0 : wisBonusSlots;
                    }
                    //TODO add optional bonus for high int here?
                    // add in bonuses from mod.spells.slots[spellType][index] = value here?
                    slotsCount[spellType] += parseInt(slotCount);
                }

                // populate prepSlots that don't exist so we have blanks to fill in character sheet
                if (slotCount < 1 || !data.spellInfo.memorization[spellType][index]) {
                    data.spellInfo.memorization[spellType][index] = {};
                }

                // Ensure the structure has the correct size and no skipped numbers
                let prepSlots = Object.keys(data.spellInfo.memorization[spellType][index]).map(Number);
                prepSlots.sort((a, b) => a - b);

                // Fill in any missing slots
                for (let i = 0; i < slotCount; i++) {
                    if (!data.spellInfo.memorization[spellType][index][i]) {
                        data.spellInfo.memorization[spellType][index][i] = {
                            name: null,
                            level: index,
                        };
                    }
                }

                // Remove any extra slots beyond slotCount
                Object.keys(data.spellInfo.memorization[spellType][index]).forEach((key) => {
                    if (parseInt(key) >= slotCount) {
                        delete data.spellInfo.memorization[spellType][index][key];
                    }
                });
            });

            if (bTypeHasSlots) {
                data.memorizations[spellType] = {
                    memslots: foundry.utils.deepClone(data.spellInfo.memorization[spellType]),
                    totalSlots: slotsCount[spellType],
                    spellslots: foundry.utils.deepClone(data.spellInfo.slots[spellType]),
                    // 'spellsByLevel': data.spellsByLevel[spellType],
                };
            }
        }

        // console.log("actor.js _prepareMemorizationSlotData 2", foundry.utils.duplicate(data), this)

        this.hasSpellSlots = hasSpellSlots;
    }

    // #psionic
    async _preparePsionicData(data) {
        const powers = this.powers;
        if (!powers?.length) return;
        data.psionicsSorted = await this.sortPowers;
        const psiPowerEffects = await ARSPsionics.getMaintainedPowerEffects(this);
        data.maintenancePowerEffects = psiPowerEffects.maintainedPowers;
        data.defensePowers = psiPowerEffects.defensePowers;
        data.contactPowers = psiPowerEffects.contactPowers;
    }

    // #psionic
    async _resetIsPsionic() {
        // reset when actors psionic state changes
        const curIsPsionic = this.system.psionics.isPsionic;
        let isPsionic = this.activeClasses?.some((classEntry) => classEntry.system.isPsionic) ?? false;
        if (this.isNPC && this.system.psionics.isPsionic) isPsionic = true;

        if (curIsPsionic !== isPsionic) await this.update({ 'system.psionics.isPsionic': isPsionic });
    }

    // /**
    //  *
    //  * Using this during getData in actor-sheet causes latency
    //  * using this during prepareData() makes compendium entries (divine and/or npc arcane/divine spells)
    //  * start dropping off.
    //  *
    //  * So when spell is memorized the spellItem is added, downside is it wont update until the spell slot is re-memorized.
    //  *
    //  * data for memslots we only need if we have a sheet populated
    //  *
    //  * @param {*} data
    //  */
    // async _prepareMemorization(data) {
    //     // console.trace("actor.js _prepareMemorization", { data }, this);

    //     for (const spellType of ['arcane', 'divine']) {
    //         if (data?.memorizations?.[spellType]) {
    //             for (let level = 0; level < data.memorizations[spellType].memslots.length; level++) {
    //                 for (let slot = 0; slot < Object.values(data.memorizations[spellType].memslots[level]).length; slot++) {
    //                     const spellId = data.memorizations[spellType].memslots[level][slot].id;
    //                     if (spellId) {
    //                         let spellItem = this.getEmbeddedDocument('Item', spellId);
    //                         if (!spellItem) {
    //                             spellItem = await utilitiesManager.getItem(spellId);
    //                             if (spellItem) {
    //                                 data.memorizations[spellType].memslots[level][slot].spellItem = spellItem;
    //                             }
    //                         } else {
    //                             data.memorizations[spellType].memslots[level][slot].spellItem = spellItem;
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    /**
     *
     * Flip through classes and calculate levels for @ranks.levels.{}
     *
     * @param {*} data
     */
    _prepareCasterLevels(data) {
        // console.log("actor.js", "_prepareCasterLevels", "data", data);

        // build casting structure
        data.rank = {
            levels: {
                arcane: 0,
                divine: 0,
            },
        };
        data.rank.levels.arcane = parseInt(data.spellInfo.level.arcane.value + parseInt(data.mods?.levels?.arcane || 0));
        data.rank.levels.divine = parseInt(data.spellInfo.level.divine.value + parseInt(data.mods?.levels?.divine || 0));

        // Go through classes on character and get levels so we can reference as @rank.levels.fighter @rank.levels.paladin etc...
        this.classes.forEach((classEntry) => {
            // const match = classEntry.name.match(/^(\w+)/);
            // const className = match ? match[1].toLowerCase() : '';
            const className = classEntry.name.slugify({ strict: true });
            if (className) {
                // get advancement records for this class to find level
                const advancement = classEntry.system?.advancement ? Object.values(classEntry.system.advancement) : undefined;
                const level = this.getClassLevel(classEntry);
                if (level) {
                    data.rank.levels[className] =
                        level + parseInt(data.mods?.levels?.[className] ? data.mods.levels[className] : 0);
                }
            }
        });

        data.rank.levels.max = this.getMaxLevel();
        data.rank.levels.min = this.getMinLevel();
        // console.log("actor.js _prepareCasterLevels====== data 2", data)
    }

    /**
     *
     * Calculate avaliable weapon/non-weapon slots based on class/levels
     *
     * @param {*} data
     */
    _prepareClassProficiencySlots(data) {
        let nonProfPenalty = -5;
        let weaponProfs = 0;
        let weaponEarn = 999;
        let weaponStart = 0;
        let skillProfs = 0;
        let skillEarn = 999;
        let skillStart = 0;
        let classpointsEarned = 0;

        // find the best weapon/non-weapon prof slot count starting and earned per level
        this.classes.forEach((classEntry) => {
            if (classEntry.system.proficiencies.weapon.earnLevel < weaponEarn)
                weaponEarn = classEntry.system.proficiencies.weapon.earnLevel;
            if (classEntry.system.proficiencies.skill.earnLevel < skillEarn)
                skillEarn = classEntry.system.proficiencies.skill.earnLevel;

            if (classEntry.system.proficiencies.weapon.starting > weaponStart)
                weaponStart = classEntry.system.proficiencies.weapon.starting;
            if (classEntry.system.proficiencies.skill.starting > skillStart)
                skillStart = classEntry.system.proficiencies.skill.starting;

            if (classEntry.system.proficiencies.penalty > nonProfPenalty)
                nonProfPenalty = classEntry.system.proficiencies.penalty;

            // calculate class points earned
            const maxLevelIndex = Math.max(classEntry.system.advancement.length, 0);
            const currentRanks = Object.values(classEntry.system.ranks);

            currentRanks.slice(0, maxLevelIndex).forEach((rank) => {
                const classpoints = parseInt(rank.classpoints) || 0;
                classpointsEarned += classpoints;
            });
        });

        weaponProfs += weaponStart;
        skillProfs += skillStart;

        // check backgrounds for extra weapon/skill prof points
        this.backgrounds.forEach((backgroundEntry) => {
            if (backgroundEntry.system.proficiencies.weapon)
                weaponProfs +=
                    utilitiesManager.evaluateFormulaValueSync(
                        backgroundEntry.system.proficiencies.weapon,
                        this.getRollData()
                    ) || 0;
            if (backgroundEntry.system.proficiencies.skill)
                skillProfs +=
                    utilitiesManager.evaluateFormulaValueSync(backgroundEntry.system.proficiencies.skill, this.getRollData()) ||
                    0;
        });

        //get race system.proficiencies.weapon|skill formula profs
        this.races.forEach((racesEntry) => {
            if (racesEntry.system.proficiencies.weapon)
                weaponProfs +=
                    utilitiesManager.evaluateFormulaValueSync(racesEntry.system.proficiencies.weapon, this.getRollData()) || 0;
            if (racesEntry.system.proficiencies.skill)
                skillProfs +=
                    utilitiesManager.evaluateFormulaValueSync(racesEntry.system.proficiencies.skill, this.getRollData()) || 0;
        });
        // now apply best prof values at specific levels
        // v2 gets profs on evenly divided levels, v0/v1 get them that many levels after 1
        const maxLevel = game.ars.config.settings.systemVariant == 2 ? this.getMaxLevel() + 1 : this.getMaxLevel();
        for (let level = 1; level < maxLevel; level++) {
            if (level % weaponEarn === 0) {
                weaponProfs++;
            }
            if (level % skillEarn === 0) {
                skillProfs++;
            }
        }

        // calculate used weapon/non-weapon profs
        let spentWeapon = 0;
        let spentNonWeapon = 0;
        let spentClasspoints = 0; //typically thief percent points earns for v2
        for (const profItem of this.proficiencies) {
            spentWeapon += parseInt(profItem.system.proficiencies.cost);
        }
        for (const skillItem of this.skills) {
            spentNonWeapon += parseInt(skillItem.system.features.cost);
            spentClasspoints += parseInt(skillItem.system.features?.modifiers?.class) || 0;
        }

        //TODO: move these outside of system? data is this.system.
        // data.attributes.proficiencies.weapon.used = spentWeapon;
        // data.attributes.proficiencies.weapon.value = weaponProfs;
        // data.attributes.proficiencies.weapon.penalty = nonProfPenalty;
        // data.attributes.proficiencies.skill.used = spentNonWeapon;
        // data.attributes.proficiencies.skill.value = skillProfs;
        // data.attributes.proficiencies.classpoints.used = spentClasspoints;
        // data.attributes.proficiencies.classpoints.value = classpointsEarned;

        this.proficiency = {
            weapon: {
                used: spentWeapon,
                value: weaponProfs,
                penalty: nonProfPenalty,
            },
            skill: {
                used: spentNonWeapon,
                value: skillProfs,
            },
            classpoints: {
                used: spentClasspoints,
                value: classpointsEarned,
            },
        };
    }

    /**
     *
     * Go through inventory, find equipped gear, set armor values
     *
     * @param {*} data
     */
    _prepareArmorClass(data) {
        // console.log("actor.js _prepareArmorClass start", this.name, foundry.utils.duplicate(data));

        const useArmorDamage = game.ars.config.settings.systemVariant == '2' && game.ars.config.settings.useArmorDamage;
        const rollData = data;
        data.armorClass = {};

        let bestArmor = data.attributes.ac.value;
        let bestArmorMod = 0;
        let shieldArmor = 0;
        let ringArmor = 0;
        let cloakArmor = 0;
        let otherArmor = 0;
        let protectionPointsMax = 0;
        let protectionPoints = 0;
        let armorDamageTooltip = [`<p>Armor Damage Breakdown</p>`];
        let wornMagicArmor = false;
        let wornArmor = false;
        let wornLeatherArmor = false;
        let wornMagicShield = false;
        let wornShield = false;
        let wornWarding = false;
        let wornRing = false;
        let bestWornRing = 0;
        let wornCloak = false;
        let bestWornCloak = 0;

        // iterate armor items and accum various statistics
        for (const item of this.armors) {
            // accum the total armor points/damage if using the armor damage system
            const protectMax = parseInt(item.system.protection.points.max);
            const protectPoints = parseInt(item.system.protection.points.value);
            if (item.isWornArmorOrShield) {
                let armorDamage = `<p>${item.name}: ${Math.floor((protectPoints / protectMax) * 100)}%</p>`;
                armorDamageTooltip.push(armorDamage);
                protectionPointsMax += parseInt(protectMax);
                protectionPoints += parseInt(protectPoints);
            }

            // if using armor damage and the protection points are 0 and max protection points > 0 dont count it.
            if (useArmorDamage && protectMax > 0 && protectPoints < 1) {
                continue;
            }

            // console.log("actor.js _prepareArmorClass", item)
            if (item.system.location.state === 'equipped') {
                switch (item.system.protection.type) {
                    case 'armor':
                    case 'warding':
                        if (item.system.protection.ac < bestArmor) {
                            // warding isn't "armor"
                            if (item.system.protection.type === 'armor') {
                                if (item.system.attributes.magic && !wornMagicArmor) wornMagicArmor = true;
                                if (!wornArmor) wornArmor = true;
                                if (!wornLeatherArmor && item.name.toLowerCase().includes('leather')) wornLeatherArmor = true;
                            } else {
                                if (!wornWarding) wornWarding = true;
                            }

                            bestArmor = parseInt(item.system.protection.ac);
                            bestArmorMod = parseInt(item.system.protection.modifier);
                        }
                        break;

                    case 'shield':
                        shieldArmor += parseInt(item.system.protection.ac) + parseInt(item.system.protection.modifier);
                        if (item.system.attributes.magic && !wornMagicShield) wornMagicShield = true;
                        if (!wornShield) wornShield = true;

                        break;

                    case 'ring':
                        ringArmor = parseInt(item.system.protection.modifier);
                        if (!wornRing) wornRing = true;
                        if (ringArmor > bestWornRing) bestWornRing = ringArmor;
                        break;

                    case 'cloak':
                        cloakArmor = parseInt(item.system.protection.modifier);
                        if (!wornCloak) wornCloak = true;
                        if (cloakArmor > bestWornCloak) bestWornCloak = cloakArmor;
                        break;

                    default:
                        otherArmor += parseInt(item.system.protection.modifier);
                        break;
                }
            } // is equipped?
        } // for all items

        /**
         * ActiveEffect
         *
         * data.mods.ac.value
         * data.mods.ac.base (use lowest)
         */
        if (data.mods?.ac?.base) {
            const modBase = parseInt(data.mods.ac.base);
            if (modBase < bestArmor) bestArmor = modBase;
        }
        let modValue = 0;
        let modFormulaValue = 0;

        if (data.mods?.ac?.value) {
            modValue = parseInt(data.mods.ac.value || 0);
        }

        if (data.mods?.formula?.ac?.value) {
            modFormulaValue = utilitiesManager.evaluateFormulaValueSync(data.mods.formula.ac.value, rollData);
        }
        // allow both formula and mods to apply
        let armorMod = -(modValue + modFormulaValue);

        // TODO: add an override like data.mods.ac.baseoverride and ac.valueoverride ?

        // we flip values in AC fields so AC: 1 is good, AC: -4 would be bad
        // so things like shield +1 with AC: 1 make sense to people
        data.armorClass.armor = bestArmor - bestArmorMod;
        data.armorClass.shield = -shieldArmor;
        // can't wear magic armor with rings, use only best ring
        data.armorClass.ring = -(wornMagicArmor ? 0 : bestWornRing);
        // cloaks...can't wear non-leather armor, or magic armor of anytype or any shield
        data.armorClass.cloak = 0;
        // this is checked individually so the logic is clear
        if (wornLeatherArmor && !wornShield && !wornMagicArmor) {
            data.armorClass.cloak = -bestWornCloak;
        } else if (!wornArmor && !wornShield && !wornMagicArmor) {
            data.armorClass.cloak = -bestWornCloak;
        }
        data.armorClass.other = -otherArmor;
        // get dex mod
        data.armorClass.dex = parseInt(data.abilities.dex.defensive);

        // get the best class acDexmod and apply it
        if (Object.values(this.activeClasses).length) {
            let acDexMod = 0;
            this.classes.forEach((classEntry) => {
                const newAcDex =
                    parseInt(utilitiesManager.evaluateFormulaValueSync(classEntry.system.features.acDexFormula, rollData)) || 0;
                if (!acDexMod || Math.abs(acDexMod) > Math.abs(newAcDex)) acDexMod = newAcDex;
            });

            data.armorClass.dex = acDexMod;
        }

        // use a effects formula for dex defensive.
        if (data.mods?.formula?.ac?.dex !== undefined) {
            const dexACOverride = data.mods.formula.ac.dex
                ? utilitiesManager.evaluateFormulaValueSync(data.mods.formula.ac.dex, rollData)
                : 0;
            data.armorClass.dex = dexACOverride;
        }

        /**
         * ActiveEffect
         *
         * data.mods.ac.melee.value
         * data.mods.ac.ranged.value
         * data.mods.ac.melee.base (use lowest)
         * data.mods.ac.ranged.base (use lowest)
         *
         * data.mods.ac.rear.value
         * data.mods.ac.rear.ranged
         * data.mods.ac.rear.melee
         * data.mods.ac.front.value
         * data.mods.ac.front.ranged
         * data.mods.ac.front.melee
         *
         * let armorFront = parseInt(system.mods.ac.front.value || 0);
         * let armorRear  = parseInt(data.mods.ac.rear.value || 0);
         *
         * let armorMeleeFront = parseInt(data.mods.ac.front.melee || 0);
         * let armorMeleeRear  = parseInt(data.mods.ac.rear.ranged || 0);
         *
         * let armorRangedFront = parseInt(data.mods.ac.front.melee || 0);
         * let armorRangedRear  = parseInt(data.mods.ac.rear.ranged || 0);
         *
         */

        // melee/ranged specific AC fields
        let armorRangedBase = data.armorClass.armor;
        let armorRangedMod = armorMod;

        if (data.mods?.ac?.ranged?.base) {
            const modValue = parseInt(data.mods.ac.ranged.base || bestArmor);
            if (modValue < armorRangedBase) armorRangedBase = modValue;
        }

        if (data.mods?.ac?.ranged?.value) {
            const modValue = parseInt(data.mods.ac.ranged.value || 0);
            armorRangedMod = armorRangedMod + -modValue;
        }

        //------------------
        let armorRangedBaseFront = armorRangedBase;
        let armorRangedModFront = armorRangedMod;
        if (data.mods?.ac?.ranged?.front?.base) {
            const modValue = parseInt(data.mods.ac.ranged.front.base || bestArmor);
            if (modValue < armorRangedBaseFront) armorRangedBaseFront = modValue;
        }
        if (data.mods?.ac?.ranged?.front?.value) {
            const modValue = parseInt(data.mods.ac.ranged.front.value || 0);
            armorRangedModFront = -modValue;
        }
        if (data.mods?.formula?.ac?.ranged?.front?.value) {
            const modValue = utilitiesManager.evaluateFormulaValueSync(data.mods.formula.ac.ranged.front.value, rollData);
            armorRangedModFront = armorRangedModFront + -modValue;
        }

        let armorRangedBaseRear = armorRangedBase;
        if (data.mods?.ac?.ranged?.rear?.base) {
            const modValue = parseInt(data.mods.ac.ranged.rear.base || bestArmor);
            if (modValue < armorRangedBaseRear) armorRangedBaseRear = modValue;
        }
        let armorRangedModRear = armorRangedMod;
        if (data.mods?.ac?.ranged?.rear?.value) {
            const modValue = parseInt(data.mods.ac.ranged.rear.value || 0);
            armorRangedModRear = -modValue;
        }
        if (data.mods?.formula?.ac?.ranged?.rear?.value) {
            const modValue = utilitiesManager.evaluateFormulaValueSync(data.mods.formula.ac.ranged.rear.value, rollData);
            armorRangedModRear = armorRangedModRear + -modValue;
        }

        let armorBaseRear = data.armorClass.armor;
        if (data.mods?.ac?.rear?.base) {
            const modValue = parseInt(data.mods.ac.rear.base || bestArmor);
            if (modValue < armorBaseRear) armorBaseRear = modValue;
        }

        let armorBaseFront = data.armorClass.armor;
        if (data.mods?.ac?.front?.base) {
            const modValue = parseInt(data.mods.ac.front.base || bestArmor);
            if (modValue < armorBaseFront) armorBaseFront = modValue;
        }
        let armorFrontMod = armorMod;
        if (data.mods?.ac?.front?.value) {
            const modValue = parseInt(data.mods.ac.front.value || 0);
            armorFrontMod = armorFrontMod + -modValue;
        }
        if (data.mods?.formula?.ac?.front?.value) {
            const modValue = utilitiesManager.evaluateFormulaValueSync(data.mods.formula.ac.front.value, rollData);
            armorFrontMod = armorFrontMod + -modValue;
        }
        let armorRearMod = armorMod;
        if (data.mods?.ac?.rear?.value) {
            const modValue = parseInt(data.mods.ac.rear.value || 0);
            armorRearMod = armorRearMod + -modValue;
        }
        if (data.mods?.formula?.ac?.rear?.value) {
            const modValue = utilitiesManager.evaluateFormulaValueSync(data.mods.formula.ac.rear.value, rollData);
            armorRearMod = armorRearMod + -modValue;
        }
        //----------------

        let armorMeleeBase = data.armorClass.armor;
        let armorMeleeMod = armorMod;
        if (data.mods?.ac?.melee?.base) {
            const modValue = parseInt(data.mods.ac.melee.base || bestArmor);
            if (modValue < armorMeleeBase) armorMeleeBase = modValue;
        }
        if (data.mods?.ac?.melee?.value) {
            const modValue = parseInt(data.mods.ac.melee.value || 0);
            armorMeleeMod = armorMeleeMod + -modValue;
        }

        let armorMeleeBaseFront = armorMeleeBase;
        let armorMeleeModFront = armorMeleeMod;
        if (data.mods?.ac?.melee?.front?.base) {
            const modValue = parseInt(data.mods.ac.melee.front.base || bestArmor);
            if (modValue < armorMeleeBaseFront) armorMeleeBaseFront = modValue;
        }
        if (data.mods?.ac?.melee?.front?.value) {
            const modValue = parseInt(data.mods.ac.melee.front.value || 0);
            armorMeleeModFront = armorMeleeModFront + -modValue;
        }
        if (data.mods?.formula?.ac?.melee?.front?.value) {
            const modValue = utilitiesManager.evaluateFormulaValueSync(data.mods.formula.ac.melee.front.value, rollData);
            armorMeleeModFront = armorMeleeModFront + -modValue;
        }
        // mods.formula.ac.melee.front.value

        let armorMeleeBaseRear = armorMeleeBase;
        let armorMeleeModRear = armorMod;
        if (data.mods?.ac?.melee?.rear?.base) {
            const modValue = parseInt(data.mods.ac.melee.rear.base || bestArmor);
            if (modValue < armorMeleeBaseRear) armorMeleeBaseRear = modValue;
        }
        if (data.mods?.ac?.melee?.front?.value) {
            const modValue = parseInt(data.mods.ac.melee.rear.value || 0);
            armorMeleeModRear = armorMeleeModRear + -modValue;
        }
        if (data.mods?.formula?.ac?.melee?.rear?.value) {
            const modValue = utilitiesManager.evaluateFormulaValueSync(data.mods.formula.ac.melee.rear.value, rollData);
            armorMeleeModRear = armorMeleeModRear + -modValue;
        }

        // let armorFront = data.mods?.ac?.front?.value ? parseInt(data.mods.ac.front.value || 0) : 0;
        // let armorRear = data.mods?.ac?.rear?.value ? parseInt(data.mods.ac.rear.value) : 0;

        // let armorMeleeFront = data.mods?.ac?.front?.melee ? parseInt(data.mods.ac.front.melee) : 0;
        // let armorMeleeRear = data.mods?.ac?.rear?.melee ? parseInt(data.mods.ac.rear.melee) : 0;

        // let armorRangedFront = data.mods?.ac?.front?.ranged ? parseInt(data.mods.ac.front.ranged) : 0;
        // let armorRangedRear = data.mods?.ac?.rear?.ranged ? parseInt(data.mods.ac.rear.ranged) : 0;

        // console.log("actor.js _prepareArmorClass", { armorBaseFront, armorFrontMod, armorBaseRear, armorRearMod, armorRangedFront, armorRangedRear, })

        // complete AC with everything
        data.armorClass.normal = Math.min(
            10,
            // data.armorClass.armor +
            armorBaseFront +
                armorFrontMod +
                data.armorClass.shield +
                data.armorClass.ring +
                data.armorClass.cloak +
                // data.armorClass.modEffects +
                data.armorClass.other +
                data.armorClass.dex
        );

        // armor damage - represents the total health of eqquiped armor as a % - rendered at progress bar on combat stats
        let armorDamage = Math.floor((protectionPoints / protectionPointsMax) * 100);
        if (isNaN(armorDamage)) armorDamage = null;
        data.armorClass.armorDamage = armorDamage;
        data.armorClass.protectionPoints = protectionPoints;
        data.armorClass.protectionPointsMax = protectionPointsMax;
        data.armorClass.armorDamageTooltip = armorDamageTooltip.join(' ');

        // remove shield
        data.armorClass.shieldless = Math.min(
            10,
            // data.armorClass.armor +
            armorBaseFront +
                armorFrontMod +
                data.armorClass.ring +
                data.armorClass.cloak +
                // data.armorClass.modEffects +
                data.armorClass.other +
                data.armorClass.dex
        );
        data.armorClass.noshield = data.armorClass.shieldless;

        // remove dex/shield
        data.armorClass.rear = Math.min(
            10,
            // data.armorClass.armor +
            armorBaseRear +
                armorRearMod +
                data.armorClass.ring +
                data.armorClass.cloak +
                // data.armorClass.modEffects +
                data.armorClass.other
        );

        // ignore armor base AC but keep mods
        data.armorClass.touch = Math.min(
            10,
            data.attributes.ac.value +
                data.armorClass.shield +
                data.armorClass.ring +
                data.armorClass.cloak +
                // data.armorClass.modEffects +
                data.armorClass.other +
                data.armorClass.dex -
                bestArmorMod
        );

        // ignore armor base ac but keep mods minus dex/shield from rear
        data.armorClass.touchrear = Math.min(
            10,
            data.attributes.ac.value +
                data.armorClass.ring +
                data.armorClass.cloak +
                // data.armorClass.modEffects +
                data.armorClass.other -
                bestArmorMod
        );

        // ignore dex
        data.armorClass.nodex = Math.min(
            10,
            data.armorClass.armor +
                data.armorClass.shield +
                data.armorClass.ring +
                data.armorClass.cloak +
                // data.armorClass.modEffects +
                armorFrontMod +
                data.armorClass.other
        );
        //nodex/noshield
        data.armorClass.nodexshield = Math.min(
            10,
            data.armorClass.armor + data.armorClass.ring + data.armorClass.cloak + armorFrontMod + data.armorClass.other
        );

        // ranged specific AC
        data.armorClass.ranged = Math.min(
            10,
            parseInt(
                // armorRangedBase +
                // armorRangedMod +
                armorRangedBaseFront +
                    armorRangedModFront +
                    data.armorClass.shield +
                    data.armorClass.ring +
                    data.armorClass.cloak +
                    // data.armorClass.modEffects +
                    data.armorClass.other +
                    // armorRangedFront +
                    data.armorClass.dex
            )
        );

        data.armorClass.rangedrear = Math.min(
            10,
            parseInt(
                // armorRangedBase +
                // armorRangedMod +
                armorRangedBaseRear +
                    armorRangedModRear +
                    data.armorClass.ring +
                    data.armorClass.cloak +
                    // data.armorClass.modEffects +
                    data.armorClass.other
                //+ armorRangedRear
            )
        );

        // melee specific AC
        data.armorClass.melee = parseInt(
            armorMeleeBase +
                armorMeleeModFront +
                data.armorClass.shield +
                data.armorClass.ring +
                data.armorClass.cloak +
                // data.armorClass.modEffects +
                data.armorClass.other +
                // armorMeleeFront +
                data.armorClass.dex
        );

        data.armorClass.meleerear = parseInt(
            armorMeleeBaseRear +
                armorMeleeModRear +
                // data.armorClass.shield +
                data.armorClass.ring +
                data.armorClass.cloak +
                // data.armorClass.modEffects +
                data.armorClass.other
            // +armorMeleeRear
        );
        // data.armorClass.dex;

        // here so people can reference as @armorClass.bestBase
        data.armorClass.bestBase = bestArmor;

        const adjust = game.settings.get('ars', 'useAscendingAC') ? 20 : 0;
        data.armorClass.tooltip =
            `<b>AC</b><p/>` +
            `Normal ${adjust ? adjust - data.armorClass.normal : data.armorClass.normal}<p/>` +
            `Rear ${adjust ? adjust - data.armorClass.rear : data.armorClass.rear}<p/>` +
            `Shieldless ${adjust ? adjust - data.armorClass.shieldless : data.armorClass.shieldless}<p/>` +
            `No Dexterity ${adjust ? adjust - data.armorClass.nodex : data.armorClass.nodex}<p/>` +
            `Touch ${adjust ? adjust - data.armorClass.touch : data.armorClass.touch}<p/>` +
            `Melee ${adjust ? adjust - data.armorClass.melee : data.armorClass.melee}<p/>` +
            `Melee Rear ${adjust ? adjust - data.armorClass.meleerear : data.armorClass.meleerear}<p/>` +
            `Ranged ${adjust ? adjust - data.armorClass.ranged : data.armorClass.ranged}<p/>` +
            `Ranged Rear ${adjust ? adjust - data.armorClass.rangedrear : data.armorClass.rangedrear}` +
            `<hr/>` +
            `Armor Base ${adjust ? adjust - bestArmor : bestArmor}<p/>` +
            `Armor Modifier ${adjust ? adjust - bestArmorMod : bestArmorMod}<p/>` +
            `Dexterity Modifier ${adjust ? adjust - data.armorClass.dex : data.armorClass.dex}<p/>` +
            `Shield ${adjust ? adjust - data.armorClass.shield : data.armorClass.shield}<p/>` +
            `Cloak ${adjust ? adjust - data.armorClass.cloak : data.armorClass.cloak}<p/>` +
            `Ring ${adjust ? adjust - data.armorClass.ring : data.armorClass.ring}` +
            ``;
    }

    /**
     * Organize and classify Items for Character sheets.
     *
     * @param {Object} data The actor to prepare.
     *
     * @return {undefined}
     */
    _prepareCharacterItems(data) {
        // console.log("actor.js _prepareCharacterItems data-->", data);

        data.weapons = this.items
            .filter(function (item) {
                return item.type === 'weapon';
            })
            .sort(utilitiesManager.sortByRecordName);

        data.potions = this.items.filter(function (item) {
            return item.type === 'potion';
        });

        data.proficiencies = this.items
            .filter(function (item) {
                return item.type === 'proficiency';
            })
            .sort(utilitiesManager.sortByRecordName);

        data.abilities = this.items
            .filter(function (item) {
                return item.type === 'ability';
            })
            .sort(utilitiesManager.sortByRecordName);

        data.skills = this.items
            .filter(function (item) {
                return item.type === 'skill';
            })
            .sort(utilitiesManager.sortByRecordName);

        // spells but not type scroll
        data.spells = this.items
            .filter(function (item) {
                return item.type === 'spell' && item.system?.attributes?.type.toLowerCase() !== 'scroll';
            })
            .sort(utilitiesManager.sortByRecordName);

        // #psionics - extract/set power items
        data.powers = this.items.filter(function (item) {
            return item.type === 'power';
        });

        // spells but type set to scroll
        data.scrolls = this.items.filter(function (item) {
            return item.type === 'spell' && item.system?.attributes?.type.toLowerCase() === 'scroll';
        });

        data.armors = this.items.filter(function (item) {
            return game.ars.config.itemProtectionTypes.includes(item.type);
        });

        data.containers = this.items.filter(function (item) {
            return item.type === 'container';
        });

        data.backgrounds = this.items.filter(function (item) {
            return item.type === 'background';
        });

        data.classes = this.items.filter(function (item) {
            return item.type === 'class';
        });

        data.abilityList = this.items.filter(function (item) {
            return item.type === 'ability';
        });

        data.activeClasses = this.items.filter(function (item) {
            return item.type === 'class' && item.system.active;
        });
        data.deactiveClasses = this.items.filter(function (item) {
            return item.type === 'class' && !item.system.active;
        });

        // data.gear = this.items.filter(function (item) { return item.type === 'item' });
        data.gear = this.items.filter(function (item) {
            return game.ars.config.itemGearTypes.includes(item.type);
        });

        // find a race item and set it to the details.race
        data.races = this.items.filter(function (item) {
            return item.type === 'race';
        });
        if (!data.details) data.details = {};
        data.details.race = data.races[0];

        data.inventory = this.items.filter(function (item) {
            return !game.ars.config.nonInventoryTypes.includes(item.type);
        });

        data.inventory.sort(utilitiesManager.sortByRecordName);

        data.actionInventory = this.items.filter(function (item) {
            if (!item.isIdentified) return false;
            if (item.type === 'spell' && item.system?.attributes?.type.toLowerCase() === 'scroll') return true;
            if (item.type !== 'spell' && item.system?.actions?.length) return true;
        });
        data.actionInventory.sort(utilitiesManager.sortByRecordName);

        data.equipped = this.items.filter(function (item) {
            return item.system?.location?.state === 'equipped';
        });

        data.carried = this.items.filter(function (item) {
            return item.system?.location?.state === 'carried' || item.system?.location?.state === 'equipped';
        });

        // carried items with quantity
        data.inventoryCarried = data.carried.filter(function (item) {
            return !game.ars.config.nonInventoryTypes.includes(item.type) && item.system?.quantity;
        });

        data.nocarried = this.items.filter(function (item) {
            return item.system?.location?.state !== 'equipped' && item.system?.location?.state !== 'carried';
        });

        data.provisions = data.carried.filter(function (item) {
            return (
                item.system?.attributes?.type.toLowerCase() === 'provisions' ||
                item.system?.attributes?.subtype.toLowerCase() === 'provisions'
            );
        });

        /** weapons carried|equipped */
        data.weaponsAvailable = data.weapons
            .filter(function (item) {
                return ['carried', 'equipped'].includes(item.system?.location?.state);
            })
            .sort(utilitiesManager.sortByRecordName);

        /** ammo in inventory */
        data.ammo = data.carried.filter(function (item) {
            return (
                item.system?.attributes?.type.toLowerCase() === 'ammunition' ||
                item.system?.attributes?.subtype.toLowerCase() === 'ammunition'
            );
        });

        // // calculate weight carried/encumbrance
        // const weightItems = this.items.filter((item) => {
        //     return ['equipped', 'carried'].includes(item.system?.location?.state);
        // });

        // let carriedweight = 0;
        // weightItems.forEach((item) => {
        //     const count = parseFloat(item.system?.quantity) || 0;
        //     const weight = parseFloat(item.system?.weight) || 0;
        //     carriedweight += count * weight;
        //     if (item.containedIn && item.system.capacity?.weightreduction) {
        //         const weightreduction = item.system.capacity.weightreduction;
        //         carriedweight = (carriedweight * weightreduction) / 100;
        //     }
        // });

        // dont need this, they can just uncarry their coins
        // //add coin weight carried
        // if (game.ars.config.settings.encumbranceIncludeCoin) {
        //     const coinWeight = 1 / ARS.currencyWeight[game.ars.config.settings.systemVariant];
        //     let coinCount = 0;
        //     for (const coin in this.system.currency) {
        //         if (!isNaN(this.system.currency[coin])) coinCount += parseInt(this.system.currency[coin]) || 0;
        //     }
        //     if (coinCount) {
        //         const coinTotalWeight = coinCount * coinWeight;
        //         carriedweight += coinTotalWeight;
        //     }
        // }

        // osric/1e have a 150 base + str allow
        // 2e uses value listed
        let maxWeight = parseInt(this.system.abilities.str.allow);
        const currentStr = parseInt(this.system.abilities.str.value);
        switch (game.ars.config.settings.systemVariant) {
            case '0':
            case '1':
                maxWeight += 150;
                break;

            default:
                break;
        }

        this.maxWeight = maxWeight;
        // this.carriedweight = Number(carriedweight).toFixed(2);

        this.dailyprovisions = {
            food: this.system.details?.provisions?.food
                ? this.getEmbeddedDocument('Item', this.system.details.provisions.food)
                : undefined,
            water: this.system.details?.provisions?.water
                ? this.getEmbeddedDocument('Item', this.system.details.provisions.water)
                : undefined,
        };
    }

    /**
     *
     * find all currency carried and store value
     *
     * @param {*} context
     */
    _calculateAvailableCurrency(context) {
        context.currency = {
            cp: 0,
            sp: 0,
            ep: 0,
            gp: 0,
            pp: 0,
        };

        for (const coin of context.carriedCurrencyItems) {
            const currencyType = coin.system.cost.currency.toLowerCase();
            context.currency[currencyType] += parseInt(coin.system.quantity) || 0;
        }
    }
    /**
     * This populates the ability fields for the sheet display and
     * it also populates values that can be used for modifiers such as
     * @data.abilities.str.dmg
     *
     * @param {*} data
     */
    _buildAbilityFields(data) {
        const systemVariant = parseInt(ARS.settings.systemVariant);

        // console.log("_buildAbilityFields", { data })

        function sanitizeValue(value, key) {
            let sanitizedValue = parseInt(value) || 1;
            if (!Number.isInteger(sanitizedValue)) {
                console.log(`${this.name} ${key}:${sanitizedValue} is bogus`);
                sanitizedValue = 1;
            }

            let min = 1,
                max = 25;

            // some abilities, like comeliness, have different ranges - support that here
            if (key === 'com') {
                (min = -16), (max = 30);
            }

            return Math.min(Math.max(sanitizedValue, min), max);
        }

        function getStrengthValue(abl) {
            if (abl.value !== 18 || abl.percent <= 0) return abl.value;
            if (abl.percent >= 100) return 100;
            if (abl.percent >= 91) return 99;
            if (abl.percent >= 76) return 90;
            if (abl.percent >= 51) return 75;
            return 50;
        }

        /**
         * Builds fields onto abilities for display
         * @param {String} key str/dex/con/etc
         * @param {Array} table ARS.strengthTable[systemVariant]
         * @param {Number} value Ability score 1-25
         * @param {Array} ignoreIndices Index entries in the table to ignore
         * @param {Number} endIndex The last entry to include in the fields
         * @returns
         */
        function buildFields(key, table, value, ignoreIndices = [], endIndex = table[0].length) {
            // support non-conventional tables - comeliness/honor have ranges outside the normal bounds
            let mainFieldIndex = 0;
            if (key === 'com') mainFieldIndex = '-17';

            return table[mainFieldIndex]
                .slice(0, endIndex)
                .map((label, index) => ({
                    value: table[value][index],
                    label,
                }))
                .filter((_, index) => !ignoreIndices.includes(index))
                .reduce((acc, field, index) => {
                    const propName = table[mainFieldIndex][index].split('.').pop();
                    if (!acc.fields) acc.fields = {};
                    // set fields
                    acc.fields[propName] = field;
                    // set values
                    acc[propName] = field.value;
                    // swap out the value with a readable version for sheet
                    // then use [1d6,2,1d0,0] for table value?
                    // die roll for normal doors, chance to open (1-2), die roll for held doors, chance to open (0-0)
                    // this allows
                    // @abilities.str.open.0 to be dice to roll
                    // @abilities.str.open.1 target for normal open
                    // @abilities.str.open.2 to be dice to roll for held doors
                    // @abilities.str.open.3 target for held open
                    // Skill item Formula input should get parsed so @abilities.str.open.0 should work there.
                    //
                    if ((systemVariant == 0 || systemVariant == 1) && key === 'str' && index == 4) {
                        let newValue = `${field.value[0]}(1-${field.value[1]})`;
                        if (parseInt(field.value[3])) {
                            newValue += `/${field.value[2]}(1-${field.value[3]})`;
                        }
                        acc.fields[propName].value = newValue;
                    }

                    return acc;
                }, {});
        }

        for (let [key, abl] of Object.entries(data.abilities)) {
            abl.value = sanitizeValue(abl.value, key);

            switch (key) {
                case 'str': {
                    const strValue = getStrengthValue(abl);
                    abl.fields = buildFields(key, ARS.strengthTable[systemVariant], strValue, [], 6);
                    Object.assign(abl, abl.fields);
                    break;
                }
                case 'dex': {
                    abl.fields = buildFields(key, ARS.dexterityTable[systemVariant], abl.value, [], 3);
                    Object.assign(abl, abl.fields);
                    break;
                }
                case 'con': {
                    abl.fields = buildFields(key, ARS.constitutionTable[systemVariant], abl.value, [], 5);
                    Object.assign(abl, abl.fields);
                    break;
                }
                case 'int': {
                    abl.fields = buildFields(key, ARS.intelligenceTable[systemVariant], abl.value, [], 5);
                    Object.assign(abl, abl.fields);
                    break;
                }
                case 'wis': {
                    abl.fields = buildFields(key, ARS.wisdomTable[systemVariant], abl.value, [], 4);
                    Object.assign(abl, abl.fields);
                    if (abl.value >= 17) {
                        // these are to long for char sheet display so add to tooltip
                        abl.fields['bonus'].tip = ARS.wisdomTable[systemVariant][parseInt(abl.value) + 100][1];
                        abl.fields['imm'].tip = ARS.wisdomTable[systemVariant][parseInt(abl.value) + 100][3];
                    }
                    break;
                }
                case 'cha': {
                    abl.fields = buildFields(key, ARS.charismaTable[systemVariant], abl.value, [], 3);
                    Object.assign(abl, abl.fields);
                    break;
                }
                case 'com': {
                    abl.fields = buildFields(key, ARS.comelinessTable[systemVariant], abl.value, [], 3);
                    Object.assign(abl, abl.fields);
                    break;
                }
                case 'hnr': {
                    abl.fields = buildFields(key, ARS.honorTable[systemVariant], abl.value, [], 3);
                    Object.assign(abl, abl.fields);
                    break;
                }
                default: {
                    console.log(`Unknown ability type ${key}`);
                    ui.notifications.error(`Unknown ability type ${key}`);
                    break;
                }
            }
        }
    }

    /**
     * Get the appropriate token for the current instance.
     * @returns {Object} The token object.
     */
    getToken() {
        let token;

        // Check if there is no active link to an actor.
        // If true, use the default token from the current instance.
        if (!this.prototypeToken.actorLink) {
            token = this.token;
        } else {
            // Otherwise, try to find the first active token among all tokens.
            // token = canvas.tokens.placeables.find((t) => t.actor?.id === this.id);
            // if (!token) {
            const activeTokens = this.getActiveTokens(true, true);
            token = activeTokens.length > 0 ? activeTokens[0] : this.sheet?.token;
            // }
        }

        return token;
    }

    /**
     * Get the token ID.
     *
     * This function returns the ID of the token from either the instance property (this.token)
     * or by calling the method this.getToken().
     *
     * @return {number} The token ID.
     */
    getTokenId() {
        // If the token property exists in the current instance, we'll use it
        if (this.token) {
            // Using optional chaining and nullish coalescing to safely access the token ID
            // and return 'undefined' if the token or its ID is not available
            return this.token.id ?? undefined;
        } else {
            // If the token property doesn't exist, we'll call the getToken method to obtain the token object
            const token = this.getToken();

            // Using optional chaining and nullish coalescing to safely access the token ID
            // and return 'undefined' if the token or its ID is not available
            return token?.id ?? undefined;
        }
    }

    /**
     *
     * _chatRoll: create chatCard for action
     *
     * @param {*} data
     */
    // async _chatRoll(data = {}, actionSource = this) {
    //     const actionKeyTag = `${this.id}_${data.actionGroup}`;
    //     let chatData = {
    //         speaker: ChatMessage.getSpeaker({ actor: this }),
    //         author: game.user.id,
    //         flags: { 'ars.chatCard.sourceId': actionKeyTag },
    //     };

    //     const token = this.getToken();

    //     // console.log("actor.js _chatRoll this ", this);
    //     // console.log("actor.js _chatRoll token===================", { token });
    //     console.log('actor.js _chatRoll', { data, actionSource });

    //     const actions = data.actionGroup
    //         ? actionSource.system.actionList[data.actionGroup].actions
    //         : actionSource.system.actionList;
    //     const rAG = actionSource.system.actionList[data.actionGroup];

    //     let cardData = {
    //         actionGroupData: rAG,
    //         actionGroups: actionSource.system.actionList,
    //         ...data,
    //         actions: actions,
    //         config: ARS,
    //         ...this,
    //         item: data.item,
    //         owner: this.id,
    //         sourceActor: this,
    //         sourceToken: token,
    //     };

    //     const popoutCard = new ARSCardPopout(cardData).render(true);

    //     // check the last message in chat to see if it matches this item, dont reprint if so.
    //     let lastMsg = game.messages.contents.length - 1;
    //     let lastEntry = game.messages.contents[lastMsg];
    //     const lastMsgSourceId = lastEntry?.flags?.ars?.chatCard?.sourceId;
    //     if (lastMsgSourceId === actionKeyTag) {
    //         console.warn(`${data.actionGroup} is already in the chat output as last message.`);
    //         return null;
    //     }

    //     chatData.content = await renderTemplate(this.chatTemplate[data.type], cardData);
    //     return ChatMessage.create(chatData);
    // }

    async _chatRoll(context = {}, actionSource = this) {
        const actionKeyTag = `${this.id}_${context.actionGroup.name}`;
        let chatData = {
            speaker: ChatMessage.getSpeaker({ actor: this }),
            author: game.user.id,
            flags: { 'ars.chatCard.sourceId': actionKeyTag },
        };

        const token = this.getToken();

        console.log('actor.js _chatRoll', { context, actionSource });

        let cardData = {
            actionV2: true,
            ...context,
            config: ARS,
            // ...this,
            owner: this.id,
            sourceActor: this,
            sourceToken: token,
        };

        const popoutCard = new ARSCardPopout(cardData).render(true);

        // check the last message in chat to see if it matches this item, dont reprint if so.
        let lastMsg = game.messages.contents.length - 1;
        let lastEntry = game.messages.contents[lastMsg];
        const lastMsgSourceId = lastEntry?.flags?.ars?.chatCard?.sourceId;
        if (lastMsgSourceId === actionKeyTag) {
            console.warn(`${context.actionGroup.name} is already in the chat output as last message.`);
            return null;
        }

        chatData.content = await renderTemplate(this.chatTemplate[context.type], cardData);
        return ChatMessage.create(chatData);
    }
    /**
     * Returns the level of the given class record.
     *
     * @param {Object} classEntry - The class entry object.
     * @returns {number} - The level of the class record.
     */
    getClassLevel(classEntry) {
        // Initialize level variable to store the class level.
        let level = 0;

        // Check if 'system' and 'advancement' properties exist in classEntry, and retrieve values if so.
        const advancement = classEntry.system?.advancement ? Object.values(classEntry.system.advancement) : undefined;

        // If advancement array exists and is not empty, set level to its length.
        if (advancement && advancement.length) {
            level = advancement.length;
        }

        // Return the calculated level.
        return level;
    }

    // Get the maximum level from all classes
    getMaxLevel() {
        return this._getLevel('max');
    }

    // Get the minimum level from all classes
    getMinLevel() {
        return this._getLevel('min');
    }

    // Get the maximum level from active classes
    getMaxActiveLevel() {
        return this._getLevel('max', true);
    }

    // get the max level of the inactive
    getMaxInactive() {
        return this._getLevel('maxinactive', false);
    }
    // Consolidated function to reduce code size and improve maintainability
    _getLevel(type, activeOnly = false) {
        // Initialize level according to the required type (min or max)
        let level = type === 'max' ? 1 : Number.MAX_SAFE_INTEGER;
        if (type === 'maxinactive') level = 0;

        // Iterate over each class entry
        this.classes.forEach((classEntry) => {
            // Get class source from item ID
            // const classSource = this.items.get(classEntry.id);

            // Check if we should consider only active classes
            if (activeOnly && !classEntry.system.active) return;

            // Get the advancement bundle length
            const advBundleLength = Object.values(classEntry.system.advancement).length;

            // Update the level based on the type and the advancement bundle length
            if (type === 'max' && advBundleLength > level) {
                level = advBundleLength;
            } else if (type === 'min' && advBundleLength < level) {
                level = advBundleLength;
            } else if (type === 'maxinactive' && advBundleLength > level && !classEntry.system.active) {
                level = advBundleLength;
            }
        });

        // Return the calculated level
        return level;
    }

    /**
     *
     * Return the max active classes for this character
     *
     * @returns
     */
    getActiveClassCount() {
        let classCount = 0;
        this.classes.forEach((classEntry) => {
            if (classEntry.system.active) classCount++;
        });

        return classCount;
    }

    /**
     *
     * @returns array of active classes
     */
    getActiveClasses() {
        let activeClasses = [];
        this.classes.forEach((classEntry) => {
            if (classEntry.system.active) activeClasses.push(classEntry);
        });

        return activeClasses;
    }

    /**
     *
     * @returns inactive classes
     */
    getInActiveClasses() {
        let inactiveClasses = [];
        this.classes.forEach((classEntry) => {
            if (!classEntry.system.active) inactiveClasses.push(classEntry);
        });

        return inactiveClasses;
    }

    /**
     *
     * Usable classes are all active (if more than 1) or
     * classes the active class has surpassed.
     *
     * @returns array of useable classes
     */
    getUsableClasses() {
        const activeCount = this.getActiveClassCount();
        const activeClasses = this.getActiveClasses();
        // if multi-class return activeClasses
        if (activeCount > 1) return activeClasses;
        if (activeCount <= 0) return [];

        //set activeClass and level values
        const activeClass = activeClasses[0];
        const activeLevel = Object.values(activeClass.system.advancement).length;

        //build usableClasses list
        let usableClasses = [activeClass];
        this.getInActiveClasses().forEach((classEntry) => {
            const level = Object.values(classEntry.system.advancement).length;
            if (activeLevel > level) usableClasses.push(classEntry);
        });

        return usableClasses;
    }
    /**
     * This function creates a token prototype for a given actor data object.
     *
     * @param {object} data - Actor data object
     */
    async _createTokenPrototype(data) {
        console.log('actor.js _createTokenPrototype', { data }, foundry.utils.duplicate(data));
        let createData = {};

        // Create prototype token for characters
        if (data.type === 'character') {
            console.log('actor.js _createTokenPrototype createData:PC');

            // Set default values for character tokens
            foundry.utils.mergeObject(createData, {
                'prototypeToken.displayName': CONST.TOKEN_DISPLAY_MODES.ALWAYS, // Always display name
                'prototypeToken.displayBars': CONST.TOKEN_DISPLAY_MODES.ALWAYS, // Always display bars
                'prototypeToken.disposition': CONST.TOKEN_DISPOSITIONS.FRIENDLY, // Set disposition as friendly
                'prototypeToken.name': data.name, // Set token name to actor name
                'prototypeToken.vision': true, // Enable vision
                'prototypeToken.actorLink': true, // Link token to actor
                'prototypeToken.bar1.attribute': 'attributes.hp',
            });

            // Create prototype token for NPCs
        } else if (data.type === 'npc') {
            console.log('actor.js _createTokenPrototype createData:NPC');

            let visionRange = 0;
            let sizeSetting = 1;

            // Set vision range and size settings for NPCs with system data
            if (data.system) {
                const visionCheck = data.system?.specialDefenses + data.system?.specialAttacks || '';
                const match = visionCheck?.match(/vision (\d+)/i);
                if (match && match[1]) visionRange = parseInt(match[1]);

                switch (data.system.attributes.size) {
                    case 'tiny':
                        sizeSetting = 0.25;
                        break;
                    case 'small':
                        sizeSetting = 0.5;
                        break;
                    case 'medium':
                        sizeSetting = 1;
                        break;
                    case 'large':
                        sizeSetting = 1;
                        break;
                    case 'huge':
                        sizeSetting = 2;
                        break;
                    case 'gargantuan':
                        sizeSetting = 3;
                        break;
                }
            }

            // Set default values for NPC tokens
            foundry.utils.mergeObject(
                createData,
                {
                    'prototypeToken.vision': visionRange ? true : false,
                    'prototypeToken.brightSight': visionRange,
                    'prototypeToken.dimSight': visionRange,
                    'prototypeToken.width': sizeSetting,
                    'prototypeToken.height': sizeSetting,
                    'prototypeToken.displayName': CONST.TOKEN_DISPLAY_MODES.HOVER, // Display name on hover
                    'prototypeToken.displayBars': CONST.TOKEN_DISPLAY_MODES.OWNER, // Display bars for owner
                    'prototypeToken.disposition': CONST.TOKEN_DISPOSITIONS.HOSTILE, // Set disposition as hostile
                    'prototypeToken.name': data.name, // Set token name to actor name
                    'prototypeToken.bar1.attribute': 'attributes.hp',
                },
                { insertValues: true }
            );

            // Create prototype token for lootable objects
        } else if (data.type === 'lootable') {
            // Set default values for lootable tokens
            foundry.utils.mergeObject(
                createData,
                {
                    'prototypeToken.vision': false,
                    'prototypeToken.brightSight': 0, // No vision range without light
                    'prototypeToken.dimSight': 0,
                    'prototypeToken.displayName': CONST.TOKEN_DISPLAY_MODES.OWNER, // Display name for owner
                    'prototypeToken.displayBars': CONST.TOKEN_DISPLAY_MODES.NONE, // Do not display bars
                    'prototypeToken.disposition': CONST.TOKEN_DISPOSITIONS.HOSTILE, // Set disposition as hostile
                    'prototypeToken.name': data.name, // Set token name to actor name
                    // 'prototypeToken.bar1.-=attribute': null,
                    'prototypeToken.bar1.attribute': '',
                },
                { insertValues: true }
            );

            // Handle unknown actor types
        } else {
            // No specific token prototype settings for unknown actor types
        }

        console.log('actor.js _createTokenPrototype', { createData });
        await this.updateSource(createData);
    }

    /**
     *
     * @override We override to remove sub-items also
     *
     * @param {String} embeddedName The name of the embedded Document type
     * @param {Array.<string>} ids An array of string ids for each Document to be deleted
     * @param {{}} contextopt  Additional context which customizes the deletion workflow
     * @returns
     */
    async deleteEmbeddedDocuments(embeddedName, ids, contextopt) {
        console.log('actor.js EmbeddedDocuments delete', {
            embeddedName,
            ids,
            contextopt,
        });
        // console.trace();
        if (embeddedName === 'Item') {
            for (const itemId of ids) {
                const source = this.items.get(itemId);
                if (source && source.system?.itemList && source.system.itemList.length) {
                    let itemIdList = [];
                    for (const entry of source.system.itemList) {
                        if (!entry.id && entry.uuid) entry.id = entry.uuid.split('.').pop();
                        const deleteItem = this.getEmbeddedDocument('Item', entry.id);
                        if (deleteItem) itemIdList.push(entry.id);
                    }
                    if (itemIdList.length) {
                        if (!contextopt?.hideChanges && !this.isLooting)
                            await dialogManager.showItems(this, itemIdList, `Contained Items Deleted`, `Items Removed`);
                        await this.deleteEmbeddedDocuments('Item', itemIdList, contextopt);
                    }
                }
            }
        }
        // return super.deleteEmbeddedDocuments(embeddedName, ids, contextopt);
        const result = await super.deleteEmbeddedDocuments(embeddedName, ids, contextopt);
        // we do this so when a class is deleted or items that adjust hp in some way we update them
        // const hpData = this._getClassHPData();
        // if (hpData) await this.update(hpData);
        if (this.token && this.getFlag('ars', 'droppedItem') && this.items.size == 0) {
            // all dropped items retried, delete dropped item token
            this.sheet.close();
            //TODO: this probably needs to run as GM...
            this.token.delete();
        }
        return result;
    }

    /**
     *
     * Override to create itemList sub-items
     *
     * @param {String} embeddedName The name of the embedded Document type
     * @param {Array.<object>} data An array of data objects used to create multiple documents
     * @param {DocumentModificationContext} contextopt Additional context which customizes the creation workflow
     */
    async createEmbeddedDocuments(embeddedName, data, contextopt) {
        // console.trace('actor.js createEmbeddedDocuments create', { embeddedName, data, contextopt });

        // check for existing same name items and ask for stack?
        for (const newItem of data) {
            const itemsFound = contextopt?.skipCombine ? undefined : utilitiesManager.findSimilarItem(this, newItem);

            // attemp to combine similar items - bit
            const combineStackItems = game.ars.config.settings.combineStack;

            if (itemsFound?.length && combineStackItems) {
                for (let i = 0; i < itemsFound.length; i++) {
                    const itemFound = itemsFound[i];
                    // console.log("actor.js createEmbeddedDocuments create", { itemFound });
                    let combine = false;
                    if (
                        this.type == 'merchant' &&
                        itemFound.system.cost.value == newItem.system.cost.value &&
                        itemFound.system.cost.currency == newItem.system.cost.currency
                    ) {
                        combine = true;
                    } else {
                        combine = await dialogManager.confirm(
                            `Combine ${newItem.name} and ${itemFound.name}?`,
                            `Found Similar Item ${itemFound.type.toUpperCase()}:${itemFound.name}`
                        );
                    }
                    if (combine) {
                        const a = parseInt(newItem.system.quantity);
                        const b = parseInt(itemFound.system.quantity);
                        const newQuantity = a + b;
                        // this is a way to avoid double checking encumbrance, we check
                        // at end of creation.
                        itemFound.skipEncumbranceCheck = true;
                        await itemFound.update({
                            'system.quantity': newQuantity,
                        });
                        const index = data.findIndex((item) => item.name === newItem.name);
                        if (index !== -1) {
                            data.splice(index, 1);
                        }
                        break;
                    }
                }
            }
        }
        // if we didnt splice (dups) everything out then we continue
        if (data.length) {
            // console.trace();
            if (embeddedName === 'Item') {
                // console.log("actor.js createEmbeddedDocuments checking for sub-items");
                for (const source of data) {
                    // console.log("actor.js createEmbeddedDocuments source", { source });
                    // check for sub-items and add them
                    if (source && source.system?.itemList && source.system.itemList.length) {
                        const createSubItems = [];
                        const addedSubItemIds = [];
                        if (!ARS.levelBasedSubitemTypes.includes(source.type))
                            for (const subItem of source.system.itemList) {
                                const subItemId = subItem.uuid.split('.').pop();
                                // console.log('actor.js createEmbeddedDocuments sub-item', { subItem, subItemId });
                                // we manage 'class' subitems as characters level up
                                // so if it's a class item we skip it and if the id is same as us we skip it
                                if (subItemId !== source._id) {
                                    // let ne0wSubItem = undefined;
                                    // let newSubItem = game.items.get(subItem.id);
                                    let newSubItem = subItem.uuid
                                        ? await fromUuid(subItem.uuid)
                                        : await utilitiesManager.getItem(subItem.id);
                                    // newSubItem = await fromUuid(subItem.uuid);
                                    // if we checked uuid and it doesnt exist, lets try by id because getItem() searches packs for same ID
                                    if (!newSubItem && subItem.uuid) newSubItem = await utilitiesManager.getItem(subItemId);
                                    // console.log("actor.js createEmbeddedDocuments newSubItem 2", { newSubItem });
                                    if (newSubItem) {
                                        const newItemData = foundry.utils.deepClone(newSubItem.toObject());
                                        // newItemData.id = subItemId;
                                        // console.log("actor.js createEmbeddedDocuments", { newItemData });
                                        if (newItemData) {
                                            newItemData.system.sourceUuid = subItem.uuid;
                                            newItemData.system.quantity = subItem.quantity;
                                            createSubItems.push(newItemData);
                                        }
                                    } else {
                                        console.warn(
                                            `Unable to find subitem ${subItem.name} (id ${subItemId} uuid ${subItem.uuid}) in ${source.name}`,
                                            {
                                                subItem,
                                                newSubItem,
                                            }
                                        );
                                    }
                                }
                            }

                        if (createSubItems.length) {
                            const newList = await this.createEmbeddedDocuments('Item', createSubItems, contextopt);
                            // rebuild itemList
                            if (newList?.length) {
                                source.system.itemList = [];
                                for (const itm of newList) {
                                    addedSubItemIds.push(itm.id);
                                    source.system.itemList.push(utilitiesManager.makeItemListRecord(itm));
                                }
                                if (!contextopt?.hideChanges && !this.isLooting)
                                    await dialogManager.showItems(this, addedSubItemIds, `Included Items`, `Items Added`);
                            }
                            // console.log("actor.js createEmbeddedDocuments newList", { newList });
                        }
                    }
                    // save the original origin/source item id to the item
                    if (source.system) {
                        source.system.sourceId = source._id;
                    }
                }
            }

            let items;
            try {
                items = await super.createEmbeddedDocuments(embeddedName, data, contextopt);
                for (let i = 0; i < items.length; i++) {
                    const item = items[i];
                    switch (item.type) {
                        case 'race':
                        case 'background':
                            if (this.sheet) {
                                this.sheet._reconfigureAcademics(item, this.getMaxLevel());
                            }
                            break;

                        case 'weapon':
                            const foundProfs = utilitiesManager.findSimilarItem(this, item, ['weapon'], 'proficiency');
                            if (foundProfs?.length) {
                                for (let i = 0; i < foundProfs.length; i++) {
                                    const foundProf = foundProfs[i];
                                    const include = await dialogManager.confirm(
                                        `Add ${item.name} to proficiency ${foundProf.name}?`,
                                        `Include in proficiency?`
                                    );
                                    if (include) {
                                        let appliedBundle = foundry.utils.deepClone(
                                            Object.values(foundry.utils.getProperty(foundProf, 'system.appliedto')) || []
                                        );
                                        // appliedBundle = Object.values(appliedBundle);
                                        appliedBundle.push({ id: item.id });
                                        await foundProf.update({
                                            'system.appliedto': appliedBundle,
                                        });
                                        break;
                                    }
                                }
                            }
                            break;

                        // this should set the item use of quantity to the item itself
                        // if its resource itemId currently not set.
                        case 'spell':
                            await item.update({ 'system.learned': false });
                            if (item.system?.attributes?.type.toLowerCase() !== 'scroll') break;
                        case 'potion':
                        case 'item':
                            if (item.system.actionGroups.length) {
                                let updateItemTarget = false;
                                for (const actionGroup of item.actionGroups.values()) {
                                    for (const action of actionGroup.actions.values()) {
                                        if (action?.resource?.type === 'item') {
                                            updateItemTarget = true;
                                            action.resource.itemId = item.id;
                                        }
                                    }
                                }

                                if (updateItemTarget) await ARSActionGroup.saveAll(item);
                            }
                            break;
                        default:
                            break;
                    }
                }
            } catch (error) {
                ui.notifications.warn(`actor.js createEmbeddedDocuments: ${error}.`);
                // console.log("actor.js createEmbeddedDocuments", error);
            }

            return items;
        }
        return undefined;
    }

    /**
     * Does this actor have spell slots
     *
     * @returns boolean
     */
    canCastSpells() {
        let hasSlots = false;
        for (const spellType in this.system.spellInfo.slots) {
            Object.values(this.system.spellInfo.slots[spellType].value).forEach((slotCount, index) => {
                if (slotCount > 0) {
                    hasSlots = true;
                }
            });
        }
        return hasSlots;
    }

    /**
     * getter to retrieve complete inventory list for Equipment tab
     * This sorts it by alpha and containers.
     */
    get inventoryItems() {
        // console.log("actor.js inventoryItems", this)
        const sortedItems = foundry.utils.deepClone(Array.from(this.items));
        // console.log("actor.js inventoryItems", { sortedItems })
        sortedItems.sort(utilitiesManager.sortBySort);
        const inventory = [];

        for (const item of sortedItems) {
            // console.log("actor.js inventoryItems", item.name, item.inContainer)
            if (!game.ars.config.nonInventoryTypes.includes(item.type) && !item.inContainer) {
                inventory.push(item);
                if (item.contains) {
                    const sortSubItems = item.contains;
                    sortSubItems.sort(utilitiesManager.sortBySort);
                    for (const subItem of sortSubItems) {
                        if (!game.ars.config.nonInventoryTypes.includes(item.type)) inventory.push(subItem);
                    }
                }
            }
        }
        // console.log("actor.js inventoryItems", { inventory })
        return inventory;
    }

    get totalMagicExp() {
        const inv = this.inventoryItems;
        let totalMagicExp = 0;
        for (const item of inv) {
            totalMagicExp += item.system.xp;
        }
        return totalMagicExp;
    }

    // is this npc dead?
    get isDead() {
        if (this.type === 'character') return false;
        // console.log("actor.js isDead", this.system.attributes.hp)
        return this.system.attributes.hp.max !== 0 && this.system.attributes.hp.value === 0;
    }

    /**
     * is npc lootable (dead and looting not disabled)
     */
    get isLootable() {
        // console.log("actor.js permission", this.isDead, game.ars.config.settings.npcLootable, this.system.lootable)

        switch (this.type) {
            case 'npc':
                const npcLootable = game.ars.config.settings.npcLootable && !this.system.disablelooting;
                if (this.isToken && this.isDead && npcLootable) return true;
                break;

            case 'lootable':
                const lootable = this.system.lootable;
                // if this is loot actor and it's a token
                if (lootable && this.isToken) return true;
                break;
        }

        return false;
    }

    // give pcs limited view if the npc is lootable/merchant
    get permission() {
        if (game.user.isGM) {
            return super.permission;
        }
        if (this.isLootable || this.type == 'merchant') {
            if (['lootable', 'npc', 'merchant'].includes(this.type))
                return Math.max(super.permission, CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED);
        }
        return super.permission;
    }

    /**
     *
     * give pcs limited permissions if npc is lootable (dead)
     *
     * @param {*} user
     * @param {*} permission
     * @param {*} options
     * @returns
     */
    testUserPermission(user, permission, options) {
        if (game.user.isGM) {
            return super.testUserPermission(user, permission, options);
        } else if (this.isLootable || this.type == 'merchant') {
            if ([CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED, 'LIMITED'].includes(permission) && !options) {
                return this.permission >= CONST.DOCUMENT_OWNERSHIP_LEVELS.LIMITED;
            }
        }
        return super.testUserPermission(user, permission, options);
    }

    // re-render if sheet is showing
    reRender() {
        if (this.sheet && this.type === 'character') {
            if (this.sheet.rendered) this.sheet.render(true);
        }
    }

    /**
     * Perform a long rest for the actor
     *
     * reset memorizations for the day
     * reset all items with daily resets
     *
     */
    async longRest(consumeDaily = true) {
        // todo: psionic regen
        console.log('actor.js longRest:', this.name);
        let updates = [];
        // reset all spell memorizations
        const memBundle = foundry.utils.deepClone(this.system.spellInfo.memorization);
        for (const spellType of ['arcane', 'divine']) {
            const spellLevels = Object.keys(memBundle[spellType]);
            for (const level of spellLevels) {
                const spellSlots = Object.keys(memBundle[spellType][level]);
                for (const slot of spellSlots) {
                    memBundle[spellType][level][slot].cast = false;

                    // refresh spell info
                    const spellId = memBundle[spellType][level][slot].id;
                    let spellItem = this.getEmbeddedDocument('Item', spellId);
                    // spell isn't local so we check the global list
                    if (!spellItem) spellItem = await utilitiesManager.getItem(spellId);
                    if (spellItem) memBundle[spellType][level][slot].spellItem = spellItem.toObject();
                }
            }
        }

        //

        await this.update({
            'system.spellInfo.memorization': memBundle,
        });

        // scan invenfory of actor for items with daily resets
        // for (const item of this.items) {
        //     if (item.isIdentifiedRaw) {
        //         if (item.system?.charges?.reuse === 'daily') {
        //             item.update({ 'system.charges.value': 0 });
        //         }
        //     }
        // }

        /**
         * these _send* helpers take the notices for the recharged ability,
         * consoliates them by actor and by game.user.id owners of said actor
         * that are active to send the least amount of chat entries as it can.
         */
        const consolidatedMessagesByActor = {};
        function _sendNoticeHelper(actor, objectName, reuseTime) {
            const filteredIDs = Object.keys(actor.ownership).filter((id) => actor.ownership[id] >= 3);
            const activeOwners = game.users.filter((user) => user.active && filteredIDs.includes(user.id));
            const message = `<div>${objectName} has recharged. (${reuseTime})</div>`;

            if (!consolidatedMessagesByActor[actor.id]) {
                consolidatedMessagesByActor[actor.id] = {
                    messages: [],
                    owners: [],
                };
            }
            consolidatedMessagesByActor[actor.id].messages.push(message);
            consolidatedMessagesByActor[actor.id].owners = [
                ...new Set([...consolidatedMessagesByActor[actor.id].owners, ...activeOwners]),
            ];
        }

        function _sendConsolidatedMessagesByActor() {
            Object.keys(consolidatedMessagesByActor).forEach((actorId) => {
                const actor = game.actors.get(actorId);
                const activeOwners = consolidatedMessagesByActor[actorId].owners;
                const messagesForActor = consolidatedMessagesByActor[actorId].messages.join(' ');

                utilitiesManager.chatMessagePrivate(
                    activeOwners,
                    ChatMessage.getSpeaker({ actor: actor }),
                    `${actor.name}, Recharged`,
                    messagesForActor,
                    actor.img,
                    { message: messagesForActor }
                );
            });
        }

        // scan inventory of actor for items with daily resets actions
        // reset daily actions on actor directly
        async function _actionRechargeHelper(actionObject) {
            let updateItemActions = false;
            for (const actionGroup of actionObject.actionGroups.values()) {
                for (const action of actionGroup.actions.values()) {
                    if (
                        !['character', 'npc', 'lootable'].includes(actionObject.type) &&
                        !actionObject.isIdentifiedRaw &&
                        !ARS.nonInventoryTypes.includes(actionObject.type)
                    )
                        continue;

                    const reuseTime = action.resource.reusetime;

                    if (action.resource.type === 'charged' && action.resource.reusetime === 'daily') {
                        action.resource.count.value = 0;
                        _sendNoticeHelper(this, action.name, reuseTime);
                        updateItemActions = true;
                    } else if (action.resource.type === 'charged' && action.resource.reusetime !== 'daily') {
                        const trackTime = action.resource.trackTime;
                        if (!trackTime) continue;
                        const currentTime = game.time.worldTime;
                        const diffTime = currentTime - trackTime;
                        if (
                            (reuseTime === 'weekly' && diffTime >= CONFIG.time.weekTime) ||
                            (reuseTime === 'monthly' && diffTime >= CONFIG.time.monthTime) ||
                            diffTime < 0
                        ) {
                            _sendNoticeHelper(this, action.name, reuseTime);
                            action.resource.trackTime = 0;
                            action.resource.count.value = 0;
                            updateItemActions = true;
                        }
                    }
                }
            }
            if (updateItemActions) {
                await ARSActionGroup.saveAll(actionObject);
            }
        }

        // reset daily actions on actor directly
        // let updateActorActions = false;
        // for (const actionGroup of this.actionGroups.values()) {
        //     for (const action of actionGroup.actions.values()) {
        //         if (!action || (!action.parent && !action.parent?.isIdentifiedRaw)) continue;
        //         if (action.resource.reusetime === 'daily') {
        //             action.resource.count.value = 0;
        //             updateActorActions = true;
        //         }
        //     }
        // }
        // if (updateActorActions) await ARSActionGroup.saveAll(this);
        await _actionRechargeHelper.bind(this)(this);

        // let updateItemActions = false;
        for (const item of this.items) {
            await _actionRechargeHelper.bind(this)(item);
        }

        // for (const item of this.items) {
        //     for (const actionGroup of item.actionGroups.values()) {
        //         for (const action of actionGroup.actions.values()) {
        //             if (!item.isIdentifiedRaw && !ARS.nonInventoryTypes.includes(item.type)) continue;

        //             const reuseTime = action.resource.reusetime;

        //             if (action.resource.type === 'charged' && action.resource.reusetime === 'daily') {
        //                 action.resource.count.value = 0;
        //                 _sendNoticeHelper(this, action.name, reuseTime);
        //                 updateItemActions = true;
        //             } else if (action.resource.type === 'charged' && action.resource.reusetime !== 'daily') {
        //                 const trackTime = action.resource.trackTime;
        //                 if (!trackTime) continue;
        //                 const currentTime = game.time.worldTime;
        //                 const diffTime = currentTime - trackTime;
        //                 if (
        //                     (reuseTime === 'weekly' && diffTime >= CONFIG.time.weekTime) ||
        //                     (reuseTime === 'monthly' && diffTime >= CONFIG.time.monthTime) ||
        //                     diffTime < 0
        //                 ) {
        //                     _sendNoticeHelper(this, action.name, reuseTime);
        //                     action.resource.trackTime = 0;
        //                     action.resource.count.value = 0;
        //                     updateItemActions = true;
        //                 }
        //             }
        //         }
        //     }
        //     if (updateItemActions) {
        //         await ARSActionGroup.saveAll(item);
        //     }
        // }
        // only send messages if changes made, bulk
        // if (updateItemActions) {
        //     _sendConsolidatedMessagesByActor();
        // }

        // Only execute if game settings allow for food and water consumption and the entity type is 'character'
        // if (game.ars.config.settings.consumeFoodWater && this.type === 'character') {
        if (this.type === 'character') {
            if (consumeDaily) {
                // Prepare an array for chat messages
                const chatText = [];

                // Function to select inventory item to consume
                const provision = async (type) => {
                    // Get the item ID of the provision type from the system details, or ask user for input if not found
                    const itemId = this.system.details.provisions[type]
                        ? this.system.details.provisions[type]
                        : await dialogManager.getInventoryItem(this, `Daily ${type}`, `Select ${type} for ${this.name}`, {
                              inventory: this.provisions,
                          });

                    if (itemId) {
                        const item = this.getEmbeddedDocument('Item', itemId);

                        // Check if the item exists and has a quantity greater than 0
                        if (item && parseInt(item.system.quantity) > 0) {
                            // Update the provision id if available
                            this.update({
                                [`system.details.provisions.${type}`]: item.id,
                            });
                            return item;
                        } else {
                            // Clear the provision id if not available
                            this.update({
                                [`system.details.provisions.${type}`]: '',
                            });
                        }
                    }
                    return undefined;
                };

                // Iterate over each type of consumable (Food and Water)
                for (const type of ['Food', 'Water']) {
                    let isFound = false;
                    let item = undefined;

                    while (!isFound) {
                        item = await provision(type.toLowerCase());

                        // Check if the item has been provisioned
                        if (item) {
                            isFound = true;
                        } else {
                            // If the choice is empty, confirm if the user wants to select one
                            if (
                                await dialogManager.confirm(
                                    `${type} choice is empty, select one?`,
                                    `Select ${type} for ${this.name}`
                                )
                            ) {
                                isFound = false;
                            } else {
                                isFound = true;
                            }
                        }
                    }

                    // If the item exists, update the chat output and decrease the item quantity
                    if (item) {
                        console.log(`${this.name} consumed a use of ${item.name}`);
                        item.update({
                            'system.quantity': item.system.quantity - 1,
                        });
                        chatText.push(`${this.name} consumed a use of ${item.name} with ${item.system.quantity - 1} remaing`);
                    } else {
                        // If the item does not exist, inform the user
                        console.log(`${this.name} did not have ${type} to consume`);
                        chatText.push(`<b>${this.name} did not have ${type} to consume</b>`);
                    }
                }
                // Output the consumption log to chat
                utilitiesManager.chatMessage(
                    ChatMessage.getSpeaker({ actor: this }),
                    'Daily Food/Water',
                    chatText.join('<p/>'),
                    'icons/consumables/food/bowl-ribs-meat-rice-mash-brown-white.webp'
                );
            } else {
                utilitiesManager.chatMessage(
                    ChatMessage.getSpeaker({ actor: this }),
                    'Long Rest',
                    `${this.name} has taken a long rest.<p/>`,
                    'icons/environment/settlement/tent.webp'
                );
            }
        }
    }

    /**getter to return if the actor has any items on them that have actions. */
    get hasActionItems() {
        return this.items.some((item) => item.isIdentified && item.actionGroups && item.actionGroups.size > 0);
    }

    /**
     * getter for spellsByLevel array
     */
    get spellsByLevel() {
        const actor = this;
        // console.log("actor.js _prepareSpellsByLevel", actor.name);

        const _insertSpellByLevel = (spellsByLevel, spell, spellType) => {
            const spellLevel = spell.system.level;
            const spellId = spell.id;
            if (!spellsByLevel[spellType][spellLevel]) spellsByLevel[spellType][spellLevel] = {};

            let spellInfo = spell.name;
            let isLearned = true;
            let isOwned = spell.isOwned && spell.isOwner;

            if (spellType === 'arcane' && !spell?.system?.learned) {
                isLearned = false;
            }

            spellsByLevel[spellType][spellLevel][spellId] = {
                spellInfo,
                isLearned,
                isOwned,
            };
            // spellsByLevel[spellType][spellLevel][spellId] = spell.name;

            // Sort the entries by spell.name value
            spellsByLevel[spellType][spellLevel] = Object.fromEntries(
                Object.entries(spellsByLevel[spellType][spellLevel]).sort((a, b) => {
                    return a[1].spellInfo.localeCompare(b[1].spellInfo);
                })
            );

            return spellsByLevel;
        };

        const filterSpells = (spells, spellType) =>
            spells.filter(
                (i) =>
                    i.type === 'spell' &&
                    i.system?.type &&
                    i.system?.attributes?.type.toLowerCase() !== 'scroll' &&
                    i.system.type.toLowerCase() === spellType
            );

        const allSpells = game.collections.get('Item');
        const packItems = game.ars.library.packs?.items;

        const arcaneSpells = filterSpells(allSpells, 'arcane');
        const divineSpells = filterSpells(allSpells, 'divine');
        const arcanePackSpells = packItems ? filterSpells(packItems, 'arcane') : [];
        const divinePackSpells = packItems ? filterSpells(packItems, 'divine') : [];

        arcaneSpells.push(...arcanePackSpells);
        divineSpells.push(...divinePackSpells);
        arcaneSpells.sort(utilitiesManager.sortByRecordName);
        divineSpells.sort(utilitiesManager.sortByRecordName);

        // console.log("actor.js _prepareSpellsByLevel", { arcaneSpells, divineSpells });
        const spellsByLevel = { arcane: {}, divine: {} };

        for (const typeOfSpell of ['arcane', 'divine']) {
            actor.spells.forEach((spell) => {
                const spellType = spell.system.type.toLowerCase();
                if (spellType === typeOfSpell) {
                    const spellLevel = spell.system.level;
                    // const spellLearned = spell.system?.learned;
                    const isSpellAvailable = actor.system.spellInfo.slots[spellType].value[spellLevel];
                    // const isArcaneSpell = spellType === "arcane" && spellLearned;
                    const isArcaneSpell = spellType === 'arcane';
                    const isDivineSpell = spellType === 'divine';

                    if (isSpellAvailable && (isArcaneSpell || isDivineSpell)) {
                        _insertSpellByLevel(spellsByLevel, spell, spellType);
                    }
                }
            });
        }

        if (actor.type === 'npc') {
            //they get everything
            arcaneSpells.forEach((spell) => {
                _insertSpellByLevel(spellsByLevel, spell, 'arcane');
            });
        }

        /**
         * collect focus (major/minor) that are used to filter spell list for divine spells
         * @param {*} classes
         * @returns
         */
        function extractFocusFilters(classes) {
            const majorFilter = [];
            const minorFilter = [];

            classes.forEach((c) => {
                if (c.system.features.focus.minor) {
                    const filter = c.system.features.focus.minor.toLowerCase().replace(/\s/g, '').split(',');
                    minorFilter.push(...filter);
                }
                if (c.system.features.focus.major) {
                    const filter = c.system.features.focus.major.toLowerCase().replace(/\s/g, '').split(',');
                    majorFilter.push(...filter);
                }
            });

            const minorFocusFilters = minorFilter.join('|');
            const majorFocusFilters = majorFilter.join('|');

            return { minorFocusFilters, majorFocusFilters };
        }

        /**
         *
         * filter spells by allowed focus spheres
         *
         * @param {*} divineSpells
         * @param {*} focusFilters
         * @param {*} levelRange
         * @returns
         */
        function filterSpellsByFocus(divineSpells, focusFilters, levelRange = null) {
            return divineSpells.filter((spell) => {
                const spellLevel = parseInt(spell.system.level) || 0;
                if (levelRange && spellLevel > levelRange) {
                    return false;
                }
                return String(spell.system.sphere)?.match(new RegExp(`${focusFilters}`, 'ig'));
            });
        }

        /**
         * add spells to list
         *
         * @param {*} spellsByLevel
         * @param {*} filteredSpells
         * @param {*} type
         */
        function insertSpells(spellsByLevel, filteredSpells, type) {
            filteredSpells.forEach((spell) => {
                _insertSpellByLevel(spellsByLevel, spell, type);
            });
        }

        const { minorFocusFilters, majorFocusFilters } = extractFocusFilters(actor.classes);

        if (minorFocusFilters || majorFocusFilters) {
            const minorSpells = filterSpellsByFocus(divineSpells, minorFocusFilters, 3);
            const majorSpells = filterSpellsByFocus(divineSpells, majorFocusFilters);

            insertSpells(spellsByLevel, minorSpells, 'divine');
            insertSpells(spellsByLevel, majorSpells, 'divine');
        } else {
            insertSpells(spellsByLevel, divineSpells, 'divine');
        }

        // console.log("actor.js _prepareSpellsByLevel ", { spellsByLevel });
        return spellsByLevel;
    } // end spellsByLevel

    /**
     *This getter calculates the effective level of an actor based on hit dice (HD)
     *
     * HD 1+1 is 2HD, HD 1+4 is 3HD, HD 1+8 is HD4.
     * If any +mod, they get +1, after than % 4 for mod value.
     *
     * @returns
     */
    get effectiveLevel() {
        const _MAX_EFFECTIVE_HD = 99;
        // Return the max level if the actor is not an NPC
        if (this.type !== 'npc') {
            return this.getMaxLevel();
        }

        // Get the system variant
        const variant = ARS.settings.systemVariant;

        // Get the hit dice and modifier value
        const [hdValue, modValue] = this.getHitDice;

        let effectiveHD = -1;

        // Handle different system variants (0 for OSRIC, others for different versions)
        if (Number(variant) === 0) {
            if (hdValue > 0 && modValue > 0) {
                effectiveHD = hdValue + 2;
            } else if (hdValue === 1 && modValue === -1) {
                effectiveHD = hdValue;
            } else if (hdValue === 1 && modValue < -1) {
                effectiveHD = 0;
            } else if (hdValue > 0) {
                effectiveHD = hdValue + 1;
            } else {
                effectiveHD = hdValue;
            }
        } else {
            const bonusHD = Math.floor(modValue / 4);
            effectiveHD = hdValue + bonusHD + (modValue > 0 ? 1 : modValue < 0 ? -1 : 0);
        }

        // Ensure the effective HD is within the valid range of 0 to _MAX_EFFECTIVE_HD
        effectiveHD = Math.min(Math.max(effectiveHD, 0), _MAX_EFFECTIVE_HD);

        // Return the effective HD
        return effectiveHD;
    }

    //getter to return effective HD like 4+4 is 5/etc.
    get effectiveHD() {
        return this.effectiveLevel;
    }

    /**
     * This class method returns the hit dice and hit dice modifier values
     * for an object depending on its type.
     * @returns {Array} An array containing the hit dice and hit dice modifier values.
     */
    get getHitDice() {
        // Check if the object type is 'npc'
        if (this.type === 'npc') {
            // Retrieve the hit dice string from the system object
            const hitDiceString = String(this.system.hitdice);

            if (hitDiceString) {
                try {
                    // Extract the hit dice value and modifier from the hit dice string using a regex pattern
                    const [_stub1, hitDiceValue, _stub2, modifierType, modifierValue] =
                        hitDiceString?.match(/^(\d+)(([+\-])(\d+))?/);

                    // If there is a hit dice value and/or modifier in the hit dice string
                    if (hitDiceValue) {
                        // Parse the hit dice value as an integer
                        const parsedHitDiceValue = parseInt(hitDiceValue);

                        // Set default modifier type and value
                        const parsedModifierType = modifierType || '';
                        const parsedModifierValue = modifierValue || 0;

                        // Calculate the modifier value by combining the modifier type and value
                        const calculatedModifierValue = parseInt(`${parsedModifierType}${parsedModifierValue}`) || 0;

                        // Return the hit dice value and modifier as an array
                        return [parsedHitDiceValue, calculatedModifierValue];
                    }
                } catch (err) {
                    ui.notifications.error(`Error in actor.js getHitDice() ${err}`);
                    return [0, 0];
                }
            } else {
                // If the object type is not 'npc', return the maximum level and 0 as the modifier
                return [this.getMaxLevel(), 0];
            }
        }

        // If no hit dice value is found, return 0 for both the hit dice and modifier
        return [0, 0];
    }

    /**
     * Calculates the effective magic potency for attack/damage
     *
     * based on the type, hit dice, and system modifiers
     *
     * Table 48: Hit Dice Vs. Immunity
     * Hit Dice             Hits creatures requiring
     * 4+1 or more          +1 weapon
     * 6+2 or more          +2 weapon
     * 8+3 or more          +3 weapon
     * 10+4 or more         +4 weapon
     *
     * if system.mods.magicpotency use it if it's higher
     *
     * return nMod
     */
    magicPotencyOffense() {
        // Initialize the magic potency modifier
        let magicPotencyModifier = 0;

        // Check if the current instance is of type 'npc'
        if (this.type === 'npc') {
            // Destructure hit dice and hit dice modifier
            const [hitDice, hitDiceModifier] = this.getHitDice;
            // Get the effective hit dice value
            const effectiveHitDice = this.getEffectiveHD;

            // Determine the magic potency modifier based on effective hit dice and hit dice values
            if (effectiveHitDice > 10 || (hitDice >= 10 && hitDiceModifier >= 4)) {
                magicPotencyModifier = 4;
            } else if (effectiveHitDice > 9 || (hitDice >= 8 && hitDiceModifier >= 3)) {
                magicPotencyModifier = 3;
            } else if (effectiveHitDice > 7 || (hitDice >= 6 && hitDiceModifier >= 2)) {
                magicPotencyModifier = 2;
            } else if (effectiveHitDice > 5 || (hitDice >= 4 && hitDiceModifier >= 1)) {
                magicPotencyModifier = 1;
            }
        }

        // Get the magic potency system modifier, defaulting to 0 if not set
        const systemMagicPotencyModifier = parseInt(this.system.mods?.magicpotency) || 0;
        magicPotencyModifier += systemMagicPotencyModifier;

        // get effect potency to hit formula
        const potencyFormulaValue = this.system.mods?.formula?.potency?.attack
            ? utilitiesManager.evaluateFormulaValueSync(this.system.mods.formula.potency?.attack, this.getRollData())
            : 0;
        if (potencyFormulaValue && !isNaN(potencyFormulaValue)) magicPotencyModifier += potencyFormulaValue;

        // Set the final magic potency modifier as the greater of the calculated modifier or the system modifier
        // if (!magicPotencyModifier || magicPotencyModifier < systemMagicPotencyModifier) {
        //   magicPotencyModifier = systemMagicPotencyModifier;
        // }

        // Return the final magic potency modifier
        return magicPotencyModifier;
    }

    /**
     *
     * Calculate the total magicPotency RESIST (required to hit)
     *
     * @returns {Number} Calculated total magic potency required to hit this actor
     */
    magicPotencyDefense() {
        // get the base version of magic potency resist (npcs)
        let potencyResist = this.system?.resistances?.weapon?.magicpotency || 0;

        // check the additional (effects/etc) resistances for magic potency
        const potencyResistMods = this.system.mods?.resists?.magicpotency || 0;
        potencyResist += potencyResistMods;

        // check for formula style (effects)
        const potencyResistFormula =
            utilitiesManager.evaluateFormulaValueSync(this.system.mods?.formula?.potency?.resist, this.getRollData()) || 0;
        if (potencyResistFormula && !isNaN(potencyResistFormula)) potencyResist += potencyResistFormula;

        // all added together, return
        return potencyResist;
    }

    /**
     *
     * Use a item to initiate a attack
     *
     * @param {*} item
     */
    async _makeAttackWithItem(event, item) {
        const source = this.getToken() ?? this;
        const diceAction = await new ARSDice(source?.object ?? this, item, { event }).makeWeaponAttack();
    }

    /**
     *
     * Returns all formula and values for skill {Name}
     *
     * @param {String} name of skill to find matches for
     * @returns { formula: modItemList, rollData }
     */
    getEquipmentSkillModsFormula(name) {
        let rollData = [],
            total = 0;
        const modItemList = [];
        // get list of equipped items
        const equippedItems = this.items.filter((item) => {
            return ['equipped'].includes(item.system?.location?.state);
        });
        // get items that are not "equipped" like skills/race/etc
        const specialItems = this.items.filter((item) => {
            return ['race', 'background', 'ability'].includes(item.type);
        });
        const itemList = equippedItems.concat(specialItems);

        itemList.forEach((item) => {
            if (item.system.attributes?.skillmods)
                for (const skillmod of Object.values(item.system.attributes.skillmods)) {
                    if (name.toLowerCase() === skillmod.name.toLowerCase()) {
                        total += skillmod.value;
                        const safekey = `item_${utilitiesManager.safeKey(item.name)}`;
                        const formulaTag = `@${safekey}`;
                        // modItemList.push(`@${safekey}`);
                        if (!modItemList.includes(formulaTag)) modItemList.push(formulaTag);
                        if (typeof rollData[safekey] !== 'number') {
                            rollData[safekey] = 0;
                        }
                        rollData[safekey] += parseInt(skillmod.value) || 0;
                    }
                }
        });
        return { formula: modItemList, rollData };
    }

    /**
     *
     * Returns enabled/active effects on this actor
     *
     * @param {ARSItem} sourceItem Item this is from? (or not)
     * @returns {Array}
     */
    getActiveEffects(sourceItem = undefined) {
        // console.log(`actor.js getActiveEffects`, this.allApplicableEffects());
        let activeEffects = this.effects.filter((effect) => !effect.disabled && !effect.isSuppressed);
        // let activeEffects = this.allApplicableEffects().filter((effect) => !effect.disabled && !effect.isSuppressed);

        if (!CONFIG.ActiveEffect.legacyTransferral) {
            this.items.forEach((item) => {
                const itemTransferedEffects = item.effects.filter((eff) => eff.transfer && !eff.disabled && !eff.isSuppressed);
                if (itemTransferedEffects.length) activeEffects = activeEffects.concat(itemTransferedEffects);
            });
        }

        // check for effects that are on the item only (doesnt transfer to owner) used in combat
        if (sourceItem && sourceItem.documentName == 'Item') {
            const inCombatOnly = sourceItem.getItemUseOnlyEffects();
            if (inCombatOnly.length) activeEffects = activeEffects.concat(inCombatOnly);
        }
        return activeEffects;
    }

    /**
     *
     * This finds modifiers from effects and creates a formula that is returned.
     * Special save modifiers, look for system.mods.saves.{type}
     * with value \d+ (fire,cold,acid property opional) or a formula
     *
     * @param {*} saveType
     * @param {*} action
     * @param {*} sourceItem
     * @returns {Array} {name, formula}
     */
    getSaveFormulaFromEffects(saveType, action = null, sourceItem = null) {
        const effectFormulas = typeof action === 'object' ? this.getSystemSaveModifiers(action) : [];
        const modes = game.ars.const.ACTIVE_EFFECT_MODES;
        for (const effect of this.getActiveEffects()) {
            for (const change of effect.changes) {
                if (change.mode == modes.CUSTOM) {
                    if (change.key === `system.mods.saves.all` || change.key === `system.mods.saves.${saveType}`) {
                        const details = JSON.parse(change.value.toLowerCase());
                        if (details.properties) {
                            if (
                                (action && typeof action === 'object' && action.properties.length) ||
                                (sourceItem &&
                                    (sourceItem?.type === 'spell' || sourceItem.system.attributes?.properties?.length))
                            ) {
                                const actionProperties = action?.properties?.map((text) => text.toLowerCase().trim()) || [];
                                const effectProperties = details.properties
                                    ?.split(',')
                                    .map((text) => text.toLowerCase().trim());
                                let itemProperties = [];
                                // is the sourceItem an actor?
                                if (sourceItem && sourceItem instanceof ARSActor) {
                                    itemProperties = Object.values(sourceItem.system?.properties).map((text) =>
                                        text.toLowerCase().trim()
                                    );
                                    // assume item then
                                } else if (sourceItem) {
                                    Object.values(sourceItem.system?.attributes?.properties).map(
                                        (text) => (itemProperties = text.toLowerCase().trim())
                                    );
                                }

                                // Function to handle the string splitting, trimming and lower-casing
                                const processStudy = (str) => str.split(/,|\//).map((text) => text.toLowerCase().trim());
                                let spellSchoolProperties = [];
                                if (sourceItem && sourceItem.type === 'spell') {
                                    const { school, sphere } = sourceItem.system;
                                    // Check if 'school' and 'sphere' exist and are not empty strings
                                    const processedSchool = school ? processStudy(school) : [];
                                    const processedSphere = sphere ? processStudy(sphere) : [];
                                    // Process school and spheres properties and concatenate them
                                    spellSchoolProperties = [...processedSchool, ...processedSphere];
                                }

                                // Assuming actionProperties, itemProperties, and spellSchoolProperties are arrays
                                const sourceProperties = [...actionProperties, ...itemProperties, ...spellSchoolProperties];

                                // Check if any property in sourceProperties is included in effectProperties
                                if (sourceProperties.some((prop) => effectProperties.includes(prop))) {
                                    // effectFormulas.push({ name: `${effect.name}-${change.key}`, formula: details.formula });
                                    effectFormulas.push({
                                        name: utilitiesManager.getUniqueFormulaName(
                                            effectFormulas,
                                            `${effect.name}-${change.key}`
                                        ),
                                        formula: details.formula,
                                    });
                                }
                            }
                        } else {
                            // effectFormulas.push({ name: `${effect.name}-${change.key}`, formula: details.formula });
                            effectFormulas.push({
                                name: utilitiesManager.getUniqueFormulaName(effectFormulas, `${effect.name}-${change.key}`),
                                formula: details.formula,
                            });
                        }
                    } // test change.keys
                } // test modes
            } // for changes
        } // for effects

        console.log('actor.js getSaveModifiersFromEffects', {
            effectFormulas,
        });
        return effectFormulas;
    }

    /**
     *
     * Return system based "global" saves for things like wis/dex bonuses
     * to mental/dodgeable spells for this action
     *
     * @param {*} action
     * @returns
     */
    getSystemSaveModifiers(action) {
        const effectMods = [];
        // const item = fromUuidSync(action?.parentuuid);
        const item = fromUuidSync(action?.parentuuid);
        let hasProperties = false;
        if (item && item.system.attributes?.properties) hasProperties = true;
        if (action && action.properties.length) hasProperties = true;
        // test for "metal" and "dodge" property actions
        if (action && hasProperties) {
            // const actionProperties = action.properties.map((text) => text.toLowerCase().trim()) || [];
            const actionProperties = Object.values(action.properties) || [];
            const itemProperties = item ? Object.values(item.system.attributes.properties || []) : [];
            const propertiesList = itemProperties.concat(actionProperties).map((entry) => entry.trim().toLowerCase());

            for (let key in CONFIG.ARS.saveProperties) {
                const testProperties = CONFIG.ARS.saveProperties[key].property
                    .split(',')
                    .map((text) => text.toLowerCase().trim());
                if (propertiesList.some((prop) => testProperties.includes(prop))) {
                    // effectMods.push(CONFIG.ARS.saveProperties[key].formula);
                    effectMods.push({ name: `property-${key}`, formula: CONFIG.ARS.saveProperties[key].formula });
                }
            }
        }

        console.log('actor.js getSystemSaveModifiers', { effectMods });
        return effectMods;
    }

    /**
     * recalculate save values for npcs by HD
     */
    async recalculateSaves() {
        console.log('actor.js recalculateSaves', this.name);
        const systemVariant = game.ars.config.settings.systemVariant;
        const systemSaveTable = ARS.npcSaveTable[systemVariant];
        let nonIntellgent = false;
        const hdValue = this.effectiveHD;
        const halfHD = Math.ceil(hdValue / 2);

        // check for non-intelligent actors
        if (this.type === 'npc') {
            const nonRegex = /non/;
            // find a 0 without ajacent numbers
            const zeroRegEx = /(?<=^|\D)0(?=$|\D)/;
            if (nonRegex.test(this.system.intelligence.toLowerCase()) || zeroRegEx.test(this.system.intelligence)) {
                nonIntellgent = true;
                ui.notifications.warn(`${this.name} is considered non-intelligent, reducing HD 1/2 for some save settings`);
            }
        }

        const saveBundle = foundry.utils.deepClone(this.system.saves);
        for (let [key, cSaveType] of Object.entries(saveBundle)) {
            // non-intelligent creatures
            if (nonIntellgent && !['poison', 'death'].includes(key)) {
                saveBundle[key].value = systemSaveTable[halfHD][ARS.saveArrayMap[key]];
            } else {
                saveBundle[key].value = systemSaveTable[hdValue][ARS.saveArrayMap[key]];
            }
        }
        this.update({ 'system.saves': saveBundle });
    }

    /**
     * Calculate max hp from class(s)
     *
     * Set class name/level
     *
     */
    _prepareClassData() {
        // const rollData = this.getRollData();
        const updates = {};
        const systemVariant = ARS.settings.systemVariant;
        const activeClassCount = Object.values(this.activeClasses).length;

        if (activeClassCount) {
            // unless n/pc has a class we dont mess with this
            for (let classEntry of this.classes) {
                // console.log("actor.js _prepareClassData classEntry", { classEntry });
                const maxLevel = Object.keys(classEntry.system.advancement).length;
                // console.log("actor.js _prepareClassData", { classEntry, maxLevel }, classEntry.system.ranks[maxLevel]?.xp);
                classEntry.classDetails = {
                    level: maxLevel,
                    xp: classEntry.xp || 0,
                    neededxp: classEntry.system.ranks[maxLevel - 1]?.xp || 0,
                    // disable wisdom spell bonus using toggle in class entry
                    wisSpellBonusDisabled: classEntry.system.features.wisSpellBonusDisabled,
                };
            }
            const classname = this.classes
                .map((c) => {
                    return [c.name, Object.values(c.system.advancement).length].filterJoin(' ');
                })
                .join('/');

            this.system.classname = classname;
        } else {
            // no classes
            if (this.type === 'character') {
                this.system.classname = '';
            }
        }

        const backgroundname = this.backgrounds.length ? this.backgrounds.map((c) => c.name).join('/') : null;
        const racename = this.races.length ? this.races.map((c) => c.name).join('/') : null;

        // set "class #level" name
        if (racename) {
            this.racename = racename;
        } else this.racename = '';

        if (backgroundname) {
            this.system.backgroundname = backgroundname;
        } else this.system.backgroundname = '';
    }

    /** this is used in _preUpdate() to make sure max hps is correctly set */
    _getClassHPData(actor = this) {
        // console.log("actor.js _getClassHPData", foundry.utils.duplicate(actor));
        let updates = null;
        const systemVariant = ARS.settings.systemVariant;
        if (this.activeClasses) {
            updates = {};
            const activeClassCount = Object.values(this.activeClasses).length;
            if (activeClassCount) {
                // unless n/pc has a class we dont mess with this
                const hpMaxOrig = actor.system.attributes.hp.max;

                let maxHP = actor.system.attributes.hp.base || 0;
                let currentHP = actor.system.attributes.hp.value || 0;

                const conScore = actor.system.abilities.con.value;
                // get normal con bonus from table
                const conBonus = game.ars.config.constitutionTable[systemVariant][conScore][0][0];
                // get warrior style con bonus from table
                let conBonusWarrior = game.ars.config.constitutionTable[systemVariant][conScore][0][1];
                if (!conBonusWarrior) conBonusWarrior = conBonus;
                let conBonusTotal = 0;
                const maxInactiveLevel = this.getMaxInactive();
                const maxActiveLevel = this.getMaxActiveLevel();
                const dualClassNotExceeded = maxInactiveLevel && maxInactiveLevel > maxActiveLevel;

                for (let classEntry of this.classes) {
                    // skip class if active and we're still lower level
                    // than our highest level inactive (dual) class
                    if (dualClassNotExceeded && classEntry.system.active) continue;
                    const usehpConFormula = classEntry.system.features.hpConFormula ? true : false;
                    const hpBonusConResult =
                        parseInt(
                            utilitiesManager.evaluateFormulaValueSync(
                                classEntry.system.features.hpConFormula,
                                this.getRollData()
                            )
                        ) || 0;
                    const lastHDLevel = parseInt(classEntry.system.features.lasthitdice) ?? 999;
                    let advancementArray = Object.values(classEntry.system.advancement);
                    for (let index = 0; index < advancementArray.length; index++) {
                        let entry = advancementArray[index];

                        // skip levels that are lower than inactive classes (dual class)
                        if (maxInactiveLevel && classEntry.system.active && entry.level <= maxInactiveLevel) continue;
                        const addHP = activeClassCount > 0 ? Math.round(entry.hp / activeClassCount) : entry.hp;
                        const conBonusMod = classEntry.system.features.bonuscon ? conBonusWarrior : conBonus;
                        if (entry.level <= lastHDLevel)
                            if (usehpConFormula) {
                                conBonusTotal += hpBonusConResult / activeClassCount;
                            } else {
                                conBonusTotal += conBonusMod / activeClassCount;
                            }
                        maxHP += addHP;
                    }
                }
                // add in Constitution HP modifier
                maxHP += Math.ceil(conBonusTotal);

                // if hp.max changed adjust value for that change
                // if (data.system.attributes.hp.max > hpMaxOrig) {
                if (maxHP > hpMaxOrig) {
                    const diff = maxHP - hpMaxOrig;
                    if (diff != 0) {
                        // data.system.attributes.hp.value += diff;
                        currentHP += diff;
                    }
                }
                // if current hp is greater than max now or currentHP == 0, adjust to current max.

                // changing on currentHP 0 resets hp when they are "dead" or at 0.
                // if (currentHP > maxHP || currentHP === 0) {
                if (currentHP > maxHP || currentHP === undefined || currentHP === null) {
                    currentHP = maxHP;
                }

                // console.log("actor.js _prepareClassData", { backgroundname, racename, classname });
                updates['system.attributes.hp.max'] = maxHP;
                updates['system.attributes.hp.value'] = currentHP;
            } else {
                // no classes
                if (actor.type === 'character') {
                    updates['system.attributes.hp.max'] = 0;
                    updates['system.attributes.hp.value'] = 0;
                }
            }
        }

        return updates;
    }

    /**
     *
     * @param {String} statusName such as "dead" "blind" "deaf" "sleep"
     * @returns Boolean
     *
     */
    hasStatusEffect(statusName = '') {
        // const status = statusName ? (this.getActiveEffects().find(e => e.getFlag("core", "statusId") === statusName)) : false;
        const simpleStatus = this.statuses.has(statusName);
        const effectStatus = statusName ? this.getActiveEffects().some((eff) => eff.statuses?.has(statusName)) : false;
        // const status = this.getToken().hasStatusEffect(statusName);
        return simpleStatus || effectStatus;
    }

    /**
     *
     * Return the formula/rolldata for statuses that are active on a actor
     *
     * @param {String} type attack, attacked, damage, save, ac, initiative, move
     * @param {*} combatType
     * @param {*} opponentToken
     */
    getStatusFormula(type = 'attack', combatType = '', opponentToken = undefined) {
        // console.log('actor.js getStatusFormula', { type, combatType, opponentToken });
        let rollData = [],
            bonusFormula = [];
        let acTags = new Set();
        const variant = parseInt(game.ars.config.settings.systemVariant);

        /** update rollData/bonusFormula */
        function _updateFormulas(statusName, value) {
            const formulaKey = utilitiesManager.safeKey(`status.${statusName}.${type}`);
            const formulaTag = `@${formulaKey}`;
            if (!bonusFormula.includes(formulaTag)) bonusFormula.push(formulaTag);
            if (typeof rollData[formulaKey] !== 'number') {
                rollData[formulaKey] = 0;
            }
            rollData[formulaKey] += value;
        }

        /**
         * Helper function to determine if the active effects include a particular status.
         * @param {string} status - The status to be checked.
         * @returns {boolean} - Return true if the status is found in the active effects, otherwise false.
         */
        function isStatusInActiveEffects(status) {
            // Fetch all active effects.
            const activeEffects = this.getActiveEffects();

            // Iterate over each effect.
            for (let effect of activeEffects) {
                // Each effect has a list of changes.
                // We're looking for the 'special.status' key and its associated value in these changes.
                const changeFound = effect.changes.find((change) => {
                    // Make sure the change has a key and a value.
                    if (change?.key && change?.value) {
                        // Check if the key includes 'special.status' (case-insensitive).
                        const keyMatches = change.key.toLowerCase() == 'special.status';
                        // Convert the value (which is an array of strings) to lowercase and trim whitespace.
                        const normalizedValues = change.value.toLowerCase().trim();
                        // Check if the transformed values include the status we're looking for.
                        const valueMatches = normalizedValues.includes(status);
                        // If both key and value match our criteria, we've found our change.
                        return keyMatches && valueMatches;
                    }

                    // By default, assume this change is not what we're looking for.
                    return false;
                });

                // If we found a matching change in this effect, there's no need to continue looking.
                if (changeFound) {
                    return true;
                }
            }

            // If we've checked all effects and found no matching change, the status is not in the active effects.
            return false;
        }

        // Helper function to apply a status effect and update the status list
        function applyStatusEffect(status, effectFn) {
            // Check if the current instance has the given status or the status is on an effect
            if (this.hasStatusEffect(status) || isStatusInActiveEffects.bind(this)(status)) {
                effectFn(status, opponentToken);
            }
        }
        /** static list of status types and function to apply formula */
        let statusEffects = [
            {
                status: 'invisible',
                effect: (statusName, targetToken) => {
                    if (targetToken && targetToken.detectionModes) {
                        //TODO: have this also do a detect invis range check ?
                        function isSeeInvisibilityEnabled(array) {
                            return array.some((item) => item.id === 'seeInvisibility' && item.enabled);
                        }

                        switch (type) {
                            case 'attack':
                                // if person is invis and target doesnt see invis then actor gets +2 to hit them
                                if (
                                    this.hasStatusEffect('invisible') &&
                                    !isSeeInvisibilityEnabled(targetToken.detectionModes)
                                ) {
                                    _updateFormulas(statusName, 2);
                                }
                                break;
                            case 'attacked':
                                //if the target cannot be seen, they are harder to hit (-4)
                                if (!isSeeInvisibilityEnabled(targetToken.detectionModes)) {
                                    _updateFormulas(statusName, -4);
                                }
                                break;
                            case 'save':
                                if (!isSeeInvisibilityEnabled(targetToken.detectionModes)) {
                                    // statusEffect.save.all += 4;
                                    _updateFormulas(statusName, 4);
                                }
                                break;
                        }
                    } else {
                        switch (type) {
                            case 'attack':
                                _updateFormulas(statusName, 2);
                                break;
                            case 'attacked':
                                _updateFormulas(statusName, -4);
                                break;
                            case 'save':
                                _updateFormulas(statusName, 4);
                                break;
                        }
                    }
                },
            },
            {
                status: 'blind',
                effect: (statusName) => {
                    switch (type) {
                        case 'attack':
                            _updateFormulas(statusName, -4);
                            break;
                        case 'attacked':
                            _updateFormulas(statusName, 4);
                            break;
                        case 'save':
                            _updateFormulas(statusName, -4);
                            break;
                        case 'initiative':
                            _updateFormulas(statusName, 2);
                            break;
                        case 'move':
                            //  move should be 1/3 also
                            break;
                    }
                },
            },
            {
                status: 'hold',
                effect: (statusName) => {
                    acTags.add('rear');
                    switch (type) {
                        case 'attacked':
                            _updateFormulas(statusName, 4);
                            break;
                    }
                },
            },
            {
                status: 'stun',
                effect: (statusName) => {
                    acTags.add('nodexshield');
                    switch (type) {
                        case 'attack':
                            _updateFormulas(statusName, -4);
                            break;
                        case 'attacked':
                            _updateFormulas(statusName, 4);
                            break;
                    }
                },
            },
            {
                status: 'paralysis',
                effect: (statusName) => {
                    acTags.add('nodexshield');
                    switch (type) {
                        case 'attacked':
                            _updateFormulas(statusName, 4);
                            break;
                    }
                },
            },
            {
                status: 'prone',
                effect: (statusName) => {
                    acTags.add('nodexshield');
                    switch (type) {
                        case 'attacked':
                            switch (combatType) {
                                case 'melee':
                                    _updateFormulas(statusName, 4);
                                    break;
                                case 'ranged':
                                case 'thrown':
                                    _updateFormulas(statusName, -2);
                                    break;
                            }
                            break;
                    }
                },
            },
            {
                status: 'charge',
                effect: (statusName) => {
                    acTags.add('nodex');
                    switch (type) {
                        case 'ac':
                            _updateFormulas(statusName, 1);
                            break;
                        case 'attack':
                            _updateFormulas(statusName, 2);
                            break;
                    }
                },
            },
            // ... other status effects
        ];

        const variant0_1StatusEffects = [
            // ... other variant status effects
            {
                status: 'restrain',
                effect: (statusName) => {
                    acTags.add('rear');
                    switch (type) {
                        case 'attacked':
                            _updateFormulas(statusName, 4);
                            break;
                    }
                },
            },
            {
                status: 'cover-25%',
                effect: (statusName) => {
                    switch (type) {
                        case 'ac':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -2);
                            }
                            break;
                        case 'save':
                            _updateFormulas(statusName, 2);
                            break;
                    }
                },
            },
            {
                status: 'cover-50%',
                effect: (statusName) => {
                    switch (type) {
                        case 'ac':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -4);
                            }
                            break;
                        case 'save':
                            _updateFormulas(statusName, 4);
                            break;
                    }
                },
            },
            {
                status: 'cover-75%',
                effect: (statusName) => {
                    switch (type) {
                        case 'ac':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -7);
                            }
                            break;
                        case 'save':
                            _updateFormulas(statusName, 7);
                            break;
                    }
                },
            },
            {
                status: 'cover-90%',
                effect: (statusName) => {
                    switch (type) {
                        case 'ac':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -10);
                            }
                            break;
                        case 'save':
                            _updateFormulas(statusName, 10);
                            break;
                    }
                },
            },
            {
                status: 'concealed-25%',
                effect: (statusName) => {
                    switch (type) {
                        case 'ac':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -1);
                            }
                            break;
                    }
                },
            },
            {
                status: 'concealed-50%',
                effect: (statusName) => {
                    switch (type) {
                        case 'ac':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -2);
                            }
                            break;
                    }
                },
            },
            {
                status: 'concealed-75%',
                effect: (statusName) => {
                    switch (type) {
                        case 'ac':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -3);
                            }
                            break;
                    }
                },
            },
            {
                status: 'concealed-90%',
                effect: (statusName) => {
                    switch (type) {
                        case 'ac':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -4);
                            }
                            break;
                    }
                },
            },
            {
                status: 'encumbrance-light',
                effect: () => {
                    //TODO: variant 0 and 1 settings?
                    // nothing
                },
            },
            {
                status: 'encumbrance-moderate',
                effect: () => {
                    // statusEffect.attack.value += -1;
                },
            },
            {
                status: 'encumbrance-heavy',
                effect: (statusName) => {
                    acTags.add('nodex');
                    switch (type) {
                        case 'attacked':
                            _updateFormulas(statusName, 2);
                            break;
                    }
                },
            },
            {
                status: 'encumbrance-severe',
                effect: (statusName) => {
                    acTags.add('nodex');
                    switch (type) {
                        case 'attacked':
                            _updateFormulas(statusName, 2);
                            break;
                    }
                },
            },
        ];

        const variant2StatusEffects = [
            {
                status: 'restrain',
                effect: (statusName) => {
                    acTags.add('rear');
                    switch (type) {
                        case 'attacked':
                            _updateFormulas(statusName, 2);
                            break;
                    }
                },
            },
            {
                status: 'cover-25%',
                effect: (statusName) => {
                    switch (type) {
                        case 'attacked':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -2);
                            }
                            break;
                        case 'save':
                            _updateFormulas(statusName, 2);
                            break;
                    }
                },
            },
            {
                status: 'cover-50%',
                effect: (statusName) => {
                    switch (type) {
                        case 'attacked':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -4);
                            }
                            break;
                        case 'save':
                            _updateFormulas(statusName, 4);
                            break;
                    }
                },
            },
            {
                status: 'cover-75%',
                effect: (statusName) => {
                    switch (type) {
                        case 'attacked':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -7);
                            }
                            break;
                        case 'save':
                            _updateFormulas(statusName, 7);
                            break;
                    }
                },
            },
            {
                status: 'cover-90%',
                effect: (statusName) => {
                    switch (type) {
                        case 'attacked':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -10);
                            }
                            break;
                        case 'save':
                            _updateFormulas(statusName, 10);
                            break;
                    }
                },
            },
            {
                status: 'concealed-25%',
                effect: (statusName) => {
                    switch (type) {
                        case 'attacked':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -1);
                            }
                            break;
                    }
                },
            },
            {
                status: 'concealed-50%',
                effect: (statusName) => {
                    switch (type) {
                        case 'attacked':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -2);
                            }
                            break;
                    }
                },
            },
            {
                status: 'concealed-75%',
                effect: (statusName) => {
                    switch (type) {
                        case 'attacked':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -3);
                            }
                            break;
                    }
                },
            },
            {
                status: 'concealed-90%',
                effect: (statusName) => {
                    switch (type) {
                        case 'attacked':
                            if (combatType == 'ranged' || combatType == 'thrown') {
                                _updateFormulas(statusName, -4);
                            }
                            break;
                    }
                },
            },
        ];

        if (variant == 0 || variant == 1) statusEffects = statusEffects.concat(variant0_1StatusEffects);
        else if (variant == 2) statusEffects = statusEffects.concat(variant2StatusEffects);
        // Iterate through the global status effects and apply them
        statusEffects.forEach(({ status, effect }) => {
            applyStatusEffect.call(this, status, effect);
        });

        // Determine the "worst" AC tag
        const acTagHierarchy = ['rear', 'nodexshield', 'nodex', ''];
        let acTag = [...acTags].sort((a, b) => acTagHierarchy.indexOf(a) - acTagHierarchy.indexOf(b))[0] || '';

        if (type == 'ac' || bonusFormula?.length) {
            return { formula: bonusFormula, rollData, acTag };
        }
        return undefined;
    }

    /**
     *
     * get combatant record
     *
     * @returns
     */
    getCombatant() {
        const token = this.getToken();
        if (token) {
            return token.combatant;
        }
        return false;
    }

    /**getter returns initiative */
    get initiative() {
        const combatant = this.getCombatant();
        return combatant ? combatant.initiative : undefined;
    }

    /**
     * Determines the Armor Class (AC) hit based on the hitRoll value.
     * @param {number} hitRoll - The value to be checked against the combat matrix.
     * @returns {number} - The AC hit value based on the hitRoll.
     */
    acHit(hitRoll) {
        let acHit = 10;

        const variant = parseInt(game.ars.config.settings.systemVariant) || 0;
        switch (variant) {
            case 0:
            case 1:
                acHit = this.acHitByMatrix(hitRoll);
                break;

            case 2:
            default:
                acHit = this.acHitByTHACO(hitRoll);
                break;
        }

        console.log('actor.js acHit()', { hitRoll, acHit });
        return acHit;
    }

    /**
     * Calculates and returns the Armor Class (AC) hit value based on a hit roll.
     * @param {number} hitRoll - The hit roll to calculate AC hit from.
     * @returns {number} The AC hit value.
     */
    acHitByTHACO(hitRoll) {
        // Get the THACO value from the system attributes.
        const thaco = this.system.attributes.thaco.value;

        // Calculate the AC hit value using the hit roll and THACO values.
        const acHit = thaco - hitRoll;

        // Log the THACO, AC hit, and hit roll values for debugging purposes.
        // console.log("acHitByTHACO:", { thaco, acHit, hitRoll });

        // Return the AC hit value.
        return acHit;
    }

    /**
     * Get the AC hit from hitRoll using the OSRIC combat matrix
     * @param {number} hitRoll - The roll needed to hit the target
     * @returns {number} - The AC hit value
     */
    acHitByMatrix(hitRoll) {
        const matrixVariant = parseInt(game.ars.config.settings.systemVariant) || 0;

        function lowestACHit(matrixSlice) {
            // Find the first occurrence of hitRoll in the matrix slice to determine AC hit
            let lowAC = 11;
            let hitIndex = -1;
            for (let i = 0; i < matrixSlice.length; i++) {
                if (matrixSlice[i] === hitRoll) {
                    hitIndex = i;
                    break;
                }
            }

            // Calculate the AC hit value based on the index of the hitRoll in the matrix slice
            if (hitIndex > -1) {
                lowAC = hitIndex - 10;
            } else {
                // If the hitRoll is greater than the lowest AC that can be hit, set AC hit to -10
                if (hitRoll > matrixSlice[0]) {
                    lowAC = -10;
                }
            }
            return lowAC;
        }

        return lowestACHit(this.matrixSlice);
    }

    /**
     *
     * Give a specific coin type to actor
     *
     * @param {*} actor
     * @param {*} copperBaseGiven
     * @param {*} currencyType
     * @returns
     */
    async giveSpecificCurrency(amount, currencyType) {
        const variant = game.ars.config.settings.systemVariant;
        const currencyItems = [];
        if (amount > 0) {
            // Try to update the actor's existing currency items
            const coinItem = this.carriedCurrencyItems.find((coinItem) => {
                const currentCurrencyType = coinItem.system.cost.currency.toLowerCase();
                return currentCurrencyType === currencyType;
            });
            if (coinItem) {
                // update existing coin item
                const quantity = parseInt(coinItem.system.quantity) || 0;
                const newQuantity = quantity + amount;
                await coinItem.update({ 'system.quantity': newQuantity });
            } else {
                // create coin item
                const currencyName =
                    game.i18n.localize(`ARS.currency.short.${currencyType}`) + ` ${game.i18n.localize('ARS.coins')}` ||
                    'Change';
                // currencyWeight is how many to a pound so we divide by 1 (so how much per pound)
                const currencyWeight = 1 / ARS.currencyWeight[variant];
                const currencyData = {
                    name: currencyName,
                    type: 'currency',
                    img: ARS.icons.general.currency[currencyType],
                    system: {
                        quantity: amount,
                        weight: currencyWeight,
                        cost: {
                            currency: currencyType,
                        },
                    },
                };
                currencyItems.push(currencyData);
            }
        }

        if (currencyItems.length > 0) {
            await this.createEmbeddedDocuments('Item', currencyItems, {
                hideChanges: true,
            });
        }
    }

    /**
     * Checks if the actor is immune to any of the provided statusIds.
     *
     * @param {String[]} statusIds - An array of statusIds like ['blind', 'sleep', 'charm'].
     * @returns {Boolean} - True if immune to any of the statusIds, false otherwise.
     */
    isStatusImmune(statusIds) {
        if (!statusIds || !Array.isArray(statusIds) || statusIds.length === 0) {
            // console.error('Invalid or no statusIds provided for isStatusImmune');
            return false;
        }

        // Retrieve effects that have status immunities
        const statusImmuneEffects = effectManager.getEffectsByChangesKey('special.statusimmune', this.getActiveEffects()) || [];

        // Create a set of all status immunities
        const statusImmunities = new Set(
            statusImmuneEffects.flatMap((effect) =>
                (effect.changes || [])
                    .map((change) => change.value?.split(',').map((text) => text?.trim()?.toLowerCase()) || [])
                    .flat()
            )
        );

        // Check if any of the statusIds are included in the immunities
        return statusIds.some((statusId) => statusImmunities.has(statusId.toLowerCase()));
    }

    /**
     *
     * Bulk item save for all items in inventory for actor
     *
     * @param {Number} againstType index of ARS.itemSaveTypes[variant][??]
     */
    async makeAllItemsSavingsThrow(againstType = undefined) {
        function getTopContainedParent(item) {
            let currentItem = item.containedIn;
            while (currentItem) {
                if (!currentItem.containedIn) {
                    return currentItem;
                }
                currentItem = currentItem?.containedIn || undefined;
            }
            return item;
        }

        async function getAllContained(item, listFailed) {
            for (const contained of item.contains) {
                const { success, saveResult, saveTarget, saveType, formula } = await contained.itemRollSave(againstType);
                listFailed.push({
                    item: contained,
                    rolled: saveResult,
                    formula,
                    parent: getTopContainedParent(contained),
                    success,
                    saveTarget,
                    saveType,
                });
                if (contained.contains) {
                    await getAllContained(contained, listFailed);
                }
            }
        }

        if (!againstType) againstType = await dialogManager.promptItemSaveType();
        // const variant = game.ars.config.settings.systemVariant;
        let saveTypeUsed = ''; //ARS.itemSaveTypes[variant][againstType];
        if (againstType >= 0) {
            let listSaved = [];
            let listFailed = [];
            for (const item of this.inventoryCarried) {
                if (!item.containedIn) {
                    const { success, saveResult, saveTarget, saveType, formula } = await item.itemRollSave(againstType);
                    saveTypeUsed = saveType;

                    if (success) {
                        console.log(`${item.name} SAVED!`, { item, saveResult, success });
                        listSaved.push({ item, rolled: saveResult, saveTarget, formula });
                    } else {
                        console.log(`${item.name} FAILED!`, { item, saveResult, success });
                        listFailed.push({ item, rolled: saveResult, saveTarget, formula });
                        if (item.contains) {
                            await getAllContained(item, listFailed);
                        }
                    }
                }
            } // end for

            console.log('makeAllItemsSavingsThrow', { listSaved, listFailed });
            const dice = await new ARSDice(this);
            dice._playAudioCheck(!listFailed.length);
            // -----------------------
            const content = await renderTemplate('systems/ars/templates/chat/chatCard-itemSaves.hbs', {
                actor: this,
                listFailed,
                listSaved,
                saveType: saveTypeUsed,
            });

            let chatData = {
                title: 'Item Saves',
                content: content,
                author: game.user.id,
                // rolls: [roll],
                rollMode: game.settings.get('core', 'rollMode'),
                speaker: ChatMessage.getSpeaker({ actor: this }),
                style: CONST.CHAT_MESSAGE_STYLES.IC,
            };
            // use user current setting? game.settings.get("core", "rollMode")
            ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
            ChatMessage.create(chatData);
            // -----------------------
        }
    }

    /**
     * * Finds an ActiveEffect by ID either on the actor directly or on any items owned by the actor.
     *
     * @param {*} effectId
     */
    getEffectOn(effectId) {
        // Check if the actor has the effect
        let effect = this.effects.find((e) => e.id === effectId);

        if (!effect) {
            // If not found on the actor, check each item
            for (let item of this.items) {
                effect = item.effects.find((e) => e.id === effectId);
                if (effect) {
                    console.log('Effect found on an item of the actor.');
                    return effect;
                }
            }
        }
        return effect;
    }

    /**
     *
     * Take a weapon and find its best attacks per round
     *
     * 1) weapon
     * 2) Proficiency
     *
     * @param {*} weapon
     * return {String} best attack rating 3/2 or 2/1 etc.
     *
     */
    getBestAttackRate(weapon) {
        /**
         * Parses an attacks per round string and returns the best value (max attacks in a single round).
         * @param {string} attackString - The attacks per round string (e.g., "3/2").
         * @returns {number} - The maximum attacks in a single round.
         */
        function parseAttacksPerRound(attackString) {
            // Split the string into attacks and rounds
            let [attacks, rounds] = attackString.split('/').map((val) => {
                let num = Number(val);
                return isNaN(num) || num < 1 ? 1 : num;
            });

            // Default rounds to 1 if undefined
            rounds = rounds || 1;

            // Calculate the average attacks per round
            const averageAttacksPerRound = attacks / rounds;

            // Calculate the best value, which is the ceiling of the average attacks per round
            const bestValue = Math.ceil(averageAttacksPerRound);

            return bestValue;
        }

        /**
         * Finds the best (maximum) attacks per round string from an array of attack strings.
         * @param {string[]} attackStrings - Array of attacks per round strings (e.g., ["1/1", "3/2", "5/2"]).
         * @returns {string} - The attack string with the maximum attacks in a single round.
         */
        function findBestAttacksPerRound(attackStrings) {
            let bestAttackString = '';
            let maxAttacks = 0;

            for (const attackString of attackStrings) {
                const attacks = parseAttacksPerRound(attackString);
                if (attacks && attacks > maxAttacks) {
                    maxAttacks = attacks;
                    bestAttackString = attackString;
                }
            }

            return bestAttackString;
        }

        // list of profeciencies
        let attackList = weapon.getProficiencyList();
        const attackStrings = [];
        for (const prof of attackList) {
            if (prof.system.attacks) attackStrings.push(prof.system.attacks);
        }

        if (weapon.system?.attack?.perRound) attackStrings.push(weapon.system.attack.perRound);

        // find the best
        return findBestAttacksPerRound(attackStrings);
    }

    /**
     *
     * Accepts an attacks per round string and returns an
     * array representing the number of attacks per round.
     *
     * It will handle 6/3 or 1/2 (heaxy xbow/etc)
     *
     * @param {string} attackString - The attacks per round string (e.g., "5/2").
     * @param {boolean} mostAttacksFirst - Whether to distribute the most attacks in the first round (true) or the last round (false).
     *
     * @returns {number[]} - An array representing the number of attacks per round.
     */
    calculateAttacksPer(attackString, mostAttacksFirst) {
        // Split the string into attacks and rounds
        let [attacks, rounds] = attackString.split('/').map((val) => {
            let num = Number(val);
            return isNaN(num) || num < 1 ? 1 : num;
        });

        // Default rounds to 1 if undefined
        rounds = rounds || 1;

        // Initialize the result array with zeros
        let attacksPerRound = Array(rounds).fill(0);

        // Distribute the attacks over the rounds
        let remainingAttacks = attacks;
        for (let i = rounds - 1; i >= 0; i--) {
            const attacksThisRound = Math.ceil(remainingAttacks / (i + 1));
            attacksPerRound[i] = attacksThisRound;
            remainingAttacks -= attacksThisRound;
        }

        // 1e - higher is first in spread of attacks/round - simply reverse
        if (mostAttacksFirst) attacksPerRound = attacksPerRound.reverse();

        return attacksPerRound;
    }

    /** lock lootable npc sheet */
    lockLootable(byUser) {
        // console.log('lockLootable', { byUser }, this);
        utilitiesManager.runAsGM({
            sourceFunction: 'lockLootable()',
            operation: 'actorUpdate',
            user: game.user.id,
            targetTokenId: this?.token?.id,
            targetActorId: this?.token?.id ? undefined : this.id,
            targetActorUuid: this?.token?.uuid ? undefined : this.uuid,
            update: { 'system.opened': game.user.id },
        });
    }
    /** unlock lootable npc sheet */
    unLockLootable() {
        // console.log('unLockLootable', this);
        utilitiesManager.runAsGM({
            sourceFunction: 'unLockLootable()',
            operation: 'actorUpdate',
            user: game.user.id,
            targetTokenId: this?.token?.id,
            targetActorId: this.id,
            targetActorUuid: this?.uuid ? this.uuid : this?.token?.uuid,
            update: { 'system.opened': '' },
        });
    }
    /** check if lootable npc has valid lock, return user locked by */
    isLockedLootable() {
        // console.log('isLockedLootable', this);
        const userId = this.system.opened;
        const user = game.users.get(userId);
        if (!user || !user.active) {
            this.unLockLootable();
            return undefined;
        }
        return user;
    }
} // end actor export
