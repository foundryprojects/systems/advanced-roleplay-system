// import { ARSActorSheet } from './actor/actor-sheet.js';
// import { ARSNPCSheet } from './actor-sheet-npc.js';
import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
export class ARSMerchantSheet extends ActorSheet {
    constructor(options = {}) {
        super(options);
        this.customer = {
            actor: undefined,
            user: undefined,
        };
    }
    //TODO: prototype work for merchant

    /**
     * Specify the path to the HTML template.
     */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ['ars', 'sheet', 'actor', 'merchant'],
            template: 'systems/ars/templates/actor/merchant-sheet.hbs',
            width: 600,
            height: 'auto',
            actor: this.actor,
            secrets: [],
            scrollY: ['.item-list'],
            tabs: [
                {
                    navSelector: '.sheet-tabs',
                    contentSelector: 'section.sheet-body',
                    initial: 'buy',
                },
            ],
            dragDrop: [
                {
                    dragSelector: '.item-sellable',
                },
            ],
        });
    }

    _canDragStart(event) {
        return false;
    }
    _canDragDrop(event) {
        return game.user.isGM;
    }

    /** override to capture close  */
    close() {
        this.customer.user = undefined;
        this.customer.actor = undefined;
        super.close();
    }

    /** override to set customer.actor  */
    async render(force = false, options = {}) {
        if (!this.customer.actor) {
            this.customer.actor = await dialogManager.selectTradingActor();
            if (!this.customer.actor) return false;
            this.customer.actor.apps[this.appId] = this;
        }

        return super.render(force, options);
    }
    /**
     * Get the data for the Merchant Actor Sheet.
     */
    async getData() {
        const context = super.getData();
        context.isGM = game.user.isGM;
        context.isDM = game.user.isDM;
        context.actor = this.actor;
        context.customer = this.customer.actor;
        //
        this.sellingList = this.calculateItemCosts(
            this.actor.system.market.canSell,
            this.actor.items,
            this.actor.system.markup.sell
        );
        context.items = this.sellingList;

        // customer items
        this.playerItems = this.calculateItemCosts(
            this.actor.system.market.canBuy,
            this.customer.actor.inventoryCarried,
            this.actor.system.markup.buy
        );
        context.playerItems = this.playerItems;
        return context;
    }

    /**
     *
     * calculate costs of each item using markup and sort
     *
     * @param {Boolean} canProcess - pass market.canSell, market.canBuy
     * @param {Array} items
     * @param {Number} markup - percent markup
     * @returns
     */
    calculateItemCosts(canProcess, items, markup) {
        function sortByItemName(arr) {
            return arr.sort((a, b) => {
                const nameA = a.item.name.toUpperCase();
                const nameB = b.item.name.toUpperCase();

                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0; // names are equal
            });
        }
        // Function to calculate the new price after markup
        function calculateMarkupPrice(cost, markupPercent) {
            // Convert the markup percentage to a decimal
            const markupDecimal = markupPercent / 100;
            // Calculate the markup amount
            const markupAmount = cost * markupDecimal;
            // Calculate the new price after applying the markup
            const newPrice = cost + markupAmount;
            return newPrice;
        }

        const sellingList = [];
        for (const item of items) {
            if (item.type == 'currency' && !game.user.isGM) continue;
            const cost = Math.round(calculateMarkupPrice(item.system.cost.value, markup));
            // let GM always sell/buy but others abide by settings
            const canSell = canProcess ? (item.system.quantity > 0 && cost > 0) || game.user.isGM : game.user.isGM;
            sellingList.push({ item, cost, canSell });
        }
        const list = sortByItemName(sellingList);
        return list;
    }

    //** get actor to place item on */
    #getTargetActor() {
        const token = canvas.tokens.controlled?.[0];
        let actor = game.user.character;
        if (token && token.isOwner) {
            actor = token.actor;
        }
        return actor;
    }

    /**
     * Activate event listeners for the Merchant Actor Sheet.
     */
    activateListeners(html) {
        super.activateListeners(html);
        // Add additional listeners for Merchant specific actions
        html.find('.item-buy').click((event) => this._onBuyItemButtonClick.bind(this)(event));
        html.find('.item-take').click((event) => this._onTakeItemButtonClick.bind(this)(event));
        html.find('.item-remove').click((event) => this._onItemRemoveClick.bind(this)(event));

        html.find('.item-sell').click((event) => this._onSellItemButtonClick.bind(this)(event));

        // html.find('.set-buy-markup').change((event) => this._onSetBuyMarkup.bind(this)(event));
        // html.find('.set-sell-markup').change((event) => this._onSetSellMarkup.bind(this)(event));

        // item-view, used for pack items in memorization-slots
        html.find('.merchant-items .item-sellable').click((event) => this._onViewItem(event, 'merchant'));

        html.find('.player-items .item-sellable').click((event) => this._onViewItem(event, 'customer'));

        html.find('.item-quantity-control').click(this.#setItemQuantity.bind(this));
    }

    async _onViewItem(event, type) {
        // if (game.user.isGM) {
        const element = event.currentTarget;
        const li = element.closest('li');
        const itemId = li.dataset.itemId;
        let actor = this.actor;
        let entry = this.sellingList.find((ent) => ent.item.id == itemId);
        if (type == 'customer') {
            entry = this.playerItems.find((ent) => ent.item.id == itemId);
            actor = this.customer.actor;
        }

        if (!entry?.item) {
            ui.notifications.error(`Item cannot be found in ${actor.name} inventory.`);
            return;
        }
        entry.item.sheet.render(true);
        // }
    }
    /**
     * Handle buy item button clicks.
     */
    async _onBuyItemButtonClick(event) {
        event.preventDefault();
        // Handle the buy item button click
        const element = event.currentTarget;
        const li = element.closest('li');
        const itemId = li.dataset.itemId;
        const entry = this.sellingList.find((ent) => ent.item.id == itemId);
        // const item = this.actor.getEmbeddedDocument('Item', itemId);
        if (entry) {
            if (this.customer.actor != this.actor) {
                const count = await utilitiesManager.purchaseItem(
                    this.customer.actor,
                    entry.item,
                    true,
                    entry.cost,
                    entry.item.system.quantity,
                    undefined,
                    this.actor
                );
                if (count) {
                    this.#reduceQuantity(entry.item, count, false);
                }
            } else {
                ui.notifications.error('Cannot trade with self');
            }
        }
    }

    async #reduceQuantity(item, count, deleteEmpty = false) {
        console.trace('#reduceQuantity', { item, count, deleteEmpty });
        if (item.system.quantity != -1) {
            let newTotal = item.system.quantity - count;
            if (newTotal > 0 || (newTotal <= 0 && !deleteEmpty)) {
                // item.update({ 'system.quantity': newTotal });
                const update = { 'system.quantity': newTotal };

                await utilitiesManager.runAsGM({
                    sourceFunction: 'actor-sheet-merchant.js #reduceQuantity',
                    operation: 'itemUpdate',
                    user: game.user.id,
                    targetTokenId: item.actor.getToken()?.id,
                    targetActorId: item.actor.id,
                    targetActorUuid: item.actor.uuid,
                    targetItemId: item.id,
                    update,
                });
            } else if (deleteEmpty) {
                await utilitiesManager.runAsGM({
                    sourceFunction: 'actor-sheet-merchant.js #reduceQuantity',
                    operation: 'deleteEmbeddedDocuments',
                    user: game.user.id,
                    targetTokenId: item.actor.getToken()?.id,
                    targetActorId: item.actor.id,
                    targetActorUuid: item.actor.uuid,
                    targetItemId: item.id,
                });
            }
        }
    }

    async _onTakeItemButtonClick(event) {
        console.log('_onTakeItemButtonClick', { event });
        event.preventDefault();
        // Handle the take item button click
        const element = event.currentTarget;
        const li = element.closest('li');
        const itemId = li.dataset.itemId;
        const entry = this.sellingList.find((ent) => ent.item.id == itemId);
        if (entry) {
            // const actor = this.#getTargetActor();
            if (this.customer.actor != this.actor) {
                const count = await utilitiesManager.purchaseItem(
                    this.customer.actor,
                    entry.item,
                    false,
                    entry.cost,
                    entry.item.system.quantity
                );
                if (count) {
                    this.#reduceQuantity(entry.item, count, false);
                }
            } else {
                ui.notifications.error('Cannot trade with self');
            }
        }
    }
    /**
     * Handle sell item button clicks.
     */
    async _onSellItemButtonClick(event) {
        console.log('_onSellItemButtonClick', { event });
        event.preventDefault();
        // Handle the sell item button click
        const element = event.currentTarget;
        const li = element.closest('li');
        // const itemUuid = li.dataset.itemUuid;
        const itemId = li.dataset.itemId;
        // get item details from actor selling
        const entry = this.playerItems.find((ent) => ent.item.id == itemId);
        // const item = this.actor.getEmbeddedDocument('Item', itemId);
        if (entry) {
            if (this.customer.actor != this.actor) {
                const count = await dialogManager.getQuantity(
                    0,
                    entry.item.system.quantity,
                    1,
                    `Sell how many ${entry.item.name} (max: ${entry.item.system.quantity}) ?`,
                    `Selling Item`,
                    `Sell`,
                    'Cancel'
                );
                if (count > 0) {
                    const numberTaken = await utilitiesManager.purchaseItem(
                        this.actor,
                        entry.item,
                        true,
                        entry.cost,
                        -1,
                        count,
                        this.customer.actor
                    );
                    const currentCount = entry.item.system.quantity;
                    const newCount = currentCount - numberTaken;
                    if (newCount <= 0) {
                        this.customer.actor.deleteEmbeddedDocuments('Item', [entry.item.id]);
                    } else {
                        entry.item.update({ 'system.quantity': newCount });
                    }
                }
            } else {
                ui.notifications.error('Cannot trade with self');
            }
        }
    }

    /**
     * Remove a item from merchant so its no longer for sale
     *
     * @param {*} event
     */
    async _onItemRemoveClick(event) {
        const element = event.currentTarget;
        const li = element.closest('li');
        const itemId = li.dataset.itemId;
        const entry = this.sellingList.find((ent) => ent.item.id == itemId);
        if (entry.item) {
            if (await dialogManager.confirm(`Remove ${entry.item.name} from merchant?`, 'Remove Sale Item')) {
                this.actor.deleteEmbeddedDocuments('Item', [entry.item.id]);
            }
        }
    }

    /**
     * Edit quantity directly from list
     *
     * @param {*} event
     * @returns
     */
    async #setItemQuantity(event) {
        const element = event.currentTarget;
        const li = element.closest('li');
        const itemId = li.dataset.itemId;
        const entry = this.sellingList.find((ent) => ent.item.id == itemId);
        if (entry) {
            const item = entry.item;
            const oldValue = item.system.quantity;
            const newValue = await dialogManager.getQuantity(
                -1,
                Infinity,
                oldValue,
                `New Quantity`,
                `Quantity for ${item.name}`,
                'Update',
                'Cancel'
            );
            if (newValue == undefined) return;
            item.update({ 'system.quantity': newValue });
            console.warn(
                `${game.user.name} changed quantity of ${item.name} from ${oldValue} to ${newValue} on ${item.actor.name}`,
                { item }
            );
        }
    }
}
