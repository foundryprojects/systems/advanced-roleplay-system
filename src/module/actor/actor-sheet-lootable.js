// import { ARSActorSheet } from "./actor-sheet.js";
import { ARSNPCSheet } from './actor-sheet-npc.js';
import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
import * as debug from '../debug.js';

/**
 *
 *
 * This class will be used as chests, bags/etc
 *
 */
export class ARSLootableSheet extends ARSNPCSheet {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ['ars', 'sheet', 'actor', 'lootable', 'lootable-sheet'],
            template: 'systems/ars/templates/actor/lootable-sheet.hbs',
            actor: this.actor, // for actor access in character-sheet.hbs
            // width: 550,
            // height: 500,
            height: 'auto',
            // tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "main" }]
        });
    }

    /** @override */
    get template() {
        if (!this.actor.isOwner && !game.user.isGM) {
            if (this.canLoot) {
                return 'systems/ars/templates/actor/lootable-sheet.hbs';
            } else {
                return `systems/ars/templates/actor/lootable-limited-sheet.hbs`;
            }
        }
        return `systems/ars/templates/actor/lootable-sheet.hbs`;
    }

    /** @override */
    get title() {
        const actorName = this.token?.name ?? this.actor.name;
        if (this.canLoot) return `${actorName} [LOOT]`; // `;
        return super.title;
    }

    get canLoot() {
        return this.actor.isLootable && !this.actor.isOwner;
    }

    // give pcs limited view if lootable
    get permission() {
        if (game.user.isGM || !this.actor.isLootable) {
            return super.permission;
        }
        return Math.max(super.permission, 1);
    }

    /** @override to clean up lock states */
    async close() {
        this.actor.unLockLootable();
        return super.close();
    }

    /** @override to apply/manage locking lootable npc */
    async render(force, options) {
        console.log('actor-sheet-lootable.js render', { force, options }, this, this.object.system.opened, game.user.id);
        // when pc openes the loot window, set opened flag

        // const validLock = utilitiesManager.cleanStaleSheetLocks(this);
        const lockedBy = this.actor.isLockedLootable();
        if (
            this.object.type === 'lootable' &&
            force &&
            !game.user.isGM &&
            !game.paused &&
            (!lockedBy || lockedBy.id === game.user.id)
        ) {
            this.actor.lockLootable(game.user);
            utilitiesManager.chatMessage(
                ChatMessage.getSpeaker({ actor: game.user?.character }),
                'Looting Item',
                `${game.user.character?.name ?? game.user.name} is looting ${this.object?.token?.name}`,
                this.object?.token?.img
            );
            return super.render(force, options);
        } else if ((this.object.type === 'lootable' && lockedBy && lockedBy.id === game.user.id) || game.user.isGM) {
            return super.render(force, options);
        } else if (this.object.type === 'lootable') {
            if (lockedBy) {
                ui.notifications.error(`${lockedBy.name} is already looting ${this.object.name}`);
            } else if (game.paused) {
                ui.notifications.error(`${this.object?.token?.name} cannot be looted while game is paused.`);
                console.warn(`${this.object?.token?.name} cannot be looted while game is paused.`);
            }
            return undefined;
        }
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);
    }
}
