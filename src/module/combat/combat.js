import * as utilitiesManager from '../utilities.js';
import { ARS } from '../config.js';
import * as dialogManager from '../dialog.js';
import * as debug from '../debug.js';

export class CombatManager {
    /**
     * @summary Armor damage - system variant 2 uses armor damage system, 1 armor damage per die of damage
     * @param {Object} damage damage class object
     * @returns
     */
    static armorDamage(damage) {
        if (damage.roll.variant != '2' || !game.ars.config.settings.useArmorDamage) {
            return; // Early return for conditions not met
        }

        const armorDamageTotal = damage.isDamage ? damage.roll.dice[0]?.results?.length || 0 : 0;
        if (armorDamageTotal === 0 || !damage.target?.actor?.armors) {
            return; // No damage to process or no armors to apply damage
        }

        // get worn armor and apply damage to each if applicable - damage should only ever be applied to equipped armor
        const armorWorn = damage.target.actor.armors.filter((armor) => {
            return armor.isWornArmor || armor.isWornShield;
        });

        // iterate and apply damage
        armorWorn.forEach((armor) => {
            const currentAP = armor.system.protection.points.value;
            if (currentAP > 0) {
                const newAP = Math.max(0, currentAP - armorDamageTotal);
                armor.update({ 'system.protection.points.value': newAP });
                if (newAP < 1) {
                    dialogManager.showNotification(
                        `Your <b>${armor.name}</b> is no longer functional.`,
                        'OK',
                        'Protection Broken'
                    );
                }
            }
        });
    }

    /**
     * @summary: Attacks per round tracking
     * @returns various functions which drive the system
     */
    static attacksPerRoundTracker() {
        /**
         * @summary - On rolling initiative with a weapon, this method creates a #attacks tracking object if one does not exist or it updates the properties on an existing one
         * @param {*} initOptions
         * @param {*} combatant
         */
        async function init(initOptions, combatant) {
            // weapon attacks
            const usingWeapon = initOptions?.useWeapon;
            const useWeaponSpeed = game.ars.config.settings.initiativeUseSpeed;

            // if not using a weapon OR weapon speed is off, reset
            if (!usingWeapon || !useWeaponSpeed) {
                this.reset(combatant.actor);
                return;
            }

            const mostFirst = +game.ars.config.settings.systemVariant !== 2;
            const currentRound = game.combat?.round || 1;
            const weaponId = initOptions?.item.id;
            const weaponAttacksPerRounds = combatant.actor.getBestAttackRate(initOptions?.item);
            const arAttacksPerRound = combatant.actor.calculateAttacksPer(weaponAttacksPerRounds, mostFirst);
            const atksRndTooltip = `: ${initOptions?.item.name} @ ${initOptions?.item.bestAttackRate}`;
            let attacksPerRoundTracking = await combatant.actor.getFlag('ars', 'attacksPerRoundTracking');

            // new tracking object
            if (!attacksPerRoundTracking) {
                attacksPerRoundTracking = {
                    weaponId,
                    arAttacksPerRound,
                    index: 0,
                    remainingAttacks: arAttacksPerRound[0],
                    currentRound,
                    attacksPerRoundDisplay: atksRndTooltip
                };
            }
            // update/reset
            else {
                const lastRound = attacksPerRoundTracking.currentRound;
                const isNewRound = lastRound < currentRound;
                const fullReset = currentRound - lastRound > 1 || weaponId !== attacksPerRoundTracking.weaponId;
                if (isNewRound || fullReset) {
                    // get correct index and #attacks
                    const fullReset = currentRound - lastRound > 1 || weaponId !== attacksPerRoundTracking.weaponId;
                    const lastIndex = attacksPerRoundTracking.index;
                    const nextIndex = lastIndex + 1;
                    const index = fullReset || arAttacksPerRound[nextIndex] === undefined ? 0 : nextIndex;
                    const remainingAttacks = arAttacksPerRound[index];

                    // update object
                    Object.assign(attacksPerRoundTracking, {
                        weaponId,
                        currentRound,
                        index,
                        remainingAttacks,
                        atksRndTooltip,
                    });
                }
            }

            // save
            await combatant.actor.setFlag('ars', 'attacksPerRoundTracking', attacksPerRoundTracking);
        }

        /**
         * @summary - When making an attack, perform validation and special logic before comitting the attack, such as checking if they switched targets/weapons, thereby forfeiting their extra attacks
         * @param {*} dice
         * @returns
         */
        async function makeAttack(dice, target) {
            // todo: this method, apart from some notices, is pretty useless at this point. It serves as a mid point between selecting the attack
            // and actually comitting it. Now, originally, this was where I tried all validtion logic, like detecting target switch ect.. but
            // frankly, all of that is just a slipery slope and will just wind up creating a complex mess.
            // im leaving this in for now because it may come in handy later and its not doing any harm.

            // Get the actor and weapon
            // Get the attacks per round tracking object
            const actor = dice.actor;
            const weapon = dice.item;
            const weaponId = weapon.id;
            const attacksPerRoundTracking = await actor.getFlag('ars', 'attacksPerRoundTracking');
            const differentWeapon = weaponId !== attacksPerRoundTracking?.weaponId;

            // If tracker doesn't exist, return
            if (!attacksPerRoundTracking) {
                console.log(`attack tracking debug - no object found for ${actor.name}`);
                return;
            }

            // notty remaining attacks
            const remainingAttacks = attacksPerRoundTracking.remainingAttacks;
            if (weaponId && remainingAttacks === 0) {
                ui.notifications.notify(`You're out of attacks this round!`);
                // Exit the function if no remaining attacks
                return;
            }
            // else {
            //     ui.notifications.notify(`REMAINING ATTACKS: ${attacksPerRoundTracking.remainingAttacks}`);
            // }

            // Check if the weapon has changed
            if (differentWeapon) {
                // attacksPerRoundTracking.remainingAttacks = 0;
                ui.notifications.notify(
                    `You're attacking with a different weapon than what you rolled init with - which forfiets your additional attacks.`
                );
            }

            // Update the attacks per round tracking object on the actor
            await actor.setFlag('ars', 'attacksPerRoundTracking', attacksPerRoundTracking);
        }

        /**
         * @summary - After committing an attack, update the flag with the new remaining attacks
         * @param {*} actor
         * @param {*} reset - reset remaining attacks on new round
         * @returns
         */
        async function updateTracking(actor, reset) {
            // get tracking object if exists and update the remaining attacks
            const attacksPerRoundTracking = await actor.getFlag('ars', 'attacksPerRoundTracking');
            if (!attacksPerRoundTracking) return;

            // reset or decrement
            if (reset) attacksPerRoundTracking.remainingAttacks = 0;
            else {
                attacksPerRoundTracking.remainingAttacks = Math.max(0, attacksPerRoundTracking.remainingAttacks - 1);
            }

            // update
            await actor.setFlag('ars', 'attacksPerRoundTracking', attacksPerRoundTracking);
        }

        /**
         * @summary - Returns display information on remaining attacks, to be consumed by other u.i., such as the modifier dialog
         * @param {*} actor
         * @returns
         */
        async function displayRemaining(actor) {
            const attacksPerRoundTracking = await actor.getFlag('ars', 'attacksPerRoundTracking');
            if (!attacksPerRoundTracking) return '';
            const remainingAttacksDisplay = `<div><b> Remaining Attacks: ${attacksPerRoundTracking.remainingAttacks}</b></div>`;
            return remainingAttacksDisplay;
        }

        /**
         * @summary - Cleanup data for actor combatant
         * @param {*} actor
         */
        async function reset(actor) {
            await actor.unsetFlag('ars', 'attacksPerRoundTracking');
            console.log(`attack tracking debug - ${actor.name} - reset and stop tracking`);
        }

        /**
         * @summary - Cleanup all data for combatants
         * @param {*} combat
         */
        async function end(combat) {
            // foreach combatant that has the tracking flag, remove the data
            for (let combatant of combat.combatants) {
                // Get the actor associated with the combatant
                let actor = combatant.actor;
                if (actor) {
                    this.reset(actor);
                }
            }
        }

        return {
            init,
            makeAttack,
            updateTracking,
            displayRemaining,
            reset,
            end,
        };
    }

    /**
     * @summary: Martial combat
     * @returns various functions which drive the system
     */
    static martialCombat() {
        // private debug bit
        const _debug = false;

        /**
         * @summary Perform entire martial combat stack if attack is successful.
         *          This encompasses getting the correct martial combat type/result from table based on total from roll
         *          Additionally, perform a knockout roll if applicable
         *          Send all information to chat to be abjudicated manually via other actions, dmg, effect, ect.
         *
         * @param {*} dice
         */
        async function makeAttack(dice, target) {
            const variant = 2;
            // if (variant !== 2) return false;

            const { actor, roll: attackRoll, action, weapon } = dice;
            const mType = action?.sMartialCombat || weapon?.sMartialCombat;
            const total = Math.min(Math.max(attackRoll.total, 0), 20);
            const specializationRange = await getSpecializedRange(actor, action || weapon);
            const rows = await getRowsAround(mType, total, specializationRange);
            const primaryRow = rows[total];
            const primaryDamage = -primaryRow.damage;
            const mainMove = primaryRow.move ? ` - <b>${utilitiesManager.capitalize(primaryRow.move)}</b>` : '';
            const showKO = mType !== 'wrestling' && mType !== 'overbear';
            const contentParts = [
                `<div class="mc-header"><h3>${utilitiesManager.capitalize(mType)}${mainMove}</h3></div>`,
                `<div class="mc-row"><b>Total: ${total}</b></div>`,
            ];

            for (const key in rows) {
                const row = rows[key];
                const isPrimaryRow = parseInt(key) === total;
                const rowClass = isPrimaryRow ? 'mc-primary-row' : 'mc-row';

                const name = row.move
                    ? `<b>${utilitiesManager.capitalize(row.move)}:</b> <i>${utilitiesManager.capitalize(row.move)}</i>, `
                    : '';
                const damage = row.damage ? `<b><a>Damage:</a></b> <i>${utilitiesManager.capitalize(row.damage)}</i>, ` : '';
                const KO = showKO && row.KO ? ` <b>KO%:</b> <i>${row.KO}</i>, ` : '';
                const hold = mType === 'wrestling' && row.effect === 'Restrained' ? '<b>Hold:</b> <i>true</i>, ' : '';
                const breakHold = mType === 'wrestling' && row.effect === 'Break' ? '<b>Break Hold:</b> <i>true</i>' : '';

                if (mType !== 'overbear') {
                    contentParts.push(`<div class="${rowClass}">${name}${damage}${KO}${hold}${breakHold}</div>`);
                }
            }

            if (mType === 'overbear') {
                contentParts.push(`<div class="mc-primary-row"><b>Successfully took down Opponent!</b></div>`);
            }

            if (showKO && primaryRow.KO) {
                const roll = await new Roll('1d100').roll();
                if (game.dice3d) await game.dice3d.showForRoll(roll, game.user, true);

                const knockoutMessage =
                    roll._total <= primaryRow.KO
                        ? '<div class="mc-knockout-message">Target has been knocked <a>Unconscious</a>!</div>'
                        : '';
                contentParts.push(`<div class="mc-row"><b>K.O. Roll Result:</b> <i>${roll._total}</i></div>${knockoutMessage}`);
            }

            return ChatMessage.create({
                speaker: ChatMessage.getSpeaker({ actor }),
                content: `<div class="martial-combat-card flexcol">${contentParts.join(
                    ''
                )}<div class="mc-non-owner-only-view">${game.i18n.localize('ARS.unknownAction')}</div></div>`,
                flags: {
                    world: {
                        context: {
                            action: dice.action,
                            actorUuid: actor.uuid,
                            itemUuid: dice.item.uuid,
                        },
                    },
                },
                system: {
                    damageType: 'bludgeoning',
                    damage: primaryDamage,
                    targetUuid: target.document.actorId,
                },
            });

            // #region utilities

            /**
             * Utility to return the rows around the row retrieved from total, compensating for clamped 0/20.
             * Used when the character is specialized in unarmed combat and has 'chart bonuses' in which they can choose the result, up or down by +N
             * @param {string} mType
             * @param {number} total
             * @param {number} specializationRange
             * @returns {Promise<Object>}
             */
            async function getRowsAround(mType, total, specializationRange = 0) {
                const clampedTotal = Math.min(Math.max(total, 0), 20);
                let startRow = clampedTotal - specializationRange;
                let endRow = clampedTotal + specializationRange;

                // Adjust if out of bounds
                if (startRow < 0) {
                    endRow += Math.abs(startRow);
                    startRow = 0;
                }
                if (endRow > 20) {
                    startRow -= endRow - 20;
                    endRow = 20;
                }

                startRow = Math.max(startRow, 0);
                const rows = {};
                for (let i = startRow; i <= endRow; i++) {
                    rows[i] = await _martialCombatTableLookup(mType, i);
                }

                return rows;
            }

            /**
             * Attempt to parse out the rank of prof. (aka specialization) for a particular unarmed combat.
             * This is used to drive the chart bonus you get for being prof. in unarmed combat, and to drive the display of your various choices in that context.
             * @param {*} actor
             * @param {*} actionOrWeapon
             * @returns
             */
            async function getSpecializedRange(actor, actionOrWeapon) {
                if (actor.isNPC) return 0;

                try {
                    for (const profItem of actor.proficiencies) {
                        const appliedTo = Object.values(profItem.system.appliedto);
                        if (
                            appliedTo.some((weapon) => weapon.id === actionOrWeapon?.id) ||
                            profItem.name.toLowerCase().includes(actionOrWeapon.sMartialCombat)
                        ) {
                            return profItem.system.proficiencies?.cost ?? 1;
                        }
                    }
                } catch (e) {
                    return 0;
                }
                return 0;
            }

            // #endregion
        }

        /**
         * @summary Get penalties for attempting to wrestle in armor
         * @param {*} actor
         * @returns
         */
        async function getWrestleInArmorMod(actor) {
            const variant = 2;
            // if (variant !== 2) return false;

            let wrestlePenalty = 0;

            // get all armor worn
            const arArmorsWorn = actor.armors.filter((armor) => {
                return armor.isWornArmor || armor.isWornShield;
            });

            // get armor penalty table for wrestling
            const tblAttackPenaltyWrestlingArmor = ARS.martialCombat[variant].wrestleInArmorPenalties;

            // iterate each armor found and cross check with the armor penalty table
            arArmorsWorn.forEach((armor) => {
                const equipArmorName = armor.name.replace(/\s+/g, '').toLowerCase();
                const equipArmorStyle = (armor.system.armorstyle || '').replace(/\s+/g, '').toLowerCase();
                for (const [armorName, penalty] of Object.entries(tblAttackPenaltyWrestlingArmor)) {
                    if (equipArmorName.includes(armorName) || equipArmorStyle.includes(armorName)) {
                        wrestlePenalty = penalty;
                        break;
                    }
                }
            });

            if (wrestlePenalty === 0) return undefined;
            else return { formula: ['@wrestleInArmor'], rollData: { wrestleInArmor: wrestlePenalty } };
        }

        /**
         * @summary Get size difference when attempting overbear and return modifiers
         * @param {*} actor
         * @returns
         */
        async function getSizeModifier(actor, target) {
            // no target, wtf you doing?
            if (!target) return;

            // Async function to get an actor's size
            const getActorSize = (actor) => {
                const sizes = {
                    T: { name: 'tiny', value: 1 },
                    S: { name: 'small', value: 2 },
                    M: { name: 'medium', value: 3 },
                    L: { name: 'large', value: 4 },
                    H: { name: 'huge', value: 5 },
                    G: { name: 'gargantuan', value: 6 },
                };
                const size = actor.system.attributes.size || 'M';
                let sizeNumber = 3; // Default to Medium if size not found
                for (const [abbreviation, sizeData] of Object.entries(sizes)) {
                    if (size.includes(sizeData.name) || size.includes(abbreviation)) {
                        sizeNumber = sizeData.value;
                        break; // Exit the loop once the size is found
                    }
                }
                return sizeNumber;
            };
            const actorSize = getActorSize(actor);
            const targetSize = getActorSize(target);
            const sizeDifference = actorSize - targetSize;
            const sizeModifier = sizeDifference * 4;
            if (sizeModifier === 0) return undefined;
            else return { formula: ['@sizeModifier'], rollData: { sizeModifier: sizeModifier } };
        }

        /**
         * @summary Extrapolate the currect martial type from the actions properties (must be exact name), else return false
         * @private
         * @param {*} actionOrWeapon
         * @returns
         */
        async function getMartialProperties(actionOrWeapon) {
            const variant = 2;
            // if (variant !== 2) return false;

            const properties = actionOrWeapon?.properties || actionOrWeapon.system?.attributes?.properties || [''];
            const validProperties = ['wrestling', 'punching', 'martialArts', 'overbear'];

            // Convert properties to lowercase for case-insensitive comparison
            const lowerCaseProperties = properties.map((prop) => prop.toLowerCase());
            const result = validProperties.find((prop) => lowerCaseProperties.includes(prop.toLowerCase())) || false;

            return result;
        }

        /**
         * @summary Private utility to return correct martial table row
         * @private
         * @param {*} sMartialCombat
         * @param {*} total
         */
        async function _martialCombatTableLookup(sMartialCombat, total) {
            const variant = 2;
            // if (variant !== 2) return false;
            const validCombatTypes = ['wrestling', 'punching', 'martialArts'];
            if (validCombatTypes.includes(sMartialCombat)) {
                return ARS.martialCombat[variant][sMartialCombat][total];
            }

            return {}; // failsafe
        }

        return {
            makeAttack,
            getWrestleInArmorMod,
            getSizeModifier,
            getMartialProperties,
        };
    }
}
