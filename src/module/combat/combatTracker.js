import { ARS } from '../config.js';
import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
import * as debug from '../debug.js';
import { CombatManager } from './combat.js';
import { SocketManager } from '../sockets.js';
import { isControlActive, isAltActive, isShiftActive } from '../utilities.js';
import { ARSPsionics } from '../psionics/psionics.js';

export class ARSCombatTracker extends CombatTracker {
    // async _preCreate(data, options, id) {
    //     console.log('combatTracker.js ARSCombatTracker _preCreate', { data, options, id });
    // }
    // async _onUpdate(data, options, id) {
    //     console.log('combatTracker.js ARSCombatTracker _onUpdate', { data, options, id });
    // }

    /** @inheritdoc */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: 'combat',
            template: 'systems/ars/templates/sidebar/combat-tracker.hbs',
            title: 'COMBAT.SidebarTitle',
            scrollY: ['.directory-list'],
        });
    }

    /** @override */
    createPopout() {
        const pop = super.createPopout();
        pop.initialize({ combat: this.viewed, render: true });
        pop.options.resizable = true;
        pop.options.height = 650;
        return pop;
    }

    /** @override
     * we do this so we can inject "game" and a few other things into the combat.turns and turns
     *
     * turns[x].index gets added so we can look up the correct index even if the turns list has hidden entries
     * turns[x].combatant is added so we can look at token.object.isVisible (so we can hide things their token can't see) and a few other things.
     *
     */
    async getData(options) {
        const data = await super.getData(options);

        data.game = game;
        // data.turn.css = `${data.turn.css}`
        // console.log('combatTracker.js getData', { data }, this);

        if (data.combat?.turns) {
            for (let [i, combatant] of data.combat.turns.entries()) {
                data.combat.turns[i].index = i;
                const turnsIndex = data.turns.findIndex((entry) => {
                    return entry.id === data.combat.turns[i].id;
                });
                if (data.turns[turnsIndex]) {
                    data.turns[turnsIndex].index = i;
                    data.turns[turnsIndex].combatant = data.combat.turns[i];
                    // Actor and Token effects records instead of just the effect image
                    data.turns[turnsIndex].effects = new Set();
                    if (combatant.token) {
                        data.turns[turnsIndex].effects = new Set(combatant.token.actor.temporaryEffects);
                    }
                    if (combatant.actor) {
                        for (const effect of combatant.actor.temporaryEffects) {
                            if (effect.img) data.turns[turnsIndex].effects.add(effect);
                        }
                    }
                }
            }
        }
        // data.turns.for(turn => { console.log("combatTracker.js getData", { turn }) })

        return data;
    }

    /**
     * Handle click events on Combat control buttons
     * @private
     * @param {Event} event   The originating mousedown event
     */
    async _onCombatControl(event) {
        event.preventDefault();
        const combat = this.viewed;
        const ctrl = event.currentTarget;
        if (ctrl.getAttribute('disabled')) return;
        else ctrl.setAttribute('disabled', true);
        try {
            const fn = combat[ctrl.dataset.control];
            if (fn) {
                const processCommand =
                    ctrl.dataset.control == 'nextRound'
                        ? await dialogManager.confirm('Are you sure you want to advance to next round?')
                        : true;
                if (processCommand) await fn.bind(combat)();
            }
        } finally {
            ctrl.removeAttribute('disabled');
        }
    }

    /**
     * Handle a Combatant control toggle
     * @private
     * @param {Event} event   The originating mousedown event
     */
    async _onCombatantControl(event) {
        // console.log("combatTracker.js _onCombatantControl", { event });
        event.preventDefault();
        event.stopPropagation();
        const btn = event.currentTarget;
        const li = btn.closest('.combatant');
        const combat = this.viewed;
        const c = combat.combatants.get(li.dataset.combatantId);

        console.log('combatTracker.js _onCombatantControl', { combat, c }, this, event.shiftKey);

        // Switch control action
        switch (btn.dataset.control) {
            // Toggle combatant visibility
            case 'toggleHidden':
                return c.update({ hidden: !c.hidden });

            // Toggle combatant defeated flag
            case 'toggleDefeated':
                return this._onToggleDefeatedStatus(c);

            // toggle casting state
            case 'toggleCasting':
                await c.setFlag('world', 'initCasting', !c.getFlag('world', 'initCasting'));
                // await c.token.setFlag("world", "initCasting", !c.token.getFlag("world", "initCasting"));
                // this.render();
                break;

            // Actively ping the Combatant
            case 'pingCombatant':
                return this._onPingCombatant(c);

            // Roll combatant initiative
            case 'rollInitiative':
                return await utilitiesManager.rollCombatantInitiative(c, combat, isControlActive());

            // delay turn to end of turn order +1
            case 'delayTurn':
                const lastInit = combat._getLastInInitiative();
                console.log('_onCombatantControl', { c, combat, lastInit }, c.id, combat.combatant.id);
                if (c.id == combat.combatant.id) {
                    // if the combatant and this person are the same, flip to next turn
                    // and reset initiative to last +1
                    await combat.nextTurn(c);
                    return c.update({ initiative: lastInit + 1 });
                }
                break;

            // Target the Combatant
            case 'targetCombatant':
                {
                    const targetedTokenIds = Array.from(game.user.targets).map((token) => token.id);
                    // if user is GM, try and set the target for the person with active initiative if they own
                    // the actor and are connected
                    if (game.user.isGM) {
                        const sourceToken = combat.combatant.token;
                        const defaultLevel = sourceToken.actor.ownership?.['default'] || 0;
                        for (const activeUser of game.users) {
                            // find active game.user that has perms for this token, if so change target for them
                            if (
                                activeUser.active &&
                                !activeUser.isGM &&
                                game.user.id != activeUser.id &&
                                (defaultLevel >= 3 || sourceToken.actor.ownership?.[activeUser.id] >= 3)
                            ) {
                                if (game.user.id != activeUser.id) {
                                    // clear all targets at user side
                                    SocketManager.notify(game.user.id, activeUser.id, 'clearTargetTokens', {});
                                    // set all targets current on user side
                                    targetedTokenIds.forEach((tokenId) => {
                                        SocketManager.notify(game.user.id, activeUser.id, 'TargetToken', {
                                            shiftKey: true, //event.shiftKey,
                                            tokenId: tokenId,
                                        });
                                    });
                                }
                            }
                        }
                    }
                    // }
                    // const _timeout1 = setTimeout(async () => targetToken.object._refreshTarget(), 1000);
                }
                break;
        }
    }

    async _onCombatCycle(event) {
        console.log(`ARSCombatTracker _onCombatCycle`, { event });
        await super._onCombatCycle(event);
        // event.preventDefault();
        // const btn = event.currentTarget;
        // const combat = game.combats.get(btn.dataset.documentId);
        // if (!combat) return;
        // await combat.activate({ render: false });
    }
}

/**
 * Extend Combat to tweak initiative order/etc
 */
export class ARSCombat extends Combat {
    async _onUpdate(data, options, id) {
        console.log('combatTracker.js ARSCombat _onUpdate', { data, options, id });

        await this.combatTurn(data, options, id);
        super._onUpdate(data, options, id);
        this.#handleCombatantSheetRefreshes();
    }

    _onCreate(data, options, userId) {
        super._onCreate(data, options, userId);
        this.#handleCombatantSheetRefreshes();
    }
    _onDelete(options, userId) {
        super._onDelete(options, userId);
        this.#handleCombatantSheetRefreshes();
    }

    /**
     * refresh sheets for actor and combat huds.
     */
    #handleCombatantSheetRefreshes() {
        for (const actor of game.combat.combatants.map((co) => co.actor)) {
            if (actor.isOwner && actor.sheet && actor.sheet.rendered) {
                actor.sheet.render();
            }
        }

        // refresh active combat huds for initiative buttons (end turn/etc)
        for (const combatant of this.combatants) {
            const token = combatant?.token?.object;
            if (token.combatHud && token.actor.isOwner) token.combatHud.render(true);
        }
    }
    /**@override so we can reset the current initiative indicator to 0 on rollAll*/
    async rollAll(options) {
        const ids = this.combatants.reduce((ids, c) => {
            if (c.isOwner && (c.initiative === undefined || c.initiative === null)) ids.push(c.id);
            return ids;
        }, []);
        const result = await this.rollInitiative(ids, options);
        await this.update({ turn: 0 });
        return result;
    }

    async rollInitiativeIndividual(
        combatant,
        { formula = null, updateTurn = true, rollData = undefined, messageOptions = {} } = {}
    ) {
        // console.log('combatTracker.js rollInitiativeIndividual() ', { combatant, formula, updateTurn, messageOptions });
        // const rawformula = formula;
        // Structure input data
        const currentId = this.combatant?.id;
        const chatRollMode = game.settings.get('core', 'rollMode');

        // Get Combatant data (non-strictly)
        if (!combatant?.isOwner) return null;

        // Produce an initiative roll for the Combatant
        const roll = combatant.getInitiativeRoll(formula, rollData);
        await roll.evaluate();
        // if (game.dice3d) game.dice3d.showForRoll(roll, game.user, utilitiesManager.diceRollModeVisibility(roll.rollMode));

        const updates = [{ _id: combatant.id, initiative: roll.total }];

        // Construct chat message data
        const toolHTML = await roll.getTooltip();
        const tooltip = await TextEditor.enrichHTML(toolHTML);
        // console.log('combatTracker.js rollInitiativeIndividual() ', { toolHTML, tooltip }, roll.getTooltip());
        const content = await renderTemplate('systems/ars/templates/dice/dice-roll.hbs', {
            ARS,
            isGM: game.user.isGM,
            isDM: game.user.isDM,
            actor: combatant.actor,
            roll,
            tooltip,
            isInitiative: true,
            img: messageOptions?.img ?? 'systems/ars/icons/general/d10.webp',
            flavor: messageOptions.flavor
                ? messageOptions.flavor
                : game.i18n.format('COMBAT.RollsInitiative', { name: combatant.name }),
            rawformula: roll.rawformula,
            formula: roll.formula,
            total: roll.total,
        });
        const chatData = {
            content: content,
            author: game.user.id,
            speaker: ChatMessage.getSpeaker({
                actor: combatant.actor,
                token: combatant.token,
                alias: combatant.name,
            }),
            rolls: [roll],
            rollMode: chatRollMode,
            style: game.ars.const.CHAT_MESSAGE_STYLES.IC,
        };

        // If the combatant is hidden, use a private roll unless an alternative rollMode was explicitly requested
        chatData.rollMode =
            'rollMode' in messageOptions
                ? messageOptions.rollMode
                : combatant.hidden
                ? CONST.DICE_ROLL_MODES.PRIVATE
                : chatRollMode;

        // Update multiple combatants
        await this.updateEmbeddedDocuments('Combatant', updates);

        // Ensure the turn order remains with the same combatant
        if (updateTurn && currentId) {
            await this.update({ turn: this.turns.findIndex((t) => t.id === currentId) });
        }
        ChatMessage.applyRollMode(chatData, chatData.rollMode);
        ChatMessage.create(chatData);
        return this;
    }

    /**
     * Sort initiative lowest to highest
     *
     * @param {Token} a
     * @param {Token} b
     */
    _sortCombatants(a, b) {
        // return ((a.initiative > b.initiative) ? 1 : -1);
        const ia = Number.isNumeric(a.initiative) ? a.initiative : 9999;
        const ib = Number.isNumeric(b.initiative) ? b.initiative : 9999;
        const ascendingInitiative = game.settings.get('ars', 'InitiativeAscending');
        let ci = ascendingInitiative ? ia - ib : ib - ia;
        if (ci !== 0) return ci;
        // dont sort this
        // let [an, bn] = [a.token?.nameRaw || "", b.token?.nameRaw || ""];
        // let cn = an.localeCompare(bn);
        // if (cn !== 0) return cn;

        // lastly...
        return a.tokenId - b.tokenId;
    }

    _getLastInInitiative() {
        let lastInit = 0;
        this.combatants.forEach((element) => {
            if (element.initiative > lastInit) lastInit = element.initiative;
        });
        return lastInit;
    }
    _getFirstInInitiative() {
        let firstInit = 0;
        this.combatants.forEach((element) => {
            if (element.initiative < firstInit) firstInit = element.initiative;
        });
        return firstInit;
    }
    /**
     * Returns the current turns initiative value
     *
     * @returns Number
     */
    _getCurrentTurnInitiative() {
        const thisTurn = this.turn;
        const result = this.turns[thisTurn]?.initiative || 0;
        return result;
    }
    /**
     * @override to remove time passage (it's only advanced at end of round)
     *
     * Advance the combat to the next turn
     * @return {Promise<Combat>}
     */
    async nextTurn(combatantDelayTurn = null) {
        console.log('combatTracker.js nextTurn START', this);

        // using this to try and block race condition where system
        // thinks initiative has started and adds last person in init to roll
        if (this.getFlag('ars', 'beginningOfRound')) {
            console.warn(`combatTracker.js nextTurn() beginningOfRound TRUE, toggling`, this);
        }
        // await this.setFlag('ars', 'beginningOfRound', false);
        await utilitiesManager.runAsGM({
            operation: 'setCombatTrackerFlag',
            user: game.user.id,
            combatTrackerId: this.id,
            flag: {
                tag: 'beginningOfRound',
                data: false,
            },
        });

        let turn = this.turn ?? -1;
        let skip = this.settings.skipDefeated;

        // Determine the next turn number
        let next = null;
        if (skip && !combatantDelayTurn) {
            for (let [i, t] of this.turns.entries()) {
                if (i <= turn) continue;
                if (t.isDefeated) continue;
                next = i;
                break;
            }
        } else if (combatantDelayTurn) {
            for (let [i, t] of this.turns.entries()) {
                if (i < turn) continue;
                if (combatantDelayTurn.id === t.id) continue;
                next = i;
                break;
            }
        } else next = turn + 1;

        // Maybe advance to the next round
        let round = this.round;
        if (this.round === 0 || next === null || next >= this.turns.length) {
            return this.nextRound();
        }

        // Update the document, passing data through a hook first
        const updateData = { round, turn: next };
        // Classic systems update time on the round, not each turn
        // const updateOptions = { advanceTime: CONFIG.time.turnTime, direction: 1 };
        Hooks.callAll('combatTurn', this, updateData);
        return this.update(updateData);
        // return this.update({ round: round, turn: next });
    }

    /**
     * @override to make adjustments to time passage
     *
     * Advance the combat to the next round
     * @return {Promise<Combat>}
     */
    async nextRound() {
        console.log('combatTracker.js nextRound START', this);
        let turn = this.turn === null ? null : 0; // Preserve the fact that it's no-one's turn currently.
        if (this.settings.skipDefeated && turn !== null) {
            turn = this.turns.findIndex((t) => !t.isDefeated);
            if (turn === -1) {
                ui.notifications.warn('COMBAT.NoneRemaining', { localize: true });
                turn = 0;
            }
        }
        // let advanceTime = Math.max(this.turns.length - (this.turn || 0), 0) * CONFIG.time.turnTime;
        // advanceTime += CONFIG.time.roundTime;
        // OSR systems advance time by round counter, not turns, per round.
        const advanceTime = CONFIG.time.roundTime;
        return this.update({ round: this.round + 1, turn }, { advanceTime });
    }
    /**
     * @override time is roundTime, not turnTime for a round
     *
     * Rewind the combat to the previous round
     * @return {Promise<Combat>}
     */
    async previousRound() {
        let turn = this.round === 0 ? 0 : Math.max(this.turns.length - 1, 0);
        if (this.turn === null) turn = null;
        const round = Math.max(this.round - 1, 0);
        // let advanceTime = -1 * (this.turn || 0) * CONFIG.time.turnTime;
        // let advanceTime = -1 * (this.turn || 0) * CONFIG.time.roundTime;
        const advanceTime = round > 0 ? -CONFIG.time.roundTime : 0;
        return this.update({ round, turn }, { advanceTime });
    }

    /**
     * @override no time for just previous persons play turn
     *
     * Rewind the combat to the previous turn
     * @return {Promise<Combat>}
     */
    async previousTurn() {
        if (this.turn === 0 && this.round === 0) return this;
        else if (this.turn <= 0 && this.turn !== null) return this.previousRound();
        // const advanceTime = -1 * CONFIG.time.turnTime;
        // return this.update({ turn: (this.turn ?? this.turns.length) - 1 }, { advanceTime });
        return this.update({ turn: (this.turn ?? this.turns.length) - 1 });
    }

    /**
     *
     * Run "new round" processes.
     *
     * @param {*} combat
     */
    processNewRound() {
        console.log('combatTracker.js newRound', { combat });
        this.combatants.forEach(async (entry) => {
            const actor = entry.actor;

            // #attacks tracking - reset remaining attacks on new round
            CombatManager.attacksPerRoundTracker().updateTracking(actor, true);

            // #psionics 1e - record base psp for all psionic combatants
            ARSPsionics.initPsionicCombatTracking(actor);

            // clear saveCache
            this.processOneRoundFlags(entry);

            // clear init.actionText from last round
            if (actor.type === 'character') entry.token.unsetFlag('world', 'init.actionText');
        });
    }

    /**
     * ReRoll initative at the start of a round.
     * @param {Boolean} forceRoll
     */
    async reRollInitiative(forceRoll) {
        const rollInitEachRound = game.settings.get('ars', 'rollInitEachRound');
        if (rollInitEachRound || forceRoll) {
            const systemVariant = game.settings.get('ars', 'systemVariant');
            const initSideVSide = game.settings.get('ars', 'initSideVSide');
            const initManualRolls = game.settings.get('ars', 'initManualRolls');
            // using this to try and block race condition where system
            // things initiative has started and adds last person in init to roll
            await this.setFlag('ars', 'beginningOfRound', true);
            await this.resetAll();
            // we do this to block sounds due to how foundry handles initiative
            // need to figure out how to detect when start of round rolls and mute during that
            // but foundry isnt aware of new round start rolls versus next person getting initiative, it sees it all the same?
            game.ars.config.muteInitiativeSound = true;

            if (initManualRolls) {
                // Manually roll initiative
                console.log('Manual initiative mode enabled.');
            } else if (initSideVSide) {
                // roll initiative side v side only
                const _roll = async (formula) => {
                    let roll;
                    try {
                        roll = await new Roll(String(formula)).evaluate();
                        // if (game.dice3d) await game.dice3d.showForRoll(roll, game.user, true);
                    } catch (err) {
                        console.error(err);
                        ui.notifications.error(`Initiative dice formula evaluation failed: ${err.message}`);
                        return null;
                    }
                    roll.diceToolTip = await roll.getTooltip();
                    return roll;
                };
                const initiativeFormula = game.settings.get('ars', 'initiativeFormula');
                //roll dice
                const npcRoll = await _roll(initiativeFormula);
                const pcRoll = await _roll(initiativeFormula);
                for (const c of this.combatants) {
                    if (c.isOwner && (c.initiative === undefined || c.initiative === null)) {
                        // console.log("combatTracker.js reRollInitiative", { c }, c.token.disposition);
                        switch (c.token.disposition) {
                            case game.ars.const.TOKEN_DISPOSITIONS.FRIENDLY:
                            case game.ars.const.TOKEN_DISPOSITIONS.NEUTRAL:
                                await c.update({ initiative: pcRoll.total });
                                break;

                            case game.ars.const.TOKEN_DISPOSITIONS.HOSTILE:
                                await c.update({ initiative: npcRoll.total });
                                break;

                            default:
                                await c.update({ initiative: npcRoll.total });
                                break;
                        }
                        // if (c.isNPC) {
                        //     await c.update({ 'initiative': npcRoll.total });
                        // } else {
                        //     await c.update({ 'initiative': pcRoll.total });
                        // }
                    }
                }
                await this.update({ turn: 0 });
                //show chat msg
                utilitiesManager.chatMessageForSideVSideInitiative(
                    ChatMessage.getSpeaker(),
                    `Opponents Initiative ${npcRoll.total}`,
                    npcRoll
                );
                utilitiesManager.chatMessageForSideVSideInitiative(
                    ChatMessage.getSpeaker(),
                    `Party Initiative ${pcRoll.total}`,
                    pcRoll
                );
            } else {
                // roll all npcs
                for (const c of this.combatants) {
                    if (c.isOwner && c.isNPC && (c.initiative === undefined || c.initiative === null)) {
                        const lastInitiativeFormula = c.token.getFlag('world', 'lastInitiativeFormula');
                        const lastInitiativeMod = c.token.getFlag('world', 'lastInitiativeMod');
                        const lastAction = c.token.getFlag('world', 'init.actionText');
                        let formula = lastInitiativeFormula ? lastInitiativeFormula : c._getInitiativeFormula();
                        if (!lastInitiativeFormula && Number(systemVariant) === 2) formula += ' + @initSizeMod';
                        // speedfactor: initOptions.initSpeedMod,
                        // casttime: initOptions.initSpeedMod,
                        await utilitiesManager.rollCombatantInitiative(c, this, true, {
                            formula: formula,
                            initSpeedMod: lastInitiativeMod,
                            rollMode: 'blindroll',
                            actionText: lastAction ? lastAction : 'Automated-Initiative',
                        });
                    }
                }
            }
            game.ars.config.muteInitiativeSound = false;
        }
    }

    /**
     *
     * Process various changes/updates for start of new round
     *
     * @param {*} tokenDocument
     */
    processOneRoundFlags(tokenDocument) {
        // console.log("processOneRoundFlags tokenDocument", { tokenDocument });
        // flush saveCache
        // tokenDocument.unsetFlag("world", "saveCache");
        tokenDocument.token.unsetFlag('world', 'saveCache');
        //flush is Casting flag
        tokenDocument.token?.combatant.unsetFlag('world', 'initCasting');
    }

    /**
     *
     * function run from hook for updateCombat
     *
     * @param {*} combat
     * @param {*} updateData
     * @param {*} options
     * @param {*} userId
     */
    // export async function _updateCombat(combat, updateData, options, userId) {
    async combatTurn(data, options, id) {
        console.log('combatTracker.js combatTurn 1', { data, options, id }, this);
        const initVolumeSound = game.settings.get('ars', 'initVolumeSound');
        const roundUpdate = foundry.utils.hasProperty(data, 'round');
        const turnUpdate = foundry.utils.hasProperty(data, 'turn');
        if (roundUpdate && !game.ars.config.muteInitiativeSound) {
            const initRoundSound = game.settings.get('ars', 'initRoundSound');
            const playRoundSound = game.settings.get('ars', 'playRoundSound');
            if (playRoundSound) {
                foundry.audio.AudioHelper.play({ src: initRoundSound, volume: initVolumeSound });
            }
            // start of the new round
            if (game.user.isDM) {
                // we always roll initiative if it's the very first round, so send force boolean
                await this.reRollInitiative(this.round === 1 && this.turn === 0);
                this.processNewRound();
            }
        } else if (turnUpdate && !game.ars.config.muteInitiativeSound) {
            const token = this.combatant.token;
            const playTurnSound = game.settings.get('ars', 'playTurnSound');
            if (token.isOwner && playTurnSound && this.turn !== 0) {
                const initTurnSound = game.settings.get('ars', 'initTurnSound');
                foundry.audio.AudioHelper.play({ src: initTurnSound, volume: initVolumeSound });
            }

            // always 'select' on turn - no matter if pan/zoom is on
            if (token.isOwner) {
                token?.object?.control({ releaseOthers: true });
                const panOnInitiative = game.settings.get('ars', 'panOnInitiative');
                if (panOnInitiative) {
                    canvas.animatePan({ x: token.x, y: token.y, scale: Math.max(1, canvas.stage.scale.x), duration: 1000 });
                    // canvas.animatePan({ x: token.data.x, y: token.data.y, scale: Math.max(1, canvas.stage.scale.x), duration: 1000 });
                    // this will select the token so vision/etc is updated
                    // if you use the pan option it will instant pan, not the slow method as above
                    // token?.object?.control({ releaseOthers: true, pan: panOnInitiative });
                }
            }

            // const turnCount = foundry.utils.getProperty(updateData, "turn");
            // next Combatants Turn
            if (game.user.isDM) {
                const combatant = this.combatant;
                let actionText = combatant.token.getFlag('world', 'init.actionText');
                const turnDescription = actionText ? actionText : 'General action';
                const chatOptions = {
                    rollMode: token.actor.type === 'character' ? game.settings.get('core', 'rollMode') : 'gmroll',
                };
                utilitiesManager.chatMessage(
                    ChatMessage.getSpeaker(),
                    `${token.name}s' turn`,
                    `${turnDescription}`,
                    undefined,
                    chatOptions
                );
            }
        } else {
            // console.log("combatTracker.js combatTurn");
        }
    }

    /**
     * Display a dialog querying the GM whether they wish to end the combat encounter and empty the tracker
     * @returns {Promise<Combat>}
     */
    async endCombat() {
        // #attacks tracking - end attack per round tracking - clean ALL data from combatants <flags>
        CombatManager.attacksPerRoundTracker().end(this);

        // #psionic - end tracking segment data on combat end
        ARSPsionics.endPsionicCombatTracking(this);

        return super.endCombat();
        // return Dialog.confirm({
        //   title: game.i18n.localize("COMBAT.EndTitle"),
        //   content: `<p>${game.i18n.localize("COMBAT.EndConfirmation")}</p>`,
        //   yes: () => this.delete()
        // });
    }

    /**
     * Begin the combat encounter, advancing to round 1 and turn 1
     * @returns {Promise<Combat>}
     */
    async startCombat() {
        const startCombatSound = game.settings.get('ars', 'startCombatSound');
        const playStartCombatSound = game.settings.get('ars', 'playStartCombatSound');
        const initVolumeSound = game.settings.get('ars', 'initVolumeSound');
        if (playStartCombatSound) {
            foundry.audio.AudioHelper.play({ src: startCombatSound, volume: initVolumeSound });
        }
        return super.startCombat();
        // this._playCombatSound('startEncounter');
        // const updateData = { round: 1, turn: 0 };
        // Hooks.callAll('combatStart', this, updateData);
        // return this.update(updateData);
    }
} // end Class ARSCombat

/**
 *
 * function run from hook for preUpdateCombat
 *
 * @param {*} combat
 * @param {*} updateData
 * @param {*} options
 * @param {*} userId
 */
// export async function _preUpdateCombat(combat, updateData, options, userId) {
// if (!game.user.isGM) return;
// const roundUpdate = foundry.utils.hasProperty(updateData, 'round');
// const turnUpdate = foundry.utils.hasProperty(updateData, 'turn');
// console.log("combatTracker.js _preUpdateCombat", { combat, updateData, options, userId, roundUpdate, turnUpdate });
// }
