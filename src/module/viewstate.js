export class ViewState {
    /**
     * @summary: Model the viewState - constructor providing defaults and ensuring that the data is always valid and in the correct format
     * @param {object} currentViewStateData -
     */
    constructor(viewStateData = {}) {
        // collapse data for user tracked with these properties
        const collapseKeys = [
            'abilities',
            'actions',
            'actions-combat',
            'background',
            'class',
            'coins',
            'combat',
            'combatDetails',
            'dailyfoodwater',
            'effects',
            'equipment',
            'equipment-proficiencies',
            'exp',
            'itembytypelist-ability',
            'itembytypelist-proficiency',
            'proficiency',
            'proficiencyDetails',
            'properties',
            'race',
            'skills',
            'skills',
            'skills-combat',
            'skills-proficiencies',
            'slotsperlevel',
            'spells',
            'spells-arcane',
            'spells-divine',
            'spellsbylevel-arcane0',
            'spellsbylevel-arcane1',
            'spellsbylevel-arcane2',
            'spellsbylevel-arcane3',
            'spellsbylevel-arcane4',
            'spellsbylevel-arcane5',
            'spellsbylevel-arcane6',
            'spellsbylevel-arcane7',
            'spellsbylevel-arcane8',
            'spellsbylevel-arcane9',
            'spellsbylevel-divine0',
            'spellsbylevel-divine1',
            'spellsbylevel-divine2',
            'spellsbylevel-divine3',
            'spellsbylevel-divine4',
            'spellsbylevel-divine5',
            'spellsbylevel-divine6',
            'spellsbylevel-divine7',
            'weapons',
            'hudActions',
            'hudItems',
            'hudWeapons',
            'hudPowers',
            'hudSkills',
            'hudSpells'
        ];

        // collapsed
        this.collapsed = Object.fromEntries(collapseKeys.map((key) => [key, viewStateData?.collapsed?.[key] ?? false]));

        // todo: implement - record search/filter states and perhaps many others
        // const searchAndFilterKeys = [
        //     'inv-search',
        //     'inv-filter'
        // ];
        // this.searchAndFilter = Object.fromEntries(searchAndFilterKeys.map((key) => [key, viewStateData?.searchAndFilter?.[key] ?? false]));
    }

    /**
     * @summary: init viewstate by retrieving current viewstate data (if any stored on object), validating it and resetting the flag to the object (new instance)
     * @param {object} obj - actor, item ...
     */
    static initViewState(obj) {
        // get current viewstate data, if any. Its always passed through the constructor to rebuild/validate and return fresh copy
        // const currentViewStateData = obj.getFlag('world', 'viewState');
        const dataKey = `viewState.${obj.id}`;
        const currentViewStateData = game.user.getFlag('ars', dataKey);
        const newInstance = new ViewState(currentViewStateData);
        // obj.setFlag('world', 'viewState', newInstance);
        game.user.setFlag('ars', dataKey, newInstance);
    }

    /**
     * @summary: update the viewstate data and save it too the object
     * @param {object} obj - actor, item ...
     * @param {string} type - type of viewstate data, collapsed, checked ...
     * @param {string} control - target control
     * @param {*} value - new value, could be boolean, could be string... depends on type
     */
    static updateViewState(obj, type, control, value) {

        // key
        const dataKey = `viewState.${obj.id}`;

        // get current viewstate data
        const viewStateData = game.user.getFlag('ars', dataKey);

        // update value for type.control - i.e. collapsed.weapons
        viewStateData[type][control] = value;

        // update viewState data to obj (flags)
        game.user.setFlag('ars', dataKey, viewStateData);
    }
}
