/**
 *
 * Not used currently, need to figure out how to intiate in "ready" instead
 * of renderSidebar hook
 *
 */
import * as debug from '../debug.js';
import { ARS } from '../config.js';
import * as effectManager from '../effect/effects.js';
import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
import { SocketManager } from '../sockets.js';
import { ARSDice } from '../dice/dice.js';
import { ViewState } from '../viewstate.js';
import { isControlActive } from '../utilities.js';

/**
 *
 * Party Tracker
 *
 */

/**
 *
 * Add party TAB to sidebar
 *
 * @param {*} app
 * @param {*} html
 */
export function addPartyTab(app, html) {
    if (!game.user.isGM) return;
    // Create tab Party
    // Calculate new tab width

    // this doesnt work in firefox, it doesnt support CSS.px
    // html[0]
    //     .querySelector("#sidebar-tabs")
    //     .style.setProperty(
    //         "--sidebar-tab-width",
    //         CSS.px(
    //             Math.floor(
    //                 parseInt(getComputedStyle(html[0]).getPropertyValue("--sidebar-width")) /
    //                 (document.querySelector("#sidebar-tabs").childElementCount + 1)
    //             )
    //         )
    //     );
    html[0].querySelector('#sidebar-tabs').style.setProperty('--sidebar-tab-width', '23px');
    const tab = document.createElement('a');
    tab.classList.add('item');
    tab.dataset.tab = 'party';
    tab.dataset.tooltip = 'Party tracker';
    if (!('tooltip' in game)) tab.title = 'Party tracker';

    // Add icon for tab
    const icon = document.createElement('i');
    icon.setAttribute('class', `fas fa-users`);
    tab.append(icon);

    // Add Party tab to sidebar before compendiums if it's not already there
    if (!document.querySelector("#sidebar-tabs > [data-tab='party']"))
        document.querySelector("#sidebar-tabs > [data-tab='combat']").after(tab);

    // this template determines where the <section> for PartySidebar is placed
    document
        .querySelector('template#combat')
        .insertAdjacentHTML('afterend', `<template class="tab" id="party" data-tab="party"></template>`);
    // end party tab
}

/**
 *
 * PartySidebar class
 *
 */
export class PartySidebar extends SidebarTab {
    constructor(options = {}) {
        super(options);
        this.initialized = false;
        if (ui.sidebar) ui.sidebar.tabs.party = this;
        // if (!this.popOut) game.party.apps.push(this);

        if (!game.party) {
            game['party'] = this;
        }
    }

    async initializePartyTracker() {
        this.initialized = true;
        let firstTimeInitialization = false;
        let partyMembers = await game.settings.get('ars', 'partyMembers');
        if (!partyMembers) {
            game.settings.set('ars', 'partyMembers', []);
            game.settings.set('ars', 'partAwards', []);
            game.settings.set('ars', 'partyLogs', []);
            firstTimeInitialization = true;
        }
        game['party'] = this;
        // game['party']['partyMembers'] = partyMembers;
        if (firstTimeInitialization) {
            await this.addLogEntry(`
               <p>Look here for logs generated from the Party-Tracker.</p>\n
               <p>***</p>\n
               `);
        }

        const membersMap = new Map();
        partyMembers.forEach((member) => {
            const actor = game.actors.get(member.id);
            if (actor) {
                actor.apps[this.appId] = this;
                membersMap.set(member.id, actor);
            }
        });
        this.members = membersMap;

        this._reRender(true);
    }

    /** @override */
    static documentName = 'Party';

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: 'party',
            template: 'systems/ars/templates/sidebar/party-tracker.hbs',
            title: 'Party',
            scrollY: ['.directory-list'],
            height: 500,
            resizable: false,
        });
    }

    /** @override */
    createPopout() {
        const pop = super.createPopout();
        pop.initialize({ party: this.viewed, render: true });
        pop.options.resizable = true;
        pop.members = this.members;
        return pop;
    }

    /** @override */
    initialize({ party = null, render = true } = {}) {
        // Also initialize the popout
        if (this._popout) this._popout.initialize({ party, render: false });

        // Render the tracker
        if (render) this.render();
    }

    /** @override */
    async getData(options) {
        // console.log("party.js getData", { options }, this.initialized);
        // Prepare rendering data
        let data;
        if (this.initialized || options.popOut) {
            data = {
                options: options,
                user: game.user,
                sidebar: this,
                // party: this.getMembers(),
                members: this.getMemberArray(),
                awards: this.getAwards(),
                logs: this.getLogs(),
                game: game,
                armorDamage: game.settings.get('ars', 'useArmorDamage'),
            };
            this.data = data;
        }

        return data;
    }

    /** @override */
    async _render(force, options) {
        // console.log("party.js _render", force, options);
        if (!game.user.isGM) return;
        return super._render(force, options);
    }

    /** @override */
    _handleDroppedDocument(target, data) {
        console.log('party.js _handleDroppedDocument', [target, data]);
    }

    get party() {
        return this.getMembers();
    }
    getMembers() {
        return game.settings.get('ars', 'partyMembers');
    }
    async setMembers() {
        await game.settings.set('ars', 'partyMembers', this.getMembersBundle());
    }
    getMembersBundle() {
        return Array.from(this.members, ([key, value]) => {
            return { id: key, uuid: value.uuid, name: value.name };
        });
    }
    getMemberArray() {
        // clean up missing actors
        // they got deleted but not removed from party
        if (this.members) {
            for (const [key, actor] of this.members) {
                if (!actor || !game.actors.get(key)) this.members.delete(key);
            }
            return Array.from(this.members, ([key, value]) => {
                return value;
            });
        }
        return [];
    }
    getAwards() {
        return game.settings.get('ars', 'partyAwards') || [];
    }
    async setAwards(awardsList) {
        await game.settings.set('ars', 'partyAwards', awardsList);
    }

    getLogs() {
        function sortLogsByReverseDate(logArray) {
            return logArray.sort((a, b) => new Date(b.date) - new Date(a.date));
        }

        let partyLogs = game.settings.get('ars', 'partyLogs') || [];
        if (Array.isArray(partyLogs)) {
            // partyLogs.sort().reverse();
            partyLogs = sortLogsByReverseDate(partyLogs);
        }
        return partyLogs;
    }
    async setLogs(logsList) {
        await game.settings.set('ars', 'partyLogs', logsList);
    }

    /**
     *
     * When detecting a dropped item on party-tracker window
     *
     * @param {*} event
     * @returns
     */
    async _onDrop(event) {
        // console.log("party.js _onDrop", {event});
        event.preventDefault();
        const data = JSON.parse(event.originalEvent.dataTransfer.getData('text/plain'));
        // console.log("party.js _onDrop", { data }, data.type, data.id);
        if (!data.type) return;
        switch (data.type) {
            case 'Actor':
                const actor = await fromUuid(data.uuid);
                if (!game.party.members.has(actor.id)) {
                    await this._addMember(actor);
                } else {
                    console.log('party.js _onDrop duplicate', { actor });
                }
                break;
            default:
                break;
        }
    }

    // one shot function to render in popout and original sidebar
    _reRender(ref) {
        if (this._original?.render) {
            this._original.render(ref);
        } else {
            this.render(ref);
        }
        if (this._popout) this._popout.render(ref);
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // allow drag member to map from party-tracker
        let _dragHandler = (ev) => this._onDragStart(ev);
        html.find('.entry.actor').each((i, li) => {
            li.setAttribute('draggable', true);
            li.addEventListener('dragstart', _dragHandler, false);
        });

        // listen for actors dropped on the party-tracker
        html.find('.party-tracker').on('drop', this._onDrop.bind(this));
        html.find('.party-longrest').click((ev) => this.partyLongRest(ev));

        html.find('.member-configure').click((ev) => this._onConfigureMember(ev));
        html.find('.member-remove').click((ev) => this._onRemoveMember(ev));
        html.find('.member-longrest').click((ev) => {
            ev.preventDefault();
            const element = ev.currentTarget;
            const li = element.closest('li');
            const actorId = li.dataset.id;
            this.longRest(game.party.members.get(actorId));
        });
        html.find('.member, .token-image').click((ev) => {
            ev.preventDefault();
            const element = ev.currentTarget;
            const li = element.closest('li');
            const actorId = li.dataset.id;
            const actor = game.party.members.get(actorId);
            actor.sheet.render(true);
        });

        html.find('.award-create').click((ev) => this._onAwardCreate(ev));
        html.find('.award-xp').change(this._awardXPChange.bind(this));
        html.find('.award-text').change(this._awardTextChange.bind(this));
        html.find('.award-destination').change(this._awardDestinationChange.bind(this));
        html.find('.award-delete').click((ev) => this._onRemoveAward(ev));
        html.find('.award-apply').click((ev) => this._onApplyAwards(ev));
        html.find('.logs-purge').click((ev) => this._onPurgeLogs(ev));
        html.find('.time-round1').click((ev) => this._onTimeAdvance('round', 1));
        html.find('.time-turn1').click((ev) => this._onTimeAdvance('turn', 1));
        html.find('.time-hour1').click((ev) => this._onTimeAdvance('hour', 1));
        html.find('.time-day1').click((ev) => this._onTimeAdvance('day', 1));
        html.find('.time-custom').click((ev) => this._onTimeAdvanceCustom(ev));
        html.find('.member-add').click((ev) => this._addPartyMember(ev));

        // gm tools - s.g.
        let btnStatsToggle = html.find('.member-stats-toggle'),
            btnPartyInventory = html.find('.btn-launch-party-inventory'),
            btnPartyAward = html.find('.btn-awards'),
            btnRolls = html.find('.btn-rolls'),
            cardAbilityLinks = html.find('.member-stats.abilities a').not('.hnr'),
            moreStatsState = localStorage.getItem('moreStats') === 'true';

        // annoying ass, full page re-render foundry bullshit, stop whacking state
        if (moreStatsState) html.find('.more-stats').show();
        btnStatsToggle.click((e) => {
            html.find('.more-stats').toggle();
            const on = html.find('.more-stats').is(':visible');
            localStorage.setItem('moreStats', on);
        });

        // roll an ability check right off their party tracker card (expanded stats ability details) - this is for the GM to speed shit up
        cardAbilityLinks.click(async function () {
            let link = $(this),
                actorId = link.closest('.entry.actor').attr('data-id'),
                actor = game.party.members.get(actorId),
                abilityType = link.data('ability');

            await new ARSDice(actor).makeAbilityCheckRollAsync(abilityType, 'd20');
        });

        // party inventory app
        btnPartyInventory.click((ev) => this._partyInventoryApp(ev));

        // party awards app
        btnPartyAward.click((ev) => this._partyAwardsApp(ev));

        // party rolls (aka requested rolls) app
        btnRolls.click((ev) => this._partyRollsApp(ev));
    }

    /** @override */
    _onDragStart(event) {
        // console.log("party.js _onDragStart", { event })
        const li = event.currentTarget.closest('.entry');
        let actor = null;
        if (li.dataset.id) {
            actor = game.actors.get(li.dataset.id);
            if (!actor || !actor.visible) return false;
        }

        // Parent directory drag start handling
        super._onDragStart(event);

        event.stopPropagation(); // Dragging from image doesn't work without this for some reason.

        // dragData of actor
        const dragData = {
            type: actor.documentName,
            id: actor.id,
            uuid: actor.uuid,
        };
        event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
        // Create the drag preview for the Token
        if (actor && canvas.ready) {
            const img = li.querySelector('img');
            // console.log("party.js _onDragStart", { actor, img })
            const td = actor.prototypeToken;
            const w = td.width * canvas.dimensions.size * td.scale * canvas.stage.scale.x;
            const h = td.height * canvas.dimensions.size * td.scale * canvas.stage.scale.y;
            const preview = DragDrop.createDragImage(img, w, h);
            event.dataTransfer.setDragImage(preview, w / 2, h / 2);
        }
    }

    /**
     *
     * Purge all log entries
     *
     * @param {*} event
     */
    async _onPurgeLogs(event) {
        event.preventDefault();
        if (await dialogManager.confirm(`Purge all party logs?`)) {
            await this.setLogs([]);
            this._reRender(true);
        }
    }

    /**
     * Apply all awards in queue to current party members
     *
     * @param {*} event
     */
    async _onApplyAwards(event) {
        event.preventDefault();
        if (await dialogManager.confirm(`Grant all experience awards to party members?`)) {
            // const memberCount = this.getMembers().length;
            const memberListArray = this.getMemberArray();
            const memberCount = memberListArray.length;
            const partyAwards = this.getAwards().filter((awd) => {
                return !awd.targetId;
            });
            const individualAwards = this.getAwards().filter((awd) => {
                return awd.targetId;
            });
            console.log('party.js _onApplyAwards', {
                memberCount,
                partyAwards,
                individualAwards,
            });
            if (memberCount > 0 && (partyAwards.length > 0 || individualAwards.length > 0)) {
                console.log('party.js _onApplyAwards', {
                    partyAwards,
                    individualAwards,
                });
                let xpTotal = 0;
                let xpPerMember = 0;
                let awardDesc = [];
                for (const awd of partyAwards) {
                    awardDesc.push(awd.text);
                    xpTotal += awd.xp;
                }
                xpPerMember = Math.round(xpTotal / memberCount);
                // for (const member of this.getMembers()) {
                for (const actor of memberListArray) {
                    // const actor = game.actors.get(member.id);
                    console.log('party.js _onApplyAwards processing :', actor.name);
                    // if (actor) {
                    if (xpPerMember > 0) {
                        const xpEarned = actor.getFlag('world', 'henchman') ? Math.round(xpPerMember * 0.5) : xpPerMember;
                        await this.awardExperienceToActor(actor, xpEarned);
                        await this.addLogEntry(`Granted group experience award of ${xpEarned} to ${actor.name}.`);
                    }
                    const personalAwards = individualAwards.filter((awd) => {
                        return awd.targetId == actor.id;
                    });
                    console.log('party.js _onApplyAwards', actor.name, {
                        personalAwards,
                    });
                    if (personalAwards.length) {
                        await this._awardIndividualAward(actor, personalAwards);
                    }
                    // }
                }

                // create final description (awards (n)) text
                awardDesc = awardDesc.join(', ');

                // flush all awards, we just applied them
                await this.setAwards([]);

                // output
                await this.addLogEntry(
                    `${awardDesc}: Group experience dispersed, awards of ${xpPerMember} per member from ${xpTotal}.`
                );
            }
        }
    }

    /**
     *
     * Award individual experience to actor
     *
     * @param {*} actor
     * @param {*} awards
     */
    async _awardIndividualAward(actor, awards) {
        let xpTotal = 0;
        let awardDesc = [];
        for (const awd of awards) {
            awardDesc.push(awd.text);
            xpTotal += awd.xp;
        }
        await this.awardExperienceToActor(actor, xpTotal);

        // create final description (awards (n)) text
        awardDesc = awardDesc.join(', ');

        // output
        await this.addLogEntry(`${awardDesc}: Granted personal experience award to ${actor.name} for a total of ${xpTotal}.`);
    }

    /**
     *
     * Apply experience to single actor
     *
     * @param {*} actor
     * @param {*} xpPerMember
     */
    async awardExperienceToActor(actor, xpPerMember) {
        // xp is running total of xp ever received
        // applyXP is newly received XP that is due to be applied to class(s)
        const applyXP = (actor.system.details.applyxp || 0) + xpPerMember;
        const xp = (actor.system.details.xp || 0) + xpPerMember;
        // console.log("party.js _onApplyAwards", actor.name, { applyXP, xp });
        await actor.update({
            'system.details.applyxp': applyXP,
            'system.details.xp': xp,
        });
        let chatData = {
            content: `
                   <div><h2>Experience Awarded!</h2></div>    
                   <div>${actor.name} earned ${xpPerMember} experience</div>
               `,
            author: game.user.id,
            speaker: ChatMessage.getSpeaker({ actor: actor }),
            style: game.ars.const.CHAT_MESSAGE_STYLES.OTHER,
        };
        //use user current setting? game.settings.get("core", "rollMode")
        // ChatMessage.applyRollMode(chatData, 'selfroll')
        ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
        ChatMessage.create(chatData);
    }

    /**
     *
     * Delete a select award
     *
     * @param {*} event
     */
    async _onRemoveAward(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const li = element.closest('li');
        const index = li.dataset.index;
        const bundle = foundry.utils.deepClone(Object.values(this.getAwards()));
        bundle.splice(index, 1);
        await this.setAwards(bundle);
        this._reRender(true);
    }

    /**
     *
     * Edit XP field on a award
     *
     * @param {*} event
     */
    async _awardXPChange(event) {
        event.preventDefault();
        const index = $(event.currentTarget).closest('li').data('index');
        const value = event.target.value;
        const bundle = foundry.utils.deepClone(this.getAwards() || []);
        bundle[index].xp = parseInt(value) || 0;

        // !look at me
        // update logs - rewrite the logs based on this text change!
        const logs = foundry.utils.deepClone(this.getLogs() || []);
        for (let i in logs) {
            const text = bundle[i]?.text || logs[i].text;
            logs[i].text = text;
        }

        // set the logs
        this.setLogs(logs);

        // re-render!
        await this.setAwards(bundle);
        this._reRender(true);
    }

    /**
     *
     * Edit text field on award
     *
     * @param {*} event
     */
    async _awardTextChange(event) {
        event.preventDefault();
        const index = $(event.currentTarget).closest('li').data('index');
        const value = event.target.value;
        const bundle = foundry.utils.deepClone(this.getAwards() || []);
        bundle[index].text = value;

        // !look at me
        // update logs - rewrite the logs based on this text change!
        const logs = foundry.utils.deepClone(this.getLogs() || []);
        for (let i in logs) {
            const text = bundle[i]?.text || logs[i].text;
            logs[i].text = text;
        }

        // set the logs
        this.setLogs(logs);

        // re-render!
        await this.setAwards(bundle);
        this._reRender(true);
    }

    /**
     * Edit the destination of xp award
     *
     * @param {*} event
     */
    async _awardDestinationChange(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const actorId = element.value;
        const li = element.closest('li');
        const index = li.dataset.index;

        const bundle = foundry.utils.deepClone(this.getAwards() || []);
        bundle[index].targetId = actorId;
        await this.setAwards(bundle);
        this._reRender(true);
    }

    /**
     *
     * Create an XP award entry
     *
     * @param {*} env
     */
    async _onAwardCreate(env) {
        env.preventDefault();
        // console.log("party.js _onAwardCreate", { env }, this);
        this.addAward('New award', 1);
    }

    /**
     *
     * Add member
     *
     * @param {*} actor
     */
    async _addMember(actor) {
        console.log('party.js _addMember', { actor });
        game.party.members.set(actor.id, actor);
        actor.apps[this.appId] = this;
        await this.setMembers();
        await this.addLogEntry(`Added ${actor.name} to Party-Tracker`);
    }

    /**
     *
     * Toggle the flag "henchman" (get 50% less XP)
     *
     * @param {*} event
     */
    async _onConfigureMember(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const li = element.closest('li');
        const actorId = li.dataset.id;
        const index = li.dataset.index;
        const actor = game.party.members.get(actorId);
        if (actor) {
            const isHenchman = actor.getFlag('world', 'henchman');
            const toggleHenchman = await dialogManager.confirm(
                `${isHenchman ? 'Deactivate' : 'Activate'} henchman status?`,
                'Configure Party Member'
            );
            if (toggleHenchman) {
                actor.setFlag('world', 'henchman', !isHenchman);
            }
        }
    }

    /**
     *
     * Remove a party member
     *
     * @param {*} event
     */
    async _onRemoveMember(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const li = element.closest('li');
        const actorId = li.dataset.id;
        const index = li.dataset.index;
        // const actor = game.actors.get(actorId);

        const actor = game.party.members.get(actorId);
        // remove this actor's app id link PartySidebar class to stop refreshes
        delete actor.apps[this.appId];

        game.party.members.delete(actorId);
        // const membersBundle = foundry.utils.deepClone(Object.values(this.getMembers()));
        // membersBundle.splice(index, 1);
        await this.setMembers();

        if (actor && actor.name) this.addLogEntry(`Removed ${actor.name} from Party-Tracker`);
        else this.addLogEntry(`Removed stale actor from Party-Tracker`);
    }

    /**
     *
     * General function to add defeated npc award ... used by system when npc is defeated
     *
     * @param {*} token
     */
    async addDefeatedAward(token) {
        if (game.user.isGM) {
            console.log('party.js addDefeatedAward', { token });
            if (token.actor && token.actor.type === 'npc') {
                const dupe =
                    this.getAwards().filter((awrd) => {
                        return awrd.sourceId === token.id;
                    }).length > 0;
                if (!dupe) {
                    const xp = parseInt(token.actor.system.xp.value) || 0;
                    this.addAward(`${token.name} defeated`, xp, token.id);
                } else {
                    console.log('party.js addDefeatedAward same defeated token already exists for ', { token });
                }
            }
        }
    }

    /**
     *
     * Add an award
     *
     * @param {*} description
     * @param {*} xp
     * @param {*} sourceId (optional)
     */
    async addAward(description, xp, sourceId = null, targetId = null) {
        // console.log("party.js addAward", { description, xp, sourceId });
        const awardsBundle = foundry.utils.deepClone(Object.values(this.getAwards())) || [];
        awardsBundle.push({
            date: new Date().toLocaleString(),
            text: description,
            xp: xp,
            sourceId: sourceId,
            targetId: targetId,
        });
        await this.setAwards(awardsBundle);
        this.addLogEntry(`Added ${description}`);
    }

    /**
     *
     * Write a log of what happened.
     *
     * Sometimes you want to await if you're doing a lot
     * of log entries because it's just an array object and
     * things get tricky otherwise.
     *
     * We use it for the central locale for reRender()
     *
     * @param {*} text
     */
    async addLogEntry(text) {
        let logBundle = foundry.utils.deepClone(this.getLogs()) || [];
        logBundle.push({ date: new Date().toLocaleString(), text: text });
        await this.setLogs(logBundle);
        this._reRender(true);
    }

    /**
     *
     * Process a long rest for entire party member list
     *
     * @param {*} event
     */
    async partyLongRest(event) {
        event.preventDefault();
        const rest = await dialogManager.askLongRest();
        if (!rest.cancel && rest.longRest) {
            for (const actor of this.getMemberArray()) {
                if (actor) await this.longRest(actor, rest.consumeProvisions);
            }
            game.time.advance(CONFIG.time.hourTime * 8);
        }
    }

    /**
     *
     * Process a long rest for an actor
     *
     * @param {*} actor
     */
    async longRest(actor, consumeProvisions) {
        console.log('party.js longRest:', actor.name);
        actor.longRest(consumeProvisions);
        await this.addLogEntry(`${actor.name} took a long rest.`);
    }

    /**
     *
     * Update member entry from actorId
     *
     * this is called in deriveddata from actor.js as backend way to keep this updated
     *
     * @param {*} actorId
     */
    // async updateMember(actorId) {
    //     // if (!game.party?.getMembers().length) return;
    //     if (!game.party?.members?.size) return;

    //     this._reRender(true);
    // }

    /**
     *
     * Take all coins from lootedToken to memberSplit()
     *
     * @param {*} lootedToken
     */
    async shareLootedCoins(lootedToken) {
        const lootedActor = lootedToken.actor;
        const memberCount = this.getMemberArray().length;
        if (memberCount) {
            let coinDetails = [];
            for (const coin in lootedActor.system.currency) {
                const count = parseInt(lootedActor.system.currency[coin]);
                const coinPer = Math.floor(count / memberCount);
                const coinRemainder = count % memberCount;
                if (count > 0 && coinPer > 0) {
                    await this.memberSplit(coinPer, coin);
                    lootedActor.update({
                        [`system.currency.${coin}`]: coinRemainder,
                    });
                    coinDetails.push(`${coinPer} ${coin}`);
                }
            }
            if (coinDetails.length) {
                const coinText = coinDetails.join(', ');
                // for (const member of this.getMembers()) {
                // for (const [key, actor] of game.party.members) {
                for (const actor of this.getMemberArray()) {
                    // const actor = game.actors.get(member.id);
                    // if (actor) {
                    utilitiesManager.chatMessage(
                        ChatMessage.getSpeaker({ actor: actor }),
                        `Aquired Split Coin`,
                        `${actor.name} received ${coinText} from ${lootedActor.name}.`
                    );
                    await this.addLogEntry(
                        `${actor.name} received ${coinText} from party split from ${lootedActor.name}.`,
                        'icons/commodities/currency/coins-plain-stack-gold-yellow.webp'
                    );
                    // }
                }
            }
        }
    }

    /**
     *
     * Split count coinType among party members
     *
     * @param {*} count
     * @param {*} coinType
     */
    async memberSplit(coinPer, coinType) {
        // const memberCount = this.getMembers().length;
        // const coinPer = Math.round(count / memberCount);
        // for (const member of this.getMembers()) {
        // for (const [key, actor] of game.party.members) {
        for (const actor of this.getMemberArray()) {
            // const actor = game.actors.get(member.id);
            // if (actor) {
            const current = parseInt(actor.system.currency[coinType]) || 0;
            actor.update({
                [`system.currency.${coinType}`]: current + coinPer,
            });
            console.log('party.js memberSplit', { actor, coinPer, current });
            // }
        }
    }

    /**
     *
     * Advance time of type amount
     *
     * @param {*} type
     * @param {*} amount
     */
    async _onTimeAdvance(type, amount, skipDialog = isControlActive()) {
        amount = Math.round(amount);
        const finalAmount = skipDialog
            ? amount
            : await dialogManager.getQuantity(-100000, 100000, 1, `How many ${type}s`, `Advance ${type}`, 'Accept', 'Cancel');
        if (finalAmount) {
            let advance = finalAmount;
            switch (type) {
                case 'round':
                    advance = 60 * finalAmount;
                    break;
                case 'turn':
                    advance = 60 * 10 * finalAmount;
                    break;
                case 'hour':
                    advance = 60 * 60 * finalAmount;
                    break;
                case 'day':
                    advance = 60 * 60 * 24 * finalAmount;
                    break;
                default:
                    advance = 0;
                    break;
            }
            console.log('party.js _onTimeAdvance', { type, advance });
            game.time.advance(advance);
        }
    }

    async _onTimeAdvanceCustom(event) {
        // console.log("party.js _onTimeAdvanceCustom", { event });
        const timePassage = await this.getTimePassage(`Passage of Time`);
        if (timePassage) {
            for (const key in timePassage) {
                // console.log("party.js _onTimeAdvanceCustom", { key }, timePassage[key])
                this._onTimeAdvance(key, timePassage[key], true);
            }
        }
    }

    /**
     *
     * Dialog to ask for custom time passage values
     *
     * @param {*} title
     * @param {*} options
     * @returns
     */
    async getTimePassage(title, options = {}) {
        const content = await renderTemplate('systems/ars/templates/dialogs/dialog-getTimePassage.hbs', {
            CONFIG,
        });

        const _onDialogSubmit = (html) => {
            const form = html[0].querySelector('form');
            let round = 0;
            let turn = 0;
            let hour = 0;
            let day = 0;
            round = parseInt(form.round.value);
            turn = parseInt(form.turn.value);
            hour = parseInt(form.hour.value);
            day = parseInt(form.day.value);
            if (isNaN(round)) round = 0;
            if (isNaN(turn)) turn = 0;
            if (isNaN(hour)) hour = 0;
            if (isNaN(day)) day = 0;

            return { round: round, turn: turn, hour: hour, day: day };
        };

        return new Promise((resolve) => {
            new Dialog(
                {
                    title,
                    content,
                    buttons: {
                        submit: {
                            label: 'Apply',
                            callback: (html) => resolve(_onDialogSubmit(html)),
                        },
                        cancel: {
                            label: 'None',
                            callback: (html) => resolve(undefined),
                        },
                    },
                    default: 'submit',
                    close: () => resolve(undefined),
                },
                options
            ).render(true);
        });
    }

    /**
     *
     * Dialog to get all character actors list for selection
     *
     * @param {*} event
     * @param {*} text
     * @param {*} title
     * @param {*} options
     * @returns
     */
    async _getCharacter(text = 'Select Character Actor', title = 'Character Select', options = {}) {
        const _onDialogSubmit = (html) => {
            const form = html[0].querySelector('form');
            const memberId = form.memberAddSelect.value || undefined;
            return memberId;
        };

        const currentMembers = this.getMembers().map((p) => p.id);
        // const currentMembers = game.party.members.map(p => p.id);
        // console.log('party.js _getCharacter', { currentMembers });
        const characterList = game.actors.filter((act) => act.type === 'character' && !currentMembers.includes(act.id));
        // const characterList = game.actors;
        // console.log('party.js _getCharacter', { characterList });

        const content = await renderTemplate('systems/ars/templates/sidebar/party-dialog-addmember.hbs', {
            text,
            title,
            characterList,
            CONFIG,
        });

        return new Promise((resolve) => {
            new Dialog(
                {
                    title,
                    content,
                    buttons: {
                        submit: {
                            label: game.i18n.localize('ARS.party.addcharacter'),
                            callback: (html) => resolve(characterList.length ? _onDialogSubmit(html) : undefined),
                        },
                        cancel: {
                            label: game.i18n.localize('ARS.party.cancel'),
                            callback: (html) => resolve(undefined),
                        },
                    },
                    default: 'submit',
                    close: () => resolve(undefined),
                },
                options
            ).render(true);
        });
    }

    /**
     *
     * Prompt dialog to add member from dialog list of actors not in party
     *
     * @param {*} event
     */
    async _addPartyMember(event) {
        event.preventDefault();
        const newMemberId = await this._getCharacter();
        if (newMemberId) this._addMember(game.actors.get(newMemberId));
    }

    /**
     * Launch the party inventory app - provides a birdseye view of inventory/totals for all characters in the party sheet
     * @param {*} event
     */
    _partyInventoryApp(event) {
        const app = new ARSPartyInventoryBrowswer();
        app.render(true);
    }

    /**
     * Launch the party rolls app - requested rolls ect.
     * @param {*} event
     */
    _partyRollsApp(event) {
        const app = new ARSRequestedRolls();
        app.render(true);
    }

    /**
     * Launch the party stats app - overview of various stats for party members
     * @param {*} event
     */
    _partyStatsApp(event) {
        const app = new ARSPartyStats();
        app.render(true);
    }

    /**
     * Launch the party award logs app - overview of all experiencs/ect rewards
     * @param {*} event
     */
    _partyAwardsApp(event) {
        const app = new ARSPartyAwardLog();
        app.render(true);
    }
}

/**
 * @summary: Defines the application for the party inventory overview modal.
 *           This modal exposes a consolidated list of all party inventry, and provides various other workflow related information/task support.
 */
export class ARSPartyInventoryBrowswer extends Application {
    constructor(app) {
        super(app);
        this.items = [];
    }

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            resizable: true,
            minimizable: true,
            id: 'party-inv-browser-sheet',
            classes: ['ars', 'party-inv-browser'],
            title: 'Party Inventory Browser',
            template: 'systems/ars/templates/apps/party-inventory-browser.hbs',
            width: 1200,
            height: 950,
            scrollY: ['.filter-list', '.item-list'],
        });
    }

    /** @override */
    async getData() {
        const data = await super.getData();

        // data.documentTypes = ['all', ...game.system.documentTypes.Item];
        const docTypes = ['all', ...Object.keys(game.system.documentTypes.Item)];

        // Convert the array to an object with keys and values being the same
        data.documentTypes = docTypes.reduce((acc, type) => {
            acc[type] = utilitiesManager.capitalize(type);
            return acc;
        }, {});

        data.this = this;
        data.filters = this.filters;
        data.game = game;
        data.config = ARS;
        data.isGM = game.user.isGM;

        // Get all actors in the party sheet
        const partyMembers = game.settings.get('ars', 'partyMembers');

        // Initialize the inventory dictionary
        let partyInventory = [];

        // Iterate through each actor's items
        for (let _actor of partyMembers) {
            _actor = game.actors.get(_actor.id);

            for (let item of _actor.inventoryItems) {
                let name = item.name;
                let id = item.id;
                let type = item.type;
                let img = item.img;
                let identified = item.system.attributes.identified;
                let magic = item.system.attributes.magic;
                let location = item.system.location.state;
                let quantity = item.system.quantity;
                let weight = item.system.weight || 0;
                let existingItem = partyInventory.find((invItem) => invItem.name === name);

                if (!existingItem) {
                    // If not, create a new entry for the item
                    existingItem = {
                        img: img,
                        ids: [],
                        name: name,
                        type: type,
                        identified: identified,
                        magic: magic,
                        location: location,
                        weight: 0,
                        total: 0,
                        owners: {},
                    };
                    partyInventory.push(existingItem);
                }

                // Accumulate weight/quantity
                existingItem.ids.push(id);
                existingItem.weight += weight;
                existingItem.total += quantity;

                // Add or update the item count for the current actor
                if (!existingItem.owners[_actor.id]) {
                    existingItem.owners[_actor.id] = {
                        actorId: _actor.id,
                        actorName: _actor.name,
                        itemId: id,
                        quantity: 0,
                    };
                }
                existingItem.owners[_actor.id].quantity += item.system.quantity;
            }
        }

        partyInventory.forEach((item) => {
            item.strOwnerTotals = Object.values(item.owners)
                .map(
                    ({ actorId, actorName, itemId, quantity }) =>
                        `<a class="party-inv-actor-item" data-id="${actorId}" data-item-id="${itemId}">${actorName} ${quantity}</a>`
                )
                .join(', ');
        });

        // Log the consolidated inventory to the console (or return it if needed)
        data.items = partyInventory;

        // console.log("item-browser.js getData", { data })
        return data;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        let selFilter = html.find('.sel_inventory_filter'),
            txtSearch = html.find('#txt_inventory_search_party'),
            btnRefresh = html.find('.refresh-list'),
            btnIdenfifyAll = html.find('.id-all'),
            btnDeleteAll = html.find('.delete-all'),
            buttonsBulkActions = html.find('.btn-bulk-actions'),
            btnQuickNav = html.find('.party-inv-actor-item'),
            itemBulkSelect = html.find('.item-image'),
            btnItemPreview = html.find('.item-preview');

        const strFilter = window.partyInvFilter || 'none';
        const strSearch = window.partyInvSearchQuery || '';

        // filter
        selFilter
            .change(function () {
                const filter = this.value;

                utilitiesManager.filterHelper(html, filter);

                window.partyInvFilter = filter;

                // always trigger a keyup on search
                txtSearch.keyup();
            })
            .val(strFilter);

        // search
        txtSearch
            .keyup(function () {
                // search string
                const searchQuery = this.value;
                const filter = selFilter.val();
                window.partyInvSearchQuery = searchQuery;
                utilitiesManager.searchHelper(html, searchQuery, filter);
            })
            .val(strSearch);

        // on load - if data from previous session, use it and trigger a filter/search
        utilitiesManager.filterHelper(html, strFilter);
        utilitiesManager.searchHelper(html, strSearch, strFilter);

        // quick navigate to item
        btnQuickNav.click(function () {
            utilitiesManager.navToItem(this);
        });

        // rebuild party inventory
        btnRefresh.click(() => {
            delete window.partyInvFilter;
            delete window.partyInvSearchQuery;
            this.render(true);
        });

        // bulk select
        window.bulkItemSelection = [];
        itemBulkSelect.click(function (event) {
            if (isControlActive()) {
                // dont bubble to collapse
                event.stopImmediatePropagation();

                const itemRow = $(this).parent();

                if (itemRow.hasClass('lock-selection')) {
                    return;
                }

                const itemIds = itemRow.attr('data-item-ids');
                const highlight = `item-row-selection-highlight`;

                // finally, push/pop out the id to/from the bulk selection array []
                if (window.bulkItemSelection?.includes(itemIds)) {
                    window.bulkItemSelection = window.bulkItemSelection?.filter((id) => id !== itemIds);
                    itemRow.removeClass(highlight);
                } else {
                    window.bulkItemSelection?.push(itemIds);
                    itemRow.addClass(highlight);
                }

                buttonsBulkActions.toggleClass('enabled', window.bulkItemSelection?.length > 0);
            }
        });

        // bulk id
        btnIdenfifyAll.click(async () => {
            // Loop through each string in the bulkItemSelection array
            for (const itemList of window.bulkItemSelection) {
                // Split the string of item IDs into an array of individual IDs
                const deleteItems = itemList.split(',');

                // Loop through each item ID in the deleteItems array
                for (const itemId of deleteItems) {
                    // Loop through all actors to find and delete the item
                    const partyMembers = game.settings.get('ars', 'partyMembers');
                    for (let actor of partyMembers) {
                        actor = game.actors.get(actor.id);

                        const item = await actor.items.get(itemId);
                        if (item) {
                            // Delete the item from the actor's items collection
                            await item.update({ 'system.attributes.identified': true });
                            console.log(`Item ${item.name} Indentified from actor ${actor.name}.`);
                        }
                    }
                }
            }

            this.render(true);
        });

        // bulk delete
        btnDeleteAll.click(async () => {
            // Loop through each string in the bulkItemSelection array
            for (const itemList of window.bulkItemSelection) {
                // Split the string of item IDs into an array of individual IDs
                const deleteItems = itemList.split(',');

                // Loop through each item ID in the deleteItems array
                for (const itemId of deleteItems) {
                    // Loop through all actors to find and delete the item
                    const partyMembers = game.settings.get('ars', 'partyMembers');
                    for (let actor of partyMembers) {
                        actor = game.actors.get(actor.id);
                        const item = actor.items.get(itemId);

                        if (item) {
                            // Delete the item from the actor's items collection
                            await actor.deleteEmbeddedDocuments('Item', [itemId]);
                            console.log(`Item ${item.name} deleted from actor ${actor.name}.`);
                        }
                    }
                }
            }

            this.render(true);
        });

        // the following will only work when the row represents a SINGLE ITEM - you cannot edit a collection of items,
        btnItemPreview.click((event) => {
            let element = event.currentTarget,
                li = $(element.closest('li')),
                ids = (li.attr('data-item-ids') || '').split(',');

            if (ids.length === 1) {
                let link = li.find('a.party-inv-actor-item'),
                    actorId = link.attr('data-id'),
                    actor = game.party.members.get(actorId),
                    itemId = link.attr('data-item-id'),
                    item = actor.getEmbeddedDocument('Item', itemId),
                    editable = isControlActive();

                if (!item) {
                    ui.notifications.error(`Item cannot be found in inventory.`);
                    return;
                }
                item.sheet.render(true, {
                    editable: editable,
                });
            }
        });
    }

    /** @override to add item population */
    async _render(force = false, options = {}) {
        await super._render(force, options);
    }
}

/**
 * @summary: Party stats
 * todo: implement - it will basically be a fun little app where you can see various statics, like who's killed what, most kills, most spells, most exp for single award, most loot aquired
 * all types of neat shit - a score card or analytics. It will require some very light tracking in several places. I'm thinking world level but will see.
 */
export class ARSPartyStats extends Application {
    constructor(app) {
        super(app);
        this.items = [];
        this.filters = [];
    }

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            resizable: true,
            minimizable: true,
            id: 'party-stats-sheet',
            classes: ['ars', 'party-stats'],
            title: 'Party Stats',
            template: 'systems/ars/templates/apps/party-stats.hbs',
            width: 600,
            height: 600,
            scrollY: ['.filter-list', '.item-list'],
        });
    }

    /** @override */
    async getData() {
        const data = await super.getData();

        // data.documentTypes = ['all', ...game.system.documentTypes.Item];
        const docTypes = ['all', ...Object.keys(game.system.documentTypes.Item)];

        // Convert the array to an object with keys and values being the same
        data.documentTypes = docTypes.reduce((acc, type) => {
            acc[type] = utilitiesManager.capitalize(type);
            return acc;
        }, {});

        data.this = this;
        data.filters = this.filters;
        data.game = game;
        data.config = ARS;
        data.isGM = game.user.isGM;

        let partyMembers = await game.settings.get('ars', 'partyMembers');
        const membersMap = new Map();
        partyMembers.forEach((member) => {
            const actor = game.actors.get(member.id);
            if (actor) {
                actor.apps[this.appId] = this;
                membersMap.set(member.id, actor);
            }
        });
        this.members = membersMap;

        data.members = this.getMemberArray();

        console.log(data.members[0]);
        return data;
    }

    /** @override */
    activateListeners(html) {
        const btnMember = html.find('.member, .token-image');

        btnMember.click((ev) => {
            ev.preventDefault();
            const element = ev.currentTarget;
            const li = element.closest('li');
            const actorId = li.dataset.id;
            const actor = game.party.members.get(actorId);
            actor.sheet.render(true);
        });
    } // end activeListeners

    /** @override to add item population */
    async _render(force = false, options = {}) {
        await super._render(force, options);
    }

    getMemberArray() {
        // clean up missing actors
        // they got deleted but not removed from party
        if (this.members) {
            for (const [key, actor] of this.members) {
                if (!actor || !game.actors.get(key)) this.members.delete(key);
            }
            return Array.from(this.members, ([key, value]) => {
                return value;
            });
        }
        return [];
    }
}

/**
 * @summary: Party award log
 * todo: extend with more robust features for given out rewards, add support for distributing currency and items
 */
export class ARSPartyAwardLog extends Application {
    constructor(app) {
        super(app);
        this.items = [];
        this.filters = [];
    }

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            resizable: true,
            minimizable: true,
            id: 'party-award-logs-sheet',
            classes: ['ars', 'party-award-logs'],
            title: 'Award Logs',
            template: 'systems/ars/templates/apps/party-award-logs.hbs',
            width: 600,
            height: 600,
            scrollY: ['.filter-list', '.item-list'],
        });
    }

    /** @override */
    async getData() {
        const data = await super.getData();

        // data.documentTypes = ['all', ...game.system.documentTypes.Item];
        const docTypes = ['all', ...Object.keys(game.system.documentTypes.Item)];

        // Convert the array to an object with keys and values being the same
        data.documentTypes = docTypes.reduce((acc, type) => {
            acc[type] = utilitiesManager.capitalize(type);
            return acc;
        }, {});

        data.this = this;
        data.filters = this.filters;
        data.game = game;
        data.config = ARS;
        data.isGM = game.user.isGM;

        data.logs = this.getLogs();
        // console.log(data.members[0]);
        return data;
    }

    /** @override */
    activateListeners(html) {
        // const btnMember = html.find('.member, .token-image');
    } // end activeListeners

    /** @override to add item population */
    async _render(force = false, options = {}) {
        await super._render(force, options);
    }

    getMemberArray() {
        // clean up missing actors
        // they got deleted but not removed from party
        if (this.members) {
            for (const [key, actor] of this.members) {
                if (!actor || !game.actors.get(key)) this.members.delete(key);
            }
            return Array.from(this.members, ([key, value]) => {
                return value;
            });
        }
        return [];
    }

    getLogs() {
        function sortLogsByReverseDate(logArray) {
            return logArray.sort((a, b) => new Date(b.date) - new Date(a.date));
        }
        let partyLogs = game.settings.get('ars', 'partyLogs') || [];
        if (Array.isArray(partyLogs)) {
            // partyLogs.sort().reverse();
            partyLogs = sortLogsByReverseDate(partyLogs);
        }
        return partyLogs;
    }
    async setLogs(logsList) {
        await game.settings.set('ars', 'partyLogs', logsList);
    }
}

/**
 * @summary: Requested rolls app is a powerful multi-purpose tool for GM's to request/track rolls from actors
 */
export class ARSRequestedRolls extends Application {
    constructor(app) {
        super(app);
        this.items = [];
        this.filters = [];
        this.selectedActors = [];
    }

    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            resizable: true,
            minimizable: true,
            id: 'party-rolls-sheet',
            classes: ['ars', 'party-rolls'],
            title: 'Party Rolls',
            template: 'systems/ars/templates/apps/party-rolls.hbs',
            width: 600,
            height: 800,
            scrollY: ['.filter-list', '.item-list'],
        });
    }

    /** @override */
    async getData() {
        const data = await super.getData();
        const rrData = await game.user.getFlag('ars', 'rrData');
        data.rrData = rrData;
        data.items = rrData?.items || [];

        if (data.items.length) {
            let groupedByType = data.items.reduce((acc, item) => {
                // If the type doesn't exist in the accumulator, create a new array for it
                if (!acc[item.type]) {
                    acc[item.type] = [];
                }
                // Push the item into the appropriate group
                acc[item.type].push(item);
                return acc;
            }, {});

            // Step 2: Sort each group's array by 'name'
            Object.keys(groupedByType).forEach((type) => {
                groupedByType[type].sort((a, b) => a.name.localeCompare(b.name));
            });

            data.items = groupedByType;
        }

        // data.documentTypes = ['all', ...game.system.documentTypes.Item];
        const docTypes = ['all', ...Object.keys(game.system.documentTypes.Item)];

        // Convert the array to an object with keys and values being the same
        data.documentTypes = docTypes.reduce((acc, type) => {
            acc[type] = utilitiesManager.capitalize(type);
            return acc;
        }, {});

        data.this = this;
        data.filters = this.filters;
        data.game = game;
        data.config = ARS;
        data.isGM = game.user.isGM;

        let partyMembers = await game.settings.get('ars', 'partyMembers');
        const membersMap = new Map();
        partyMembers.forEach((member) => {
            const actor = game.actors.get(member.id);
            if (actor) {
                actor.apps[this.appId] = this;
                membersMap.set(member.id, actor);
            }
        });
        this.members = membersMap;
        data.members = this.getMemberArray();
        return data;
    }

    /** @override */
    async activateListeners(html) {
        const selectActors = html.find('#actor-request-roll-select');
        const multiSelectControlGroup = html.find('#multi-select-control-group');
        const chkUseSelected = html.find('#useSelected');
        const getSelected = () => {
            let selectedTokens = canvas.tokens.controlled;
            let actorIds = selectedTokens.map((token) => token.actor.id);
            return actorIds;
        };
        const selectAbility = html.find('#member-ability-select');
        const selectSaves = html.find('#member-saves-select');
        const selectMisc = html.find('#member-misc-select');
        const inputDice = html.find('#member-dice-formula');
        const inputCheckTarget = html.find('#member-dice-checktarget');
        const selectDiceRollType = html.find('#member-dice-type');
        const inputDesc = html.find('#member-dice-desc');
        const btnRequestRolls = html.find('#btn-request-roll');
        const btnRequestRollsClear = html.find('#btn-request-roll-clear');
        const btnCollapsible = html.find('.ars_clps');

        // get data
        const rrData = await game.user.getFlag('ars', 'rrData');
        let items = rrData?.items || [];
        if (rrData) {
            // preselection of actor ids - will failsafe
            const useSelected = localStorage.getItem('rrUseSelected');
            if (useSelected !== 'true') {
                try {
                    selectActors.val(rrData.actorIds);
                } catch (e) {
                    rrData.actorIds = [];
                    selectActors.val(rrData.actorIds);
                }
            } else {
                multiSelectControlGroup.hide();
                chkUseSelected.prop('checked', true);
            }

            selectAbility.val(rrData.ability || 'none');
            selectSaves.val(rrData.save || 'none');
            selectMisc.val(rrData.misc || 'none');
            inputDice.val(rrData.dice || '');
            inputCheckTarget.val(rrData.checkTarget || '');
            selectDiceRollType.val(rrData.diceType || '');
            inputDesc.val(rrData.desc || '');
            rrData.actorIds.forEach((actorId) => {
                const actor = game.actors.get(actorId);
                this.selectedActors.push(actor);
            });
        }

        // record preference for using selected (lasso) vs. just selected from party check (dropdown)
        chkUseSelected.click(function (e) {
            const checked = $(this).prop('checked');
            if (checked) multiSelectControlGroup.hide();
            else multiSelectControlGroup.show();

            localStorage.setItem('rrUseSelected', checked);
        });

        // roll them dice
        btnRequestRolls.click(async (e) => {
            const shortUniqueId = () => {
                return Math.random().toString(36).substr(2, 9);
            };

            let actorIds = chkUseSelected.is(':checked') ? getSelected() : selectActors.val(),
                ability = selectAbility.val(),
                save = selectSaves.val(),
                misc = selectMisc.val(),
                dice = inputDice.val(),
                checkTarget = inputCheckTarget.val(),
                diceType = selectDiceRollType.val(),
                desc = inputDesc.val();

            if (actorIds.includes('all'))
                actorIds = [...selectActors[0].querySelectorAll('select option:not([value="all"])')]
                    .map((option) => option.value)
                    .filter(Boolean);

            ability = ability !== 'none' ? ability : null;
            save = save !== 'none' ? save : null;
            misc = misc !== 'none' ? misc : null;
            dice = dice !== '' ? dice : null;
            checkTarget = checkTarget !== '' ? +checkTarget : null;
            diceType = checkTarget !== '' ? diceType : null;

            const invalid = (!ability && !save && !misc && !dice && !checkTarget && !diceType) || actorIds.length === 0;
            if (invalid) {
                ui.notifications.error(
                    'Invalid selection - Make sure you have at least one Actor selected and at least one roll type'
                );
                return;
            }

            const createPendingEntry = (actor, type, id, value) => {
                const ability = game.i18n.localize(ARS.abilityTypes[value]);
                const save = game.i18n.localize(ARS.saveTypes[value]);
                let strLabel = '';
                if (type === 'ability') strLabel = ability + ' Check';
                else if (type === 'save') strLabel = save;

                if (value === 'surprise') {
                    type = 'surprise';
                    strLabel = 'Surprise';
                }
                if (type === 'dice') {
                    const vs = checkTarget ? ` vs ${checkTarget}` : '';
                    const type = checkTarget ? ` (${diceType})` : '';
                    strLabel = `Dice Roll ${value}${vs}${type}`;
                }

                items.push({
                    name: actor.name,
                    img: actor.img,
                    type: type,
                    label: strLabel,
                    result: 'Pending Roll...',
                    id: id,
                    pending: true,
                });
            };

            let selectedActors = [];
            for (const actorId of actorIds) {
                const actor = await game.actors.get(actorId);
                if (actor) {
                    const id = shortUniqueId();
                    if (ability) createPendingEntry(actor, 'ability', id, ability);
                    if (save) createPendingEntry(actor, 'save', id, save);
                    if (misc) createPendingEntry(actor, 'surprise', id, misc);
                    if (dice) createPendingEntry(actor, 'dice', id, dice);
                    selectedActors.push(actor);
                    actor.setFlag('ars', 'rrPendingId', id);
                }
            }

            const rrData = {
                actorIds,
                ability,
                save,
                dice,
                checkTarget,
                diceType,
                desc,
                misc,
                items,
                selectedActors,
            };

            await game.user.setFlag('ars', 'rrData', rrData);

            await this._requestRollsFromPlayers(rrData);

            this.render(true);
        });

        // clear
        btnRequestRollsClear.click(async (e) => {
            await game.user.unsetFlag('ars', 'rrData');
            localStorage.removeItem('rrUseSelected');
            this.render();
        });

        // collapsible containers
        btnCollapsible.click(function () {
            let header = $(this),
                container = header.next('.ars_clps_container'),
                open = container.is(':visible'),
                control = header.attr('data-vs-control'); // pull the correct property from the attribute on the node, should be object (parse with json.parse) to support multiple types of state!

            // collapsed toggle
            if (open) header.addClass('ars_clps_on');
            else header.removeClass('ars_clps_on');

            // update viewState data
            // ViewState.updateViewState(actor, 'collapsed', control, open);

            // dont bubble
            return false;
        });
    } // end activeListeners

    /** @override to add item population */
    async _render(force = false, options = {}) {
        await super._render(force, options);
    }

    getMemberArray() {
        // clean up missing actors
        // they got deleted but not removed from party
        if (this.members) {
            for (const [key, actor] of this.members) {
                if (!actor || !game.actors.get(key)) this.members.delete(key);
            }
            return Array.from(this.members, ([key, value]) => {
                return value;
            });
        }
        return [];
    }

    async _requestRollsFromPlayers(rollData) {
        const selectedActors = rollData.selectedActors;
        const gms = game.users.filter((user) => user.isGM)[0]; // Get the first GM

        // for each selected actor
        for (let actor of selectedActors) {
            // get all active, non-gm owners
            const owners = game.users.filter((user) => actor.testUserPermission(user, 'OWNER') && !user.isGM && user.active);
            if (owners.length) {
                for (let owner of owners) {
                    console.log(`Sending roll request to active owner ${owner.name} for ${actor.name}`);
                    SocketManager.notify(owner.id, actor.id, 'requestRoll', {
                        ownerId: owner.id,
                        actorId: actor.id,
                        rollData,
                    });
                }
            } else {
                console.log(`No active owners for ${actor.name}. Sending request to GM ${gms.name}`);
                await ARSRequestedRolls.handlePlayerRollRequest({
                    ownerId: gms.id,
                    actorId: actor.id,
                    rollData,
                });
            }
        }
    }

    static async handlePlayerRollRequest(data) {
        const { ability, save, misc, dice, checkTarget, diceType, desc } = data.rollData;
        const abilityText = game.i18n.localize(ARS.abilityTypes[ability]);
        const saveText = game.i18n.localize(ARS.saveTypes[save]);
        const miscText = misc !== 'none' ? utilitiesManager.capitalize(misc) : null;
        const sAbility = abilityText ? `<li>${abilityText} Check</li>` : '';
        const sSave = saveText ? `<li>${saveText} Save</li>` : '';
        const sSurprise = miscText ? `<li>${miscText}</li>` : '';
        const sCheckTarget = checkTarget ? ` vs. ${checkTarget}` : '';
        const sDicetype = checkTarget ? ` (${diceType})` : '';
        const sDiceDesc = desc !== '' ? `<h3 style="text-align: center;">${desc}</h3>` : '';
        const sDice = dice ? `<li>Dice Roll: ${dice}${sCheckTarget}${sDicetype}</li>` : '';

        if (game.user.id !== data.ownerId) return;

        const actor = game.actors.get(data.actorId);
        if (!actor) return ui.notifications.error('Actor not found!');

        const pendingId = actor.getFlag('ars', 'rrPendingId');
        actor.unsetFlag('ars', 'rrPendingId');

        const chatContent = `
            <div>
                ${sDiceDesc}
                <p><b>${actor.name}</b> has been requested to <b>roll dice for:</b></p>  
                <ul>             
                ${sAbility}${sSave}${sSurprise}${sDice}
                </ul>
                <button class="roll-confirm" 
                    data-actor-id="${actor.id}" 
                    data-ability-type="${ability}" 
                    data-save-type="${save}"  
                    data-misc-type="${misc}" 
                    data-dice-formula="${dice}" 
                    data-check-target="${checkTarget}" 
                    data-dice-type="${diceType}"
                    data-pending-type="${pendingId}"
                    data-desc="${desc}"
                >Roll Now</button>
            </div>
        `;

        ChatMessage.create({
            content: chatContent,
            speaker: { alias: actor.name },
            whisper: [data.ownerId],
        });
    }

    static async handleGMReceivedRollResult(data) {
        const rrData = await game.user.getFlag('ars', 'rrData');
        if (!rrData) return;

        // pending id - look for and remove any item with matching id from the final collection
        let pendingId = data.pending;
        let filteredItems = rrData.items.filter((item) => item.id !== pendingId);

        // add the filtered collection with the new items
        rrData.items = filteredItems.concat(data.items);

        // update requested roll data for GM
        await game.user.setFlag('ars', 'rrData', rrData);

        // refresh the app
        for (let key in ui.windows) {
            let app = ui.windows[key];
            if (app instanceof ARSRequestedRolls) {
                app.render(true, { focus: true });
            }
        }
    }

    static async confirmRoll(e) {
        // unpack all data
        const { actorId, abilityType, saveType, miscType, diceFormula, checkTarget, diceType, desc, pendingType } =
            e.currentTarget.dataset;

        // serialize
        const rollData = {
            abilityType: abilityType !== 'null' ? abilityType : null,
            saveType: saveType !== 'null' ? saveType : null,
            miscType: miscType !== 'null' ? miscType : null,
            diceFormula: diceFormula !== 'null' ? diceFormula : null,
            checkTarget: checkTarget !== 'null' ? checkTarget : null,
            diceType: diceType !== 'null' ? diceType : null,
            desc,
            pendingIds: pendingType,
        };

        const actor = game.actors.get(actorId);
        if (!actor) return ui.notifications.error('Actor not found!');

        // do roll
        this.performRoll(actor, rollData);
    }

    static async performRoll(actor, rollData) {
        // unpack all data from the request
        const { abilityType, saveType, miscType, diceFormula, checkTarget, diceType, pendingIds } = rollData;

        // utility for building out the final results object
        const buildRollResult = (actor, rollData, rollType) => {
            if (!rollData) return null;

            let result = rollData.success ? 'Success' : 'Failure';
            let label = rollData.label;
            if (rollType === 'dice') {
                const vs = checkTarget ? ` vs. ${checkTarget}` : '';
                const type = checkTarget ? ` (${diceType})` : '';
                label = `Dice Roll: ${diceFormula}${vs}${type}`;
                result = rollData._total;
            }

            return {
                name: actor.name,
                img: actor.img,
                type: rollType,
                label: label,
                result: result,
            };
        };

        // roll objects
        let rollAbilityCheckOnAttack = null,
            rollSaveCheck = null,
            rollSurprise = null,
            rollDice = null;

        // ability check
        if (abilityType) {
            rollAbilityCheckOnAttack = await new ARSDice(actor).makeAbilityCheckRollAsync(abilityType, 'd20');
        }

        // saving throw
        if (saveType) {
            rollSaveCheck = await new ARSDice(actor).makeSaveRollAsync(saveType);
        }

        // dice roll
        if (diceFormula) {
            rollDice = await new ARSDice(actor).makeGenericRoll(diceFormula, checkTarget, diceType);
        }

        // surprise
        if (miscType) {
            const surpriseItem = actor.skills.find((item) => item.name?.toLowerCase().includes('surprise'));
            if (surpriseItem) {
                rollSurprise = await new ARSDice(actor, surpriseItem).makeSkillRoll(false, saveType, true);

                if (!rollSurprise.success) {
                    const effectData = {
                        label: 'Surprised',
                        icon: surpriseItem.img,
                        origin: actor.uuid,
                        disabled: false,
                        duration: {
                            seconds: 60,
                            startRound: game.combat?.round ?? 0,
                            startTime: game.time.worldTime,
                            combat: game.combat?.id || null,
                        },
                        changes: [],
                    };

                    // Apply effect if not already present
                    if (!actor.effects.some((eff) => eff.name === 'Surprised')) {
                        await actor.createEmbeddedDocuments('ActiveEffect', [effectData]);
                    }
                }
            }
        }

        // build results using the helper function
        const results = [
            buildRollResult(actor, rollAbilityCheckOnAttack, 'ability'),
            buildRollResult(actor, rollSaveCheck, 'save'),
            buildRollResult(actor, rollSurprise, 'surprise'),
            buildRollResult(actor, rollDice, 'dice'),
        ].filter(Boolean);

        // get owners and GM
        const owners = game.users.filter((user) => actor.testUserPermission(user, 'OWNER') && !user.isGM && user.active);
        const gmUserId = game.users.find((user) => user.isGM)?.id;
        const data = {
            actorId: actor.id,
            ownerId: gmUserId,
            items: results,
            pending: pendingIds,
        };
        if (owners.length) {
            for (let owner of owners) {
                console.log(`Sending roll results from ${owner.name} to GM`);
                SocketManager.notify(game.user.id, gmUserId, 'sendRequestRollResult', data);
            }
        } else {
            console.log(`Owner is inactive. Sending roll results to GM`);
            await ARSRequestedRolls.handleGMReceivedRollResult(data);
        }
    }
}
