import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
import { CombatManager } from '../combat/combat.js';
import * as debug from '../debug.js';
import { ARS } from '../config.js';
import { ARSDamage } from '../dice/damage.js';
import { ARSItem } from '../item/item.js';
import { ARSActor } from '../actor/actor.js';
import {
    ARSRoll,
    ARSRollAttack,
    ARSRollBase,
    ARSRollCombat,
    ARSRollDamage,
    ARSRollSave,
    ARSRollInitiative,
    ARSRollSkill,
    ARSRollAbilityCheck,
} from '../dice/rolls.js';
import { ARSTokenDocument } from '../token/token.js';
import { ARSPsionics } from '../psionics/psionics.js';

/**
 *
 * Extend this to do some things with getters in ActiveEffect
 *
 *
 */
export class ARSActiveEffect extends ActiveEffect {
    // async _preCreate(data, options, user) {
    //     super._preCreate(data, options, user);
    // }

    // async _preUpdate(data, options, userId) {
    //     return super._preUpdate(data, options, userId);
    // }

    /**
     * Insert isMacro and lastTime into the changes.* fields
     *
     * @returns schemaData
     */
    //
    // you cannot override/change system datamodels apparently. This is ignored ;()
    //
    // static defineSchema() {
    //     const fields = foundry.data.fields;
    //     const schemaData = super.defineSchema();
    //     schemaData.changes = foundry.utils.mergeObject(schemaData.changes, {
    //         isMacro: new fields.BooleanField({ initial: false, label: 'ARS.EFFECT.isMacro' }),
    //         lastTime: new fields.NumberField({ initial: 0, label: 'ARS.EFFECT.lastTime' }),
    //     });
    //     return schemaData;
    // }
    /** @override */
    async _onDelete(options, id) {
        // console.log('effects.js ARSActiveEffect _onDelete', { options, id });
        await super._onDelete(options, id);

        if (game.user.isDM) {
            await this._tokenVisionHelper();
            // await this._hpHelper();
            await this._auraHelper();
        }
    }
    /** @override */
    async _onCreate(data, options, id) {
        // console.log('effects.js ARSActiveEffect _onCreate', { data, options, id });
        await super._onCreate(data, options, id);

        if (game.user.isDM) {
            await this._tokenVisionHelper();
            // await this._hpHelper();
            await this._auraHelper();
        }
    }
    /**@override */
    async _onUpdate(data, options, id) {
        // console.log('effects.js ARSActiveEffect _onUpdate', { data, options, id });
        await super._onUpdate(data, options, id);

        // const auraFound = this.changes.find((c) => c.key.toLowerCase() === 'special.aura');
        if (this.hasAura) {
            if (game.user.isDM) {
                if (this.target && this.target instanceof ARSActor) {
                    try {
                        const token = this.target?.getToken();
                        await token.checkForAuras();
                        await token.object.updateAuras();
                        // await this?.parent?.getToken()?.checkForAuras();
                    } catch (err) {
                        console.warn(`${err}`);
                    }
                } else if (this.target) {
                    if (this.target instanceof ARSItem && this?.target?.parent instanceof ARSActor) {
                        await this.target.refreshEffects();
                    }
                }
            }
            await this._auraHelper();
        }

        if (game.user.isDM) {
            await this._tokenVisionHelper();
        }
    }

    /**
     * getter override to determine if the item is equipped/identified, if not, supress.
     */
    get isSuppressed() {
        // const originItem = this?.originItem;
        const originItem = this?.parent instanceof ARSItem ? this.parent : undefined;
        const isNPC = this.target && ['npc'].includes(this.target?.type);
        // check to see if target npc and if so we dont suppress for un-identified items
        // if (this.target && ['npc'].includes(this.target?.type)) {
        //     return super.isSuppressed;
        // } else
        // if the effect comes from a item then we check item identification and supress if not id'd
        if (originItem) {
            // these types of items dont need to be equipped to work
            if (['ability', 'background', 'class', 'race', 'proficiency', 'skill'].includes(originItem.type)) {
                return super.isSuppressed;
            } else {
                const activeEffect = originItem.isEquipped && (isNPC || originItem.isIdentifiedRaw);
                return !activeEffect;
            }
        }
        return super.isSuppressed;
    }

    /** should effect be visible in UI */
    get isVisible() {
        if (game.user.isGM) return true;
        return !this.isSuppressed;
    }

    /**
     * Can the viewer see this effect?
     *
     */
    get canSee() {
        let canSee = true;
        if (this?.system?.visibility) {
            switch (this.system.visibility) {
                case 'owner':
                    if (this.system?.source) {
                        const origin = fromUuidSync(this.system.source);
                        if (origin) {
                            canSee = origin?.isOwner;
                        }
                    }
                    break;
                case 'gmonly':
                    canSee = game.user.isGM;
                    break;
                // 'always' falls through here to be true
                default:
                    canSee = true;
                    break;
            }
        }

        return canSee;
    }

    /** getter to return if the effect object has a aura effect change entry*/
    get hasAura() {
        return this.changes.find((c) => c.key.toLowerCase() === 'special.aura');
    }

    get calculateEffectDuration() {
        const startTime = this.duration.startTime;
        const currentTime = game.time.worldTime;

        // Calculate elapsed time in seconds
        const elapsedTime = currentTime - startTime;

        // Constants for time conversions
        const secondsInHour = 3600;
        const secondsInDay = 86400;
        const secondsInTurn = 600;
        const secondsInRound = 60;

        // Remaining time variables
        let remainingTime = elapsedTime;

        // Calculate days
        const days = Math.floor(remainingTime / secondsInDay);
        remainingTime -= days * secondsInDay;

        // Calculate hours
        const hours = Math.floor(remainingTime / secondsInHour);
        remainingTime -= hours * secondsInHour;

        // Calculate turns
        const turns = Math.floor(remainingTime / secondsInTurn);
        remainingTime -= turns * secondsInTurn;

        // Calculate rounds
        const rounds = Math.floor(remainingTime / secondsInRound);
        remainingTime -= rounds * secondsInRound;

        // Remaining seconds
        const seconds = Math.floor(remainingTime);

        // Build the formatted string from the largest to smallest component
        const parts = [];
        if (days > 0) parts.push(`${days} day${days > 1 ? 's' : ''}`);
        if (hours > 0) parts.push(`${hours} hr${hours > 1 ? 's' : ''}`);
        if (turns > 0) parts.push(`${turns} trn${turns > 1 ? 's' : ''}`);
        if (rounds > 0) parts.push(`${rounds} rnd${rounds > 1 ? 's' : ''}`);
        if (seconds > 0 || parts.length === 0) parts.push(`${seconds} sec${seconds > 1 ? 's' : ''}`);

        return parts.join('/');
    }

    /**override */
    /**
     * @override
     *
     * This is so I can process formula in values
     *
     * @param {*} model
     * @param {*} change
     * @param {*} field
     * @returns
     */
    static applyField(model, change, field) {
        // console.log('effects.js applyField', { model, change, field });
        field ??= model.schema.getField(change.key);
        const current = foundry.utils.getProperty(model, change.key);
        // so we run through formula
        const isJSON = utilitiesManager.isJSONString(change.value);
        const isFormula = utilitiesManager.isValidFormula(change.value);
        change.value =
            !isJSON && isFormula ? utilitiesManager.evaluateFormulaValueSync(change.value, model?.getRollData()) : change.value;
        //
        const update = field.applyChange(current, model, change);
        foundry.utils.setProperty(model, change.key, update);
        return update;
    }

    /**
     * Apply an ActiveEffect that uses an UPGRADE, or DOWNGRADE application mode.
     * Changes which UPGRADE or DOWNGRADE must be numeric to allow for comparison.
     * @param {Actor} actor                   The Actor to whom this effect should be applied
     * @param {EffectChangeData} change       The change data being applied
     * @param {*} current                     The current value being modified
     * @param {*} delta                       The parsed value of the change object
     * @param {object} changes                An object which accumulates changes to be applied
     * @private
     */
    _applyUpgrade(actor, change, current, delta, changes) {
        // console.log("effects.js _applyUpgrade isSuppressed", { actor, change, current, delta, changes })
        let update;
        const ct = foundry.utils.getType(current);
        switch (ct) {
            case 'boolean':
            case 'number':
                if (change.mode === CONST.ACTIVE_EFFECT_MODES.UPGRADE && delta > current) update = delta;
                else if (change.mode === CONST.ACTIVE_EFFECT_MODES.DOWNGRADE && delta < current) update = delta;
                break;
            // Fix: IF there is not an existing value, this will use delta as it
            // so it wont try to turn a number into a string or something
            default:
                update = delta;
                break;
        }
        changes[change.key] = update;
    }

    /**
     * @override to allow formula evaluations in values
     *
     * Cast a raw EffectChangeData change string to the desired data type.
     * @param {string} raw      The raw string value
     * @param {string} type     The target data type that the raw value should be cast to match
     * @returns {*}             The parsed delta cast to the target data type
     * @private
     */
    _castDelta(raw, type) {
        // console.trace('effects.js ARSActiveEffect', { raw, type });
        let delta;
        switch (type) {
            case 'boolean':
                delta = Boolean(this._parseOrString(raw));
                break;
            case 'number':
                // delta = Number.fromString(utilitiesManager.evaluateFormulaValueSync(raw, this?.target?.getRollData()));
                delta = Number.fromString(raw);
                if (Number.isNaN(delta)) delta = 0;
                break;
            case 'string':
                // delta = String(utilitiesManager.evaluateFormulaValueSync(raw, this?.target?.getRollData()));
                delta = String(raw);
                break;
            default:
                // test if json string, then test if is formula
                // BUG: Need to sort out ordering of ability score modifiers
                // like @abilities.dex.reaction
                // derived data before effects are dealt with... but then effects
                // also set ability scores...

                // const isJSON = utilitiesManager.isJSONString(raw);
                // const isFormula = utilitiesManager.isValidFormula(raw);
                // if (!isJSON && isFormula) {
                //     delta = utilitiesManager.evaluateFormulaValueSync(raw, this?.target?.getRollData());
                // } else delta = this._parseOrString(raw);
                const isFormula = utilitiesManager.isValidFormula(raw);
                if (isFormula) {
                    // these formula are parsed in actor.js
                    // test if its a number, convert it to that from string
                    delta = raw?.trim() !== '' && Number.isFinite(Number(raw)) ? Number(raw) : raw;
                } else delta = this._parseOrString(raw);
        }
        return delta;
    }

    /** helper for light and sight updates */
    async _tokenVisionHelper() {
        if (['character', 'npc'].includes(this?.target?.type)) {
            await this.target.getToken()?.updateLight();
            await this.target.getToken()?.updateSight();
        }
    }
    /** aura helper for effects with auras */
    async _auraHelper() {
        // if deleting a aura effect
        if (game.user.isDM && this.hasAura) {
            if (await this.target?.getToken) {
                const token = await this.target.getToken();
                token.checkForAuras();
                for (const t of canvas.tokens.placeables) {
                    if (t.hasAura) {
                        await t.updateAuras();
                    }
                }
            }
        }
    }
    /** helper for hp changes due to effects Shouldnt be needed anymore */
    // async _hpHelper() {
    //     // if deleting a hp.base effect
    //     const _isRelevantChange = ({ key }) => {
    //         const loweredKey = key.toLowerCase();
    //         return loweredKey === 'system.attributes.hp.base' || loweredKey === 'system.abilities.con.value';
    //     };
    //     if (this.changes.find(_isRelevantChange)) {
    //         const updatedClassHPData = await this.target._getClassHPData();
    //         await this.target.update(updatedClassHPData);
    //     }
    // }
}

/**
 * A form designed for creating and editing an Active Effect on an Actor or Item entity.
 * @implements {FormApplication}
 *
 * @param {ActiveEffect} object     The target active effect being configured
 * @param {object} [options]        Additional options which modify this application instance
 */
export class ARSActiveEffectConfig extends ActiveEffectConfig {
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ['ars', 'sheet', 'active-effect-sheet'],
            title: 'EFFECT.ConfigTitle',
            template: 'systems/ars/templates/effects/active-effect-config.hbs',
            width: 560,
            // height: 560,
            resizable: true,
            tabs: [{ navSelector: '.tabs', contentSelector: 'form', initial: 'details' }],
        });
    }

    async getData() {
        const context = await super.getData();
        context.selectEffectKeys = game.ars.config.selectEffectKeys;
        return context;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        html.find('.changes-list .effect-change .effect-change-key').change((event) => this._updateKeyValue(event));

        // setup codemirror for effects textareas
        utilitiesManager.initCodeMirror(html);
    }

    async _updateKeyValue(event) {
        // event.stopPropagation();
        console.log('effects.js _updateKeyValue', { event }, this);
        // const effect = this.document;

        const element = event.currentTarget;
        const value = element.value;
        const li = element.closest('li');
        // const dataset = li.dataset;
        // const index = dataset.index;

        // console.log("effects.js _updateKeyValue", { element, effect, value, li, dataset, index });
        if (value) {
            const details = game.ars.config.selectEffectKeys.find((a) => a.name === value);
            if (details) {
                event.preventDefault();
                // Find the input elements with class names "mode" and "value"
                const modeInput = li.querySelector(`.mode select`);
                const valueInput = li.querySelector(`.value textarea`);

                modeInput.value = details.mode;
                valueInput.value = details.value;
            }
        }
    }
} // end ARSActiveEffectConfig
/**
 * Manage Active Effect instances through the Actor Sheet via effect control buttons.
 * @param {MouseEvent} event      The left-click event on the effect control
 * @param {Actor|Item} owner      The owning entity which manages this effect
 */
export async function onManageActiveEffect(event, owner) {
    event.stopImmediatePropagation();
    const a = event.currentTarget;
    const li = a.closest('li');
    const Uuid = li.dataset.effectUuid;
    const Id = li.dataset.effectId;
    let effect = owner.effects.get(Id) ?? null;
    if (!effect && Uuid) effect = (await fromUuid(Uuid)) || null;

    // const effect = li.dataset.effectId ? owner.effects.get(li.dataset.effectId) : null;

    // console.log('effects.js onManageActiveEffect', { event, owner, a, li, Uuid, Id, effect });

    switch (a.dataset.action) {
        case 'create':
            return await owner.createEmbeddedDocuments('ActiveEffect', [
                {
                    name: 'New Effect',
                    img: 'icons/svg/aura.svg',
                    origin: owner.uuid,
                    'duration.rounds': li.dataset.effectType === 'temporary' ? 1 : undefined,
                    disabled: li.dataset.effectType === 'inactive',
                },
            ]);
            break;
        case 'edit':
            return effect.sheet.render(true);
            break;
        case 'delete':
            if (await dialogManager.confirm(`Delete <b>${effect.name}</b> effect?`, 'Delete Effect')) {
                // ! bulk selection support
                if (window.bulkItemSelection?.length) {
                    for (const itemId of window.bulkItemSelection) {
                        let effect = owner.effects.get(itemId) ?? null;
                        if (effect) effect.delete();
                    }
                } else {
                    return effect.delete();
                }
            }
            break;
        case 'toggle':
            return effect.update({ disabled: !effect.disabled });
            break;
    }
}

/**
 * Prepare the data structure for Active Effects which are currently applied to an Actor or Item.
 * @param {ActiveEffect[]} effects    The array of Active Effect instances to prepare sheet data for
 * @return {object}                   Data for rendering
 */
export function prepareActiveEffectCategories(effects) {
    // console.log("effects.js prepareActiveEffectCategories effects:", effects);

    // Define effect header categories
    const categories = {
        temporary: {
            type: 'temporary',
            label: game.i18n.localize('ARS.ActiveEffects.categories.temporary'),
            info: ['Temporary Effects'],
            effects: [],
        },
        passive: {
            type: 'passive',
            label: game.i18n.localize('ARS.ActiveEffects.categories.passive'),
            info: ['Passive Effects'],
            effects: [],
        },
        inactive: {
            type: 'inactive',
            label: game.i18n.localize('ARS.ActiveEffects.categories.inactive'),
            info: ['Inactive Effects'],
            effects: [],
        },
        suppressed: {
            type: 'suppressed',
            label: game.i18n.localize('ARS.ActiveEffects.categories.suppressed'),
            effects: [],
            info: [`Suppressed`],
        },
    };

    // Iterate over active effects, classifying them into categories
    for (let e of effects) {
        // e._getSourceName(); // Trigger a lookup for the source name
        e.SourceName = e.parent?.name || game.i18n.localize('None');
        if (e.isSuppressed) categories.suppressed.effects.push(e);
        else if (e.disabled) categories.inactive.effects.push(e);
        else if (e.isTemporary) categories.temporary.effects.push(e);
        else categories.passive.effects.push(e);
    }

    return categories;
}

/**
 * Get the changes.key that matches 'keymatch' string from a list of effects.
 *
 * @param {String} keymatch - The key to match in the effect changes.
 * @param {Array} effects - The list of effects to search through.
 * @returns {Array} An array of effects with matching change keys.
 */
export function getEffectsByChangesKey(keymatch, effects) {
    if (!keymatch || typeof keymatch !== 'string') {
        console.error('effects.js getEffectsByChangesKey() Invalid keymatch provided.');
        return [];
    }

    if (!Array.isArray(effects)) {
        console.error('effects.js getEffectsByChangesKey() Invalid effects array provided.');
        return [];
    }

    return effects.filter((effect) =>
        effect.changes?.some((change) => change.key?.toLowerCase() === keymatch.trim().toLowerCase())
    );
}

/**
 *
 * Create ActiveEffect from sourceActor on targetActor using action properties
 *
 * @param {*} sourceActor  The actor source of the action triggering the effect application
 * @param {*} targetActor The token target of the action effect.
 * @param {*} action The action that contains the effect to apply to targetActor
 * @param {*} chatUser The user that executed the application (used for when GM has to apply the effect)
 *
 */
export async function applyActionEffect(sourceObject, targetToken, action, chatUser = game.user.id) {
    // console.log('effects.js applyActionEffect ', { sourceObject, targetToken, action, chatUser });
    const sourceActor = sourceObject instanceof ARSActor ? sourceObject : sourceObject.actor;
    // const sourceToken = sourceObject instanceof ARSToken ? sourceObject : sourceObject.getToken();
    const sourceItem = sourceObject instanceof ARSItem ? sourceObject : undefined;
    const actionItemParent = action ? await fromUuid(action.parentuuid) : undefined;
    const itemRollData = actionItemParent && !actionItemParent.isSpell ? actionItemParent.getRollData() : undefined;

    const rollData = itemRollData
        ? itemRollData
        : sourceActor
        ? sourceActor.getRollData()
        : sourceItem
        ? sourceItem.getRollData()
        : undefined;

    /**
     *
     * we use this since you can't async in a string.replace() and we need it to get formula results
     *
     * @param {*} str
     * @param {*} regex
     * @param {*} asyncFn
     * @returns
     */
    async function replaceAsync(str, formulaActor, regex, regexCleanup, asyncFn) {
        const promises = [];
        str.replace(regex, (match, ...args) => {
            const promise = asyncFn(match, formulaActor, regexCleanup, ...args);
            promises.push(promise);
        });
        const data = await Promise.all(promises);
        return str.replace(regex, () => data.shift());
    }
    const _evaluateFormulaValue = async (match, rollData, regexCleanup, p1, offset, string) => {
        /// evaluate [[1d6]] or [[@valid.formula +3]] in key/value before applying
        let formula = p1.replace(regexCleanup, '');
        //be able to formulate source or target actor using {{}} for source and [[]] for target
        // const rollData = formulaActor ? formulaActor.getRollData() : null;
        const formulaResult = await utilitiesManager.evaluateFormulaValue(formula, rollData);
        return formulaResult;
    };
    const _evalKeys = async (changes) => {
        for (let index = 0; index < changes.length; index++) {
            // manage "dice rolls" within the key/value
            //be able to formulate source or target actor using {{}} for source and [[]] for target
            const regExSource = new RegExp('({{.*}})', 'ig');
            const regExSourceCleanup = /[\{\}]/g;

            const regExTarget = new RegExp('(\\[\\[.*\\]\\])', 'ig');
            const regExTargetCleanup = /[\[\]]/g;

            // check for source formula
            if (changes[index].key?.match(regExSource)) {
                changes[index].key = await replaceAsync(
                    changes[index].key,
                    rollData,
                    regExSource,
                    regExSourceCleanup,
                    _evaluateFormulaValue
                );
            }
            if (changes[index].value?.match(regExSource)) {
                changes[index].value = await replaceAsync(
                    changes[index].value,
                    rollData,
                    regExSource,
                    regExSourceCleanup,
                    _evaluateFormulaValue
                );
            }

            // check for target formula
            if (changes[index].key?.match(regExTarget)) {
                changes[index].key = await replaceAsync(
                    changes[index].key,
                    targetToken.actor.getRollData(),
                    regExTarget,
                    regExTargetCleanup,
                    _evaluateFormulaValue
                );
            }
            if (changes[index].value?.match(regExTarget)) {
                changes[index].value = await replaceAsync(
                    changes[index].value,
                    targetToken.actor.getRollData(),
                    regExTarget,
                    regExTargetCleanup,
                    _evaluateFormulaValue
                );
            }
        }
        return changes;
    };

    //look for a status "effect" entry
    let statusIds = [];
    action.effect.changes.forEach(async (change, index) => {
        // console.log('effects.js applyActionEffect', { change, index });
        if (change.key.toLowerCase() === 'special.status' && change.value) {
            statusIds = statusIds.concat(change.value.split(',').map((text) => text.trim().toLowerCase()) || []);
        }
    });

    // here we look for status effects the actor is immune too and if match dont apply
    if (targetToken?.actor?.isStatusImmune(Array.isArray(statusIds) ? statusIds : [statusIds])) {
        ui.notifications.warn(`${targetToken.actor.name} is immune to ${statusIds}.`);
        return;
    }

    const formula = action.effect.duration.formula || '';
    const durationRollResult = formula ? await utilitiesManager.evaluateFormulaValue(formula, rollData) : 0;
    let effectDuration = utilitiesManager.convertTimeToSeconds(formula ? durationRollResult : 0, action.effect.duration.type);

    const targetOfEffect = targetToken?.actor || sourceItem;
    let canSee = action.effect?.system?.visibility;
    // if npc and effect visibility set to default, we hide it
    if ((!canSee || canSee == 'default') && sourceActor.type == 'npc' && targetToken?.actor?.type == 'character') {
        canSee = 'owner';
    }
    const systemData = foundry.utils.duplicate(action.effect?.system ?? {});
    //store source uuid on effect
    systemData.source = sourceObject?.uuid;
    // set effect visibility
    systemData.visibility = canSee;

    const effect = await targetOfEffect.createEmbeddedDocuments(
        'ActiveEffect',
        [
            {
                system: systemData,
                statuses: statusIds, // status is a Set now
                label: action.name,
                img: action.img,
                origin: `${targetToken?.actor?.uuid || sourceItem.uuid}`,
                'duration.seconds': effectDuration,
                'duration.startTime': game.time.worldTime,
                'duration.startRound': game.combat?.round,
                'duration.startTurn': game.combat?.turn,
                transfer: sourceItem ? true : false,
                changes: await _evalKeys(foundry.utils.deepClone(action.effect.changes)),
            },
        ],
        { parent: targetToken?.actor || sourceItem, target: targetToken?.actor || sourceItem }
    );
    // console.log("effects.js effect", { effect });
    const appliedEffect = Object?.values(effect)?.[0];
    // console.log('effects.js applyActionEffect', { appliedEffect });

    // what does .priority do?
    // effect.changes.priority =

    let cardData = {
        action: action,
        targetActor: targetToken?.actor || sourceItem.actor,

        targetToken: targetToken,
        sourceActor: sourceActor || sourceItem.actor,
        sourceItem: sourceItem,
        owner: sourceActor?.id || sourceItem.actor.id,
        effect: appliedEffect,
        game: game,
    };

    let chatData = {
        author: chatUser,
        speaker: ChatMessage.getSpeaker({ actor: sourceActor || sourceItem.actor }),
    };

    chatData.content = await renderTemplate('systems/ars/templates/chat/parts/chatCard-action-effectApplied.hbs', cardData);

    ChatMessage.create(chatData);
}

/**
 * Apply effect to sourceActor's targets using sourceAction properties
 *
 * @param {*} sourceObject ARSActor or ARSItem
 * @param {*} sourceAction
 */
export async function applyActionEffectHelper(sourceObject, sourceAction) {
    const sourceActor = sourceObject instanceof ARSActor ? sourceObject : undefined;
    const sourceItem = sourceObject instanceof ARSItem ? sourceObject : undefined;
    let targets = game.user.targets || undefined; // these are "targeted"
    // const aTargets =canvas.tokens.controlled;  // these are "selected"
    // game.actors.tokens[]

    // set self at effect level - granular
    const applySelf = sourceAction.effect.system?.applyself ?? false;
    if (applySelf) {
        let selfToken = undefined;
        if (sourceActor.token) {
            const sourceToken = sourceActor?.token?.object;
            selfToken = canvas.tokens.placeables.find((token) => token.id === sourceToken?.id);
        } else {
            selfToken = canvas.tokens.placeables.find((token) => token.actor.id === sourceActor?.id);
        }
        if (selfToken) {
            // remove existing targets
            game.user.targets.forEach((entry) => {
                entry.setTarget(false, {
                    user: game.user,
                    releaseOthers: true,
                    groupSelection: true,
                });
            });
            // set self as target
            await selfToken.setTarget(true, { user: game.user, releaseOthers: false, groupSelection: false });
        } else {
            ui.notification.error(`${sourceActor.name}: Unable to find valid token or actor for auto-self-target effect.`);
            return undefined;
        }
    }

    if (sourceActor && targets.size) {
        targets.forEach((target) => {
            // console.log("effects.js applyActionEffectHelper", { target });
            // if (game.user.isGM) {
            //     applyActionEffect(sourceActor, target, sourceAction);
            // } else {
            utilitiesManager.runAsGM({
                operation: 'applyActionEffect',
                sourceFunction: 'applyActionEffectHelper',
                user: game.user.id,
                targetActorId: target.actor.id,
                targetActorUuid: target.actor.uuid,
                targetTokenId: target.id,
                sourceActorId: sourceActor.id,
                sourceActorUuid: sourceActor.uuid,
                sourceTokenId: sourceActor.token ? sourceActor.token.id : null,
                sourceAction: sourceAction,
            });
            // }
        });
    } else if (sourceItem) {
        utilitiesManager.runAsGM({
            operation: 'applyActionEffect-Item',
            sourceFunction: 'applyActionEffectHelper',
            user: game.user.id,
            sourceActorId: sourceActor?.id || sourceItem.actor.id,
            sourceActorUuid: sourceActor?.uuid || sourceItem.actor.uuid,
            sourceTokenId: sourceActor?.token ? sourceActor.token.id : null,
            sourceAction: sourceAction,
            sourceItem: sourceItem.uuid,
        });
    } else {
        ui.notifications.error(`Need target to apply effect.`);
    }
}

/**
 * Remove effect on actor by ID
 *
 * @param {*} actor
 * @param {*} effectId
 */
// export async function undoEffect(actor, effectId) {
export async function undoEffect(sourceObject, effectId) {
    const targetActor = sourceObject instanceof ARSActor ? sourceObject : undefined;
    const sourceItem = sourceObject instanceof ARSItem ? sourceObject : undefined;
    // console.log("effects.js undoEffect", { targetToken, effectId });

    const effectTarget = targetActor ? targetActor : sourceItem;

    if (!game.user.isGM) return;

    let deleted = false;
    try {
        deleted = await effectTarget.deleteEmbeddedDocuments('ActiveEffect', [effectId]);
    } catch {}
    // console.log("effects.js undoEffect", { deleted });
    if (!deleted) {
        ui.notifications.error(`Unable to find effect ${effectId} on ${effectTarget.name}.`);
    } else {
        ui.notifications.info(`Removed effect ${deleted[0].name} on ${effectTarget.name}.`);
    }
}

/**
 *
 * Manage expirations for effects on NPCs in combat tracker
 * and PCs in the Party Tracker (ignored otherwise)
 *
 * Currently duration.seconds is all we watch for duration
 *
 * @param {*} worldTime
 */
export async function manageEffectExpirations(worldTime) {
    console.log('effects.js manageEffectExpirations CONFIG.time.roundTime', CONFIG.time.roundTime);

    /**
     *
     * Checks actor for expired effects, flags them and bulk removes
     *
     * @param {*} actor
     */
    async function _expireEffects(actor) {
        if (actor) {
            let reRender = false;
            // console.log("effects.js manageEffectExpirations _expireEffects", { actor });
            const expiredEffects = [];
            const disableEffects = [];
            for (const effect of actor.getActiveEffects()) {
                // console.log("effects.js manageEffectExpirations _expireEffects !effect.disabled && !effect.isSuppressed");
                if (effect.duration?.seconds) {
                    // console.log("effects.js manageEffectExpirations _expireEffects effect.duration?.seconds");
                    if (!effect.duration?.startTime) {
                        // console.log("effects.js manageEffectExpirations _expireEffects !effect.duration?.startTime");
                        // set a startTime to game.time.worldTime?
                        effect.update({
                            'duration.startTime': worldTime,
                        });
                    } else {
                        const startTime = effect.duration.startTime;
                        const duration = effect.duration.seconds;
                        const timePassed = worldTime - startTime;
                        if (timePassed >= duration) {
                            // if this is a from a item that the pc owns then we dont delete the effect
                            if (effect.parent instanceof ARSItem && effect.parent.parent instanceof ARSActor) {
                                disableEffects.push(effect.id);
                            } else {
                                expiredEffects.push(effect.id);
                            }
                        }
                    }
                }
            } // end list of active effects

            if (disableEffects.length) {
                for (const effectId of disableEffects) {
                    const effect = actor.getEffectOn(effectId);
                    console.log('effects.js manageEffectExpirations _expireEffects disableEffects', { effect });
                    effect.update({
                        'duration.startTime': 0,
                        disabled: true,
                    });
                }
                reRender = true;
            }
            if (expiredEffects.length) {
                //TODO: Feature. capture expired effects and look for follow up
                // effect that is attached to this one?
                actor.deleteEmbeddedDocuments('ActiveEffect', expiredEffects, { 'expiry-reason': 'expired duration' });
                console.log('effects.js manageEffectExpirations _expireEffects expired ------->', { actor });
                reRender = true;
            }
            if (reRender) actor.sheet.render();
        }
    }

    async function applyOngoing(actor, formula, isDamage, dmgType = '', hideDice = false) {
        // stop spamming the chat if healing/damaging and already MIN/MAX hp - just do it silently
        const skipBroadCast =
            (!isDamage && actor.system.attributes.hp.max == actor.system.attributes.hp.value) ||
            (isDamage && actor.system.attributes.hp.min == actor.system.attributes.hp.value);

        console.log('dice.js applyOngoing', { actor, formula, isDamage, dmgType });

        const _rolledDamage = new ARSRoll(formula, actor.getRollData());

        // _rolledDamage.rollMode = 'gmroll';
        await _rolledDamage.roll();

        // dont show dice rolls to anyone but the GM
        // if (game.dice3d)
        //     await game.dice3d.showForRoll(
        //         _rolledDamage,
        //         game.user,
        //         false

        //         // utilitiesManager.diceRollModeVisibility(_rolledDamage.rollMode)
        //     );

        //need this so that undo/npc damage is correct
        let targetToken = actor.getToken()?.object;
        if (actor.type !== 'character') {
            targetToken = actor.token?.object;
        }

        const dmg = new ARSDamage(isDamage, actor, targetToken, undefined, dmgType, _rolledDamage);
        await dmg.applyDamageAdjustments();

        console.log('dice.js makeDamageRoll', { dmg, _rolledDamage });

        // send damage chat message
        const otherDiff = await dmg.applyDamageToActor();

        // send to chat
        if (!skipBroadCast)
            await dmg.sendDamageChatCard(otherDiff, { flavor: ' ONGOING', showRolls: false, whisperActors: [actor] });
    }

    /**
     *
     * Check active effects for onGoing effects and apply them
     *
     * @param {*} actor
     */
    async function _ongoingEffects(actor) {
        // console.log("effects.js _ongoingEffects", { actor });

        function _duplicateFormula(formula, count) {
            let result = formula;
            for (let i = 1; i < count; i++) {
                result += ' + ' + formula;
            }
            return result;
        }

        const modes = game.ars.const.ACTIVE_EFFECT_MODES;
        const ongoingEffects = actor.getActiveEffects().filter((effect) => {
            return effect.changes.some((changes) => {
                return changes.key.toLowerCase() === 'special.ongoing';
            });
        });
        for (const effect of ongoingEffects) {
            // console.log('effects.js _ongoingEffects', { effect, worldTime });
            const startTime = effect.duration.startTime;
            const change = effect.changes.find((c) => c.key.toLowerCase() === 'special.ongoing');
            // for (const change of effect.changes) {
            if (change) {
                if (change.mode == modes.CUSTOM) {
                    // const flagPath = `lastTime.${change.key.slugify({ strict: true })}`;
                    const flagPath = `lastOngoingTime`;
                    const ongoingCountPath = 'ongoingCount';
                    let lastTime = (await effect.getFlag('world', flagPath)) ?? 0;
                    let ongoingCount = (await effect.getFlag('world', ongoingCountPath)) ?? 0;
                    // let lastTime = change.lastTime;

                    //sanity check, if somehow lastTime is in the future, reset to worldTime (now)
                    if (lastTime > worldTime) lastTime = worldTime;

                    // const lastTime = change.lastTime ?? 0;
                    const timePassed = lastTime ? worldTime - lastTime : worldTime - startTime;

                    /**
                     *
                     * Change.Key:
                     * * ongoing heal every 1 round
                     * * ongoing heal every 1 turn
                     * * ongoing damage every 1 round
                     *
                     * Change.Value:
                     * * 1d6+1 heal
                     * * 1d3+1 fire
                     */
                    //get "ongoing heal every 1 round"

                    // {type:"heal", rate:"1", cycle: "round", formula: "1d4"}
                    const details = JSON.parse(change.value.toLowerCase());
                    const type = details.type.toLowerCase();
                    const isDamage = details.type === 'damage';
                    // const isHeal = details.type === 'heal';
                    const ongoingPspEffect = type === 'pspmaintain' ? 'maintain' : type == 'psprecover' ? 'recover' : null;
                    const byRound = details.cycle === 'round';
                    const byTurn = details.cycle === 'turn';
                    const rate = parseInt(details.rate) || 1;
                    const cycle = details.cycle;
                    const formula = details.formula;
                    const dmgType = details.dmgtype;
                    const count = details.count ?? -1;
                    let canCountTrigger = (count && count > ongoingCount) || count == -1;
                    //if limited count what count are we on that has triggered so far?
                    if (!canCountTrigger && count > ongoingCount) canCountTrigger = true;
                    if (rate > 0 && canCountTrigger) {
                        const roundsPassed = Math.floor(timePassed / CONFIG.time.roundTime);
                        const turnsPassed = Math.floor(timePassed / CONFIG.time.turnTime);

                        // console.log("effects.js _ongoingEffects", { roundsPassed, turnsPassed });

                        if ((byTurn && turnsPassed >= rate) || (byRound && roundsPassed >= rate)) {
                            let count = 0;
                            let remaining = 0;

                            if (byRound) {
                                count = Math.floor(roundsPassed / rate);
                                remaining = (roundsPassed % rate) * CONFIG.time.roundTime;
                                // console.log("effects.js _ongoingEffects", { byRound, roundsPassed, count, remaining });
                            } else if (byTurn) {
                                count = Math.floor(turnsPassed / rate);
                                remaining = (turnsPassed % rate) * CONFIG.time.turnTime;
                                // console.log("effects.js _ongoingEffects", { byTurn, turnsPassed, count, remaining });
                            }
                            // apply count many of the ongoing effects
                            const _MAX_COUNT_TRIGGERS = actor.type === 'character' ? 30 : 1; // incase someone advances things by large amount.
                            const _LOTS_OF_TIME_PASSED = 20; // we dont show dice rolls after this many
                            // const resultOngoing = { token: undefined, total: 0, dmgTypes: '' };
                            if (count > _LOTS_OF_TIME_PASSED) {
                                ui.notifications.warn(
                                    `effects.js _ongoingEffects: Excessive count (${count}), limited number of ongoing effect applications of to ${_MAX_COUNT_TRIGGERS}`
                                );
                                count = _MAX_COUNT_TRIGGERS;
                            }

                            // #psionic - ongoing effects for psionics
                            if (ongoingPspEffect) {
                                const _formula = _duplicateFormula(formula, count);
                                await ARSPsionics.ongoingPspEffect({
                                    actor,
                                    effect,
                                    _formula,
                                    type: ongoingPspEffect,
                                    cycle,
                                });
                            }

                            // standard ongoing dmg/heal logic
                            else {
                                await applyOngoing(actor, _duplicateFormula(formula, count), isDamage, dmgType);
                            }

                            //modify last based on remaining so we dont lose
                            // store any remaining time that wasn't exact
                            const lastChecked = remaining > 0 ? worldTime - remaining : worldTime;
                            await effect.setFlag('world', flagPath, lastChecked);
                            // set the triggered count
                            await effect.setFlag('world', ongoingCountPath, parseInt(ongoingCount, 10) + 1);
                            // change.lastTime = lastChecked;
                            // effect.update({ changes: changes }, { diff: false });
                            // activatedOngoing = true;
                        } else {
                            // do nothing
                        }
                    } //rate > 0
                }
                // await effect.update({ changes: effect.changes }, { diff: false });
            } // for (const change of effect.changes)
            // if (activatedOngoing)
            // await actor.update({ 'effects': effectsBundle });
        }
    } // end _ongoingEffects

    if (game.combat?.active) {
        // flip through all NPCs in combat tracker
        for (const actor of game.combat.combatants.map((co) => co.actor)) {
            // check everything in CT but PCs
            if (actor.type !== 'character') {
                await _expireEffects(actor);
                await _ongoingEffects(actor);
            }
        }
    }
    // flip through all PCs in party tracker
    // const partyMembers = game.party.getMembers();
    // for (const member of partyMembers) {
    for (const [key, actor] of game.party.members) {
        // const actor = game.actors.get(member.id);
        await _expireEffects(actor);
        await _ongoingEffects(actor);
    }

    /**
     * Deal with dropped items, such as torches/lanterns with light/effects
     */
    if (game.scenes.current) {
        const currentScene = game.scenes.current;

        // Filter tokens on the current scene
        const lootableTokens = currentScene.tokens.filter((token) => {
            const actor = token.actor;
            if (!actor) return false;

            // Check if the actor type is 'lootable' and has the flag set to true
            return actor.type === 'lootable' && actor.getFlag('ars', 'droppedItem') === true;
        });

        for (const token of lootableTokens) {
            await _expireEffects(token.actor);
            await _ongoingEffects(token.actor);
        }
    }

    // console.log("effects.js manageEffectExpirations _expireEffects DONE");
}

/**
 *
 * Returns enabled/active effects
 *
 * @param {*} sourceItem Item this is from? (or not)
 * @returns {Array}
 */
export function getActiveEffects(sourceItem = undefined) {
    // let activeEffects = [];
    console.log('effects.js getActiveEffects', this, { sourceItem });
    let activeEffects = this.effects.filter((effect) => !effect.disabled && !effect.isSuppressed);

    // check for effects that are on the item only used in combat
    if (sourceItem) {
        const inCombatOnly = sourceItem.getItemUseOnlyEffects();
        if (inCombatOnly.length) activeEffects = activeEffects.concat(inCombatOnly);
    }

    // console.log("actor.js getActiveEffects", { activeEffects })
    return activeEffects;
}

/**
 *
 * Get Aura information from an effect
 *
 * @param {*} effect
 * @returns
 */
export function getAuraInfo(effect) {
    let auraInfo = undefined;
    // console.log("effects.js getAuraInfo", { effect })
    //only a single aura allowed per "effect"
    const change = effect.hasAura; // changes.find((c) => c.key.toLowerCase() === 'special.aura');
    if (change && change.value) {
        try {
            // let [distance, color, faction, opacity, shape] = change.value.toLowerCase().trim().split(" ");
            // console.log("effects.js getAuraInfo", change.value.toLowerCase())
            const details = JSON.parse(change.value.toLowerCase());
            // console.log("effects.js getAuraInfo", { details })
            details.distance = parseInt(details.distance);
            if (ARS.htmlBasicColors[details.color]) details.color = ARS.htmlBasicColors[details.color];
            let isSquare = false;
            if (details.shape == 'square') isSquare = true;
            if (details.opacity) details.opacity = parseFloat(details.opacity);

            // game.ars.const.TOKEN_DISPOSITIONS.FRIENDLY:
            // game.ars.const.TOKEN_DISPOSITIONS.NEUTRAL:
            // game.ars.const.TOKEN_DISPOSITIONS.HOSTILE:

            // default to hostile disposition
            let disposition = game.ars.const.TOKEN_DISPOSITIONS.HOSTILE;

            //TODO: allow comma separated faction list
            // const friendlyFaction = ['ally', 'friendly', 'friend', 'neutral', 'all'];
            // const factionsList = details.faction.toLowerCase().split(',').map(text => text.trim());
            // if (factionsList.some(faction => friendlyFaction.includes(faction))) {
            //     disposition = game.ars.const.TOKEN_DISPOSITIONS.FRIENDLY;
            // }

            switch (details.faction) {
                case 'self':
                case 'ally':
                case 'friend':
                case 'friendly':
                case 'neutral':
                case 'all':
                    disposition = game.ars.const.TOKEN_DISPOSITIONS.FRIENDLY;
                    break;

                // case 'foe':
                // case 'enemy':
                //     break;

                default:
                    break;
            }

            auraInfo = {
                distance: details.distance,
                color: details.color,
                isSquare: isSquare,
                opacity: details.opacity,
                permission: details.permission ?? 'all',
                disposition: disposition,
                faction: details.faction,
            };
        } catch (err) {
            ui.notifications.error(
                `Aura effect has invalue value configured ${change?.value}. See console log for more details. :${err}`
            );
            console.error(`Aura effect has invalue value ${err}`, { err, effect, change });
        }
    } else {
        ui.notifications.error(`Aura effect does not have value configured. See console log for more details.`);
        console.error(`Aura effect does not have value configured`, { effect });
    }

    return auraInfo;
}
