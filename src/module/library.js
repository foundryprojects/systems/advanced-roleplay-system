import { ARSItem } from './item/item.js';
import * as chatManager from './chat.js';
import * as utilitiesManager from './utilities.js';
import * as debug from './debug.js';

/**
 *
 * This scans compendiums and world spells and places them into game.ars.library.packs.items.*
 * for use in various spell lists
 *
 */
export async function buildPackItemList() {
    console.log('Building pack items list for game.ars.library.packs.items', {
        game,
    });
    const packItems = await utilitiesManager.getPackItems('Item', game.user.isDM);

    if (!game.ars.library.packs?.items) foundry.utils.mergeObject(game.ars.library.packs, { items: packItems });
    else game.ars.library.packs.items = packItems;

    console.log('Pack list built as game.ars.library.packs.items', {
        packItems,
    });
}

export async function buildPackActorList() {
    if (game.ars.library.loadingActors) return;

    game.ars.library.loadingActors = true;
    console.log('Building pack actors list for game.ars.library.packs.actors', {
        game,
    });

    let packActors = [];
    if (game.user.isDM) packActors = await utilitiesManager.getPackItems('Actor', game.user.isDM);

    if (!game.ars.library.packs?.actors) foundry.utils.mergeObject(game.ars.library.packs, { actors: packActors });
    else game.ars.library.packs.actors = packActors;
    console.log('Pack list built as game.ars.library.packs.actors', {
        packActors,
    });
    game.ars.library.loadingActors = false;
}

export default async function () {
    game.ars.library = {
        packs: {
            items: undefined,
            actors: undefined,
        },
        const: {
            location: {
                CARRIED: 'carried',
                EQUIPPED: 'equipped',
                NOCARRIED: 'nocarried',
            },
        },
    };

    buildPackItemList();
}
