import { ARS } from './config.js';
import * as effectManager from './effect/effects.js';
import { ARSActionGroup, createActionForNPCToken } from './action/action.js';
import * as initLibrary from './library.js';
import * as dialogManager from './dialog.js';
import { ARSCharacterSheet } from './actor/actor-sheet-character.js';
import { ARSNPCSheet } from './actor/actor-sheet-npc.js';

import * as debug from './debug.js';
import { CombatManager } from './combat/combat.js';
import { ARSPsionics } from './psionics/psionics.js';

/**
 *
 * Capitalize string
 *
 * @param {*} s
 * @returns
 */
export function capitalize(s) {
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
}

/**
 *
 * Request a action to be performed by the connected GM
 *
 *           utilitiesManager.runAsGM({
 *               operation: 'applyActionEffect',
 *               user: game.user.id,
 *               sourceActorId: source.id,
 *               sourceTokenId: sourceToken.id
 *               targetActorId: target.id,
 *               targetTokenId: token.id,
 *               targetItemId: item.id,
 *               itemUpdate: {'some.path': value}
 *               sourceAction: sourceAction
 *           });
 *
 * @param {Object} data Requested command data
 */
export async function runAsGM(data = {}) {
    // if GM we skip to the command
    if (game.user.isGM) {
        await runGMCommand(data);
    } else {
        // send data to socket and look for GM to run the command for user
        const dataPacket = {
            requestId: foundry.utils.randomID(16),
            type: 'runAsGM',
            ...data,
        };
        // Emit a socket event
        console.trace('runAsGM', { data, dataPacket });
        await game.socket.emit('system.ars', dataPacket);
    }
}

/**
 *
 * Process the requested GM Command
 *
 * @param {*} data Requested command data
 */
export async function processGMCommand(data = {}) {
    const findGM = game.users.activeGM;
    console.trace('processGMCommand', { data, findGM });
    if (!findGM) {
        ui.notifications.error(`No GM connected to process requested command.`);
        console.trace('processGMCommand No GM connected to process requested command.');
    }
    // check to see if the GM we found is the person we've emit'd to and if so run command.
    else if (findGM.id === game.user.id) {
        if (!game.ars.runAsGMRequestIds[data.requestId]) {
            // console.log("utilities.js processGMCommand", { data });

            // We do this to make sure the command is only run once if more than
            // one GM is on the server
            game.ars.runAsGMRequestIds[data.requestId] = data.requestId;

            /**
             * "data" is serialized and deserialized so the type is lost.
             * Because of that we just exchange IDs when we need protos/etc
             * and load them where needed
             *
             */
            // let source = data.sourceId ? canvas.tokens.get(data.sourceId) : undefined;
            // let target = data.targetId ? canvas.tokens.get(data.targetId) : undefined;

            // const sourceActor = data.sourceActorId ? game.actors.get(data.sourceActorId) : undefined;
            // const targetActor = data.targetActorId ? game.actors.get(data.targetActorId) : undefined;
            // const targetToken = data.targetTokenId ? canvas.tokens.get(data.targetTokenId) : undefined;

            // console.log("utilities.js processGMCommand", { data });

            await runGMCommand(data);
        } else {
            // requestId already processed
            console.log('utilities.js', 'processGMCommand', 'Unknown asGM request DUPLICATE ', { data });
            ui.notifications.error(`Duplicate asGM request command.`, {
                permanent: true,
            });
        }
    }
}

/**
 *
 * Run command as GM, final leg (after sending it to socket or directly executed by GM)
 *
 * @param {Object} data
 */
async function runGMCommand(data) {
    console.log('utilitis.js runGMCommand', { data });

    // const sourceActor = data.sourceActorId ? game.actors.get(data.sourceActorId) : undefined;
    const sourceActor = data.sourceActorUuid ? await fromUuid(data.sourceActorUuid) : game.actors.get(data.sourceActorId);

    const sourceToken = data.sourceTokenId ? canvas.tokens.get(data.sourceTokenId) : undefined;
    const targetActor = data.targetActorUuid ? await fromUuid(data.targetActorUuid) : game.actors.get(data.targetActorId);
    const targetToken = data.targetTokenId ? canvas.tokens.get(data.targetTokenId) : undefined;
    const targetItemId = data.targetItemId ? data.targetItemId : undefined;
    const targetItemUuid = data.sourceItem ? data.sourceItem : undefined;
    const combatTracker = data.combatTrackerId ? game.combats.get(data.combatTrackerId) : undefined;

    // console.log("utilitis.js runGMCommand", { sourceActor, sourceToken, targetActor, targetToken, targetItemId })
    switch (data.operation) {
        // case 'game.party.updateMember':
        //     if (data.sourceActorId) {
        //         game.party.updateMember(data.sourceActorId);
        //     }
        //     break;

        case 'gmDropItemOnMap':
            {
                console.log('Running gmDropItemOnMap...');
                _asGM_gmDropItemOnMap(data);
            }
            break;
        case 'deleteEmbeddedDocuments':
            if (targetToken && targetItemId) {
                targetToken.actor.deleteEmbeddedDocuments('Item', [targetItemId], { hideChanges: true });
            } else if (targetActor && targetItemId) {
                targetActor.deleteEmbeddedDocuments('Item', [targetItemId], {
                    hideChanges: true,
                });
            }
            break;

        case 'createEmbeddedDocuments':
            if (targetActor && data.itemData) {
                await targetActor.createEmbeddedDocuments('Item', [data.itemData], { hideChanges: true, skipCombine: true });
            }
            break;

        case 'deleteActiveEffect':
            await targetToken.actor.deleteEmbeddedDocuments('ActiveEffect', data.effectIds);
            break;

        case 'applyActionEffect':
            await effectManager.applyActionEffect(
                sourceToken ? sourceToken.actor : sourceActor,
                targetToken,
                data.sourceAction,
                data.user
            );
            break;

        case 'applyActionEffect-Item':
            if (targetItemUuid) {
                const item = fromUuidSync(targetItemUuid);
                if (item) {
                    await effectManager.applyActionEffect(item, undefined, data.sourceAction, data.user);
                }
            }
            break;

        case 'adjustTargetHealth':
            await setActorHealth(targetActor ? targetActor : targetToken.actor, data.targetHPresult);
            if (targetToken?.hasActiveHUD) canvas.tokens.hud.render();
            break;

        case 'adjustTargetPSP':
            await setActorPSP(targetActor ? targetActor : targetToken.actor, data.dmgType, data.dmgAdjustment);
            if (targetToken?.hasActiveHUD) canvas.tokens.hud.render();
            break;

        case 'unsetFlag':
            await targetToken.document.unsetFlag('world', data.flag.tag);
            break;

        case 'setFlag':
            await targetToken.document.setFlag('world', data.flag.tag, data.flag.data);
            break;

        case 'setCombatTrackerFlag':
            if (combatTracker) await combatTracker.setFlag('ars', data.flag.tag, data.flag.data);
            break;

        case 'itemUpdate':
            let itemToUpdate;
            if (targetToken && targetItemId && data.update) {
                itemToUpdate = await targetToken.actor.getEmbeddedDocument('Item', targetItemId);
                if (itemToUpdate) itemToUpdate.update(data.update);
            } else if (targetActor && targetItemId && data.update) {
                itemToUpdate = await targetActor.getEmbeddedDocument('Item', targetItemId);
                if (itemToUpdate) itemToUpdate.update(data.update);
            }
            break;

        case 'actorUpdate':
            if (targetActor && data.update) {
                await targetActor.update(data.update);
            } else if (targetToken && data.update) {
                await targetToken.actor.update(data.update);
            }
            break;

        case 'partyAddLogEntry':
            if (data.text && game.party?.addLogEntry) {
                await game.party.addLogEntry(data.text);
            }
            break;

        // case 'partyShareLootedCoins':
        //     if (sourceToken && game.party?.shareLootedCoins) {
        //         game.party.shareLootedCoins(sourceToken);
        //     }
        //     break;

        default:
            console.log('utilities.js processGMCommand Unknown asGM/runGMCommand request ', data.operation, { data });
            break;
    }
}

/**
 *
 * Adjust actor heath accounting for min/max
 *
 * @param {*} actor
 * @param {*} adjustment  12 or -12
 */
export function adjustActorHealth(actor, adjustment) {
    console.log('utilitis.js adjustActorHealth ', { actor, adjustment });

    const nCurrent = parseInt(actor.system.attributes.hp.value);
    const nMax = parseInt(actor.system.attributes.hp.max);
    const nMin = parseInt(actor.system.attributes.hp.min);

    // console.log("utilitis.js adjustActorHealth ", { actor, adjustment, nCurrent, nMax, nMin });

    const nNew = Math.clamp(nCurrent + parseInt(adjustment), nMin, nMax);
    setActorHealth(actor, nNew);
}
/**
 *
 * Set new health value on token.
 *
 * @param {*} targetActor  targetActor's health to adjust
 * @param {*} value  The new value of target tokens health
 */
export async function setActorHealth(targetActor, value) {
    console.log('utilities.js setActorHealth', { targetActor, value });
    await targetActor.update({ 'system.attributes.hp.value': value });
    // set status markers for health values
    // setHealthStatusMarkers(targetActor);
    // moved to _update() in actor.js
}

/**
 *
 * Set psp value on token.
 *
 * @param {*} targetActor  targetActor's health to adjust
 * @param {*} value  The new value of target tokens health
 */
export async function setActorPSP(targetActor, dmgType, pspAdjustment) {
    console.log('utilities.js setActorPSP', { targetActor, dmgType, pspAdjustment });

    if (isNaN(pspAdjustment)) {
        console.log('not a valid number');
        return;
    }

    const usePsionicsV1 = game.settings.get('ars', 'usePsionicsV1');
    const newPsp = Math.max(0, targetActor.system.psionics.psp.value - pspAdjustment);
    const pspPspAttack = Math.max(0, targetActor.totalPsionicAttackStrength - pspAdjustment);
    const newPspDefense = Math.max(0, targetActor.totalPsionicDefenseStrength - pspAdjustment);
    const pathPsp = 'system.psionics.psp.value';
    const pathPspAttack = 'system.psionics.pspattack.value';
    const pathPspDefense = 'system.psionics.pspdefense.value';

    // intBase = Math.max(0, intScore - 12);
    if (usePsionicsV1) {
        const bothPspPools = dmgType === 'psp';
        if (bothPspPools) targetActor.update({ [pathPspAttack]: pspPspAttack, [pathPspDefense]: newPspDefense });
        else if (dmgType == 'pspattack') targetActor.update({ [pathPspAttack]: pspPspAttack });
        else if (dmgType == 'pspdefense') targetActor.update({ [pathPspDefense]: newPspDefense });
    } else {
        targetActor.update({ [pathPsp]: newPsp });
    }
}

/**
 *
 * If the actor hp is at min or lower, mark them defeated
 *
 * @param {*} actorData
 */
export async function setHealthStatusMarkers(targetActor) {
    console.log('utilities.js setHealthStatusMarkers', { targetActor });
    // need token object, not token document
    const token = targetActor.getToken()?.object;
    if (token) {
        // console.log("utilities.js setHealthStatusMarkers", { token });
        const optionNegativeHP = game.settings.get('ars', 'optionNegativeHP');
        const isDead =
            optionNegativeHP && targetActor.type == 'character'
                ? targetActor.system.attributes.hp.value <= targetActor.system.attributes.hp.min
                : targetActor.system.attributes.hp.value <= 0;
        const isUnconscious = !isDead && optionNegativeHP ? targetActor.system.attributes.hp.value <= 0 : false;
        const hasDeathStatus = token.actor.hasStatusEffect('dead');
        const hasUnconsciousStatus = token.actor.hasStatusEffect('dying');
        // find dead status
        const statusDead = CONFIG.statusEffects.find((e) => e.id === 'dead');
        // const effectDead = token.actor && statusDead ? statusDead : CONFIG.controlIcons.defeated;
        const statusUnconscious = CONFIG.statusEffects.find((e) => e.id === 'dying');
        // const effectUnconscious = token.actor && statusUnconscious ? statusUnconscious : undefined;

        // check to see if they need dead mark or remove dead mark
        // if ((!dead && alreadyDown) || (dead && !alreadyDown)) {
        if ((!hasUnconsciousStatus && isUnconscious) || (hasUnconsciousStatus && !isUnconscious)) {
            await token.actor.toggleStatusEffect('dying', {
                overlay: isUnconscious,
                active: isUnconscious,
            });
        }

        if ((!hasDeathStatus && isDead) || (hasDeathStatus && !isDead)) {
            await token.actor.toggleStatusEffect('dead', { overlay: isDead, active: isDead });
        }
        // if (hasDeathStatus && isDead) {
        //     // dont need to add it, we have it already
        // } else {
        //     await token.toggleEffect(effect, {
        //         overlay: defeated,
        //         active: defeated,
        //     });
        // }
        // }
    }
}

/**
 *
 * @param {Number} slotIndex
 * @param {String} slotType arcaneSlots or divineSlots
 * @param {Boolean} bValue  Set true or false
 */
export async function memslotSetUse(actor, slotType, slotLevel, slotIndex, bValue) {
    let memSlots = foundry.utils.deepClone(actor.system.spellInfo.memorization);
    memSlots[slotType][slotLevel][slotIndex].cast = bValue;
    await actor.update({ 'system.spellInfo.memorization': memSlots });
}

/**
 *
 * @param {String} slotType  arcaneSlots or divineSlots
 * @param {Number} slotIndex
 *
 */
export function isMemslotUsed(actor, slotType, slotLevel, slotIndex) {
    // console.log("utilitis.js  isMemslotUsed", { actor, slotType, slotLevel, slotIndex });
    const spellUsed = actor.system.spellInfo.memorization[slotType][slotLevel][slotIndex]?.cast || false;
    return spellUsed;
}

// not sure we need this
// /**
//  * Return ammo remaining for weapon
//  *
//  * @param {*} actor
//  * @param {*} weapon
//  * @returns
//  */
// export async function getAmmoRemaining(actor, weapon) {
//     let itemId = weapon.system.resource.itemId;
//     if (!itemId) {
//         // itemId = await dialogManager.getInventoryItem(
//         //     actor,
//         //     `Ammo for ${weapon.name}`,
//         //     `Select Ammo`,
//         //     { inventory: actor.weapons }
//         // );
//         if (!itemId) return 0; // If no item id found, assume no ammo left
//     }
//     if (itemId) {
//         // let item = actor.items.get(itemId);
//         let item = actor.getEmbeddedDocument("Item", itemId);
//         if (item) {
//             return item.system.quantity;
//         }
//     }
//     return 0; // Default return value indicating no ammo left
// }

/**
 * Use ammo from a weapon. If ammo runs out, prompts user to select a new ammo item.
 *
 * @param {Object} actor - The actor entity using the weapon.
 * @param {Object} weapon - The weapon entity from which ammo is being used.
 * @returns {Promise<boolean>} - Returns true if ammo was successfully used, false otherwise.
 */
export async function useWeaponAmmo(actor, weapon) {
    if (!game.ars.config.ammoAttacks.includes(weapon.system.attack.type)) return true;
    try {
        // if weapon has ammo set to infinite yeap out
        if (weapon.system?.attributes?.infiniteammo) {
            return true;
        }

        // Use optional chaining to ensure there's no error if weapon is undefined.
        const ammoItem = weapon?.ammo;

        // Determine the quantity of the current ammo item.
        const ammoQuantity = ammoItem ? ammoItem.system.quantity : 0;

        // Check if there's a valid ammo item with non-zero quantity.
        if (ammoItem && ammoQuantity > 0) {
            await adjustAmmoQuantity(ammoItem, ammoQuantity);
            return true;
        }

        // If no ammo or quantity is zero, prompt user to select a new ammo item.
        const newAmmoItemId = await promptForAmmoSelection(actor, weapon);
        if (!newAmmoItemId) return false;

        // Update weapon with new ammo item's ID.
        await weapon.update({ 'system.resource.itemId': newAmmoItemId });

        // Recursively call this function to handle the newly set ammo.
        return await useWeaponAmmo(actor, weapon);
    } catch (error) {
        console.error('Error in useWeaponAmmo function:', error);
        return false;
    }
}

/**
 * Prompt the user to select a new ammo item from their inventory.
 *
 * @param {Object} actor - The actor entity in need of new ammo.
 * @param {Object} weapon - The weapon entity requiring ammo.
 * @returns {Promise<string|null>} - Returns the ID of the selected ammo item or null if none was chosen.
 */
async function promptForAmmoSelection(actor, weapon) {
    return dialogManager.getInventoryItem(actor, `Ammo for ${weapon.name}`, `Select Ammo`, { inventory: actor.weapons });
}

/**
 * Adjust the quantity of a given ammo item.
 *
 * @param {Object} ammoItem - The ammo item whose quantity needs to be adjusted.
 * @param {number} currentQuantity - The current quantity of the ammo item.
 */
async function adjustAmmoQuantity(ammoItem, currentQuantity) {
    // If there's more than one ammo item left, decrease its quantity.
    if (currentQuantity - 1 >= 0) {
        await ammoItem.update({ 'system.quantity': currentQuantity - 1 });
    } else {
        // If no ammo is left, remove the ammo item from the weapon.
        // await ammoItem.update({ 'system.resource.-=itemId': null });
        await ammoItem.update({ 'system.resource.itemId': '' });
    }
}

/**
 * Return charges remaining for action
 *
 * @param {*} actor
 * @param {*} itemSource
 * @param {*} action
 * @returns
 */
export function getActionCharges(actor, itemSource, action) {
    if (!actor || !action) return 0;
    const type = action.resource.type;
    const cost = action.resource.count.cost;
    const max = Number.isInteger(action.resource.count.max)
        ? action.resource.count.max
        : evaluateFormulaValueSync(action.resource.count.max, actor.getRollData());
    let used = parseInt(action.resource.count.value);
    const remaining = max - used;
    const remainingUses = Math.floor(remaining / cost);
    let itemId = action.resource.itemId;

    switch (type) {
        case 'charged':
            return remainingUses;

        case 'item':
        case 'powered':
            if (!itemId && type !== 'powered') {
                if (!itemId) return 0;
            } else if (!itemId && type === 'powered' && itemSource) {
                itemId = itemSource.id;
            }
            let item = actor.getEmbeddedDocument('Item', itemId);
            if (!item && itemSource && type === 'powered') item = itemSource;

            if (item) {
                if (type === 'item') {
                    const itemCount = item.system.quantity;
                    const itemCountUses = Math.floor(itemCount / cost);
                    return itemCountUses;
                } else if (type === 'powered' && item.system.charges) {
                    const poweredUsed = item.system.charges.value;
                    const maxCount = item.system.charges.max;
                    const poweredRemaining = maxCount - poweredUsed;
                    const powerRemainingUses = Math.floor(poweredRemaining / cost);
                    return powerRemainingUses;
                }
            } else {
                return 0;
            }
            break;

        default:
            return 0;
    }
    return 0;
}

/**
 *
 * Use a charge for an action
 *
 * @param {*} actor
 * @param {*} action
 * @returns
 */
export async function useActionCharge(actor, action, itemSource) {
    // console.trace('utilities.js useActionCharge', { actor, action, itemSource });

    if (!game.ars.config.chargedActions.includes(action.type) || action.resource.type == 'none') return true;

    const type = action.resource.type;
    const cost = action.resource.count.cost;
    const max = Number.isInteger(action.resource.count.max)
        ? action.resource.count.max
        : await evaluateFormulaValue(action.resource.count.max, actor.getRollData());
    let used = parseInt(action.resource.count.value);
    let itemId = action.resource.itemId;

    console.log('utilities.js useActionCharge', {
        type,
        cost,
        max,
        used,
        itemId,
    });

    switch (type) {
        case 'charged':
            if (used + cost <= max) {
                action.resource.count.value = used + cost;
                if (!action.resource.trackTime) action.resource.trackTime = game.time.worldTime;
                await action.parentGroup.save();
                return true;
            } else {
                // no charges left
                return false;
            }
            break;

        case 'item':
        case 'powered':
            if (!itemId && type !== 'powered') {
                if ((['potion'].includes(action.source.type) || action.source.isScroll) && action.source.system.quantity) {
                    itemId = action.source.id;
                }
                // if still no itemId prompt
                if (!itemId) {
                    itemId = await dialogManager.getInventoryItem(actor, `Resource for ${action.name}`, `Select Resource`);
                }
                if (!itemId) return false;
                action.resource.itemId = itemId;
                await action.parentGroup.save();
            } else if (!itemId && type === 'powered' && action.source) {
                action.resource.itemId = action.source.id;
                await action.parentGroup.save();
            }
            let item = await actor.getEmbeddedDocument('Item', itemId);
            if (!item && action.source && type === 'powered') item = action.source;

            if (item) {
                if (type === 'item') {
                    let itemCount = item.system.quantity;
                    if (itemCount - cost >= 0) {
                        await item.update({
                            'system.quantity': itemCount - cost,
                        });
                        return true;
                    } else {
                        // no more left, remove this item so next time it prompts to select
                        if (itemCount <= 0) {
                            action.resource.itemId = '';
                            await action.parentGroup.save();
                        }
                        return false;
                    }
                } else if (type === 'powered') {
                    // we use charges from another item to power this action
                    let poweredUsed = item.system.charges.value;
                    let maxCount = item.system.charges.max;
                    if (poweredUsed + cost <= maxCount) {
                        const newUsed = poweredUsed + cost;
                        await item.update({ 'system.charges.value': newUsed });
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                ui.notifications.error(
                    `Unable to find the object associated with this power. Check the action->resource section and verify the item is set correctly. `
                );
            }
            break;
    }
    // action.parentGroup.save();
    // console.log("utilities.js useActionCharge FALSE")
    return false;
}

/**
 * Evaluate a formula to it's total number value, async
 * @param {String} formula
 * @param {*} data
 * @param {*} options
 * @returns
 */
export async function evaluateFormulaValue(formula, data = {}, options = {}) {
    // console.log('utilities.js evaluateFormulaValueAsync', { formula, data });
    if (formula) {
        let fRoll;
        try {
            fRoll = await new Roll(String(formula), data).evaluate();
            // console.log("utilities.js evaluateFormulaValue 2", { formula, data, fRoll });
        } catch (err) {
            console.warn('utilities.js evaluateFormulaValueAsync formula process error', { err, formula, data, options });
            return 0;
        }

        // console.log("utilities.js evaluateFormulaValue 1", { fRoll });
        if (options && options.showRoll) {
            if (game.dice3d) await game.dice3d.showForRoll(fRoll, game.user, true);
        }
        return fRoll.total; // round it off
    } else {
        // console.log("utilities.js evaluateFormulaValue NOT FORMULA PASSED");
        return 0;
    }
}

/**
 *
 * Eval a formula w/o async
 *
 * @param {*} formula
 * @param {*} RollData
 * @returns
 */
export function evaluateFormulaValueSync(formula, data = {}, options = {}) {
    // console.log('utilities.js evaluateFormulaValue 1', { formula, data });
    if (formula) {
        let fRoll;
        try {
            fRoll = new Roll(String(formula), data).evaluateSync();
        } catch (err) {
            // console.trace();
            console.warn('utilities.js evaluateFormulaValue formula process', { err, data, formula, options });
            return 0;
        }
        if (options && options.showRoll) {
            if (game.dice3d) game.dice3d.showForRoll(fRoll, game.user, false);
        }
        return fRoll.total;
    } else {
        // console.log("utilities.js evaluateFormulaValue NO FORMULA PASSED");
        return 0;
    }
}

/**
 * Do somethings when NPC token first placed.
 *
 * Roll hp using static hp, hp calculation or hitdice
 *
 * @param {Object} token NPC Token instance
 */
export async function postNPCTokenCreate(token) {
    // wont run this if the npc token already has a .max value
    // if (!token.actor.system.attributes.hp.max) {
    const hitdice = String(token.actor.system.hitdice);
    const hpCalc = String(token.actor.system.hpCalculation);
    const hpStatic = parseInt(token.actor.system.attributes.hp.value);
    let formula = '';

    let hp = 0;
    if (hpStatic > 0) {
        hp = hpStatic;
    } else if (hpCalc.length > 0) {
        formula = hpCalc;
    } else if (hitdice.length > 0) {
        try {
            const aHitDice = hitdice.match(/^(\d+)(.*)/);
            const sHitDice = aHitDice[1];
            const sRemaining = aHitDice[2];
            formula = sHitDice + 'd8' + sRemaining;
        } catch (err) {
            ui.notifications.error(`Error in utilities.js postNPCTokenCreate() ${err}`);
        }
    }

    if (!hpStatic) {
        const sanitizedFormula = formula.replace(/\([^)]*\)/g, '');
        const roll = await new Roll(sanitizedFormula).evaluate();
        hp = roll.total;
    }
    // at least 1 hp
    hp = Math.max(hp, 1);

    console.log('_postNPCTokenCreate:createToken:hp', { hp });

    const npcNumberedNames = game.settings.get('ars', 'npcNumberedNames');
    let newName = '';
    if (npcNumberedNames) {
        // set NPC name # with random value so they can be called out
        const MaxRandom = 25;
        const npcTokens = token.collection
            ? token.collection.filter(function (mapObject) {
                  return mapObject?.actor?.type === 'npc';
              })
            : [];
        let randomRange = MaxRandom + npcTokens.length;
        let loops = 0;

        do {
            loops += 1;
            const nNumber = Math.floor(Math.random() * (randomRange + loops)) + 1;
            // if the name contains #XX remove it, happens when you copy/paste a existing npc token
            let sName = token.name.replace(/#\d+/, '');
            sName = `${sName} #${nNumber}`;
            const matchedNPCs = npcTokens.filter(function (npcToken) {
                return npcToken.name.toLowerCase() === sName.toLowerCase();
            });
            if (matchedNPCs.length < 1) {
                newName = sName;
            }
            if (loops > 100) {
                newName = `${token.name} #xXxXxX`;
            }
        } while (newName == '');
    } else {
        newName = token.name;
    }

    // if we have a list of damage entries and nothing in actions or no weapons, create some
    try {
        const damageList = token.actor.system.damage.match(/(\d+[\-dD]\d+([\-\+]\d+)?)/g);
        // token.actor.weapons
        // if (damageList?.length && token.actor.system?.weapons?.length < 1) {
        if (damageList?.length && token.actor?.weapons?.length < 1) {
            if (!token.actor.system.actionGroups || token.actor.system.actionGroups?.length < 1) {
                await createActionForNPCToken(token, damageList);
            }
        }
    } catch (err) {
        ui.notifications.error(`Error in utilities.js postNPCTokenCreate() ${err}`);
    }

    // calculate size when dropped.
    const protoSizeWidth = token.actor.prototypeToken.width;
    const protoSizeHeight = token.actor.prototypeToken.height;
    const customSettingSize = protoSizeHeight != 1 || protoSizeWidth != 1;
    console.log('_postNPCTokenCreate:createToken:hp', { token, protoSizeHeight, protoSizeWidth, hp }, protoSizeHeight != 1);
    const sizeSetting = ARS.tokenSizeMapping[token.actor.system.attributes.size] ?? 1;

    /**
     * do this so we can calculate xp at drop time with current hp for older systems
     *
     * 15+(@system.attributes.hp.max*100) would be 15+maxHP*100 and when killed granted to party xp
     *
     */
    const rollData =
        foundry.utils.mergeObject(token.actor.getRollData(), {
            // system: {
            attributes: {
                hp: { max: hp },
            },
            // }
        }) || 0;

    const xpTotal = await evaluateFormulaValue(token.actor.system.xp.value, rollData);

    console.log('utilities.js postNPCTokenCreate', { rollData, xpTotal }, token.actor.system.xp.value);

    // set values for updates
    const actorUpdates = {
        'system.attributes.hp.value': hp,
        'system.attributes.hp.max': hp,
        'system.attributes.hp.min': 0,
        'system.xp.value': xpTotal,
        name: newName,
    };
    const tokenUpdates = {
        name: newName,
        width: customSettingSize ? protoSizeWidth : sizeSetting,
        height: customSettingSize ? protoSizeHeight : sizeSetting,
        'xp.value': xpTotal,
    };

    // generate coin formulas when spawning npc
    for (const coin in token.actor.system.currency) {
        const formula = token.actor.system.currency[coin];
        if (formula) {
            const coinValue = parseInt(await evaluateFormulaValue(formula, null)) || 0;
            // actorUpdates[`system.currency.${coin}`] = coinValue;
            token.actor.giveSpecificCurrency(coinValue, coin);
        }
    }
    // console.log("utilities.js postNPCTokenCreate", { actorUpdates, tokenUpdates })
    // use this for token update instead?
    Hooks.call('arsUpdateToken', this, token, tokenUpdates, 300);
    Hooks.call('arsUpdateActor', token.actor, actorUpdates);
}

/**
 *
 * Sort callback function to sort by record.name
 *
 * @param {*} a
 * @param {*} b
 * @returns
 */
export function sortByRecordName(a, b) {
    if (a?.name && b?.name) {
        const nameA = a.name.toLowerCase();
        const nameB = b.name.toLowerCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
    }
    return 0;
}

/**
 *
 * Sort by the item.data.sort value
 *
 * @param {*} a
 * @param {*} b
 * @returns
 */
export function sortBySort(a, b) {
    const nameA = a.sort;
    const nameB = b.sort;
    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }
    return 0;
}

/**
 *
 * Sort callback function to sort by record.level
 *
 * @param {*} a
 * @param {*} b
 * @returns
 */
export function sortByLevel(a, b) {
    if (a.level < b.level) {
        return -1;
    }
    if (a.level > b.level) {
        return 1;
    }
    return 0;
}

/**
 *
 * Wrapper function to get item from world or from packs
 *
 * @param {*} itemId
 * @returns
 */
export async function getItem(itemId) {
    // look for item in world first
    let item = getWorldItem(itemId);
    // if not in world, find in packs
    if (!item) item = await getPackItem(itemId);
    return item;
}

/**
 *
 * Get item by name in world or pack file, first match
 *
 * @param {*} name
 * @returns
 */
export function getItemByName(name) {
    let item = getWorldItemByName(name);
    if (!item) item = getPackItemByName(name);
    return item;
}

/**
 *
 * Get an item from any pack file
 *
 * @param {*} itemId
 * @returns
 */
export async function getPackItem(itemId) {
    let item;
    const allItemPacks = game.packs.filter((i) => i.metadata.type === 'Item');
    // console.log('utilities.js getPackItem', { allItemPacks });
    for (const pack of allItemPacks) {
        const foundItem = await game.packs.get(pack.collection).getDocument(itemId);
        if (foundItem) {
            if (game.user.isGM || pack.visible) {
                item = foundItem;
            }
            break;
        }
    }
    // const itemIdx = game.ars.library.packs.itemsIndex.find((item) => item.id == `_${itemId}`);
    // const item = fromUuidSync(itemIdx.uuid);
    return item;
}

/**
 *
 * Get an item in the Campaign local
 *
 * @param {*} itemId
 * @returns
 */
export function getWorldItem(itemId) {
    const item = game.items.get(itemId);
    // console.log("utilitiesManager.js getWorldItem 1", { itemId, item });
    return item;
}

/**
 *
 * Get a item by name from pack files
 *
 * @param {*} name
 * @returns
 */
export function getPackItemByName(name) {
    return game.ars.library.packs.items.find((entry) => entry.name.toLowerCase() === name.toLowerCase());
}

/**
 *
 * Get a item from world files by name
 *
 * @param {*} name
 * @returns
 */
export function getWorldItemByName(name) {
    return game.items.getName(name);
}

/**
 * Function to allow refresh of library data, used by modules
 */
export async function initializeLibrary() {
    await initLibrary.default();
}

/**
 *
 * Get folder record or create one to match name/type.
 *
 * @param {String} folderName "Dropped NPCs"
 * @param {String} type "Actor" or "Item" .etc
 * @param {String} sort "a" for auto, "m" for manual
 * @param {Number} sortNumber "0"
 * @returns Folder
 */
export async function getFolder(folderName, type, sort = 'a', sortNumber = 0) {
    let folder;
    // const folderList = game.folders.filter(a => { a.name === folderName && a.type == type });
    const foundFolder = game.folders.getName(folderName);
    // console.log("utilitiesManager.js getFolder 1", { foundFolder })
    if (foundFolder) {
        // console.log("utilitiesManager.js getFolder 2", { foundFolder })
        folder = foundFolder;
    } else {
        let folderData = {
            name: folderName,
            type: type,
            sorting: 'a',
            sort: 0,
        };
        const rootFolder = await Folder.implementation.create(folderData);
        // console.log("utilitiesManager.js getFolder 3", { rootFolder })
        folder = rootFolder;
    }

    // console.log("utilitiesManager.js getFolder", { folder })
    return folder;
}
/**
 *
 *  Clear save Cache
 *
 * @param {Actor} sourceActor
 * @param {Token} targetToken
 */
export async function deleteSaveCache(targetToken) {
    await runAsGM({
        operation: 'unsetFlag',
        user: game.user.id,
        targetTokenId: targetToken.id,
        targetActorId: targetToken.actor.id,
        targetActorUuid: targetToken.actor.uuid,
        // sourceActorId: sourceActor.id,
        flag: {
            tag: 'saveCache',
        },
    });
}

/**
 *
 * Set the saveCache for follow up spell damage reductions
 *
 * @param {Actor} sourceActor
 * @param {Token} targetToken
 */
export async function setSaveCache(sourceActor, targetToken) {
    await runAsGM({
        operation: 'setFlag',
        user: game.user.id,
        targetTokenId: targetToken.id,
        targetActorId: targetToken.actor.id,
        targetActorUuid: targetToken.actor.uuid,
        sourceActorId: sourceActor.id,
        sourceActorUuid: sourceActor.uuid,
        flag: {
            tag: 'saveCache',
            data: { save: 'halve', sourceId: sourceActor.id },
        },
    });
}

/**
 * Send generic chat message with minimal requirements
 *
 * @param {*} speaker ChatMessage.getSpeaker({ actor: this.actor })
 * @param {*} title
 * @param {*} message
 * @param {*} img
 * @param {*} chatCustomData
 */
export function chatMessage(
    speaker = ChatMessage.getSpeaker(),
    title = `Message`,
    message = `Forgot something...`,
    img = '',
    chatCustomData = { rollMode: game.settings.get('core', 'rollMode') }
) {
    let imageHTML = '';
    if (img) {
        imageHTML = `<div class="a25-image"><img src="${img}" height="64"/></div>`;
    }
    let chatData = {
        title: title,
        content: `<div><h2>${title}</h2></div>` + `${imageHTML ? imageHTML : ''}` + `<div>${message}</div>`,
        author: game.user.id,
        speaker: speaker,
        style: game.ars.const.CHAT_MESSAGE_STYLES.OTHER,
    };
    foundry.utils.mergeObject(chatData, { ...chatCustomData });

    console.log('utilities.js chatMessage()', { speaker, title, message, img, chatCustomData, chatData });

    //use user current setting? game.settings.get("core", "rollMode")
    if (chatCustomData.rollMode) ChatMessage.applyRollMode(chatData, chatCustomData.rollMode);
    return ChatMessage.create(chatData);
}

/**
 *
 * Send a private message to list of "targetUsers"
 *
 * @param {Object} targetUsers
 * @param {*} speaker
 * @param {String} title
 * @param {String} message
 * @param {String} img
 * @param {Object} chatCustomData
 */
export function chatMessagePrivate(
    targetUsers = undefined,
    speaker = ChatMessage.getSpeaker(),
    title = `Message`,
    message = `Forgot something...`,
    img = '',
    chatCustomData = { rollMode: game.settings.get('core', 'rollMode') }
) {
    if (!targetUsers) {
        throw new Error('utilities.js, chatMessagePrivate() missing targetUsers');
    }
    console.log('utilities.js chatMessagePrivate', { targetUsers, speaker, title, message, img, chatCustomData });

    const imageHTML = img ? `<div class="a25-image"><img src="${img}" height="64"/></div>` : '';
    const chatData = {
        title: title,
        content: `<div><h2>${title}</h2></div>` + `${imageHTML}` + `<div>${message}</div>`,
        author: game.user.id,
        speaker: speaker,
        whisper: targetUsers.map((u) => u.id),
        ...chatCustomData,
    };

    //use user current setting? game.settings.get("core", "rollMode")
    if (chatCustomData.rollMode) ChatMessage.applyRollMode(chatData, chatCustomData.rollMode);
    ChatMessage.create(chatData);
}
/**
 *
 * Simple message for SideVSide initiative rolls
 *
 * @param {*} speaker
 * @param {*} title
 * @param {*} message
 * @param {*} roll
 */
export async function chatMessageForSideVSideInitiative(speaker, title, roll) {
    const content = await renderTemplate('systems/ars/templates/chat/parts/chatCard-sidevside-roll.hbs', {
        title,
        roll,
    });

    let chatData = {
        title: title,
        content: content,
        author: game.user.id,
        rolls: [roll],
        rollMode: game.settings.get('core', 'rollMode'),
        speaker: speaker,
        style: game.ars.const.CHAT_MESSAGE_STYLES.OTHER,
    };
    //use user current setting? game.settings.get("core", "rollMode")
    ChatMessage.applyRollMode(chatData, game.settings.get('core', 'rollMode'));
    ChatMessage.create(chatData);
}

/**
 *
 * Return a itemList record from a item
 *
 * @param {*} itm Item Record
 * @returns { id: itm.id, uuid: itm.uuid, img: itm.img, name: itm.name, type: itm.type, quantity: itm.system.quantity }
 */
export function makeItemListRecord(itm) {
    if (itm) {
        return {
            id: itm.id ? itm.id : itm?.uuid?.split('.')?.pop(),
            uuid: itm.uuid,
            img: itm.img,
            name: itm.name,
            type: itm.type,
            quantity: itm.system.quantity,
        };
    }
}

/**
 *
 * Take a time type (round|turn|hour|day) and convert the
 * count to seconds for duration management.
 *
 * @param {*} type
 * @param {*} count
 * @returns Integer (seconds)
 */
export function convertTimeToSeconds(count = 1, type = 'round') {
    // console.log("utilities.js convertTimeToSeconds", { count, type })
    let timeMultiplier = 60;
    switch (type) {
        // turn is 10 rounds
        case 'turn':
            timeMultiplier = 600;
            break;
        // hour is 6 turns
        case 'hour':
            timeMultiplier = 3600;
            break;
        // day is 24 hours
        case 'day':
            timeMultiplier = 86400;
            break;

        // default is round, 60 seconds
        default:
            timeMultiplier = 60;
            break;
    }

    // console.log("utilities.js convertTimeToSeconds count * timeMultiplier", count * timeMultiplier)
    const result = count ? count * timeMultiplier : undefined;
    return result;
}

/**
 *
 * return array of all items in all compendiums
 *
 * @param {String} objectType Default 'Item'
 * @param {Boolean} gmOnly  Default false
 * @returns
 */
export async function getPackItems(objectType = 'Item', isGM = false) {
    // console.log("utilities.js getPackItems", { objectType, isGM })
    const allItemPacks = game.packs.filter((i) => i.metadata.type === objectType);
    // console.log("utilities.js getPackItems", { allItemPacks }, typeof allItemPacks, allItemPacks.length)
    const maxPacks = allItemPacks.length;
    let packItems = [];
    let count = 0;

    console.groupCollapsed('ARS | Loaded Compendium Data');

    SceneNavigation.displayProgressBar({
        label: `${game.i18n.localize('ARS.loadingItemBrowser')}`,
        // pct: ((count / allItemPacks.length) * 100).toFixed(0),
        pct: 1,
    });

    for (const pack of allItemPacks) {
        // console.log("utilities.js getPackItems", { pack }, pack.collection, pack.index.size)
        if (pack.visible) {
            // cap it at 99 so it shows until we're finished (see pct: 100 later)
            const percent = Math.min((count / maxPacks) * 100, 99).toFixed(0);

            SceneNavigation.displayProgressBar({
                label: `${game.i18n.localize('ARS.loadingItemBrowser')}: ${pack.title}`,
                pct: percent,
            });

            // const fields = ['system'];
            // const itemsIndex = await pack.getIndex({ fields });
            const items = await pack.getDocuments();
            console.log(`Loaded ${pack.title} ${objectType}:`, { items });
            packItems = packItems.concat(items);
        }
        count++;
    }

    SceneNavigation.displayProgressBar({
        label: `${game.i18n.localize('ARS.loadingItemBrowser')}`,
        pct: 100,
    });
    console.groupEnd();
    return packItems;
}

/**
 *
 * Check to see if using speed to initiative, if they've already roll, then roll initiative
 *
 * @param {Object} actor
 * @param {Object} item
 * @param {Object} action
 * @returns
 */
export async function rollInitiativeWithSpeed(actor, item, manualRoll = false, actionGroup = undefined) {
    const variant = game.ars.config.settings.systemVariant;
    const initiativeUseSpeed = game.settings.get('ars', 'initiativeUseSpeed');
    let useSpeed = true;
    if (
        initiativeUseSpeed &&
        item &&
        actor.getCombatant() &&
        (actor.initiative == undefined || (actor.initiative != undefined && manualRoll))
    ) {
        const combatant = actor.getCombatant();
        let initSpeed = 0;
        if (!actionGroup) {
            switch (item.type) {
                case 'weapon':
                    // for whatever reason v0/v1 do not use weapon speed
                    if (variant != 0 && variant != 1) {
                        initSpeed = item.speed || 0;
                    } else {
                        useSpeed = false;
                    }
                    break;
                case 'spell':
                    const spellCastInit = parseInt(item.system.castingTime);
                    if (Number(item.system.castingTime) || spellCastInit === 0) initSpeed = spellCastInit;
                    // assume its 1 round, turn/hour/etc so it doesn't really need a speed cept last.
                    else initSpeed = 99;
                    break;
                case 'power': {
                    initSpeed = 0; // #psionic
                    break;
                }
                default:
                    return false;
                    break;
            }
        } else {
            initSpeed = actionGroup.speed || 1;
        }

        // set descriptions for init roll based on what you're rolling with
        const isSpell = item.type === 'spell';
        const isPower = item.type === 'power';
        const itemName = item.system.attributes.identified ? item.name : item.alias ? item.alias : null;
        const attackingWith = itemName ? itemName : isPower ? 'Power' : isSpell ? 'Spell' : 'Weapon';
        let actionText = `${isSpell ? 'Casting' : isPower ? 'Using Power' : 'Attack with'} ${attackingWith}`;
        let actionFlavor = `Initiative for ${attackingWith}`;
        if (actionGroup) {
            actionText = `${actionGroup.name}`;
            actionFlavor = `Initiative for ${actionGroup.name}`;
        }

        await rollCombatantInitiative(combatant, combatant.combat, isControlActive(), {
            initSpeedMod: initSpeed,
            useSpeed,
            useAction: actionGroup != undefined,
            useWeapon: item.type === 'weapon',
            useSpell: item.type === 'spell',
            casting: item.type === 'spell' || item.type === 'power',
            usePower: item.type === 'power',
            question: 'Modifier',
            title: 'Initiative',
            flavor: actionFlavor,
            actionText: actionText,
            // action: action,
            actionGroup: actionGroup,
            img: item.img,
            item: item,
        });
        return true;
    }
    return false;
}

/**
 * Roll initiative. Check turn, check values, check for situation dialog() then roll
 *
 * @param {*} combatant
 * @param {*} combat
 * @param {*} ctrlKey
 * @param {*} initOptions
 * @returns
 */
export async function rollCombatantInitiative(
    combatant,
    combat,
    ctrlKey = isControlActive(),
    initOptions = {
        initSpeedMod: 0,
        useSpeed: false,
        useWeapon: false,
        useSpell: false,
        usePower: false,
        casting: false,
        question: 'Modifier',
        title: 'Initiative',
        flavor: 'Rolling Initiative',
        formula: undefined,
        actionText: game.i18n.localize('ARS.dialog.actionDefault'),
        img: 'systems/ars/icons/general/d10.webp',
        item: undefined,
    }
) {
    console.log('utilities.js rollCombatantInitiative', {
        combatant,
        combat,
        ctrlKey,
        initOptions,
    });
    // const initSideVSide = game.settings.get('ars', 'initSideVSide');
    const beginningOfRound = combat.getFlag('ars', 'beginningOfRound');

    // if (!game.user.isGM && (combatant.initiative !== null || combat.turn !== 0)) {
    if (!game.user.isGM && combatant.initiative !== null) {
        ui.notifications.warn(`You cannot roll initiative more than once.`);
        return;
    }

    const dialogOptions = {
        actionText: initOptions.actionText,
        usePower: initOptions.usePower,
    };
    const init = ctrlKey
        ? {
              mod: 0,
              rollMode: initOptions?.rollMode ? initOptions.rollMode : '',
          }
        : await dialogManager.getInitiative(
              initOptions.question,
              initOptions.title,
              initOptions.flavor,
              initOptions.casting,
              dialogOptions
          );

    // #attacks tracking - on rolling init track #attacks per round - create/update the tracking object
    CombatManager.attacksPerRoundTracker().init(initOptions, combatant);

    const currentInitiative = combat._getCurrentTurnInitiative();
    const startingTurnIndex = combat.turn;

    if (isNaN(init.mod)) return null;
    if (init.casting) {
        combatant.setFlag('world', 'initCasting', true);
    }
    const initFormula = combatant._getInitiativeFormula();
    let formula = initOptions.formula ? initOptions.formula : `${initFormula}`;
    // do this incase they adjusted.
    if (init.actionText) {
        initOptions.actionText = init.actionText;
    }

    if (initOptions.useSpeed) {
        // formula += ` + ${initOptions.initSpeedMod}`;
        formula += ` + ${initOptions.useWeapon ? '@speedfactor' : '@casttime'}`;
        if (initOptions.useWeapon) {
            //store some values and reuse during automated rolls as "default" weapon
            combatant.token.setFlag('world', 'lastInitiativeFormula', formula);
            combatant.token.setFlag('world', 'lastInitiativeMod', initOptions.initSpeedMod);
            combatant.token.setFlag('world', 'init.actionText', initOptions.actionText);
        } else if (initOptions.useAction) {
            combatant.token.setFlag('world', 'init.actionText', initOptions.actionText);
        }
    }

    if (initOptions?.item) {
        const itemInitFormula = initOptions.item.system?.attack?.speedmod;
        if (itemInitFormula) formula += ` + ${itemInitFormula}`;
    }

    if (combatant.actor?.system?.mods?.initiative) formula += ' + @mods.initiative';
    if (combatant.actor.initiativeModifier) formula += ' + @initStatusMod';
    // if (init.mod) formula += ` + ${init.mod}`;
    if (init.mod) formula += ` + @situational`;
    if (!beginningOfRound && combat.turn && currentInitiative) {
        // formula += ` +${currentInitiative + 1}`;
        formula += ` + @initOutOfTurn`;
    } else if (!beginningOfRound && combat.turn && !currentInitiative) {
        ui.notifications.warn(`${combatant?.actor?.name} rolled after round started.`);
        console.warn(`${combatant?.actor?.name} rolled after round started?`, { combat, currentInitiative }, beginningOfRound);
        // formula += ` +${combat._getLastInInitiative() + 1}`;
        formula += ` + @initLate`;
    }
    console.log('utilities.js rollCombatantInitiative', { formula, combat, combatant });
    // await combat.rollInitiative([combatant.id], {
    //     formula: formula,
    //     updateTurn: !startingTurnIndex,
    //     messageOptions: { rawformula: formula, flavor: initOptions.flavor, rollMode: init.rollMode },
    // });
    const rollData = foundry.utils.mergeObject(combatant.actor?.getRollData(), {
        speedfactor: initOptions.initSpeedMod,
        casttime: initOptions.initSpeedMod,
        situational: init.mod,
        initOutOfTurn: beginningOfRound ? 0 : currentInitiative + 1,
        initLate: combat._getLastInInitiative() + 1,
    });
    await combat.rollInitiativeIndividual(combatant, {
        formula: formula,
        updateTurn: !startingTurnIndex || beginningOfRound,
        rollData: rollData,
        messageOptions: { img: initOptions.img, rawformula: formula, flavor: initOptions.flavor, rollMode: init.rollMode },
    });
    if (initOptions.actionText) {
        combatant.token.setFlag('world', 'init.actionText', initOptions.actionText);
    }
    // dont update turn if combat is already progressed past first person
    if (startingTurnIndex && !beginningOfRound) {
        if (combatant.index < startingTurnIndex) {
            console.log('utilities.js rollCombatantInitiative', 'SET TO :', combatant.index);
            await combat.update({ turn: combatant.index });
        }
    } else {
        console.log('utilities.js rollCombatantInitiative', 'SET TO DEFAULT:', 0);
        await combat.update({ turn: 0 });
    }
    // return combat.turn ? combat : await combat.update({ turn: 0 });
    if (combatant.actor?.sheet?.rendered && combatant.actor.isOwner) combatant.actor.sheet.render();
    return combat;
}

/**
 *
 * Debug code to see what total CP value before/after resulted in to check math on calculateCoins()
 *
 * @param {*} availableCurrency
 * @returns
 */
function getCurrentCPTotal(availableCurrency) {
    const variant = game.ars.config.settings.systemVariant;
    const currencyBaseExchange = ARS.currencyValue[variant];
    let totalAvailable = 0;
    for (let currency in availableCurrency) {
        totalAvailable += availableCurrency[currency] * currencyBaseExchange[currency];
    }

    return totalAvailable;
}

/**
 *
 * Calculate copper base from current carried coins
 * Calculate copper base for costAmount
 *
 * @param {*} availableCurrency
 * @param {*} costAmount
 * @param {*} costCurrency
 * @returns { avaliable: totalAvailable, costBase: costInBaseValue, canPurchase: (totalAvailable < costInBaseValue) }
 */
export function calculateCopperBase(availableCurrency, costAmount, costCurrency) {
    const variant = game.ars.config.settings.systemVariant;
    const currencyBaseExchange = ARS.currencyValue[variant];

    // Convert the cost amount and currency type to base value
    let costInBaseValue = costAmount * currencyBaseExchange[costCurrency];

    // Calculate the total available currency in base value
    let totalAvailable = 0;
    for (let currency in availableCurrency) {
        totalAvailable += availableCurrency[currency] * currencyBaseExchange[currency];
    }

    return {
        avaliable: totalAvailable,
        costBase: costInBaseValue,
        currencyType: costCurrency,
        canBuy: totalAvailable >= costInBaseValue,
    };
}

/**
 *
 * Pay for something using costInBase (copper value)
 *
 * adjust quantities of carried currency or
 * delete carried currency item until costInBase has been paid.
 *
 * Pay a specified amount from an actor's available currencies.
 *
 * @param {Object} actor - The actor from whom the amount will be deducted.
 * @param {number} costInBase - The total cost in base currency (copper).
 * @param {string} costCurrency - The currency type for the cost.
 * @return {Object} An object containing the currencies spent and change remaining.
 * @returns { spent: spentCurrency, change: changeGiven }
 */
export async function payCopperBase(actor, costInBase, costCurrency) {
    // Get the variant and currency exchange rates from the game settings
    const variant = game.ars.config.settings.systemVariant;
    const currencyBaseExchange = ARS.currencyValue[variant];
    const carriedCurrency = actor.carriedCurrencyItems;

    // Initialize lists for updates and deletions
    const updateList = [];
    const deleteList = [];

    // Initialize counters for spent and change
    let spentCurrency = { cp: 0, sp: 0, ep: 0, gp: 0, pp: 0 };
    let change = { cp: 0, sp: 0, ep: 0, gp: 0, pp: 0 };

    // Initialize remaining cost and change variables
    let costRemaining = costInBase;
    let changeGiven = 0;

    // Loop through each type of currency from lowest to highest value
    for (const currencyOrder of Object.keys(currencyBaseExchange)) {
        // Loop through each carried currency item to find matching types
        for (const coinItem of carriedCurrency) {
            const currencyType = coinItem.system.cost.currency.toLowerCase();

            // Skip if the current item's currency type does not match
            if (currencyOrder !== currencyType) continue;

            // Calculate the item's total value in base currency
            const quantity = parseInt(coinItem.system.quantity) || 0;
            const itemBaseValue = quantity * currencyBaseExchange[currencyType];

            // Handle cases where the item's value can be fully used for payment
            if (itemBaseValue <= costRemaining && itemBaseValue >= currencyBaseExchange[costCurrency]) {
                spentCurrency[currencyType] += quantity;
                costRemaining -= itemBaseValue;
                deleteList.push(coinItem);

                // Handle cases where only part of the item's value is needed
            } else if (itemBaseValue > costRemaining) {
                const remaining = itemBaseValue - costRemaining;
                const remainingBase = remaining % currencyBaseExchange[currencyType];
                const correctChangeRemaining = Math.floor(remaining / currencyBaseExchange[currencyType]);

                spentCurrency[currencyType] += Math.ceil((itemBaseValue - remaining) / currencyBaseExchange[currencyType]);
                if (remainingBase) changeGiven += remainingBase;
                costRemaining = 0;

                // Add to update list if some quantity remains, else mark for deletion
                if (correctChangeRemaining) {
                    updateList.push({
                        item: coinItem,
                        quantity: correctChangeRemaining,
                    });
                } else {
                    deleteList.push(coinItem);
                }
            }
        }
    }

    // Apply all the queued updates and deletions

    for (const { item, quantity } of updateList) {
        if (actor.isOwner) {
            // might seem redundant but we need async and can't async a socket/runAsGM
            await item.update({ 'system.quantity': quantity });
        } else {
            await runAsGM({
                sourceFunction: 'utilities.js payCopperBase',
                operation: 'itemUpdate',
                user: game.user.id,
                targetTokenId: actor.getToken()?.id,
                targetActorId: actor.id,
                targetActorUuid: actor.uuid,
                targetItemId: item.id,
                update: { 'system.quantity': quantity },
            });
        }
    }

    for (const coinItem of deleteList) {
        if (actor.isOwner) {
            // might seem redundant but we need async and can't async a socket/runAsGM
            await actor.deleteEmbeddedDocuments('Item', [coinItem.id]);
        } else {
            const itemId = coinItem.id;
            await runAsGM({
                sourceFunction: 'actor-sheet.js handleSourceActor',
                operation: 'deleteEmbeddedDocuments',
                user: game.user.id,
                targetTokenId: actor.getToken()?.id,
                targetActorId: actor.id,
                targetActorUuid: actor.uuid,
                targetItemId: itemId,
            });
        }
    }

    console.log('actor.carriedCurrencyItems', actor.carriedCurrencyItems);
    // Calculate change if any
    if (changeGiven) change = await giveCurrency(actor, changeGiven);

    return { spent: spentCurrency, change };
}

/**
 *
 * Give currency to actor of amount copperBaseGiven as copperBase
 * Will attempt to give highest value currency first looking for
 * existing currency items of those types, if they do not exist
 * will create
 *
 * @param {*} actor
 * @param {*} copperBaseGiven
 *
 * @returns { change: change }
 *
 */
export async function giveCurrency(actor, copperBaseGiven) {
    const variant = game.ars.config.settings.systemVariant;
    const currencyBaseExchange = ARS.currencyValue[variant];
    const currencies = Object.keys(currencyBaseExchange);
    const change = {
        cp: 0,
        sp: 0,
        ep: 0,
        gp: 0,
        pp: 0,
    };
    let remainingChange = copperBaseGiven;
    const currencyItems = [];

    // Loop through each currency from highest to lowest
    for (let i = currencies.length - 1; i >= 0; i--) {
        const currencyOrder = currencies[i];
        let count = Math.floor(remainingChange / currencyBaseExchange[currencyOrder]);
        // const leftOver = remainingChange % currencyBaseExchange[currencyOrder];
        if (count > 0) {
            // Decrease the remaining change
            remainingChange -= count * currencyBaseExchange[currencyOrder];
            // Try to update the actor's existing currency items
            const coinItem = actor.carriedCurrencyItems.find((coinItem) => {
                const currencyType = coinItem.system.cost.currency.toLowerCase();
                return currencyOrder === currencyType;
            });
            if (coinItem) {
                // update existing coin item
                change[currencyOrder] += Math.ceil(count);
                const quantity = parseInt(coinItem.system.quantity) || 0;
                const newQuantity = quantity + count;
                // await coinItem.update({ 'system.quantity': newQuantity });

                await runAsGM({
                    sourceFunction: 'utilities.js giveCurrency',
                    operation: 'itemUpdate',
                    user: game.user.id,
                    targetTokenId: actor.getToken()?.id,
                    targetActorId: actor.id,
                    targetActorUuid: actor.uuid,
                    targetItemId: coinItem.id,
                    update: { 'system.quantity': newQuantity },
                });
            } else {
                // create coin item
                change[currencyOrder] += Math.ceil(count);
                const currencyName =
                    game.i18n.localize(`ARS.currency.short.${currencyOrder}`) + ` ${game.i18n.localize('ARS.coins')}` ||
                    'Change';
                const currencyWeight = 1 / ARS.currencyWeight[variant];
                const currencyData = {
                    name: currencyName,
                    type: 'currency',
                    img: ARS.icons.general.currency[currencyOrder],
                    system: {
                        quantity: count,
                        weight: currencyWeight,
                        cost: {
                            currency: currencyOrder,
                        },
                    },
                };
                currencyItems.push(currencyData);
            }
        }
    }

    if (currencyItems.length > 0) {
        // await actor.createEmbeddedDocuments('Item', currencyItems, {
        //     hideChanges: true,
        // });
        if (actor.isOwner) {
            await actor.createEmbeddedDocuments('Item', currencyItems, { hideChanges: true });
        } else {
            for (const itemData of currencyItems) {
                await runAsGM({
                    sourceFunction: 'utilities.js giveCurrency()',
                    operation: 'createEmbeddedDocuments',
                    user: game.user.id,
                    targetTokenId: actor.getToken()?.id,
                    targetActorId: actor.id,
                    targetActorUuid: actor.uuid,
                    itemData,
                });
            }
        }
    }

    return { ...change };
}

/**
 *
 * Actor purchase item (or take if buy false)
 *
 * @param {Object} actor to get item
 * @param {Object} item to give to actor
 * @param {Boolean} buy charge if buy, free if false
 * @param {Number} cost per item cost of item
 * @param {Number} quantity available
 * @param {Number} count amount taken/purchased
 * @param {Object} merchant actor who gets the amount spent (if any)
 * @returns {Number} total count of items given
 *
 */
export async function purchaseItem(actor, item, buy, cost, quantity = 10000, count = 0, merchant = undefined) {
    console.log('utilities.js purchaseItem', { actor, item, buy, cost, quantity, count, merchant });

    async function getCount(quantity, item) {
        return ['item', 'container', 'potion', 'weapon'].includes(item.type)
            ? await dialogManager.getQuantity(
                  0,
                  quantity,
                  1,
                  `${buy ? 'Buy' : 'Take'} how many ${item.name}?`,
                  `Aquire Item`,
                  `${buy ? 'Buy' : 'Take'}`,
                  'Cancel'
              )
            : 1;
    }
    let finalCount = 0;
    let amountPaid = undefined;
    if (item) {
        if (actor) {
            if (!count) count = await getCount(quantity, item);
            if (count && count > 0) {
                let purchaseDetails = '';
                let changeDetails = '';
                if (buy) {
                    const price = cost * count;
                    const currency = item.system.cost.currency;
                    const purchase = calculateCopperBase(actor.currency, price, currency);
                    console.log(`utilties.js pruchaseItem, cost:`, { price, currency, purchase });
                    if (!purchase.canBuy) {
                        // if (!purchase) {
                        ui.notifications.error(`${actor.name} cannot afford ${price} ${currency} for ${count} ${item.name}`);
                        return 0;
                    } else {
                        const buyIt = await payCopperBase(actor, purchase.costBase, currency);

                        purchaseDetails = Object.entries(buyIt.spent)
                            .filter(([, value]) => value > 0)
                            .map(([currency, value]) => `${value} ${currency.toUpperCase()}`)
                            .join(', ');

                        changeDetails = Object.entries(buyIt.change)
                            .filter(([, value]) => value > 0)
                            .map(([currency, value]) => `${value} ${currency.toUpperCase()}`)
                            .join(', ');

                        ui.notifications.notify(
                            `${actor.name} purchased ${count} ${item.name} and spent ${purchaseDetails}` +
                                (changeDetails ? ` and received ${changeDetails} in change` : '')
                        );
                        console.log(
                            'utilities.js purchaseItem',
                            `${actor.name} purchased ${count} ${item.name} and ${purchaseDetails}` +
                                (changeDetails ? ` and received ${changeDetails} in change` : '')
                        );
                        if (merchant) {
                            const changeGiven = calculateCopperBase(buyIt.spent, 0, '');
                            amountPaid = await giveCurrency(merchant, changeGiven.avaliable);
                        }
                    }
                }
                finalCount = count;
                const itemData = item.toObject();
                itemData.system.quantity = count;
                itemData.system.sourceUuid = item.uuid;
                // have to runAsGM because players can sell to merchant
                if (actor.isOwner) {
                    await actor.createEmbeddedDocuments('Item', [itemData]);
                } else {
                    await runAsGM({
                        sourceFunction: 'utilities.js purchaseItem()',
                        operation: 'createEmbeddedDocuments',
                        user: game.user.id,
                        targetTokenId: actor.getToken()?.id,
                        targetActorId: actor.id,
                        targetActorUuid: actor.uuid,
                        itemData,
                    });
                }

                console.log('utilities.js purchaseItem', { itemData });
                let messageText = `
                <div>
                    ${buy ? 'Bought' : 'Took'} ${count} ${itemData.name}
                    ${purchaseDetails ? ` and spent ${purchaseDetails}` : ''}
                    ${changeDetails ? ` and received ${changeDetails} in change` : ''}.
                </div>                
            `;
                if (merchant) {
                    const currencyToString = (currency) => {
                        return Object.entries(currency)
                            .filter(([key, value]) => value !== 0)
                            .map(([key, value]) => `${value} ${key.toUpperCase()}`)
                            .join(', ');
                    };
                    messageText += `
                <div>
                ${merchant.name} received ${currencyToString(amountPaid)}.
                </div>
                `;
                }

                const msg = chatMessage(ChatMessage.getSpeaker({ actor: actor }), `Aquired Item`, messageText, itemData.img, {
                    flags: {
                        world: {
                            item: {
                                itemData,
                            },
                            details: {
                                purchased: buy,
                                spent: purchaseDetails,
                                change: changeDetails,
                            },
                        },
                    },
                    rollMode: game.user.isGM ? 'selfroll' : '',
                });
                console.log('ITEM-BROWSER', `${actor.name} aquired ${count} ${itemData.name}.`);
            }
        } else {
            ui.notifications.error(`ERROR: Must control an actor (GM: or select token) to aquire items.`);
        }
    }
    return finalCount;
}

/**
 *
 * Escape any regular expression characters in string
 *
 * @param {*} string
 * @returns
 */
export function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

/**
 * @summary: Navigate to item by id and scroll the item into view
 * @param {*} node - dom node containing the required data in order to perform the task
 * @returns
 * @returns
 */
export async function navToItem(node) {
    const entry = $(node);
    const characterId = entry.attr('data-id');
    const itemId = entry.attr('data-item-id');
    const actor = game.actors.get(characterId);

    if (!actor) {
        ui.notifications.error('Actor not found');
        return;
    }

    // Open the actor sheet
    actor.sheet.render(true);

    // Wait for the sheet to render
    Hooks.once('renderActorSheet', (app, html, data) => {
        // Activate the inventory tab
        app.activateTab('items');

        // Scroll to the specific item
        let itemElement = html.find(`[data-item-id="${itemId}"]`);

        // item found
        if (itemElement.length) {
            // if inside a collapsed container, open it up!
            itemElement.closest('.container-collapse').css('display', '');

            // scroll into view and highlight
            itemElement[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
            itemElement.addClass('item-row-selection-highlight');

            // self-cleaning
            setTimeout(function () {
                $('.item-row-selection-highlight').removeClass('item-row-selection-highlight');
            }, 2000);
        } else {
            ui.notifications.error('Item not found in inventory');
        }
    });
}

/**
 *
 * Find a item name/type match in current inventory of actor if it exists
 *
 * finds itemToMatch.name split up by space or comma to see if its in the item.name
 * of the actors inventory of items
 *
 * @param {*} targetActor actor these items exist on
 * @param {*} itemToMatch item object to find similar
 * @param {*} itemTypes  Filter itemToMatch types out (optional)
 * @param {*} matchToType Only compare itemToMatch against items of this type (optional) i.e. look for a name of weapon itemToMatch in proficiency item names
 * @returns
 */
export function findSimilarItem(targetActor, itemToMatch, itemTypes = ['item', 'weapon', 'currency'], matchToType = null) {
    const MIN_WORD_LENGTH = 3;
    const attributeTypes = [
        // "alchemical",
        'ammunition',
        // "animal",
        // "art",
        // "clothing",
        'daily food and lodging',
        // "equipment packs",
        // "gear",
        // "gem",
        // "jewelry",
        'provisions',
        // "scroll",
        // "service",
        'herb or spice',
        // "tack and Harness",
        'tool',
        // "transport",
        'other',
    ];

    //if this isnt a item type valid to look for, return not found
    if ((itemTypes.length && !itemTypes.includes(itemToMatch.type)) || itemToMatch?.system?.attributes?.magic) {
        return undefined;
    }

    // if only matching a item, only look at specific attribute types, not everything
    if (
        (itemToMatch.system?.attributes?.type != '' || itemToMatch.system.attributes?.subtype != '') &&
        !matchToType &&
        !['type', 'subtype'].some((attr) =>
            attributeTypes.includes(itemToMatch?.system?.attributes?.[attr]?.toLowerCase() ?? '')
        )
    ) {
        return undefined;
    }

    function matchWords(nameWord, queryWord) {
        const sanitizedName = escapeRegExp(nameWord);
        const sanitizedQuery = escapeRegExp(queryWord);
        const nameRegex = new RegExp(`\\b${sanitizedName}\\b`, 'i');
        const queryRegex = new RegExp(`\\b${sanitizedQuery}\\b`, 'i');
        return nameRegex.test(sanitizedQuery) || queryRegex.test(sanitizedName);
    }

    function escapeRegExp(str) {
        return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }

    /**search for every piece of item.name in every part of the object name in the list of objects     */
    function searchObjectsByName(queryItem, objectList) {
        const ignoreWords = ['and', 'art', 'day', 'etc', 'from', 'for', 'gem', 'level', 'night', 'per', 'the', 'tun', 'was'];
        const escapedQuery = escapeRegExp(queryItem.name);
        const filteredList = objectList.filter((obj) => {
            const { id, type, name } = obj;
            if (queryItem.id === id) return false;

            const nameWords = name
                .toLowerCase()
                .split(/[\s,]+/)
                .filter((w) => w.length >= MIN_WORD_LENGTH && !ignoreWords.includes(w));
            const queryWords = escapedQuery
                .toLowerCase()
                .split(/[\s,]+/)
                .filter((w) => w.length >= MIN_WORD_LENGTH && !ignoreWords.includes(w));
            const matchType = matchToType ? type === matchToType : queryItem.type === type;
            // dont prompt to combine currency if they dont match currency (gp to gp, etc)
            if (matchType && queryItem.type == 'currency' && queryItem.system.cost.currency != obj.system.cost.currency)
                return false;

            return matchType && queryWords.some((qWord) => nameWords.some((nWord) => matchWords(nWord, qWord)));
        });

        return filteredList;
    }

    const foundItems = searchObjectsByName(itemToMatch, targetActor.items);

    // console.log("utilities.js findSimilarItem", { foundItems });
    return foundItems;
}

/**
 *
 * Fix double spacing.
 * Remove end of line when ending in -CR
 * Remove CR on lines not ending with .
 * Replace EOL with <p/>
 *
 * @param {*} text
 * @returns
 */
export function textCleanPaste(text) {
    let newText = text;
    // remove double space
    newText = newText.replace(/ {2,}/g, ' ');
    // remove double space
    newText = newText.replace(/[\s]+[\r\n]/g, '\r\n');
    // unwrap lines ending in -
    newText = newText.replace(/-(?:\s*[\n\r])/g, '');
    // remove CRs for lines w/o .
    newText = newText.replace(/(?<!\.|\.\s)[\n\r]/g, ' ');
    // replace EOL with <p/>
    newText = newText.replace(/[\n\r]+/g, '<p/>');
    // clean up highbit chars and replace with ascii
    newText = this.replaceHighBitChars(newText);

    return newText;
}

/**
 *
 * Swap out odd characters for matching ascii
 *
 * @param {*} inputString
 * @returns
 */
export function replaceHighBitChars(inputString) {
    // Define a lookup table for high-bit characters and their ASCII equivalents
    const highBitToAsciiMap = {
        Á: 'A',
        á: 'a',
        Â: 'A',
        â: 'a',
        Ã: 'A',
        ã: 'a',
        Ä: 'A',
        ä: 'a',
        Å: 'A',
        å: 'a',
        Æ: 'AE',
        æ: 'ae',
        Ç: 'C',
        ç: 'c',
        È: 'E',
        è: 'e',
        É: 'E',
        é: 'e',
        Ê: 'E',
        ê: 'e',
        Ë: 'E',
        ë: 'e',
        Ì: 'I',
        ì: 'i',
        Í: 'I',
        í: 'i',
        Î: 'I',
        î: 'i',
        Ï: 'I',
        ï: 'i',
        Ð: 'D',
        ð: 'd',
        Ñ: 'N',
        ñ: 'n',
        Ò: 'O',
        ò: 'o',
        Ó: 'O',
        ó: 'o',
        Ô: 'O',
        ô: 'o',
        Õ: 'O',
        õ: 'o',
        Ö: 'O',
        ö: 'o',
        Ø: 'O',
        ø: 'o',
        Ù: 'U',
        ù: 'u',
        Ú: 'U',
        ú: 'u',
        Û: 'U',
        û: 'u',
        Ü: 'U',
        ü: 'u',
        Ý: 'Y',
        ý: 'y',
        Þ: 'P',
        þ: 'p',
        ß: 's',
        Œ: 'OE',
        œ: 'oe',
        Š: 'S',
        š: 's',
        Ÿ: 'Y',
        ž: 'z',
        Ž: 'Z',
        ƒ: 'f',
        '—': '--', // Add em dash to its ASCII equivalent
    };

    // Replace high-bit characters with their ASCII equivalents
    const asciiString = inputString.replace(/[^\x00-\x7F]/g, (char) => highBitToAsciiMap[char] || char);

    return asciiString;
}

/**
 *
 * If line starts with Word: add <h3>Word</h3>
 * Find any dice roll and add [[//roll diceRoll]]
 *
 * @param {*} text
 * @returns
 */
export function textAddSimpleMarkup(text) {
    let newText = text;

    // remove period from vs. and vrs.
    function replaceVsAndVrs(text) {
        const regex = /(vs|vrs)[.]/gi;
        return text.replace(regex, (match) => match.slice(0, -1));
    }

    newText = replaceVsAndVrs(newText);

    // replace Word: with <h3>word</h3>
    newText = newText.replace(/<p\/>([\w\/]+):/gim, (match, word) => `<h2>${word}</h2>`);
    // replace dice roll strings with inline roll
    // newText = newText.replace(/(\d+)?d(\d+)([+-]\d+)?/gm, (match) => `[[/roll ${match}]]`);
    newText = newText.replace(
        /(?<!\[\[?\/roll [^\]]*?)(\d+)?d(\d+)([+-]\d+)?(?![^\[]*?\]?])/gm,
        (match) => `[[/roll ${match}]]`
    );
    newText = textAddBoldTags(newText, ARS.markupPhrases);

    return newText;
}

export function textAddBoldTags(text, phrases) {
    const regexPhrases = phrases.join('|');
    const regex = new RegExp(`[^.!?]*(?:${regexPhrases})[^.!?]*[.!?]`, 'gi');

    return text.replace(regex, (match) => `<b>${match}</b>`);
}

// This function converts a range into a dice roll string (e.g., '2d6+1')
// range: a string representing a range of possible values (e.g., '1-6')

/**
 * This function converts a range into a dice roll string (e.g., '2d6+1')
 *
 * @param {String} range: a string representing a range of possible values (e.g., '1-6')
 * @returns String
 */
export function convertToDiceRoll(range) {
    // Split the range into min and max, then convert them to numbers
    const [min, max] = range.split('-').map(Number);

    // Array of valid dice types (number of sides)
    const validDiceTypes = [2, 4, 6, 8, 10, 12, 20, 100];

    // Check if the range is valid (max should be greater or equal to min)
    if (max < min) {
        throw new Error('Invalid range: Max value should be greater than or equal to Min value');
    }

    // Calculate the difference between max and min values
    const difference = max - min + 1;

    // Initialize variables for optimal dice roll configuration
    let numberOfSides = difference;
    let numberOfDice = 1;
    let bestDifference = Number.MAX_SAFE_INTEGER;
    let bestModifier = 0;

    // Iterate through each dice type to find the optimal configuration
    for (let dice of validDiceTypes) {
        // Iterate through possible dice counts
        for (let count = 1; count <= difference; count++) {
            // Iterate through possible modifiers
            for (let modifier = 0; modifier < dice; modifier++) {
                // Calculate min and max values for the current configuration
                const minValue = count + modifier;
                const maxValue = count * dice + modifier;
                const currentDifference = Math.abs(maxValue - minValue + 1 - difference);

                // Check if the current configuration satisfies the input range
                // and has a smaller difference than the best configuration found so far
                if (min >= minValue && max <= maxValue && currentDifference < bestDifference) {
                    bestDifference = currentDifference;
                    numberOfSides = dice;
                    numberOfDice = count;
                    bestModifier = modifier;
                }
            }
        }
    }

    // Generate the modifier string based on the bestModifier value
    const modifierString = bestModifier > 0 ? `+${bestModifier}` : '';

    // Return the dice roll string with the optimal configuration
    return `${numberOfDice}d${numberOfSides}${modifierString}`;
}

// This function calculates the effective level of a character based on hit dice (HD)
// hitDice: the hit dice string (e.g., '1d6+2')
export function effectiveLevel(hitDice) {
    // Helper function to extract hit dice value and modifier from the hit dice string
    function helperGetHitDice(hitDice) {
        try {
            const hitDiceString = String(hitDice);
            const [, hitDiceValue, , modifierType, modifierValue] = hitDiceString?.match(/^(\d+)(([+\-])(\d+))?/);

            if (hitDiceValue) {
                const parsedHitDiceValue = parseInt(hitDiceValue);
                const parsedModifierType = modifierType || '';
                const parsedModifierValue = modifierValue || 0;
                const calculatedModifierValue = parseInt(`${parsedModifierType}${parsedModifierValue}`) || 0;

                return [parsedHitDiceValue, calculatedModifierValue];
            }
        } catch (err) {
            ui.notifications.error(`Error in utilities.js effectiveLevel() ${err}`);
            return [1, 0];
        }
        return [1, 0];
    }

    // Get the hit dice and modifier value
    const [hdValue, modValue] = helperGetHitDice(hitDice);
    const variant = ARS.settings.systemVariant;
    let effectiveHD = -1;

    // Handle different system variants (0 for OSRIC, others for different versions)
    if (Number(variant) === 0) {
        if (hdValue > 0 && modValue > 0) {
            effectiveHD = hdValue + 2;
        } else if (hdValue === 1 && modValue === -1) {
            effectiveHD = hdValue;
        } else if (hdValue === 1 && modValue < -1) {
            effectiveHD = 0;
        } else if (hdValue > 0) {
            effectiveHD = hdValue + 1;
        } else {
            effectiveHD = hdValue;
        }
    } else {
        const bonusHD = Math.floor(modValue / 4);
        effectiveHD = hdValue + bonusHD + (modValue > 0 ? 1 : modValue < 0 ? -1 : 0);
    }

    // Ensure the effective HD is within the valid range of 0 to 21
    effectiveHD = Math.min(Math.max(effectiveHD, 0), 21);

    // Return the effective HD
    return effectiveHD;
}

/**
 * Converts a given string into a format safe for use as an object key.
 *
 * @param {string} str The string to be converted.
 * @return {string} A string safe for use as an object key.
 */
export function safeKey(str) {
    // Replace spaces with underscores and remove special characters
    str = str;
    // let key = str.slugify({ strict: true });
    let key = str
        .replace(/([_\-\.])([a-z])/g, (match, separator, nextChar) => separator + nextChar.toUpperCase()) // Capitalize character following hyphen/dot/underscore
        .replace(/\s+/g, '-') // Replace spaces with underscores
        .replace(/[^\w\-]+/g, '') // Remove all non-word chars
        .replace(/\-\-+/g, '-') // Replace multiple hyphens with single underscore
        .replace(/^-+/, '') // Trim hyphens from the start
        .replace(/-+$/, '');

    // Ensure the key does not start with a digit
    if (/^\d/.test(key)) {
        key = '_' + key;
    }

    // Convert to camelCase
    key = key.replace(/_([a-z])/g, function (g) {
        return g[1].toUpperCase();
    });

    return key;
}

/**
 *
 * This takes a dice string like 2-5 and figures out a way
 * to make it a dice roll (2d3-1, 1d4+1/etc)
 *
 * Old NPC entries have this ;(
 *
 * @param {String} diceString
 * @returns
 */
export function diceFixerOLD(diceString) {
    let fixedRoll = '';
    const rollValues = diceString?.match(/^(\d+)[\-](\d+)$/);
    let nCount = rollValues[1];
    let nSize = rollValues[2];
    if (nCount === 1) {
        fixedRoll = `${nCount}d${nSize}`;
    } else {
        const nSizeAdjusted = Math.floor(nSize / nCount);
        const nRemainder = nSize % nCount; // nSize - (nSizeAdjusted * nCount);

        if (nCount > nSizeAdjusted) {
            fixedRoll = `${nSizeAdjusted}d${nCount}`;
        } else {
            fixedRoll = `${nCount}d${nSizeAdjusted}`;
        }

        if (nRemainder > 0) {
            fixedRoll += `+ ${nRemainder}`;
        } else if (nRemainder < 0) {
            fixedRoll += `${nRemainder}`;
        }
    }

    return fixedRoll;
}

export function diceFixer(rangeStr) {
    const [min, max] = rangeStr.split('-').map(Number);
    if (isNaN(min) || isNaN(max) || min >= max) {
        throw new Error('Invalid range string. Ensure it is in the format "min-max" with min < max.');
    }

    const range = max - min + 1;
    let diceRoll = '';

    if (range <= 6) {
        diceRoll = `1d${range} + ${min - 1}`;
    } else {
        const diceCombinations = [
            { sides: 4, count: Math.ceil(range / 4) },
            { sides: 6, count: Math.ceil(range / 6) },
            { sides: 8, count: Math.ceil(range / 8) },
            { sides: 10, count: Math.ceil(range / 10) },
            { sides: 12, count: Math.ceil(range / 12) },
            { sides: 20, count: Math.ceil(range / 20) },
        ];

        for (const combo of diceCombinations) {
            if (combo.sides * combo.count >= range) {
                diceRoll = `${combo.count}d${combo.sides} + ${min - 1}`;
                break;
            }
        }
    }

    return diceRoll;
}

/**
 *
 * Look at the trigger effects on source and see if they are triggered because of opponent
 *
 * @param {*} source actorObject
 * @param {*} opponent actorObject
 * @param {String} directionType 'target' or 'attacker' from target.alignment or attacker.alignment
 * @param {String} combatType null for all, melee,ranged,thrown or save types such as spell,breath/etc
 * @param {String} type attack, damage, save, magicpotency
 * @param {*} sourceItem
 * @returns
 */
export async function getTriggerFormula(
    source,
    opponent,
    directionType = 'target',
    combatType = undefined,
    type = 'attack',
    sourceItem
) {
    let rollData = [],
        bonusFormula = [];

    /** update rollData/bonusFormula */
    async function _updateFormulas(effect, triggerType, details) {
        const formulaKey = safeKey(`effect.${effect?.name ? effect.name : 'trigger'}.${directionType}.${triggerType}`);
        const formulaTag = `@${formulaKey}`;
        if (!bonusFormula.includes(formulaTag)) bonusFormula.push(formulaTag);
        if (typeof rollData[formulaKey] !== 'number') {
            rollData[formulaKey] = 0;
        }
        const formulaResult = await evaluateFormulaValue(details.formula, source.getRollData(), {
            showRoll: details.type === 'damage',
        });
        rollData[formulaKey] += parseInt(formulaResult) || 0;
        console.log('_updateFormulas', { rollData });
    }
    /**
     * generate and set formula for this trigger
     * @param {Object} effect
     * @param {String} triggerType alignment, type, distance etc...
     * @param {Object} details
     */
    async function _setFormula(effect, triggerType, details) {
        //attack and damage details.type
        if (details.type == type || (details.type == 'damage' && type.startsWith('damage.'))) {
            await _updateFormulas(effect, triggerType, details);
        } else if (combatType && details.type.startsWith('attack') && details.type.startsWith(type)) {
            const checkType = `${type}.${combatType}`;
            switch (checkType) {
                //attack.melee, attack.ranged, attack.thrown, damage.melee, damage.ranged, ramage.thrown
                case `${type}.melee`:
                case `${type}.ranged`:
                case `${type}.thrown`:
                    {
                        if (details.type == type || details.type.endsWith(combatType)) {
                            await _updateFormulas(effect, triggerType, details);
                        }
                    }
                    break;

                default:
                    // attack or damage, use for all
                    {
                        if (details.type == type) _updateFormulas(effect, triggerType, details);
                    }
                    break;
            }
            //save details.type
        } else if (details.type.startsWith('save') && details.type.startsWith(type)) {
            const saveTypes = details?.saveTypes
                ?.toLowerCase()
                ?.split(',')
                ?.map((text) => text.trim()) || ['all'];

            if (saveTypes.length && (saveTypes.includes(combatType) || saveTypes.includes('all'))) {
                await _updateFormulas(effect, triggerType, details);
            }
            //save details.type
        } else if (details.type.startsWith('magicpotency') && details.type.startsWith(type)) {
            await _updateFormulas(effect, triggerType, details);
        } else {
            // ui.notifications.warn(`utilities.js getTriggerFormula() _setFormula ${details.type}`);
            console.log('utilities.js getTriggerFormula _setFormula unknown details.type', { details });
        }
    }

    /**
     * test triggers
     * @param {*} effect
     * @param {*} triggerType
     * @param {*} details
     */
    async function _testTrigger(effect, triggerType, details) {
        switch (triggerType) {
            case 'alignment':
                const alignments = details.trigger.split(',').map((text) => text.trim().toLowerCase());
                if (alignments.includes(opponent.system.details.alignment)) {
                    await _setFormula(effect, triggerType, details);
                }
                break;

            case 'type':
                let types = opponent.types;
                if (opponent.type !== 'character') {
                    if (opponent.system.details.type) {
                        types = opponent.system.details.type.split(',').map((text) => text.trim().toLowerCase());
                    }
                }

                if (types?.length) {
                    const triggerTypes = details.trigger?.split(',').map((text) => text.trim().toLowerCase());
                    const foundTypeMatch = triggerTypes?.some((typeTrigger) => {
                        return types.includes(typeTrigger);
                    });
                    if (foundTypeMatch) await _setFormula(effect, triggerType, details);
                }
                break;

            case 'size':
                const sizes = details.trigger.split(',').map((text) => text.trim().toLowerCase());
                if (sizes.includes(opponent.system.attributes.size)) {
                    await _setFormula(effect, triggerType, details);
                }
                break;

            case 'distance':
                // console.log("opponent.js getTriggerFormula", { thisActor, actor, triggerType, details, details.trigger })
                // const distances = details.trigger.split(',').map((text) => text.trim());
                const [minDistanceStr, maxDistanceStr] = details.trigger.split(',').map((text) => text.trim());
                const minValue = parseInt(minDistanceStr || 0);
                const maxValue = parseInt(maxDistanceStr || 1);
                const distance = source.getToken().getDistance(opponent.getToken());
                if (distance <= maxValue && distance >= minValue) {
                    await _setFormula(effect, triggerType, details);
                }
                break;

            case 'always':
                await _setFormula(effect, triggerType, details);
                break;

            case 'hitdice':
                console.log('utilities.js getTriggerFormula TODO', {
                    triggerType,
                    details,
                });
                break;

            case 'weapontype':
                console.log('utilities.js getTriggerFormula TODO', {
                    triggerType,
                    details,
                });
                break;

            case 'properties_all':
                {
                    const actorPropertiesSanitized = Object.values(opponent.system?.properties).map((value) =>
                        value.toLowerCase().trim()
                    );
                    const propsSearch = details.trigger.split(',').map((text) => text.trim().toLowerCase());
                    if (propsSearch.every((prop) => actorPropertiesSanitized.includes(prop))) {
                        await _setFormula(effect, triggerType, details);
                    }
                }
                break;
            case 'properties_any':
                {
                    const actorPropertiesSanitized = Object.values(opponent.system?.properties).map((value) =>
                        value.toLowerCase().trim()
                    );
                    const propsSearch = details.trigger.split(',').map((text) => text.trim().toLowerCase());
                    if (propsSearch.some((prop) => actorPropertiesSanitized.includes(prop))) {
                        await _setFormula(effect, triggerType, details);
                    }
                }
                break;
            // case 'properties_missing': {
            //   const actorPropertiesSanitized = Object.values(opponent.system?.properties).map(value => value.toLowerCase().trim());
            //   const propsSearch = details.trigger.toLowerCase().split(',').map(text => text.trim());
            //   console.log("utilities.js getTriggerFormula TODO", { triggerType, details, actorPropertiesSanitized, propsSearch })
            //   if (actorPropertiesSanitized.every(prop => !propsSearch.includes(prop))) {
            //     _setFormula(effect, triggerType, details);
            //   }
            // }
            //   break;

            default:
                ui.notifications.warn(`utilities.js _processEffectTests() unknown triggerType (${triggerType})`);
                console.log('utilities.js _processEffectTests() unknown triggerType', { triggerType });
                break;
        } // end switch triggerType
    }

    /** start of function */
    if (source && opponent) {
        /**
         * Creates an array of effect data objects, each with a single change based on provided key-value pairs.
         * We build this so that the conditionals that are just key/value can be iterated over properly
         *
         * @param {Array} changesArray - An array of objects with key and value properties.
         * @returns {Array} An array of effect data objects.
         */
        async function helper_createEffectsFromConditionals(changesArray) {
            if (!changesArray) return [];
            return changesArray.map((change) => {
                return {
                    label: change.name ? change.name : 'Proficiency Conditional Trigger',
                    img: 'icons/svg/sword.svg', // Customize the icon as needed
                    changes: [{ key: change.key, mode: CONST.ACTIVE_EFFECT_MODES.CUSTOM, value: change.value }],
                };
            });
        }

        const sourceEffects = source.getActiveEffects(sourceItem);
        const conditionalEffects = sourceItem ? await helper_createEffectsFromConditionals(sourceItem.conditionals) : [];

        const effectList = conditionalEffects?.length ? sourceEffects.concat(conditionalEffects) : sourceEffects;
        for (const effect of effectList) {
            for (const change of effect?.changes) {
                try {
                    const details = JSON.parse(change.value);
                    //target.alignment to [target, alignement]
                    //^direction ^
                    //            \-triggerType
                    const triggers = change.key.split('.').map((text) => text.trim().toLowerCase());
                    // [target, alignement]
                    // direction = target
                    // triggerType = alignment
                    const [direction, triggerType] = triggers;
                    if (direction == directionType) {
                        // console.log('----------->', { directionType, direction, effect, change, details });
                        await _testTrigger(effect, triggerType, details);
                    }
                } catch (err) {}
            } // end changes
        } // end all effects

        if (bonusFormula.length) {
            return { formula: bonusFormula, rollData };
        }
    }
    return undefined;
}

/**
 * Refresh actor sheets that are opened
 *
 */
export function refreshAllActorSheetsOpened() {
    for (let key in ui.windows) {
        const app = ui.windows[key];
        // Check if the window is an instance of ActorSheet
        if (app instanceof ARSCharacterSheet || app instanceof ARSNPCSheet) {
            // Re-render the actor sheet
            app.render(true);
        }
    }
}

/**
 *
 * Pass text in, returns an array of all unique words in lower case
 *
 * @param {*} text
 * @returns {Array}
 */
export function getUniqueWordsAsArray(text) {
    // Convert the text to lowercase and then split into words using a regular expression.
    const words = text?.toLowerCase()?.match(/\b\w+\b/g);

    // Use a Set to remove duplicates and then convert it back to an array
    const uniqueWords = Array.from(new Set(words));

    return uniqueWords;
}

/**
 * Tests if a given formula is valid based on simplified criteria:
 * - Contains mathematical operations and/or recognized JavaScript Math function names.
 * - If non-Math words are present, "@" must also be present anywhere in the formula.
 *
 * @param {string} formula The formula to test.
 * @returns {boolean} True if the formula meets the criteria, false otherwise.
 */
export function isValidFormula(formula) {
    // List of JavaScript Math function names
    const mathFunctions = [
        'abs',
        'acos',
        'acosh',
        'asin',
        'asinh',
        'atan',
        'atan2',
        'atanh',
        'cbrt',
        'ceil',
        'cos',
        'cosh',
        'exp',
        'floor',
        'log',
        'max',
        'min',
        'pow',
        'random',
        'round',
        'sin',
        'sinh',
        'sqrt',
        'tan',
        'tanh',
        'trunc',
    ];

    // Regex to match mathematical operations
    const mathOpsRegex = /[\d\s]+[\+\-\*\/]+[\d\s]+/;
    // Check for presence of any Math function name
    const containsMathFunction = mathFunctions.some((fn) => formula.includes(fn));
    // Check for "@" symbol
    const containsAtSymbol = formula.includes('@');

    // Determine if there are any words that are not recognized Math functions
    let containsNonMathWords = false;
    formula.replace(/\b([a-zA-Z_][a-zA-Z0-9_]*)\b/g, (match, p1) => {
        if (!mathFunctions.includes(p1) && !p1.startsWith('@')) {
            containsNonMathWords = true;
        }
    });

    const mathops = mathOpsRegex.test(formula);

    let valid = mathops || containsMathFunction;
    if (!valid) valid = !containsNonMathWords || (containsNonMathWords && containsAtSymbol);

    // A formula is valid if:
    // It contains mathematical operations or recognized Math functions, and
    // If it contains non-Math words, "@" must also be present
    return valid;
}

/**
 * Test if a value is a json parsable string
 *
 * @param {*} value
 * @returns
 */
export function isJSONString(value) {
    try {
        JSON.parse(value);
        return true;
    } catch (e) {
        return false;
    }
}

/**
 *
 * Make sure name used for formula is unique
 *
 * @param {Array} effectFormulas ({ name: `${effect.name}-${change.key}`, formula: details.formula })
 * @param {String} baseName
 * @returns String
 *
 */
export function getUniqueFormulaName(effectFormulas, baseName) {
    let uniqueName = baseName;
    let counter = 1;

    // Check if the uniqueName already exists in effectFormulas
    while (effectFormulas.some((formula) => formula.name === uniqueName)) {
        // If it does, append/increment a counter to make the name unique
        uniqueName = `${baseName}-${counter}`;
        counter++;
    }

    return uniqueName;
}

/**
 * Retrieves the human-readable label of a compendium pack given an item's pack identifier.
 * @param {string} packIdentifier - The compendium pack identifier from item.pack.
 * @returns {string|null} The human-readable label of the compendium, or null if not found.
 */
export function getCompendiumLabelFromPackIdentifier(packIdentifier) {
    // Find the compendium pack in the game.packs collection
    const pack = game.packs.find((p) => p.collection === packIdentifier);

    // If the pack is found, return its label; otherwise, return null
    return pack ? pack.metadata.label : null;
}

/**
 * Sync owned spells with source spells.
 *
 * Search through an actor's owned items for spells, look for matching spells
 * in the game items and compendiums, and update the actor's spell with
 * differences from the found spell.
 *
 * @param {Actor} actor - The actor to search through.
 */
export async function updateActorSpellsFromSources(actor) {
    // Ensure the actor has items
    if (!actor.items) return;

    /** helper function to match spell */
    function validSpellMatch(item, spell) {
        return (
            item.type === 'spell' &&
            item.system?.attributes?.type.toLowerCase() !== 'scroll' &&
            item.name === spell.name &&
            item.system.type === spell.system.type &&
            item.system.level === spell.system.level
        );
    }

    /**
     * Update the spell on the actor with differences from the found spell.
     * @param {Item} spell - The actor's spell to update.
     * @param {Item} foundSpell - The found matching spell.
     */
    async function updateSpell(spell, foundSpell) {
        const diff = foundry.utils.diffObject(spell.toObject(), foundSpell.toObject());

        // Define an array of properties to exclude from the update
        const excludeProperties = ['quantity', 'learned', 'xp', 'attributes', 'weight'];

        // Remove the excluded properties from diff.system if present
        if (diff.system) {
            excludeProperties.forEach((prop) => {
                if (diff.system.hasOwnProperty(prop)) delete diff.system[prop];
            });
        }

        // Check if there's anything left to update after excluding properties
        if (!foundry.utils.isEmpty(diff) && !foundry.utils.isEmpty(diff.system)) {
            // ui.notifications.warn(`Updating ${spell.name}.`);
            console.log(`Updating ${spell.name}`, { spell, foundSpell }, diff);
            await spell.update({ system: diff.system });
            ARSActionGroup.loadAll(spell);
        }
    }

    // Search in compendiums
    const allItemPacks = game.packs.filter((i) => i.metadata.type === 'Item');
    let packItems = [];
    for (let pack of allItemPacks) {
        const packContent = await pack.getDocuments();
        packItems = packItems.concat(packContent);
    }

    // Search for matches in game.items and loaded compendiums

    let loopTotal = game.items.size;
    let loopCount = 0;
    for (let spell of actor.spells) {
        loopCount++;
        let matches = [];
        game.items.forEach((item) => {
            if (validSpellMatch(item, spell)) {
                matches.push({ item, source: 'World' });
            }
        });

        packItems.forEach((item) => {
            if (validSpellMatch(item, spell)) {
                // matches.push({ item, source: pack.metadata.label });
                matches.push({ item, source: getCompendiumLabelFromPackIdentifier(item.pack) });
            }
        });

        // If there are multiple matches, prompt the user to select one
        if (matches.length > 1) {
            let dialogContent = `<form><div class="form-group"><label>Multiple matches found for ${spell.name}, select one:</label><select id="match-select">`;
            matches.forEach((match, index) => {
                dialogContent += `<option value="${index}">${match.item.name} (${match.source})</option>`;
            });
            dialogContent += `</select></div></form>`;

            new Dialog({
                title: `Select Match for ${spell.name}`,
                content: dialogContent,
                buttons: {
                    select: {
                        icon: '<i class="fas fa-check"></i>',
                        label: 'Select',
                        callback: (html) => {
                            const selectedIndex = parseInt(html.find('#match-select').val());
                            const selectedMatch = matches[selectedIndex];
                            updateSpell(spell, selectedMatch.item);
                        },
                    },
                },
            }).render(true);
        } else if (matches.length === 1) {
            // If there is exactly one match, update the spell directly
            updateSpell(spell, matches[0].item);
        }
        SceneNavigation.displayProgressBar({
            label: `Updating spell ${spell.name}`,
            pct: Math.round((loopCount * 100) / loopTotal),
        });
    }
    SceneNavigation.displayProgressBar({ label: 'Updating spells', pct: 100 });
    ui.notifications.notify(`Update complete.`);
}

/**
 * Sync owned powers with source powers. A simple port of the sync-spells method
 * @param {Actor} actor - The actor to search through.
 */
export async function updateActorPowersFromSources(actor) {
    // Ensure the actor has items
    if (!actor.items) return;

    /** helper function to match power */
    function validPowerMatch(item, power) {
        return item.type === 'power' && item.name === power.name;
    }

    /**
     * Update the power on the actor with differences from the found power.
     * @param {Item} power - The actor's power to update.
     * @param {Item} foundPower - The found matching power.
     */
    async function updatePower(power, foundPower) {
        const diff = foundry.utils.diffObject(power.toObject(), foundPower.toObject());

        // Define an array of properties to exclude from the update
        const excludeProperties = ['quantity', 'learned', 'xp', 'attributes', 'weight'];

        // Remove the excluded properties from diff.system if present
        if (diff.system) {
            excludeProperties.forEach((prop) => {
                if (diff.system.hasOwnProperty(prop)) delete diff.system[prop];
            });
        }

        // Check if there's anything left to update after excluding properties
        if (!foundry.utils.isEmpty(diff) && !foundry.utils.isEmpty(diff.system)) {
            // ui.notifications.warn(`Updating ${power.name}.`);
            console.log(`Updating ${power.name}`, { power, foundPower }, diff);
            await power.update({ img: foundPower.img, system: diff.system });
            ARSActionGroup.loadAll(power);
        }
    }

    // Search in compendiums
    const allItemPacks = game.packs.filter((i) => i.metadata.type === 'Item');
    let packItems = [];
    for (let pack of allItemPacks) {
        const packContent = await pack.getDocuments();
        packItems = packItems.concat(packContent);
    }

    // Search for matches in game.items and loaded compendiums
    let loopTotal = game.items.size;
    let loopCount = 0;
    for (let power of actor.powers) {
        loopCount++;
        let matches = [];
        game.items.forEach((item) => {
            if (validPowerMatch(item, power)) {
                matches.push({ item, source: 'World' });
            }
        });

        packItems.forEach((item) => {
            if (validPowerMatch(item, power)) {
                // matches.push({ item, source: pack.metadata.label });
                matches.push({ item, source: getCompendiumLabelFromPackIdentifier(item.pack) });
            }
        });

        // If there are multiple matches, prompt the user to select one
        if (matches.length > 1) {
            let dialogContent = `<form><div class="form-group"><label>Multiple matches found for ${power.name}, select one:</label><select id="match-select">`;
            matches.forEach((match, index) => {
                dialogContent += `<option value="${index}">${match.item.name} (${match.source})</option>`;
            });
            dialogContent += `</select></div></form>`;

            new Dialog({
                title: `Select Match for ${power.name}`,
                content: dialogContent,
                buttons: {
                    select: {
                        icon: '<i class="fas fa-check"></i>',
                        label: 'Select',
                        callback: (html) => {
                            const selectedIndex = parseInt(html.find('#match-select').val());
                            const selectedMatch = matches[selectedIndex];
                            updatePower(power, selectedMatch.item);
                        },
                    },
                },
            }).render(true);
        } else if (matches.length === 1) {
            // If there is exactly one match, update the power directly
            updatePower(power, matches[0].item);
        }
        SceneNavigation.displayProgressBar({
            label: `Updating power ${power.name}`,
            pct: Math.round((loopCount * 100) / loopTotal),
        });
    }
    SceneNavigation.displayProgressBar({ label: 'Updating powers', pct: 100 });
    ui.notifications.notify(`Update complete.`);
}

export function playAudioDeath(rollMode = 'publicroll') {
    const audioPlayTriggers = game.settings.get('ars', 'audioPlayTriggers');
    const audioTriggersVolume = game.settings.get('ars', 'audioTriggersVolume');
    console.log('utilities.js playAudioDeath', {
        rollMode,
        audioPlayTriggers,
        audioTriggersVolume,
    });
    if (audioPlayTriggers) {
        const audioTriggerDeath = game.settings.get('ars', 'audioTriggerDeath');
        foundry.audio.AudioHelper.play(
            {
                src: audioTriggerDeath,
                volume: audioTriggersVolume,
            },
            false
        );
    }
}

export function playAudioUnconscious(rollMode = 'publicroll') {
    const audioPlayTriggers = game.settings.get('ars', 'audioPlayTriggers');
    const audioTriggersVolume = game.settings.get('ars', 'audioTriggersVolume');
    console.log('utilities.js playAudioUnconscious', {
        rollMode,
        audioPlayTriggers,
        audioTriggersVolume,
    });
    if (audioPlayTriggers) {
        const audioTriggerUnconscious = game.settings.get('ars', 'audioTriggerUnconscious');
        foundry.audio.AudioHelper.play(
            {
                src: audioTriggerUnconscious,
                volume: audioTriggersVolume,
            },
            false
        );
    }
}

/**
 * Delays the execution of a function.
 * @param {Function} callback - A function to execute after the delay.
 * @param {number} delay - The delay in milliseconds before executing the callback.
 * @returns {number} The timeout ID that can be used to cancel the timeout with clearTimeout.
 */
export function delayExecution(callback, delay) {
    const timeoutId = setTimeout(callback, delay);
    return timeoutId;
}

/**
 * Return whether dice rolls for dice so nice should show to everyone or not
 * @param {String} rollMode
 * @returns {Boolean}
 */
export function diceRollModeVisibility(rollMode, self = false) {
    console.log('CONST', { CONST });
    switch (rollMode) {
        case CONST.DICE_ROLL_MODES.PUBLIC:
            return true;
        case CONST.DICE_ROLL_MODES.PRIVATE:
        case CONST.DICE_ROLL_MODES.BLIND:
        case CONST.DICE_ROLL_MODES.SELF:
            return false;
            break;
        default:
            return true;
    }
}

/**
 *
 * Find Token by ID and return it
 *
 * @param {string} tokenId - The ID of the token to find.
 * @param {string} [sceneId] - The ID of the scene to search. If not provided, searches all scenes.
 * @returns {Token|null} The found Token object or null if not found.
 *
 */
export function getTokenById(tokenId, sceneId = null) {
    // Check if a specific sceneId is provided
    if (sceneId) {
        // Try to find the specified scene using the provided sceneId
        let scene = game.scenes.get(sceneId);
        // If the scene is found, try to find the token within that scene
        if (scene) {
            let token = scene.tokens.get(tokenId);
            // If the token is found, return the token object
            if (token) {
                return token;
            }
        }
    } else {
        // If no sceneId is provided, loop through all scenes in the game
        for (let scene of game.scenes.contents) {
            // Try to find the token within the current scene using the provided tokenId
            let token = scene.tokens.get(tokenId);
            // If the token is found, return the token object
            if (token) {
                return token;
            }
        }
    }
    // If the token is not found in the specified or all scenes, return null
    return null;
}

/**
 *
 * Create a hash id that is the same using the provided effectId and actorId
 *
 * @param {String} effectId
 * @param {String} actorId
 * @returns {String} hash of the 2 ids.
 */
export function createHashFromIds(effectId, actorId) {
    const combinedString = `${effectId}-${actorId}`;
    let hash = 0;
    for (let i = 0; i < combinedString.length; i++) {
        const char = combinedString.charCodeAt(i);
        hash = (hash << 5) - hash + char;
        hash |= 0; // Convert to 32bit integer
    }
    const uniqueId = Math.abs(hash).toString(36).padStart(16, '0').slice(0, 16);
    return uniqueId;
}

/**
 * Convert the old *.system.actions to the new *.system.actionGroups
 *
 * Incase something is missed in a migration this can be used.
 *
 * This is safe to use as it will only convert items that do not
 * have a *.system.actionGroup but do have .actions.
 *
 *
 * This can be removed after v13 probably
 */
export async function ConvertOldActionsManually() {
    /**
     * this is the code used to convert old actions to new style
     *
     * this might be used in older migrate style
     *
     * const ag1 = game.ars.ARSAction.convertFromActionBundle(_token.actor, _token.actor.system.actions);
     *
     * const t1 = game.ars.ARSActionGroup.serializeGroups(ag1);
     *
     * _token.actor.update({'system.actionGroups':t1})
     *
     */
    ui.notifications.info(`Starting action to actionV2...`);

    // convert the bundle to actionGroups
    async function processObject(obj) {
        if (!obj) return;
        if (obj?.system?.actions?.length && !obj.system.actionGroups?.length) {
            console.log(`Processing object ${obj.name}...`, { obj });
            // console.log(`Processing ${obj.name}'s actions to actionsV2`, { obj });

            obj.actionGroups = game.ars.ARSAction.convertFromActionBundle(obj, obj.system.actions);
            await game.ars.ARSActionGroup.saveAll(obj);
        } else if (obj.system.actionGroups?.length) {
            // to save the new format of castShape
            await game.ars.ARSActionGroup.saveAll(obj);
        }
    }

    // convert actor and their contained items actions
    async function processActor(actor) {
        if (!actor) return;

        processObject(actor);
        for (const item of actor.items) {
            await processObject(item);
        }
    }

    // check all scenes and convert all tokens actions
    for (const scene of game.scenes) {
        console.log(`Processing scene `, { scene });
        for (const token of scene.tokens) {
            // console.log(`Processing token ${token.name}`, { token });
            await processActor(token.actor);
        }
    }

    // check all actors in world and process actions
    for (const actor of game.actors) {
        await processActor(actor);
    }

    // check all items in world and process actions
    for (const item of game.items) {
        await processObject(item);
    }
    ui.notifications.info(`...DONE`, { permanent: true });
}

/**
 * @summary Drives inventory filtering
 * @param {*} html - Container context
 * @param {*} filter - Filter value
 */
export async function filterHelper(html, filter) {
    const items = html.find('.item-list.inventory .item-entry');
    const isGM = game.user.isGM;

    if (filter === 'none') {
        items.show();
    } else {
        items.hide();
        items.each(function () {
            const row = $(this);
            const type = (row.attr('data-type') || '').trim().toLowerCase();
            const isMagicItem = row.attr('data-magic') === 'true';
            const isSpell = row.attr('data-type') === 'spell';
            const location = (row.attr('data-location') || '').trim().toLowerCase();
            const identified = row.attr('data-identified') === 'true';

            // dont show unidentified magic or items to players who dont know what they're yet!
            if (!isGM) {
                if ((isMagic && row.attr('data-identified') === 'false') || !identified) {
                    return;
                }
            }

            if (
                (filter === 'magic' && isMagicItem) ||
                (filter === 'spells' && isSpell) ||
                (filter === 'indentified' && identified) ||
                (filter === 'unidentified' && !identified) ||
                (filter === 'equipped' && location === 'equipped') ||
                (filter === 'carried' && location === 'carried') ||
                filter === type
            ) {
                row.parents('.in-container').show();
                row.show();
            }
        });
    }
}

/**
 * @summary Drives inventory searching
 * @param {*} html - Container context
 * @param {*} searchQuery - Search string
 * @param {*} filter - Filter value
 */
export async function searchHelper(html, searchQuery, filter) {
    let items = html.find('.item-list.inventory .item-entry');

    if (filter !== 'none') {
        switch (filter) {
            case 'spells':
                if (game.user.isGM) {
                    items = items.filter('[data-type="spell"]');
                } else {
                    items = items.filter('[data-type="spell"][data-identified="true"]');
                }
                break;
            case 'magic':
                if (game.user.isGM) {
                    items = items.filter('[data-magic="true"]');
                } else {
                    items = items.filter('[data-magic="true"][data-identified="true"]');
                }
                break;
            case 'indentified':
                items = items.filter('[data-identified="true"]');
                break;
            case 'unidentified':
                if (game.user.isGM) {
                    items = items.filter('[data-identified="false"]');
                }
                break;
            case 'equipped':
                items = items.filter('[data-location="equipped"]');
                break;
            case 'carried':
                items = items.filter('[data-location="carried"]');
                break;
            default:
                items = items.filter(`[data-type="${filter}"]`);
                break;
        }
    }

    if (searchQuery === '') {
        items.show();
    } else {
        items.hide();

        items.each(function () {
            const row = $(this);
            const name = (row.attr('data-name') || '').trim().toLowerCase();

            if (name.includes(searchQuery)) {
                row.parents('.in-container').show();
                row.show();
            }
        });
    }
}

/**
 * @summary Drives powerful generic/dynamic tooltip which display verious useful mechanical information, saving the need to have to drill into something for details
 * @param {*} infoContainer - static contain - enabled via special class - see .info-helper-enabled
 * @param {*} delay - optional delay calibration
 * @returns
 */
export async function initInfoTooltips(infoContainer, { delay = 900 } = {}) {
    const useInfoHelper = game.settings.get('ars', 'useInfoHelper');

    if (!useInfoHelper) return;

    let tooltipTimeout;
    let activeTooltip = null;

    // Ensure all previously bound events in this namespace are removed
    infoContainer.off('.infoHelper');

    // Attach event handlers
    infoContainer.on('mouseover.infoHelper', '.info-helper-enabled .info-item', async function (event) {
        clearTimeout(tooltipTimeout);

        const infoItem = $(this);
        const container = infoItem.closest('.info-helper-enabled');
        const type = container.data('info-type');
        let _name = '',
            _img = '';

        const addRow = (label, value, playerHide, stack) => {
            if (!value || value === '<p></p>') return '';

            const hideClass = playerHide ? 'player-hide' : '';
            const formattedLabel = cap(label);

            if (stack) {
                return `
                <tr class="${hideClass}">
                    <td class="ars-info-helper__label">${formattedLabel}:</td>
                    <td class="ars-info-helper__value"></td>
                </tr>
                <tr class="${hideClass}">
                    <td colspan="2" class="ars-info-helper__value">${value}</td>
                </tr>
                `;
            }

            const formattedValue = label.includes('changes') ? value : cap(value);

            return `
            <tr class="${hideClass}">
                <td class="ars-info-helper__label">${formattedLabel}:</td>
                <td class="ars-info-helper__value">${formattedValue}</td>
            </tr>
            `;
        };
        const renderChanges = (changes) => {
            // Define the mode mapping
            const effectModes = {
                0: 'Custom',
                1: 'Multiply',
                2: 'Add',
                3: 'Downgrade',
                4: 'Upgrade',
                5: 'Override',
            };
            if (!changes || !changes.length) return '';

            return `
                <tr><td colspan="2" class="ars-info-helper__subheader" style="padding: 5px !important;">Effects:</td></tr>
                ${changes
                    .map(
                        (change) => `
                            <tr>                                            
                                <td colspan="2" class="ars-info-helper__value">
                                    ${change.key} (${effectModes[change.mode] || 'Unknown'}) ${change.value}
                                </td>
                            </tr>
                        `
                    )
                    .join('')}
            `;
        };
        const renderDuration = (duration) => {
            if (!duration?.type || duration.type === 'none') return '';

            const totalRounds = duration.seconds ? (duration.seconds / 60).toFixed(0) : 'N/A';
            let remainingRounds = 'N/A';

            if (duration.type === 'seconds' && duration.startTime) {
                const worldTime = game.time.worldTime;
                const timePassed = worldTime - duration.startTime;
                const leftOver = duration.seconds - timePassed;
                remainingRounds = (leftOver / 60).toFixed(0);
            }

            return `
                <tr>
                    <td class="ars-info-helper__label">Total Duration:</td>
                    <td class="ars-info-helper__value">${totalRounds} Rounds</td>
                </tr>
                <tr>
                    <td class="ars-info-helper__label">Remaining Duration:</td>
                    <td class="ars-info-helper__value">${remainingRounds} Rounds</td>
                </tr>
            `;
        };
        const renderEffects = async (effects) => {
            if (!Array.isArray(effects) || !effects.length) return '';

            let tooltipContent = '';

            // Loop through each effect and build the tooltip
            for (const effect of effects) {
                const {
                    name = 'Unknown',
                    description = 'No description available.',
                    img,
                    duration = {},
                    changes = [],
                } = effect;

                // Enrich description HTML
                const enrichedDescription = await TextEditor.enrichHTML(description, { async: true });
                const durationDisplay = renderDuration(duration);

                tooltipContent += `
                    <tr class="ars-info-helper__wrapper">       
                        <td class="ars-info-helper__header">${cap(name)}</td>      
                        <td class="ars-info-helper__label image">
                            ${img ? `<img src="${img}" alt="${cap(name)}" class="tooltip-image smaller">` : ''}
                        </td>                                                                                        
                    </tr>  
                    ${enrichedDescription !== '' ? addRow('Desc', enrichedDescription) : ''}
                    ${durationDisplay}
                    ${changes.length ? renderChanges(changes) : ''}
                `;
            }

            return tooltipContent;
        };
        const renderActions = async (infoItem, actionGroup, justRows) => {
            const sourceItemId = infoItem.data('sourceitem-uuid');
            const sourceObject = await fromUuid(sourceItemId);
            const actionGroupId = infoItem.attr('data-action-group-id');
            actionGroup = actionGroup || sourceObject?.actionGroups?.get(actionGroupId);

            if (!actionGroup || !(actionGroup.actions instanceof Map)) return;

            const actions = Array.from(actionGroup.actions.values()); // Convert Map values to array
            let actionRows = '';

            // Helper to create a resource row
            const buildResourceRow = (resource) => {
                `Type: ${cap(resource.type)},
                 Reuse Time: ${cap(resource.reusetime || 'N/A')},
                 Cost: ${resource.count?.cost || 'N/A'},
                 Value: ${resource.count?.value || 'N/A'},
                 Max: ${resource.count?.max || 'N/A'}`;
            };

            for (const action of actions) {
                const {
                    type = 'N/A',
                    name = 'Unknown',
                    img,
                    ability = 'N/A',
                    abilityCheck = { type: 'none', formula: '' },
                    damagetype = 'N/A',
                    effect = {},
                    formula = 'N/A',
                    resource = { type: 'none', count: {}, reusetime: '' },
                    saveCheck = { type: 'none', formula: '' },
                    speed = 0,
                    successAction = 'None',
                    otherdmg = [],
                    misc = '',
                    macro = { script: 'N/A' },
                    properties = [], // Properties array
                } = action;

                // Start with the action type, name, and misc
                let actionContent = `
                    <tr class="ars-info-helper__wrapper">
                        <td class="ars-info-helper__header">${cap(type)} : ${cap(name)}</td>
                        <td class="ars-info-helper__label image">
                            ${img ? `<img src="${img}" class="tooltip-image">` : ''}
                        </td>
                    </tr>
                    ${addRow('Type', type)}
                `;

                // Add misc (description) if present
                if (misc) {
                    actionContent += addRow('Desc', misc);
                }

                // Add properties row if not empty
                if (properties.length) {
                    const propertiesRow = properties.map((prop) => cap(prop)).join(', ');
                    actionContent += addRow('Properties', propertiesRow);
                }

                // Add type-specific rows
                switch (type) {
                    case 'damage':
                        actionContent += `
                            ${addRow('Ability', ability)}
                            ${addRow('Damage Type', damagetype)}
                            ${addRow('Formula', formula)}
                        `;
                        if (otherdmg.length > 0) {
                            actionContent += `
                                <tr><td class="ars-info-helper__label">Additional Damage:</td><td class="ars-info-helper__value">
                                ${otherdmg.map((dmg) => `Formula: ${cap(dmg.formula)}, Type: ${cap(dmg.type)}`).join('<br>')}
                                </td></tr>
                            `;
                        }
                        break;
                    case 'cast':
                        actionContent += `
                            ${addRow('Success Action', successAction)}
                            ${addRow('Speed', speed)}
                        `;
                        if (resource.type !== 'none') {
                            actionContent += addRow('Resource', buildResourceRow(resource));
                        }
                        if (saveCheck.type !== 'none') {
                            actionContent += `
                                ${addRow('Save Type', saveCheck.type)}
                                ${addRow('Save Formula', saveCheck.formula)}
                            `;
                        }
                        if (abilityCheck.type !== 'none') {
                            actionContent += `
                                ${addRow('Ability Check Type', abilityCheck.type)}
                                ${addRow('Ability Check Formula', abilityCheck.formula)}
                            `;
                        }
                        break;
                    case 'effect':
                    case 'item-effect': {
                        const {
                            duration = { formula: 'N/A', type: 'N/A' },
                            changes = [],
                            system = { applyself: false, visibility: 'N/A' },
                        } = effect;

                        actionContent += `
                            ${addRow('Duration', `${duration.formula} (${duration.type})`)}
                            ${addRow('Apply Self', system.applyself ? 'Yes' : 'No')}
                            ${addRow('Visibility', system.visibility)}
                        `;

                        if (changes.length > 0) {
                            actionContent += renderChanges(changes);
                        }

                        break;
                    }
                    case 'heal':
                        actionContent += `
                            ${addRow('Formula', formula)}
                        `;
                        break;
                    case 'use':
                        if (resource.type !== 'none') {
                            actionContent += addRow('Resource', buildResourceRow(resource));
                        }
                        break;
                    case 'macro':
                        actionContent += addRow('Macro', macro.script);
                        break;
                    case 'melee':
                    case 'ranged':
                    case 'thrown':
                        actionContent += `
                            ${addRow('Ability', ability)}
                            ${addRow('Speed', speed)}
                            ${addRow('Formula', formula)}
                        `;
                        break;
                    default:
                        break;
                }

                actionRows += actionContent;
            }

            return actionRows;
        };
        const cap = (str) => {
            str = str.toString();
            return str ? str.charAt(0).toUpperCase() + str.slice(1) : '';
        };

        // tooltip - small delay
        tooltipTimeout = setTimeout(async () => {
            let tooltipContent = '';
            const existingTooltip = $('.ars-info-helper');
            existingTooltip.remove();

            switch (type) {
                case 'effect': {
                    const itemId = infoItem.data('effect-uuid');
                    const effect = await fromUuid(itemId);

                    if (!effect) return;

                    const {
                        name = 'Unknown',
                        description = 'No description available.',
                        img,
                        duration = {},
                        changes = [],
                    } = effect;

                    _name = name;
                    _img = img;

                    // description
                    const enrichedDescription = await TextEditor.enrichHTML(description, { async: true });

                    // duration
                    const durationDisplay = renderDuration(duration);

                    tooltipContent = `                    
                    ${enrichedDescription !== '' ? addRow('Desc', enrichedDescription) : ''}
                    ${durationDisplay}
                    ${changes.length ? renderChanges(changes) : ''}
                    `;
                    break;
                }
                case 'skill': {
                    const itemId = infoItem.data('skill-uuid');
                    const data = await fromUuid(itemId);

                    if (!data) return;

                    const { name = 'Unknown', img = '', system = {} } = data;
                    const { description = '', features = {} } = system;
                    const { ability = 'N/A', formula = 'N/A' } = features;
                    const target = infoItem.find('.target-info').html() || 'N/A';
                    const enrichedDescription = await TextEditor.enrichHTML(description, { async: true });

                    _name = name;
                    _img = img;

                    tooltipContent = `
                    ${addRow('Type', 'Skill')}
                    ${addRow('Name', name)}
                    ${addRow('Description', enrichedDescription, false, true)}
                    ${addRow('Ability', ability)}
                    ${addRow('Target', target)}
                    ${addRow('Formula', formula)}
                    `;
                    break;
                }
                case 'action': {
                    tooltipContent = await renderActions(infoItem);
                    break;
                }
                case 'item':
                case 'spell':
                case 'power': {
                    const itemId = infoItem.data('uuid');
                    const item = await fromUuid(itemId);

                    if (!item) return;

                    const { name = 'Unknown', type = 'N/A', img = '', system = {} } = item;

                    const {
                        alias = '',
                        attack = {},
                        damage = {},
                        description = '',
                        dmonlytext = '',
                        weaponstyle = '',
                        armorstyle = '',
                        weight = '',
                        xp = '',
                        cost = {},
                        protection = {},
                    } = system;

                    // Add a default range object inside the attack structure
                    attack.range = attack.range || { short: '', medium: '', long: '' };

                    const playerHide = !system.attributes?.identified && !game.user.isGM;
                    const enrichedDescription = await TextEditor.enrichHTML(description, { async: true });
                    const enrichedGMText = await TextEditor.enrichHTML(dmonlytext, { async: true });

                    _name = playerHide ? alias : name;
                    _img = img;

                    // Function to render attack properties, passing playerHide conditionally for specific keys
                    const renderAttack = (attackData, playerHide) => {
                        const hideKeys = ['magic', 'magicpotency', 'magicbonus']; // Keys to pass playerHide as 3rd parameter to addRow

                        return Object.entries(attackData)
                            .filter(([_, value]) => value) // Skip invalid values
                            .map(([key, value]) => {
                                if (typeof value === 'object' && value !== null) {
                                    // Handle nested objects
                                    return Object.entries(value)
                                        .filter(
                                            ([_, subValue]) => subValue !== '' && subValue !== null && subValue !== undefined
                                        )
                                        .map(([subKey, subValue]) => {
                                            const shouldHide = hideKeys.includes(key.toLowerCase());
                                            return addRow(
                                                `${cap(key)} ${cap(subKey)}`,
                                                subValue,
                                                shouldHide ? playerHide : false
                                            );
                                        })
                                        .join('');
                                }

                                // For top-level properties
                                const shouldHide = hideKeys.includes(key.toLowerCase());
                                return addRow(cap(key.replace(/_/g, ' ')), value, shouldHide ? playerHide : false);
                            })
                            .join('');
                    };

                    // Function to render damage properties under a single "Damage" header
                    const renderDamageProperties = (damageData, playerHide) => {
                        const excludeKeys = ['magicbonus', 'magic']; // Keys to exclude if playerHide is true

                        const rows = Object.entries(damageData)
                            .filter(([key, value]) => value !== '' && value !== null && value !== undefined) // Filter out empty or null values
                            .flatMap(([key, value]) => {
                                const formattedKey = cap(key.replace(/_/g, ' '));

                                if (key.toLowerCase() === 'otherdmg' && Array.isArray(value) && value.length > 0) {
                                    // Special handling for 'otherdmg' array
                                    return value.map((damageObj) => {
                                        // Build a comma-separated string for each damage object
                                        const damageDescription = Object.entries(damageObj)
                                            .filter(
                                                ([_, objValue]) =>
                                                    objValue !== '' && objValue !== null && objValue !== undefined
                                            )
                                            .map(([objKey, objValue]) => `${cap(objKey)}: ${objValue}`)
                                            .join(', ');

                                        return addRow(formattedKey, damageDescription, false); // Never hide rows from 'otherdmg'
                                    });
                                }

                                // General handling for all other keys
                                const shouldHide = playerHide && excludeKeys.includes(key.toLowerCase());
                                return addRow(formattedKey, value, shouldHide);
                            })
                            .join('');

                        if (!rows) return ''; // No valid damage rows, skip entirely

                        // Return the unified "Damage" section with rows below it
                        return `
                            <tr>
                                <td class="ars-info-helper__label">Damage</td>
                                <td></td>
                            </tr>
                            ${rows}
                        `;
                    };

                    // Function to render attributes as individual rows, with "magic" and "identified" hidden when playerHide is true
                    const renderAttributes = (attributesData, playerHide) => {
                        const excludeKeys = ['magic', 'identified']; // Keys to exclude when playerHide is true

                        return Object.entries(attributesData)
                            .filter(([key, value]) => {
                                if (value === '' || value === null || value === undefined) return false; // Skip invalid values
                                if (playerHide && excludeKeys.includes(key.toLowerCase())) return false; // Hide specific keys if playerHide
                                if (key.toLowerCase() === 'infiniteammo' && value === false) return false; // Hide infiniteammo if false
                                return true;
                            })
                            .map(([key, value]) => {
                                // If the value is an array, join it into a single comma-separated string
                                if (Array.isArray(value)) {
                                    const joinedValues = value.filter((v) => v).join(', '); // Remove empty values and join
                                    return joinedValues ? addRow(cap(key.replace(/_/g, ' ')), joinedValues) : '';
                                }
                                // If the value is an object (like skillsMod), join its values into a single comma-separated string
                                if (typeof value === 'object' && value !== null) {
                                    const joinedValues = Object.values(value)
                                        .filter((v) => v !== '' && v !== null && v !== undefined) // Filter out invalid values
                                        .join(', ');
                                    return joinedValues ? addRow(cap(key.replace(/_/g, ' ')), joinedValues) : '';
                                }
                                // Standard key-value rendering
                                return addRow(cap(key.replace(/_/g, ' ')), value);
                            })
                            .filter((row) => row) // Filter out empty rows
                            .join('');
                    };

                    // Common tooltip content
                    const sharedRows = `
                        ${addRow('Type', cap(type))}
                        ${!playerHide ? addRow('Description', enrichedDescription, playerHide, true) : ''}
                        ${addRow('GM Notes', enrichedGMText, true, true)}
                        ${addRow('Weight', weight)}
                        ${addRow('XP', xp, playerHide)}
                        ${addRow('Cost', `${cost?.value || ''} ${cost?.currency || ''}`.trim(), playerHide)}
                    `;

                    // ${renderAttributes(system.attributes, playerHide)}

                    // Type-specific content
                    const itemSpecificRows = (() => {
                        if (type === 'weapon') {
                            return `
                                ${addRow('Weapon Style', cap(weaponstyle))}
                                ${renderAttack(attack, playerHide)}
                                ${renderDamageProperties(damage, playerHide)}
                            `;
                        }
                        if (type === 'armor') {
                            return `
                                ${addRow('Armor Style', cap(armorstyle))}
                                ${addRow('AC', protection?.ac)}
                                ${addRow('AC Modifier', protection?.modifier, playerHide)}
                            `;
                        }
                        return ''; // Fallback for generic items
                    })();

                    // actions
                    const actionGroups = item.actionGroups;
                    let actions = '';
                    if (actionGroups instanceof Map && !playerHide) {
                        for (const [groupId, groupData] of actionGroups.entries()) {
                            actions += await renderActions(infoItem, groupData, true);
                        }
                    }

                    // effects
                    const itemEffects = Array.from(item.effects);
                    const effectsRows = !playerHide ? await renderEffects(itemEffects) : '';

                    // Final tooltip content
                    tooltipContent = `
                    ${sharedRows}
                    ${itemSpecificRows}
                    ${actions ? actions : ''}
                    ${effectsRows ? effectsRows : ''}
                    `;
                    break;
                }
                default:
                    return;
            }

            if (activeTooltip) activeTooltip.remove();

            tooltipContent = `
            <div class="ars-info-helper">
                <table class="ars-info-helper__table">
                    <tr class="ars-info-helper__wrapper">
                        <td class="ars-info-helper__header">${cap(_name)}</td>
                        <td class="ars-info-helper__label image main-image">
                        ${_img ? `<img src="${_img}" class="tooltip-image">` : ''}
                        </td>
                    </tr>
                    ${tooltipContent}
                </table>
            </div>
            `;

            $('body').append(tooltipContent);
            activeTooltip = $('.ars-info-helper');

            const rect = infoItem[0].getBoundingClientRect();
            const tooltipRect = activeTooltip[0].getBoundingClientRect();
            if (!rect.x && !rect.y) {
                activeTooltip.remove();
                console.log('row is no longer available');
                return;
            }

            activeTooltip.css({
                position: 'absolute',
                top: `${rect.bottom + window.scrollY - tooltipRect.height / 2}px`,
                left: `${rect.right + window.scrollX}px`,
                opacity: 0,
                transition: 'opacity 0.3s ease-in-out',
            });

            activeTooltip.draggable({
                containment: 'window',
                scroll: false,
            });

            requestAnimationFrame(() => {
                activeTooltip.css('opacity', 1);
            });

            activeTooltip.on('mouseover', () => {
                clearTimeout(tooltipTimeout);
            });

            activeTooltip.on('mouseout', () => {
                tooltipTimeout = setTimeout(() => {
                    if (activeTooltip) {
                        activeTooltip.css('opacity', 0);
                        setTimeout(() => {
                            if (activeTooltip) {
                                activeTooltip.remove();
                                activeTooltip = null;
                            }
                        }, 300);
                    }
                }, delay);
            });
        }, delay);
    });

    infoContainer.on('mouseout.infoHelper', '.info-helper-enabled .info-item', function () {
        clearTimeout(tooltipTimeout);

        tooltipTimeout = setTimeout(() => {
            if (activeTooltip) {
                activeTooltip.css('opacity', 0);
                setTimeout(() => {
                    if (activeTooltip) {
                        activeTooltip.remove();
                        activeTooltip = null;
                    }
                }, 300);
            }
        }, delay);
    });
}

/**
 * @summary Sort by item type and alpha - maintaining hierarchy
 * @param {*} actor
 */
export async function sortActorItems(actor) {
    // Filter items by ARS.itemGearTypes
    const filteredItems = actor.inventoryItems;

    // Identify containers and their IDs
    const containerIds = new Set(filteredItems.filter((item) => item.type === 'container').map((container) => container.id));

    // Map items into containers or loose items
    const containerMap = new Map(); // Map of container IDs to their contents
    const looseItems = []; // Loose items not inside any container

    for (const item of filteredItems) {
        const containerId = item.system.containedIn?.id; // Get the ID from the containedIn object
        if (containerId && containerIds.has(containerId)) {
            // If item belongs to a valid container
            if (!containerMap.has(containerId)) {
                containerMap.set(containerId, []);
            }
            containerMap.get(containerId).push(item);
        } else {
            // Loose item (no container or invalid container)
            looseItems.push(item);
        }
    }

    // Helper function to sort items by type and then alphabetically
    const sortItems = (items) => {
        return items.sort((a, b) => {
            // Sort by type first
            const typeCompare = a.type.localeCompare(b.type);
            if (typeCompare !== 0) return typeCompare;

            // If same type, sort alphabetically by name
            return a.name.localeCompare(b.name);
        });
    };

    // Sort loose items
    const sortedLooseItems = sortItems(looseItems);

    // Sort each container's contents
    for (const [containerId, containerItems] of containerMap.entries()) {
        containerMap.set(containerId, sortItems(containerItems));
    }

    // Rebuild items array with sorted containers and their contents
    const rebuildHierarchy = (items, containerMap) => {
        const result = [];
        for (const item of items) {
            result.push(item); // Add the item itself
            if (containerMap.has(item.id)) {
                // If the item is a container, add its sorted contents
                const sortedContents = rebuildHierarchy(containerMap.get(item.id), containerMap);
                result.push(...sortedContents);
            }
        }
        return result;
    };

    const sortedItems = rebuildHierarchy(sortedLooseItems, containerMap);

    // Prepare update data for reordering in the actor's inventory
    const updates = [];
    let sortIndex = 0;

    for (const item of sortedItems) {
        updates.push({
            _id: item.id,
            sort: sortIndex * 10, // Assign a numeric sort value for ordering
        });
        sortIndex++;
    }

    // Apply the updated sort order to the actor's items
    await actor.updateEmbeddedDocuments('Item', updates);

    // console.log('Actor items sorted successfully, maintaining container hierarchy.');
}

/**
 * Initialize codeMirror dynamically on (n) textareas within a given scope
 * @param {$element} container - scope
 */
export async function initCodeMirror(container) {
    // delegate button launch event to the container
    container.not('.cm-initialized').on('click', '.cm-launch', function (e) {
        const modal = $(this).closest('.window-app');
        const effectContainer = $(this).closest('.effect-change');
        const textarea = effectContainer.find('.effect-editor')[0];
        const toolbar = effectContainer.find('.cm-toolbar');
        const btnFormat = toolbar.find('#format-code');
        const btnClose = toolbar.find('#close');

        // if toggling instance - change back to textarea and destroy
        if (textarea.codemirrorInstance) {
            textarea.codemirrorInstance.toTextArea();
            textarea.codemirrorInstance = null;
            toolbar.hide();
            return;
        }

        // whack w/h to show entire editor when opening the fucking thing, i have to drag resize this shit everytime i open - this auto-sizes
        modal.css('height', '');
        modal.css('width', '640px');

        // expose toolbar
        toolbar.show();

        // initialization
        const cmInstance = CodeMirror.fromTextArea(textarea, {
            mode: 'javascript',
            lineNumbers: true,
            theme: 'default',
        });

        // on change - update source textarea
        cmInstance.on('change', (cm) => {
            textarea.value = cm.getValue();
        });

        // format
        btnFormat.click((e) => {
            try {
                const code = cmInstance.getValue();
                const formattedCode = js_beautify(code);
                cmInstance.setValue(formattedCode);
            } catch (e) {
                // console.error('JSON formatting error:', e);
                return false;
            }
            return false;
        });

        // destroy codemirror instance
        btnClose.click(() => {
            textarea.value = cmInstance.getValue();
            cmInstance.toTextArea();
            textarea.codemirrorInstance = null;
            toolbar.hide();
            return false;
        });

        // set instance to node
        textarea.codemirrorInstance = cmInstance;

        // format on open
        btnFormat.click();
    });

    // tag as completed so that it never double initializes
    container.addClass('cm-initialized');
}

// import { isControlActive, isAltActive,isShiftActive } from './utilities.js';
/** detect if CONTROL key is active */
export function isControlActive() {
    return keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.CONTROL);
}
/** detect if ALT key is active */
export function isAltActive() {
    return keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.ALT);
}
/** detect if SHIFT key is active */
export function isShiftActive() {
    return keyboard.isModifierActive(KeyboardManager.MODIFIER_KEYS.SHIFT);
}

/**
 * Get the compendium name an actor belongs to.
 * @param {Actor} actor - The actor record to check.
 * @returns {string|null} - The compendium name if the actor is from a compendium, otherwise null.
 */
export function getActorCompendiumName(actor) {
    // Check if the actor has a `pack` property
    if (actor.pack) {
        return actor.pack; // This is the compendium identifier (e.g., "Compendium.osric-compendium")
    }
    // console.warn('This actor does not belong to a compendium.');
    return null;
}

/**
 *
 * Prompt for NPC Actor UUID and replicate their memorized spells
 * into a spellbook in the world item list.
 *
 * 'Spellbook: ${actor.name}'
 *
 * @returns
 */
export async function buildSpellbookFromActorMemorizations(actor = undefined) {
    if (!actor) {
        // Prompt the user to input the Actor UUID
        const actorUuid = await new Promise((resolve) => {
            new Dialog({
                title: 'Enter Actor UUID',
                content: `
                    <p>Please enter the Actor UUID:</p>
                    <div class="form-group">
                        <label for="actor-uuid">UUID:</label>
                        <input type="text" id="actor-uuid" style="width:100%;" />
                    </div>
                `,
                buttons: {
                    ok: {
                        label: 'Submit',
                        callback: (html) => resolve(html.find('#actor-uuid').val()),
                    },
                    cancel: {
                        label: 'Cancel',
                        callback: () => resolve(null),
                    },
                },
                default: 'ok',
            }).render(true);
        });

        // Exit if no UUID is provided
        if (!actorUuid) return ui.notifications.warn('No Actor UUID provided!');

        // Load the actor using the UUID
        actor = await fromUuid(actorUuid);
    }
    if (!actor) return ui.notifications.error('Actor not found!');

    // Helper function to collect spell objects
    const collectSpellObjects = (spellInfo) => {
        const items = [];
        for (const type of ['arcane', 'divine']) {
            if (!spellInfo[type]) continue;
            for (let level = 0; level <= 9; level++) {
                if (!spellInfo[type][level]) continue;
                for (const spell of Object.values(spellInfo[type][level])) {
                    if (spell.uuid) {
                        items.push({
                            uuid: spell.uuid,
                            sourceuuid: spell.uuid,
                            type: 'spell',
                            name: spell.name || 'Unknown Spell',
                            img: spell.img || 'icons/svg/mystery-man.svg',
                            id: spell.id || randomID(),
                            quantity: 1,
                            count: 0,
                            cost: 0,
                            level: level,
                            skilltarget: 0,
                        });
                    }
                }
            }
        }
        return items;
    };

    // Collect all spell objects from the actor's spell info
    let spellItems = collectSpellObjects(actor.system.spellInfo?.memorization);

    if (spellItems.length === 0) {
        return ui.notifications.info(`${actor.name} has no memorized spells.`);
    }

    // Remove duplicate spells based on their UUID
    const uniqueSpells = new Map(); // Use a Map for unique checks
    spellItems.forEach((spell) => {
        if (!uniqueSpells.has(spell.uuid)) {
            uniqueSpells.set(spell.uuid, spell);
        }
    });
    spellItems = Array.from(uniqueSpells.values()); // Convert back to an array

    // Create the Spellbook container item in the world
    const spellbookData = {
        name: `Spellbook: ${actor.name}`,
        type: 'container',
        img: 'icons/sundries/books/book-clasp-spiral-green.webp',
        system: {
            itemList: [], // Initialize with an empty item list
        },
    };
    const spellbookItem = await Item.create(spellbookData, { showItem: true });

    if (!spellbookItem) {
        return ui.notifications.error('Failed to create Spellbook item!');
    }

    // Copy the collected unique spell objects to the Spellbook's itemList
    await spellbookItem.update({
        'system.itemList': spellItems,
    });

    ui.notifications.info(`"Spellbook: ${actor.name}" created for ${actor.name} with ${spellItems.length} unique spells.`);
}

/**
 * Generate a random name and set it on the actor.
 *
 * @param {ARSActor} actor - The actor to assign the generated name.
 */
export async function generateRandomName(actor) {
    // Helper function to generate a random first name from two parts
    function generateFirstName(type) {
        const part1 = ARS.nameSegments.first[type].part1[Math.floor(Math.random() * ARS.nameSegments.first[type].part1.length)];
        const part2 = ARS.nameSegments.first[type].part2[Math.floor(Math.random() * ARS.nameSegments.first[type].part2.length)];
        return `${part1}${part2}`; // Combine first name segments
    }

    // Helper function to generate a random last name from two parts, avoiding duplicates
    function generateLastName() {
        let part1, part2;

        do {
            part1 = ARS.nameSegments.last.part1[Math.floor(Math.random() * ARS.nameSegments.last.part1.length)];
            part2 = ARS.nameSegments.last.part2[Math.floor(Math.random() * ARS.nameSegments.last.part2.length)];
        } while (part1.toLowerCase() === part2.toLowerCase()); // Ensure no "same-same" last names

        return `${part1}${part2}`; // Combine last name segments
    }

    // Helper function to generate a random full name (first + last)
    function generateFullName(type) {
        const firstName = generateFirstName(type);
        const lastName = generateLastName();
        return `${firstName} ${lastName}`;
    }

    let content = `<p>Generate a name for a male or female character?</p>`;
    if (actor) {
        content = `${content} <p>Note: Will change Actor name.</p>`;
    }
    // Create a Promise to handle dialog input
    const name = await new Promise((resolve) => {
        new Dialog({
            title: 'Name Generator',
            content,
            buttons: {
                male: {
                    label: 'Male',
                    callback: () => {
                        const generatedName = generateFullName('male');
                        ChatMessage.create({
                            content: `<strong>Generated Male Name:</strong> ${generatedName}`,
                            whisper: [game.user.id], // Send the message only to the current user
                        });
                        resolve(generatedName); // Resolve the Promise with the generated name
                    },
                },
                female: {
                    label: 'Female',
                    callback: () => {
                        const generatedName = generateFullName('female');
                        ChatMessage.create({
                            content: `<strong>Generated Female Name:</strong> ${generatedName}`,
                            whisper: [game.user.id], // Send the message only to the current user
                        });
                        resolve(generatedName); // Resolve the Promise with the generated name
                    },
                },
            },
            default: 'male',
            close: () => resolve(null),
        }).render(true);
    });

    // Update the actor's name if an actor is provided and a name was generated
    if (actor && name) {
        await actor.update({ name });

        // Find and update tokens on the active scene linked to this actor
        const tokens = canvas.scene.tokens.filter((t) => t.actorId === actor.id);
        for (const token of tokens) {
            await token.update({ name });
        }
    }
}

/**
 *
 * Notify we need to runAsGM a item drop on map.
 *
 * @param {*} scene
 * @param {*} coords
 * @param {*} actor
 * @param {*} item
 */
export async function gmDropItemOnMap(scene, coords, actor, item) {
    await runAsGM({
        sourceFunction: 'utilities.js gmDropItemOnMap',
        operation: 'gmDropItemOnMap',
        user: game.user.id,
        coords: { x: coords.x, y: coords.y },
        itemUuid: item.uuid,
        actorUuid: actor?.uuid ?? undefined,
        sceneUuid: scene.uuid,
    });
}

/**
 *
 * Executed from runAsGM
 *
 * This will drop a item on a map inside a lootable actor and equip it on the lootable
 *
 * @param {*} data
 * @returns
 */
export async function _asGM_gmDropItemOnMap(data) {
    const gameScene = await fromUuid(data.sceneUuid);
    const sourceActor = data.actorUuid ? await fromUuid(data.actorUuid) : undefined;
    const sourceItem = await fromUuid(data.itemUuid);

    const lootActorName = 'Dropped Item'; // this is the stub actor dropped in world list
    const lootActorType = 'lootable';
    // Look for an existing "Dropped Item" lootable actor
    let lootActor = game.actors.find((a) => a.name === lootActorName && a.type === lootActorType);

    // Create the actor if it doesn't exist
    if (!lootActor) {
        lootActor = await Actor.create({
            name: lootActorName,
            type: lootActorType,
            flags: {
                ars: {
                    droppedItem: true,
                },
            },
            system: {
                lootable: true,
                attributes: {
                    identified: true,
                },
                details: {
                    biography: {
                        value: 'Stub actor created by automation, used for any dropped items on scenes. Do not modify.',
                    },
                },
            },
            token: {
                actorLink: false, // Non-linked token
                disposition: CONST.TOKEN_DISPOSITIONS.NEUTRAL,
            },
        });

        if (!lootActor) {
            ui.notifications.error('Drop Item on Map: Failed to create the lootable actor.');
            return;
        }
    }

    // Get the current scene
    const scene = gameScene;
    if (!scene) {
        ui.notifications.error('Drop Item on Map: No active scene found.');
        return;
    }

    // Prepare token data for the lootable actor
    const tokenData = await lootActor.getTokenDocument();
    tokenData.updateSource({
        x: data.coords.x,
        y: data.coords.y,
        height: 0.5,
        width: 0.5,
        name: sourceItem.isIdentifiedRaw ? sourceItem.name : sourceItem.alias,
        texture: {
            src: sourceItem.img, // Use the source item's image for the
        },
    });

    // Validate the final position within the scene
    if (!canvas.dimensions.rect.contains(tokenData.x, tokenData.y)) {
        ui.notifications.error('Drop Item on Map: Token coordinates are out of bounds.');
        return;
    }

    // Create the token on the scene
    const token = await tokenData.constructor.create(tokenData, { parent: canvas.scene });
    if (!token) {
        ui.notifications.error('Drop Item on Map: Failed to create the token for the lootable actor.');
        return;
    }
    token.actor.update({ name: token.name });

    // token.actor.setFlag('ars', 'droppedItem', true);
    // Duplicate the source item and add it to the lootable actor
    const copiedItemData = foundry.utils.duplicate(sourceItem.toObject());
    const givenItems = await token.actor.createEmbeddedDocuments('Item', [copiedItemData]);
    const givenItem = givenItems[0];

    // make sure item dropped is "equipped" on the lootable actor incase of effects such as light
    givenItem.update({ 'system.location.state': 'equipped' });
    // all done, remove item from sourceItem we just dropped
    if (sourceActor) sourceActor.deleteEmbeddedDocuments('Item', [sourceItem.id]);
    console.log(`Map: Successfully dropped ${sourceItem.name} at (${data.coords.x}, ${data.coords.y}).`);
}
/**
 * Recursively searches an object, array, or Map for any value containing a target substring
 * and returns a list of paths where the partial match is found.
 *
 * @param {Object|Array|Map} obj - The object, array, or Map to search.
 * @param {string} targetValue - The substring to search for.
 * @param {string} [currentPath=""] - The current path within the object, used for recursive tracking.
 * @param {Set} [visited=new Set()] - A Set to track visited objects and prevent infinite recursion.
 * @returns {string[]} - A list of paths where the target substring is found.
 */
// export function findPathsForValue(obj, targetValue, currentPath = '', visited = new Set()) {
//     const paths = []; // Array to store matching paths

//     // Sanity check: stop if this object has already been visited
//     if (visited.has(obj)) {
//         return paths;
//     }

//     // Mark this object as visited
//     visited.add(obj);

//     // Handle Map objects
//     if (obj instanceof Map) {
//         for (const [key, value] of obj.entries()) {
//             const newPath = currentPath ? `${currentPath}.${key}` : key;

//             // Check for a partial match
//             if (typeof value === 'string' && value.includes(targetValue)) {
//                 paths.push(newPath);
//             }

//             // If the value is another object, array, or Map, recursively search it
//             if (typeof value === 'object' && value !== null) {
//                 paths.push(...findPathsForValue(value, targetValue, newPath, visited));
//             }
//         }
//     }
//     // Handle plain objects and arrays
//     else if (typeof obj === 'object' && obj !== null) {
//         for (const [key, value] of Object.entries(obj)) {
//             const newPath = currentPath ? `${currentPath}.${key}` : key;

//             // Check for a partial match
//             if (typeof value === 'string' && value.includes(targetValue)) {
//                 paths.push(newPath);
//             }

//             // If the value is another object, array, or Map, recursively search it
//             if (typeof value === 'object' && value !== null) {
//                 paths.push(...findPathsForValue(value, targetValue, newPath, visited));
//             }
//         }
//     }

//     return paths;
// }
