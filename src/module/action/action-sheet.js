import { ARSActionGroup } from './action.js';
import * as utilitiesManager from '../utilities.js';

export class ActionSheet extends FormApplication {
    constructor(object = {}, options = {}) {
        super(object, options);
        this.action = object;
    }
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ['ars', 'sheet', 'action-sheet'],
            closeOnSubmit: true,
            width: 500,
            height: 400,
            // width: 'auto',
            // height: 'auto',
            // id: 'action-sheet',
            resizable: true,
            submitOnChange: true,
            submitOnClose: true,
            closeOnSubmit: false,
            template: 'systems/ars/templates/apps/actionV2/action-sheet.hbs',
            title: 'Action',
            scrollY: ['.sheet-body'],
            tabs: [
                {
                    navSelector: '.sheet-tabs',
                    contentSelector: 'section.sheet-body',
                    initial: 'main',
                },
            ],
        });
    }

    get id() {
        return `${this.action.id}`;
    }
    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.action.source.isOwner) return;

        //look for changes here and accept.
        html.find('.action-type-select').change(async (event) => {
            this.action.type = event.target.value;
            this.render();
        });
        html.find('.action-targetShape-type').change(async (event) => {
            this.action.castShape.shape.type = event.target.value;
            this.render();
        });

        html.find('.action-coneShape-type').change(async (event) => {
            this.action.castShape.coneShape.type = event.target.value;
            this.render();
        });

        html.find('.action-resource-type').change(async (event) => {
            this.action.resource.type = event.target.value;
            this.render();
        });

        //manage the "other" damage on actions (add/remove)
        html.find('.action-control').click(this._onActionDamageOtherControl.bind(this));

        html.find('.action-image').click((ev) => {
            this.onEditImage(ev);
        });

        html.find('.effect-control').not('.cm-launch').click(this._onEffectControl.bind(this));

        html.find('.general-properties-controls').click(this._manageProperties.bind(this));

        html.find('.changes-list .effect-change .effect-change-key').change((event) =>
            this._updateEffectChangeProperties(event)
        );

        // clicked save so we close, we auto-save on close anyway
        html.find('button.save-action').click(async (event) => {
            this.close();
        });

        // setup codemirror for effects textareas
        utilitiesManager.initCodeMirror(html);
    }

    static _MIN_EFFECT_WIDTH = 550;

    _render(force, options) {
        //ensure some minimum widths
        if (this.action.type === 'effect') {
            options.width = this.constructor._MIN_EFFECT_WIDTH;
        }
        if (this._tabs[0].active == 'description') options.width = this.constructor._MIN_EFFECT_WIDTH;

        return super._render(force, options);
    }
    /** @override */
    async getData() {
        let context = {
            isDM: game.user.isDM,
            isGM: game.user.isGM,
            item: this.action,
            action: this.action,
            actor: this.action.source?.actor,
            config: CONFIG.ARS,
            const: CONST,
            enrichedDesc: await TextEditor.enrichHTML(this.action?.description, { async: true }),
            enrichedMisc: await TextEditor.enrichHTML(this.action?.misc, { async: true }),
            selectEffectKeys: game.ars.config.selectEffectKeys,
            isEditable: this.action.source.isOwner,
            owner: this.action.source.isOwner,
        };

        return context;
    }

    /* -------------------------------------------- */

    // async _onSubmit(event) {
    //     event.preventDefault();
    //     const formData = new FormData(event.target);
    //     console.log('_onSubmit=', { event,formData });
    // }
    /** @override */
    async _updateObject(event, formData) {
        // console.log('_updateObject=', { event, formData });

        for (const [key, value] of Object.entries(formData)) {
            // console.log('_updateObject=', { key, value });
            foundry.utils.setProperty(this.action, key, value);
        }
        /**
         * I am not sure why the action source effect.changes.* version
         * of this action is not the same as this.action but to save it
         * so that it is I had to create a save of the parentGroup.
         *
         * this.action.save() worked for all other values... *shrug*
         */
        await ARSActionGroup.saveGroup(this.action.parentGroup);
        // await this.action.save();
        // this.render(true);
    }

    /**
     *
     * updates sheet when a key is selected that we know about and set defaults.
     *
     * This automates some prepopulated values to show some presetup value that are probably json
     *
     * @param {*} event
     */
    async _updateEffectChangeProperties(event) {
        // console.log("action-sheet.js _updateEffectChangeProperties-------------->", { event }, this);
        const element = event.currentTarget;
        const value = element.value;
        const li = element.closest('li');
        const index = li.dataset.index;
        // console.log('action-sheet.js _updateEffectChangeProperties', { element, value, li, index });
        if (value) {
            const findObjectByName = (arr, name) => {
                return arr.find((obj) => obj.name === name);
            };
            // block submitOnChange for this
            // event.stopPropagation();
            // const details = game.ars.config.selectEffectKeys.find((a) => a.name === value);
            const details = findObjectByName(game.ars.config.selectEffectKeys, value);
            if (details) {
                // console.log('action-sheet.js _updateEffectChangeProperties', { details }, this);
                this.action.effect.changes[index] = {
                    key: details.name,
                    mode: details.mode,
                    value: details.value,
                };
                this.render();
            }
        }
    }

    /**
     * Manage other damage objects on action
     *
     * @param {*} event
     */
    async _onActionDamageOtherControl(event) {
        // console.log("action-sheet.js _onActionDamageOtherControl event", event);
        event.preventDefault();
        const element = event.currentTarget;
        const dataset = element.dataset;
        const item = this.object;
        const actionId = this.options.actionId;
        const index = Number(dataset.index);

        // console.log('action-sheet.js _onActionDamageOtherControl', { dataset, index });

        if (element.classList.contains('add-damage')) {
            let newDMG = {};
            newDMG.type = 'slashing';
            newDMG.formula = '1d4';
            newDMG.id = foundry.utils.randomID(16);
            this.action.otherdmg.push(newDMG);
        }

        if (element.classList.contains('delete-damage')) {
            this.action.otherdmg.splice(index, 1);
        }
        await this.action.save();
        this.render();
    }

    /**
     * Manage effects on action objects
     *
     *
     * @param {*} event
     */
    async onManage_ActionEffects(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const dataset = element.dataset;
        const li = element.closest('li');
        const actionToPerform = dataset.action;
        const effectId = li.dataset.effectId;

        console.log('action-sheet.js onManage_ActionEffects', { dataset, li, actionToPerform, effectId });
        switch (actionToPerform) {
            case 'create':
                let newActionEffect = foundry.utils.duplicate(game.ars.templates['actionEffect']);

                if (!newActionEffect.id) {
                    newActionEffect.id = foundry.utils.randomID(16);
                }

                this.action.effectList.push(newActionEffect);
                await this.action.save();
                break;

            case 'delete':
                this.action.effectList.splice(effectId, 1);
                await this.action.save();
                break;

            default:
                console.log('action-sheet.js', 'onManage_ActionEffects', 'Unknown actionToPerform type=', actionToPerform);
                break;
        }
    }

    /**
     * edit image/icon for action
     *
     * @param {*} event
     */
    onEditImage(event) {
        // console.log("action-sheet.js", "onEditImage", { event });
        if (event.currentTarget.dataset.edit === 'img') {
            const current = this.action.img || '';
            const fp = new FilePicker({
                type: 'image',
                // current: this.img,
                current,
                callback: (path) => {
                    event.currentTarget.src = path;
                    return this._onSubmit(event);
                },
                top: this.position.top + 40,
                left: this.position.left + 10,
            });
            // console.log("action-sheet.js", "onEditImage", { fp });
            return fp.browse();
        }
    }

    /**
     *
     * Add/Delete effect.changes on an effect for an action
     *
     * @param {MouseEvent} event      The originating click event
     * @private
     */
    async _onEffectControl(event) {
        event.preventDefault();
        console.log('action-sheet.js _onEffectControl event', event);
        const element = event.currentTarget;
        const index = element.closest('li')?.dataset?.index;

        console.log('action-sheet.js _onEffectControl', { element, index });

        switch (element.dataset.action) {
            case 'add':
                this.action.effect.changes.push({ mode: 0, key: '', value: '' });
                await this.action.save();
                break;
            case 'delete':
                this.action.effect.changes.splice(index, 1);
                await this.action.save();
                break;
        }
        this.render();
    }

    /**
     *
     * Add/remove properties
     *
     * @param {*} event
     */
    async _manageProperties(event) {
        event.preventDefault();
        // event.stopPropagation();
        const element = event.currentTarget;
        const dataset = element.dataset;
        const li = element.closest('li');
        const index = li?.dataset?.index || undefined;
        const actionCommand = dataset.action;
        const actionId = this.options.actionId;

        console.log('action-sheet.js _manageProperties', this.object);

        switch (actionCommand) {
            case 'create':
                this.action.properties.push('');
                break;

            case 'remove':
                if (index) {
                    this.action.properties.splice(index, 1);
                }
                break;
        }
        this.render();
    }
    /* ----------------------------------------- */
}

/**
 * ActionGroupV2 editor sheet
 */
const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;
// export class ActionGroupAppV2 extends HandlebarsApplicationMixin(ApplicationV2) {
//     constructor(context, options = {}) {
//         super(options);
//         this.actionGroup = context.actionGroup;
//         this.object = context.actionGroup;
//         // this.#dragDrop = this.#createDragDropHandlers();
//     }
//     //appV2 specific
//     static DEFAULT_OPTIONS = {
//         tag: 'form',
//         classes: ['app-ars', 'app-action-group-editor'],
//         window: {
//             title: 'Edit Action Group',
//             resizable: true,
//             icon: 'fas fa-edit',
//             // contentTag: 'contentTagwhatgoesthis',
//             contentClasses: ['ars', 'action-group-editor'],
//         },
//         position: {
//             width: 'auto',
//             height: 'auto',
//             // zIndex: number
//         },
//         form: {
//             handler: ActionGroupAppV2.formHandler,
//             submitOnChange: false,
//             closeOnSubmit: true,
//         },
//         // dragDrop: [{ dragSelector: '[data-drag]', dropSelector: null }],
//     }; // end DEFAULT_OPTIONS

//     static async formHandler(event, form, formData) {
//         console.log('action.js ActionGroupAppV2 formHandler ', { event, form, formData });
//         for (const [key, value] of Object.entries(formData.object)) {
//             // console.log('_updateObject=', { key, value });
//             foundry.utils.setProperty(this.actionGroup, key, value);
//         }
//         await this.actionGroup.save();
//     }

//     //appV2 specific
//     static PARTS = {
//         body: { template: 'systems/ars/templates/apps/actionV2/action-group-editor.hbs' },
//         //     header: { template: '' },
//         //     tabs: { template: '' },
//         //     description: { template: '' },
//         //     foo: { template: '' },
//         //     bar: { template: '' },
//     };

//     // appV2 "getData()"
//     async _prepareContext(options) {
//         let context = {
//             isGM: game.user.isGM,
//             isDM: game.user.isDM,
//             isOwner: this.actionGroup.source.isOwner,
//             editable: this.actionGroup.source.isOwner,
//             actionGroup: this.actionGroup,
//             // buttons: [{ type: 'submit', icon: 'fa-solid fa-save', label: 'SETTINGS.Save' }],
//         };

//         context.enrichedDescription = await TextEditor.enrichHTML(this.actionGroup.description, {
//             // Only show secret blocks to owner
//             secrets: this.actionGroup.source.isOwner,
//             // For Actors and Items
//             rollData: this.actionGroup.source.getRollData,
//             async: true,
//         });

//         context.enrichedDescription = 'Testing text here';
//         // await TextEditor.enrichHTML(this.actionGroup.description, { async: true });

//         return context;
//     }

//     // async _preparePartContext(partId, context) {
//     //     context.partId = `${this.id}-${partId}`;
//     //     return context;
//     //   }

//     // //appV2 get template... ish
//     // _configureRenderOptions(options) {
//     //     super._configureRenderOptions(options);
//     //     // set the parts to only the proper card
//     //     options.parts = [this.properCard.type];
//     // }

//     // /**
//     //  * Create drag-and-drop workflow handlers for this Application
//     //  * @returns {DragDrop[]}     An array of DragDrop handlers
//     //  * @private
//     //  */
//     // #createDragDropHandlers() {
//     //     return this.options.dragDrop.map((d) => {
//     //         d.permissions = {
//     //             dragstart: this._canDragStart.bind(this),
//     //             drop: this._canDragDrop.bind(this),
//     //         };
//     //         d.callbacks = {
//     //             dragstart: this._onDragStart.bind(this),
//     //             dragover: this._onDragOver.bind(this),
//     //             drop: this._onDrop.bind(this),
//     //         };
//     //         return new DragDrop(d);
//     //     });
//     // }

//     // #dragDrop;

//     // // Optional: Add getter to access the private property

//     // /**
//     //  * Returns an array of DragDrop instances
//     //  * @type {DragDrop[]}
//     //  */
//     // get dragDrop() {
//     //     return this.#dragDrop;
//     // }

//     // _onRender(context, options) {
//     //     this.#dragDrop.forEach((d) => d.bind(this.element));
//     // }

//     // /**
//     //  * Define whether a user is able to begin a dragstart workflow for a given drag selector
//     //  * @param {string} selector       The candidate HTML selector for dragging
//     //  * @returns {boolean}             Can the current user drag this selector?
//     //  * @protected
//     //  */
//     // _canDragStart(selector) {
//     //     // game.user fetches the current user
//     //     return this.isEditable;
//     // }

//     // /**
//     //  * Define whether a user is able to conclude a drag-and-drop workflow for a given drop selector
//     //  * @param {string} selector       The candidate HTML selector for the drop target
//     //  * @returns {boolean}             Can the current user drop on this selector?
//     //  * @protected
//     //  */
//     // _canDragDrop(selector) {
//     //     // game.user fetches the current user
//     //     return this.isEditable;
//     // }

//     // /**
//     //  * Callback actions which occur at the beginning of a drag start workflow.
//     //  * @param {DragEvent} event       The originating DragEvent
//     //  * @protected
//     //  */
//     // _onDragStart(event) {
//     //     const el = event.currentTarget;
//     //     if ('link' in event.target.dataset) return;

//     //     // Extract the data you need
//     //     let dragData = null;

//     //     if (!dragData) return;

//     //     // Set data transfer
//     //     event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
//     // }

//     // /**
//     //  * Callback actions which occur when a dragged element is over a drop target.
//     //  * @param {DragEvent} event       The originating DragEvent
//     //  * @protected
//     //  */
//     // _onDragOver(event) {}

//     // /**
//     //  * Callback actions which occur when a dragged element is dropped on a target.
//     //  * @param {DragEvent} event       The originating DragEvent
//     //  * @protected
//     //  */
//     // async _onDrop(event) {
//     //     const data = TextEditor.getDragEventData(event);

//     //     // Handle different data types
//     //     switch (
//     //         data.type
//     //         // write your cases
//     //     ) {
//     //     }
//     // }
// }

export class ActionGroupSheet extends FormApplication {
    constructor(context = {}, options = {}) {
        super(context, options);
        this.actionGroup = context.actionGroup;
    }
    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            classes: ['ars', 'actionGroup-sheet'],
            closeOnSubmit: true,
            width: '450',
            // height: 'auto',
            resizable: true,
            template: 'systems/ars/templates/apps/actionV2/action-group-editor.hbs',
            title: 'Action Group Edit',
            // icon: '<i class="fas fa-edit"></i>',
            // scrollY: ['.sheet-body'],
            // tabs: [
            //     {
            //         navSelector: '.sheet-tabs',
            //         contentSelector: 'section.sheet-body',
            //         initial: 'main',
            //     },
            // ],
        });
    }

    /** @override */
    async _updateObject(event, formData) {
        for (const [key, value] of Object.entries(formData)) {
            console.log('_updateObject=', { key, value });
            foundry.utils.setProperty(this.actionGroup, key, value);
        }
        await this.actionGroup.save();
        // await ARSActionGroup.saveGroup(this.actionGroup);
        // this.actionGroup.source.actionGroups = ARSActionGroup.loadAll(this);
    }

    // appV2 "getData()"
    async getData() {
        const superData = await super.getData();
        let context = {
            ...superData,
            isGM: game.user.isGM,
            isDM: game.user.isDM,
            isOwner: this.actionGroup.source.isOwner,
            actionGroup: this.actionGroup,
            speed: this.actionGroup.speed,
        };

        // context.enrichedDescription = await TextEditor.enrichHTML(this.actionGroup.description, {
        //     // Only show secret blocks to owner
        //     secrets: this.actionGroup.source.isOwner,
        //     // For Actors and Items
        //     rollData: this.actionGroup.source.getRollData,
        //     async: true,
        // });

        // context.enrichedDescription = 'Testing text here';
        context.enrichedDescription = await TextEditor.enrichHTML(this.actionGroup.description, { async: true });

        return context;
    }

    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.actionGroup.source.isOwner) return;

        html.find('.action-image').click((ev) => {
            this._onEditImage(ev);
        });
    }

    _onEditImage(event) {
        const attr = event.currentTarget.dataset.edit;
        const current = foundry.utils.getProperty(this.actionGroup, attr);
        const img = this.img;
        const fp = new FilePicker({
            current,
            type: 'image',
            redirectToRoot: img ? [img] : [],
            callback: (path) => {
                event.currentTarget.src = path;
                // if (this.options.submitOnChange) return this._onSubmit(event);
            },
            top: this.position.top + 40,
            left: this.position.left + 10,
        });
        return fp.browse();
    }
}
