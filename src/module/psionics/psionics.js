import * as utilitiesManager from '../utilities.js';
import { ARS } from '../config.js';
import { ARSRoll } from '../dice/rolls.js';
import * as dialogManager from '../dialog.js';
import * as debug from '../debug.js';

// #psionic - Psionics utlity class
export class ARSPsionics {
    constructor(actor = undefined, power = undefined) {
        this.actor = actor;
        this.power = power;
        this.targets = game.user.targets.size;
    }

    // get inherent potential - the initial base PSP for a character
    static getInherentPotential(actor) {
        const { wis, con, int } = actor.system.abilities;

        // Base scores and ability modifiers
        const baseScores = { 15: 20, 16: 22, 17: 24, 18: 26 };
        const abilityModifiers = { 15: 0, 16: 1, 17: 2, 18: 3 };

        // Normalize ability scores between 1 and 18
        const wisdom = Math.min(18, Math.max(1, wis.value));
        const constitution = Math.min(18, Math.max(1, con.value));
        const intelligence = Math.min(18, Math.max(1, int.value));

        // Calculate the base score and inherent potential
        const baseScore = baseScores[wisdom] || 0;
        const conMod = abilityModifiers[constitution] || 0;
        const intMod = abilityModifiers[intelligence] || 0;
        const inherentPotential = baseScore + conMod + intMod;

        // Create chat message content
        const chatContent = `
        <h2>${actor.name}'s Inherent Potential</h2>
        <p><strong>Wisdom:</strong> ${wisdom}</p>
        <p><strong>Base Score (from Wisdom):</strong> ${baseScore}</p>
        <p><strong>Constitution:</strong> ${constitution} (Modifier: ${conMod})</p>
        <p><strong>Intelligence:</strong> ${intelligence} (Modifier: ${intMod})</p>        
        <p><strong>Inherent Potential:</strong> ${inherentPotential} PSP</p>
        `;

        // Send the chat message
        ChatMessage.create({
            content: chatContent,
            speaker: ChatMessage.getSpeaker({ actor }),
        });

        // Return the inherent potential
        return inherentPotential;
    }

    // compute psp gained on level up
    static gainPSPsOnLevelUp(actor) {
        const { wis } = actor.system.abilities;
        const abilityModifiers = { 15: 0, 16: 1, 17: 2, 18: 3 };

        // Normalize wisdom score between 1 and 18
        const wisdom = Math.min(18, Math.max(1, wis.value));

        // Get the modifier based on Wisdom
        const modifier = abilityModifiers[wisdom] || 0;

        // Calculate PSP gain
        const pspsGained = 10 + modifier;

        // Create a chat card to display the results
        const chatContent = `
            <h2>${actor.name}'s PSP Gain on Level Up</h2>
            <p><strong>Wisdom:</strong> ${wisdom}</p>
            <p><strong>Wisdom Modifier:</strong> ${modifier}</p>
            <p><strong>PSPs Gained:</strong> ${pspsGained}</p>
        `;

        // Send the chat message
        ChatMessage.create({
            content: chatContent,
            speaker: ChatMessage.getSpeaker({ actor }),
        });

        // Return PSPs gained
        return pspsGained;
    }

    // 1e function to determine if an actor is psionic and, if so, generate Psionic Strength
    static async checkIfPsionic(actor) {
        // Get the relevant mental abilities: Intelligence, Wisdom, Charisma
        const intScore = actor.system.abilities.int.value;
        const wisScore = actor.system.abilities.wis.value;
        const chaScore = actor.system.abilities.cha.value;

        // Psionics require at least one mental ability at 16 or above
        if (intScore < 16 && wisScore < 16 && chaScore < 16) {
            return ChatMessage.create({
                speaker: ChatMessage.getSpeaker({ actor }),
                content: `${actor.name} has no chance to be psionic because none of their mental abilities are 16 or above.`,
            });
        }

        // Calculate the chance to be psionic
        let psionicChance = 1; // Base chance
        if (intScore > 16) {
            psionicChance += Math.floor((intScore - 16) * 2.5); // +2.5% per point of Int over 16
        }
        if (wisScore > 16) {
            psionicChance += Math.floor((wisScore - 16) * 1.5); // +1.5% per point of Wis over 16
        }
        if (chaScore > 16) {
            psionicChance += Math.floor((chaScore - 16) * 1.5); // +1.5% per point of Cha over 16
        }

        // Roll percentage to determine if the actor is psionic (with 3D dice animation)
        let roll = await new Roll('1d100').roll();
        game.dice3d?.showForRoll(roll); // Show 3D dice roll if Dice So Nice is enabled
        const isPsionic = roll.total <= psionicChance;

        // Prepare message content
        let resultMessage;
        if (isPsionic) {
            // If the actor is psionic, generate Psionic Strength
            const { psionicStrength, psionicAbility } = await generatePsionicStrength(actor);
            resultMessage =
                `${actor.name} is psionic! Rolled ${roll.total} against a ${psionicChance}% chance.\n` +
                `**Psionic Strength:** ${psionicStrength}\n**Psionic Ability:** ${psionicAbility}`;
        } else {
            resultMessage = `${actor.name} is not psionic. Rolled ${roll.total} against a ${psionicChance}% chance.`;
        }

        // Display the result in a chat message
        await roll.toMessage({
            flavor: `${actor.name} is checking if they are psionic!`,
            content: resultMessage,
            speaker: ChatMessage.getSpeaker({ actor }),
        });

        return isPsionic;
    }

    // 1e function to generate Psionic Strength for a psionic character
    static async generatePsionicStrength(actor) {
        // Get the relevant mental abilities: Intelligence, Wisdom, Charisma
        const intScore = actor.system.abilities.int.value;
        const wisScore = actor.system.abilities.wis.value;
        const chaScore = actor.system.abilities.cha.value;

        // Calculate the base psionic strength by subtracting 12 from each score
        let intBase = Math.max(0, intScore - 12);
        let wisBase = Math.max(0, wisScore - 12);
        let chaBase = Math.max(0, chaScore - 12);

        // Total of the mental attributes adjustments
        let psionicBase = intBase + wisBase + chaBase;

        // Check if two or three scores are above 16 to apply the multiplier
        const highMentalScores = [intScore, wisScore, chaScore].filter((score) => score > 16).length;
        if (highMentalScores >= 2) {
            psionicBase *= highMentalScores === 3 ? 4 : 2;
        }

        // Roll 1d100 to add to the base psionic strength
        const roll = await new Roll('1d100').roll();
        if (game.dice3d) {
            // Await the display of the 3D dice roll
            await game.dice3d.showForRoll(roll);
        }

        const psionicStrength = psionicBase + roll.total;

        // Calculate Psionic Ability (Psionic Strength * 2)
        const psionicAbility = psionicStrength * 2;

        // Create a chat card to display the results
        const chatContent = `
        <h2>${actor.name}'s Psionic Strength</h2>
        <p><strong>Intelligence:</strong> ${intScore}</p>
        <p><strong>Wisdom:</strong> ${wisScore}</p>
        <p><strong>Charisma:</strong> ${chaScore}</p>
        <p><strong>Base Psionic Strength:</strong> ${psionicBase}</p>
        <p><strong>1d100 Roll:</strong> ${roll.total}</p>
        <p><strong>Total Psionic Strength:</strong> ${psionicStrength}</p>
        <p><strong>Psionic Ability (Strength x2):</strong> ${psionicAbility}</p>
        `;

        // Send the chat message
        ChatMessage.create({
            content: chatContent,
            speaker: ChatMessage.getSpeaker({ actor }),
        });

        // Return the psionic strength and psionic ability
        return { psionicStrength, psionicAbility };
    }

    // get all effects on the actor relevant to maintaining a power or recovery
    static async getMaintainedPowerEffects(actor) {
        const ongoingEffects = actor.getActiveEffects().filter((effect) => {
            return effect.changes.some((change) => {
                return change.key.toLowerCase() === 'special.ongoing';
            });
        });
        const maintainedPowers = [];
        for (const effect of ongoingEffects) {
            const change = effect.changes.find((c) => c.key.toLowerCase() === 'special.ongoing');
            if (change && change.mode === CONST.ACTIVE_EFFECT_MODES.CUSTOM) {
                const details = JSON.parse(change.value);
                const type = details.type.toLowerCase();
                const formula = details.formula;
                const cycle = utilitiesManager.capitalize(details.cycle);
                const rate = parseInt(details.rate) || 1;
                const maintainingPower = type === 'pspmaintain' || type === 'psprecover';
                if (maintainingPower) {
                    effect.formula = formula;
                    effect.cycle = cycle;
                    effect.rate = rate;
                    maintainedPowers.push(effect);
                }
            }
        }

        // Filter active effects on the actor where the psionics flag is present and defensePower is true
        const defensePowers = actor.getActiveEffects().filter((effect) => {
            const psionicsFlag = effect.flags?.psionics;
            const defenseEffect = psionicsFlag && psionicsFlag.defensePower === true;
            return defenseEffect;
        });

        const contactPowers = actor.getActiveEffects().filter((effect) => {
            const psionicsFlag = effect.flags?.psionics;
            const contactPower = psionicsFlag && psionicsFlag.contactPower === true;
            return contactPower;
        });

        return {
            maintainedPowers,
            defensePowers,
            contactPowers,
        };
    }

    // effect driven psp, maintain a power, recovery and more to come
    static async ongoingPspEffect(data) {
        const { actor, _formula, effect, type, cycle } = data;
        const powerName = effect.name;
        const isMaintain = type === 'maintain';
        const time = `per ${cycle}`;
        const title = `<h3>${isMaintain ? `Maintaining <b>${powerName}</b>` : 'Recovering PSP'}</h3>`;

        const roll = new ARSRoll(_formula, actor.getRollData());
        await roll.roll();
        const powerCost = roll.total;

        if (isNaN(powerCost)) throw new Error('Invalid PSP Cost - Not a Valid Number');

        const usePsionicsV1 = game.settings.get('ars', 'usePsionicsV1');
        let insuffecientPSPMessage = '';

        if (usePsionicsV1) {
            const dividedCost = powerCost / 2;
            const currentPSPAttack = actor.system.psionics.pspattack.value;
            const currentPSPDefense = actor.system.psionics.pspdefense.value;

            const newPSPAttack = isMaintain
                ? Math.max(0, currentPSPAttack - dividedCost)
                : Math.min(actor.system.psionics.pspattack.base, currentPSPAttack + dividedCost);
            const newPSPDefense = isMaintain
                ? Math.max(0, currentPSPDefense - dividedCost)
                : Math.min(actor.system.psionics.pspdefense.base, currentPSPDefense + dividedCost);

            await actor.update({
                'system.psionics.pspattack.value': newPSPAttack,
                'system.psionics.pspdefense.value': newPSPDefense,
            });

            if (isMaintain && (currentPSPAttack < dividedCost || currentPSPDefense < dividedCost)) {
                insuffecientPSPMessage = `<div class="use-pwr-insuffecient"><span>You have insufficient PSP</span></div>`;
            }
        } else {
            const currentPSP = actor.system.psionics.psp.value;
            const newPSP = isMaintain
                ? Math.max(0, currentPSP - powerCost)
                : Math.min(currentPSP + powerCost, actor.system.psionics.psp.base);

            await actor.update({ 'system.psionics.psp.value': newPSP });

            if (isMaintain && currentPSP < powerCost) {
                insuffecientPSPMessage = `<div class="use-pwr-insuffecient"><span>You have insufficient PSP</span></div>`;
            }
        }

        const speaker = ChatMessage.getSpeaker({ actor });
        const details = `<div class="pwr-details">${
            isMaintain ? `Maintenance Cost ${powerCost} ${time}` : `Recovering ${powerCost} ${time}`
        }</div>`;
        const msg = `${details}${insuffecientPSPMessage}`;

        utilitiesManager.chatMessage(speaker, title, msg, '');
    }

    // use power message
    static async broadcastUsePower(actor, data) {
        const usePsionicsV1 = game.settings.get('ars', 'usePsionicsV1');
        const speaker = ChatMessage.getSpeaker({ actor });
        const { name, img, powerCost, powerCostAttack, powerCostDefense, maintenance } = data;
        const currentPSP = actor.system.psionics.psp.value;
        const currentPSPAttack = actor.system.psionics.pspattack.value;
        const currentPSPDefense = actor.system.psionics.pspdefense.value;
        const bypassCost = data.bypassCost;

        const title = `<h3 class="pwr-chat-output-header">Use <b>${name}</b></h3>`;
        const maintain = `<div class="pwr-details"><span>Maintenance Cost: ${maintenance}</span></div>`;

        let msg = '';
        let insufficientMessage = '';
        if (usePsionicsV1) {
            msg = `<div class="pwr-details hide-2e"><span>PSP Atk Cost: ${powerCostAttack}</span></div>
                   <div class="pwr-details hide-2e"><span>PSP Def Cost: ${powerCostDefense}</span></div>`;

            insufficientMessage = [
                currentPSPAttack < powerCostAttack
                    ? `<div class="use-pwr-insuffecient hide-2e"><span>You have insufficient Attack PSP</span></div>`
                    : '',
                currentPSPDefense < powerCostDefense
                    ? `<div class="use-pwr-insuffecient hide-2e"><span>You have insufficient Defense PSP</span></div>`
                    : '',
            ].join('');
        } else {
            if (bypassCost) {
                msg = `<div class="pwr-details"><span>PSP Cost: 0 - Bypassed Cost (Shift Held)</span></div>`;
            }
            else {
                msg = `<div class="pwr-details"><span>PSP Cost: ${powerCost}</span></div>`;
                insufficientMessage =
                    currentPSP < powerCost
                        ? `<div class="use-pwr-insuffecient hide-1e"><span>You have insufficient PSP</span></div>`
                        : '';
            }
        }

        msg = `${msg}${insufficientMessage}${maintain}<div class="non-owner-only-view">${game.i18n.localize(
            'ARS.unknownAction'
        )}</div>`.trim();
        utilitiesManager.chatMessage(speaker, title, msg, img);
    }

    // actor, targets and power is attack mode
    get isPsychicAttack() {
        return this.actor && this.targets > 0 && this.power?.system.isAttackMode;
    }

    // add a defense mode effect for psionic defense powers
    static async addDefensePowerEffect(actor, power) {
        const usePsionicsV1 = game.settings.get('ars', 'usePsionicsV1');
        const psp = actor.system.psionics.psp.value;
        const pspdefense = actor.system.psionics.pspdefense.value;
        const powerName = power.name;
        const defName = power.system.atkDefType;
        const cost = +power.system.powercost;
        const icon = power.img;
        const speaker = ChatMessage.getSpeaker({ actor });
        const hasSufficientPSP = usePsionicsV1 ? pspdefense >= cost : psp >= cost;
        const title = `<h3 class="pwr-chat-output-header">Activate <b>${powerName}</b></h3>`;
        const msg = hasSufficientPSP
            ? `<div class="pwr-details"><b>${actor.name}</b> has activated <b>${powerName}</b> psionic defense.</div>`
            : `<div class="pwr-details"><b>${actor.name}</b> has insufficient PSP to activate <b>${powerName}</b>.</div>`;

        // If sufficient PSP, create the effect
        const effectData = {
            label: powerName,
            icon,
            origin: actor.uuid,
            disabled: false,
            duration: {
                rounds: power.duration || undefined,
                startRound: game.combat ? game.combat.round : 0,
            },
            changes: [],
            flags: {
                psionics: {
                    power,
                    defensePower: true,
                    defName,
                },
            },
        };

        // Check if the effect already exists on the actor and if so, dont apply it
        let alreadyApplied = actor.effects.some((eff) => eff.name === powerName);
        if (!alreadyApplied) await actor.createEmbeddedDocuments('ActiveEffect', [effectData]);

        const targets = Array.from(game.user.targets);
        for (const targetToken of targets) {
            const defenderActor = targetToken.actor;
            const isPsionic = defenderActor.system.psionics.isPsionic;
            alreadyApplied = defenderActor.effects.some((eff) => eff.name === powerName);
            if (!alreadyApplied) await defenderActor.createEmbeddedDocuments('ActiveEffect', [effectData]);
        }

        utilitiesManager.chatMessage(speaker, title, msg, icon);
    }

    // #region psychic combat 2e

    // attack vs. defense modifiers
    static attackDefenseModifiers = {
        mindthrust: { mindblank: 5, thoughtshield: -2, mentalbarrier: -4, intellectfortress: -4, towerofironwill: -5 },
        egowhip: { mindblank: 5, thoughtshield: 0, mentalbarrier: -3, intellectfortress: -4, towerofironwill: -3 },
        idinsinuation: { mindblank: -3, thoughtshield: 2, mentalbarrier: 4, intellectfortress: -1, towerofironwill: -3 },
        psychiccrush: { mindblank: 1, thoughtshield: -3, mentalbarrier: -1, intellectfortress: -3, towerofironwill: -4 },
        psionicblast: { mindblank: 2, thoughtshield: 3, mentalbarrier: 0, intellectfortress: -1, towerofironwill: -2 },
    };

    // psychic contest automation
    static async broadcastPsionicContestResults(defendingActor, attackerRoll = {}, defenderRoll) {
        const { isPsionic, closedMind } = defendingActor.system.psionics;
        const {
            success: attackerSuccess,
            checkDiff: attackerCheckDiff,
            checkTarget: attackerCheckTarget,
            result: attackerResult,
            actor: attackerActor,
            power: attackerPower,
        } = attackerRoll;

        const {
            success: defenderSuccess = false,
            checkDiff: defenderCheckDiff = 0,
            checkTarget: defenderCheckTarget = 0,
            result: defenderResult = 0,
            actor: defenderActor = defendingActor,
            power: defenderPower = { name: 'No Defense' },
        } = defenderRoll || {};

        const isGM = game.user.isGM;
        let contactMessage = 'Psychic Attack Failed';
        let effectLabel = '';
        let tangentLabel = '';
        let currentClosedMind = closedMind;
        const speaker = ChatMessage.getSpeaker({ actor: attackerActor });

        // Helper function to create or update the effect
        const applyEffect = async (label, tangent) => {
            // Remove any "Telepathic Tangent" effects from the attacker that are targeting the current defender
            await attackerActor.deleteEmbeddedDocuments(
                'ActiveEffect',
                attackerActor.effects
                    .filter(
                        (effect) =>
                            effect.name === 'Telepathic Tangent' && effect.flags?.psionics?.tangentTarget === defenderActor.name
                    )
                    .map((e) => e.id)
            );

            // Check if there's an existing effect with the specified label targeting the current defender
            const existingEffect = attackerActor.effects.find(
                (effect) => effect.name === label && effect.flags?.psionics?.tangentTarget === defenderActor.name
            );

            if (existingEffect) {
                // Update the existing effect
                await existingEffect.update({
                    icon: 'icons/magic/perception/third-eye-blue-red.webp',
                    origin: attackerActor.uuid,
                    disabled: false,
                    flags: {
                        psionics: {
                            contactPower: true,
                            tangentLabel: tangent,
                            tangentTarget: defenderActor.name,
                        },
                    },
                });
            } else {
                // Create a new effect
                await attackerActor.createEmbeddedDocuments('ActiveEffect', [
                    {
                        label,
                        icon: 'icons/magic/perception/third-eye-blue-red.webp',
                        origin: attackerActor.uuid,
                        disabled: false,
                        changes: [],
                        flags: {
                            psionics: {
                                contactPower: true,
                                tangentLabel: tangent,
                                tangentTarget: defenderActor.name,
                            },
                        },
                    },
                ]);
            }

            // If full contact is established, add a 'Mind Breached' effect to the defender
            const fullContact = tangent === 'Full Contact';
            if (fullContact) {
                const mindBreachedEffect = defenderActor.effects.find(
                    (effect) => effect.name === 'Telepathic Contact' && effect.origin === attackerActor.uuid
                );

                if (!mindBreachedEffect) {
                    await defenderActor.createEmbeddedDocuments('ActiveEffect', [
                        {
                            label: 'Telepathically Contacted',
                            icon: 'icons/magic/control/hypnosis-mesmerism-eye.webp',
                            origin: attackerActor.uuid,
                            disabled: false,
                            changes: [],
                            flags: {
                                psionics: {
                                    contactPower: true,
                                    tangentLabel: 'Mind Breached',
                                    tangentTarget: attackerActor.name,
                                },
                            },
                        },
                    ]);
                }
            }
        };

        if (game.dice3d) game.dice3d.showForRoll(attackerRoll, game.user, true);
        if (defenderRoll) {
            if (game.dice3d) game.dice3d.showForRoll(defenderRoll, game.user, true);
        }

        // Directly apply contact effect if non-psionic and attack successful
        if (!isPsionic && attackerSuccess) {
            await applyEffect('Telepathic Contact', 'Full Contact');
            contactMessage = 'Telepathic Contact Automatically Established';
        } else if (isPsionic) {
            if (attackerSuccess && (!defenderSuccess || attackerCheckDiff < defenderCheckDiff)) {
                currentClosedMind = Math.max(0, currentClosedMind - 1);
                await defendingActor.update({ 'system.psionics.closedMind': currentClosedMind });
                effectLabel = currentClosedMind === 0 ? 'Telepathic Contact' : 'Telepathic Tangent';
                tangentLabel = currentClosedMind === 2 ? '1 Finger' : currentClosedMind === 1 ? '2 Fingers' : 'Full Contact';
                contactMessage = currentClosedMind === 0 ? 'Telepathic Contact Established' : 'Telepathic Tangent Established';
                await applyEffect(effectLabel, tangentLabel);
            }
        }

        // Prepare the chat message
        const attackerDetails = `<div class="pwr-details owner-only-view"><hr><b>${attackerActor.name}</b> - Roll: ${attackerResult}, Target: ${attackerCheckTarget}, Check Diff: ${attackerCheckDiff}</div>`;
        const defenderDetails =
            defenderRoll && isPsionic
                ? `<div class="pwr-details gm-only-view"><hr><b>${
                      defenderActor.name
                  }</b> - Roll: ${defenderResult}, Target: ${defenderCheckTarget}, Check Diff: ${
                      defenderCheckDiff !== false ? defenderCheckDiff : 'N/A'
                  }</div>`
                : '';
        const msg = `
        <div class="pwr-details">${attackerActor.name} <b>attacks</b> ${defenderActor.name}</div>
        <div class="pwr-details" style="margin-bottom: 10px;"><hr><b class="owner-only-view">${attackerPower.name}</b> <span class="gm-only-view">vs <b>${defenderPower.name}</b><span></div>        
        ${attackerDetails}
        ${defenderDetails}            
        `;

        // Send the chat message
        utilitiesManager.chatMessage(
            speaker,
            `<h3 class="pwr-chat-output-header"><b>${contactMessage}</b></h3>`,
            msg,
            attackerPower.img
        );
    }

    // #endregion

    // #region psychic combat 1e

    static async makePsionicAttack(actor, power) {
        // psionic combat segment data
        const attackerPsionicCombatSegmentData = actor.flags.ars.psionicCombatSegmentData;

        // not prepped
        if (!attackerPsionicCombatSegmentData) {
            ui.notifications.notify('You must be in the combat tracker for Psychic combat automation to work!');
            return;
        }

        // total psionic strength of the attacker
        const attackerTotalPsionicStrength = Math.max(1, attackerPsionicCombatSegmentData.totalPsionicStrength);

        // get psionic attack type
        const attackPower = power.system.atkDefType;

        // psychic crush attack
        const isPsychicCrush = power.system.atkDefType === 'psychiccrush';

        // psionic blast
        const isPsychicBlast = power.system.atkDefType === 'psionicblast';

        // is AoE
        const isAoE = power.system.atkDefType === 'psionicblast' || power.system.atkDefType === 'idinsinuation';
        let skipDeplete = false;

        // for each target
        const targets = Array.from(game.user.targets);
        if (targets.length === 0) return;
        for (const targetToken of targets) {
            const defenderActor = targetToken.actor;
            const isPsionic = defenderActor.system.psionics.isPsionic;
            let modifier;
            let bestDefensePower;
            let defenseless;
            let defenselessResult;
            let defenselessResultDescription;

            // Identify the best defense power and modifier if the target is psionic
            if (isPsionic) {
                // psionic combat segment data for defender
                const defenderPsionicCombatSegmentData = defenderActor.flags.ars.psionicCombatSegmentData;

                // not prepped
                if (!defenderPsionicCombatSegmentData) {
                    ui.notifications.notify('Opponent must be in the combat tracker for Psychic combat automation to work!');
                    continue;
                }

                // find defenses on the opponent
                const defenseEffects = defenderActor.effects.filter((effect) => effect.flags?.psionics?.defName);

                // psionic with no defense up
                defenseless = defenderPsionicCombatSegmentData.totalPsionicDefenseStrength <= 0;

                // table IV.B
                if (defenseless) {
                    // total psionic attack strength for attacker
                    const attackersTotalPsionicAttackStrength = Math.max(
                        1,
                        attackerPsionicCombatSegmentData.totalPsionicAttackStrength
                    );

                    // defenders total psionic strength BASE (for fuck sake)
                    const defenderTotalPsionicStrength = Math.max(
                        10,
                        defenderActor.system.psionics.pspattack.base + defenderActor.system.psionics.pspdefense.base
                    );

                    // get result
                    defenselessResult = await this.lookupPsionicDefenseless(
                        attackersTotalPsionicAttackStrength,
                        attackPower,
                        defenderTotalPsionicStrength
                    );

                    // extract defense point cost if applicable
                    modifier = defenselessResult?.result && !defenselessResult.description ? defenselessResult.result : 0;
                    defenselessResultDescription = defenselessResult?.description;
                }

                // table IV.A
                else {
                    // get correct row from IV.A. - Psionic vs. Psionic in mental combat
                    const psionicCombatTable = await this.lookupPsionicCombat(attackerTotalPsionicStrength);

                    // get first defense effect in array - will cycle through all effects and find the best power to roll with for the defender, below
                    bestDefensePower = defenseEffects[0] || { name: 'Mind Blank' };

                    // no defense effect up, a psionic character will automatically activate one if he has defense psp
                    if (!defenseEffects.length) modifier = psionicCombatTable[attackPower]?.['mindblank'];

                    // for each defense, cross reference modifiers from the table
                    defenseEffects.forEach((effect) => {
                        const defensePower = effect.flags.psionics.defName;
                        const value = psionicCombatTable[attackPower]?.[defensePower] || null;

                        // low modifier === bad for the attacker as its less 'damage' to the opponents defense or less chance to kill (psychic crush)
                        if (!modifier) {
                            bestDefensePower = effect.flags.psionics.power;
                            modifier = value;
                        } else if (modifier && value < modifier) {
                            bestDefensePower = effect.flags.psionics.power;
                            modifier = value;
                        }
                    });
                }
            }

            // psionic blast upon non-psionic creature - different stack
            else {
                if (isPsychicBlast) {
                    skipDeplete = await this.makePsionicBlastAttack(actor, defenderActor, power, skipDeplete);
                }
                continue;
            }

            // Attacker roll - sum all modifiers and roll the attack
            const totalAttackCost = +power.system.powerCostAttack;
            const totalLoss = modifier || 0;
            let psychiCrushRoll;
            if (isPsychicCrush) {
                psychiCrushRoll = await new Roll('1d100').roll();
                if (game.dice3d) {
                    game.dice3d.showForRoll(psychiCrushRoll);
                }
            }

            // update psp values
            const newAttackValue = actor.system.psionics.pspattack.value - totalAttackCost;
            let insuffecientAttack = false;
            if (!isAoE || (isAoE && !skipDeplete)) insuffecientAttack = newAttackValue < 0;

            let hpDrain;
            let completelyDrained =
                defenderActor.system.psionics.pspattack.value === 0 && defenderActor.system.psionics.pspdefense.value === 0;

            if (!isAoE || (isAoE && !skipDeplete))
                await actor.update({ 'system.psionics.pspattack.value': Math.max(0, newAttackValue) });

            if (isAoE) skipDeplete = true;

            if (!isPsychicCrush) {
                if (!completelyDrained) {
                    if (defenseless) {
                        const attackValue = defenderActor.system.psionics.pspattack.value - totalLoss;
                        const newAttackValue = Math.max(0, attackValue);
                        await defenderActor.update({ 'system.psionics.pspattack.value': newAttackValue });
                        if (attackValue < 0) hpDrain = attackValue;
                    } else {
                        const defenseValue = defenderActor.system.psionics.pspdefense.value - totalLoss;
                        const newDefenseValue = Math.max(0, defenseValue);
                        await defenderActor.update({ 'system.psionics.pspdefense.value': newDefenseValue });

                        // if fully depleted defense, start eating into the attack psp
                        if (defenseValue < 0) {
                            const attackValue = defenderActor.system.psionics.pspattack.value - Math.abs(defenseValue);
                            const newAttackValue = Math.max(0, attackValue);
                            await defenderActor.update({ 'system.psionics.pspattack.value': newAttackValue });
                            if (attackValue < 0) hpDrain = attackValue;
                        }
                    }
                }

                // you're in big trouble
                completelyDrained =
                    defenderActor.system.psionics.pspattack.value === 0 && defenderActor.system.psionics.pspdefense.value === 0;
                if (completelyDrained) {
                    const newHp = defenderActor.system.attributes.hp.value - Math.abs(hpDrain || totalLoss);
                    await defenderActor.update({ 'system.attributes.hp.value': newHp });
                }
            }

            // gather all result data for chat and send to broadcast
            const results = {
                actorName: actor.name,
                defenderName: defenderActor.name,
                attackType: power.name,
                powerImage: power.img,
                totalAttackCost,
                insuffecientAttack,
                defenseType: bestDefensePower?.name,
                totalDefenseLost: totalLoss,
                completelyDrained,
                defenseless,
                defenselessResultDescription,
                isPsychicCrush,
                psychiCrushRoll,
            };

            // Resolve psychic contest
            await ARSPsionics.broadcastPsionicCombatResults1e(results);
        }

        // audio
        if (this?.power?.system?.audio?.file) {
            this.power.playAudio();
        } else {
            this._playAudioCast(true, 'publicroll', false);
        }
    }

    static async broadcastPsionicCombatResults1e(results) {
        const {
            actor,
            actorName,
            defenderName,
            attackType,
            powerImage,
            totalAttackCost,
            insuffecientAttack,
            defenseType = 'Defenseless',
            totalDefenseLost,
            completelyDrained,
            defenseless,
            defenselessResultDescription,
            isPsychicCrush,
            psychiCrushRoll,
        } = results;

        const speaker = ChatMessage.getSpeaker({ actor: actor });

        let title = `Psionic vs. Psionic in Mental Combat`;
        if (defenseless) title = `Psionic Attack vs. Defenseless Psionic`;
        title = `<h3 class="pwr-chat-output-header"><b>${title}</b></h3>`;

        // Prepare the chat message
        const insuffecientAttackPool = insuffecientAttack
            ? `<div class="pwr-details use-pwr-insuffecient">You have insuffecient Attack PSP</div>`
            : '';
        const completelyDrainedPool = completelyDrained
            ? `<div class="pwr-details use-pwr-insuffecient">Target is completely spent of PSP</div>`
            : '';
        const attackerDetails = `<div class="pwr-details"><b>Total Attack Cost: </b>${totalAttackCost}</div>`;
        const defenderDetails = `<div class="pwr-details"><b>Total Defense Lost: </b>${
            !isPsychicCrush ? totalDefenseLost : 'N/A'
        }</div>`;
        const defenselessResults = defenselessResultDescription ? `<div>${defenselessResultDescription}</div>` : '';
        let psychicCrushDetails = '';
        if (isPsychicCrush) {
            const total = psychiCrushRoll._total;
            const percentDeath = totalDefenseLost;
            psychicCrushDetails = `<div class="pwr-details">${percentDeath}% Chance of Instant Death</di><div class="pwr-details">Roll Results: ${total}%</div>`;
            if (total <= percentDeath) psychicCrushDetails += `<h2>Opponent has been Killed!</h2>`;
        }

        const msg = `
            <div class="pwr-details">${actorName} <b>attacks</b> ${defenderName}</div>
            <div class="pwr-details" style="margin-bottom: 10px;">${attackType} <b>vs</b> ${defenseType}</div>
            <hr>
            ${insuffecientAttackPool}
            ${completelyDrainedPool}
            ${attackerDetails}
            ${defenderDetails}
            ${psychicCrushDetails}
            ${defenselessResults}
        `;

        // Send the chat message
        utilitiesManager.chatMessage(speaker, title, msg, powerImage);
    }

    static async makePsionicBlastAttack(actor, defenderActor, power, skipDeplete) {
        const attackerTotalPsionicAttackStrength = actor.flags.ars.psionicCombatSegmentData.totalPsionicAttackStrength;
        const validAttackStrength = attackerTotalPsionicAttackStrength >= 100;
        const totalAttackCost = +power.system.powerCostAttack;
        const defenderIntellect = defenderActor.system.abilities.int.value + defenderActor.system.abilities.wis.value;
        const tblPsiBlast = await this.lookupPsionicBlastNonPsionic(defenderIntellect);
        const savingThrow = await new Roll('1d20').roll();

        let saveResultsAtRange = [];
        let effect;

        if (game.dice3d) {
            game.dice3d.showForRoll(savingThrow);
        }

        if (validAttackStrength) {
            const saveTotal = savingThrow._total;
            for (let key in tblPsiBlast) {
                if (tblPsiBlast.hasOwnProperty(key)) {
                    const value = tblPsiBlast[key];
                    const saved = saveTotal >= value;
                    saveResultsAtRange.push({
                        key,
                        value,
                        saved,
                        saveTotal,
                    });
                }
            }
            const rollPercentile = await new Roll('1d100').roll();

            if (game.dice3d) {
                game.dice3d.showForRoll(rollPercentile);
            }

            const total = rollPercentile._total;

            effect = await this.lookupPsionicAttackPercentage(defenderIntellect, total);
            effect = {
                result: effect,
                total,
            };

            // do only once, its an AoE
            if (!skipDeplete) {
                const newAttackValue = Math.max(0, actor.system.psionics.pspattack.value - totalAttackCost);
                await actor.update({ 'system.psionics.pspattack.value': newAttackValue });
            }
        }

        const results = {
            actor,
            powerImage: power.img,
            totalAttackCost,
            validAttackStrength,
            actorName: actor.name,
            defenderName: defenderActor.name,
            attackType: power.name,
            defenderIntellect,
            saveResultsAtRange,
            effect,
        };

        this.broadcastPsionicBlastResults(results);

        return true;
    }

    static async broadcastPsionicBlastResults(results) {
        const {
            actor,
            powerImage,
            totalAttackCost,
            validAttackStrength,
            actorName,
            defenderName,
            attackType,
            defenderIntellect,
            saveResultsAtRange,
            effect,
        } = results;

        // console.log(actor, defenderActor, results);
        const speaker = ChatMessage.getSpeaker({ actor: actor });
        const title = `<h3 class="pwr-chat-output-header"><b>Psionic Blast vs. Non-Psionic</b></h3>`;

        let details = '';
        if (validAttackStrength) {
            details += `<div class="pwr-details">Opponents Intellect: ${defenderIntellect}</div>`;

            // Get all keys and values
            for (const obj of saveResultsAtRange) {
                details += `<div class="pwr-details">Save: <span style="text-transform: uppercase;">${
                    obj.saved ? '<b>Success</b>' : '<b>Fail</b>'
                }</span> ${obj.saveTotal} vs. ${obj.key} range: ${obj.value}</div>`;
            }

            if (effect) {
                details += `<div class="pwr-details">Roll: ${effect.total}%</div>`;
                details += `<div class="pwr-details" style="text-transform: uppercase;">On Failure: <b>${effect.result}</b></div>`;
            }
        } else {
            details = `<div class="pwr-details">Insuffecient Attack Strength: ${actor.system.psionics.pspattack.value}</div>`;
        }

        // Prepare the chat message
        const msg = `
            <div class="pwr-details">${actorName} <b>attacks</b> ${defenderName}</div>
            <div class="pwr-details" style="margin-bottom: 10px;"><b>${attackType}</b></div>
            <hr>
            ${details}            
        `;

        // Send the chat message
        utilitiesManager.chatMessage(speaker, title, msg, powerImage);
    }

    static async initPsionicCombatTracking(actor) {
        if (game.settings.get('ars', 'usePsionicsV1')) {
            const combat = game.combat;
            if (combat) {
                const combatant = combat.combatants.find((c) => c.actorId === actor.id);
                if (!combatant) return;
                const totalPsionicStrength = actor.totalPsionicStrength;
                const totalPsionicAttackStrength = actor.totalPsionicAttackStrength;
                const totalPsionicDefenseStrength = actor.totalPsionicDefenseStrength;
                const psionicCombatSegmentData = {
                    totalPsionicStrength,
                    totalPsionicAttackStrength,
                    totalPsionicDefenseStrength,
                };
                await actor.setFlag('ars', 'psionicCombatSegmentData', psionicCombatSegmentData);
            }
        }
    }

    static async endPsionicCombatTracking(combat) {
        for (let combatant of combat.combatants) {
            let actor = combatant.actor;
            if (actor) {
                await actor.unsetFlag('ars', 'psionicCombatSegmentData');
            }
        }
    }

    static async lookupPsionicCombat(value) {
        for (let entry of ARS.psionics.psionicCombatTable) {
            const [min, max] = entry.range;
            if (value >= min && value <= max) {
                return entry.result;
            }
        }
        return null; // If no match is found
    }

    static async lookupPsionicDefenseless(value, attack, subValue) {
        for (let entry of ARS.psionics.psionicDefenselessTable) {
            const [min, max] = entry.range;
            if (value >= min && value <= max) {
                const attackResult = entry.result[attack];
                for (let result of attackResult) {
                    if (subValue >= result.range[0] && subValue <= result.range[1]) {
                        return {
                            result: result.result,
                            description: result.description,
                        };
                    }
                }
            }
        }
        return null; // If no match is found
    }

    static async lookupPsionicBlastNonPsionic(intellect) {
        for (let entry of ARS.psionics.psionicBlastNonPsionicTable) {
            const [min, max] = entry.range;
            if (intellect >= min && intellect <= max) {
                return entry.result;
            }
        }
        return null; // If no match is found
    }

    static async lookupPsionicAttackPercentage(intelligenceWisdom, roll) {
        for (let entry of ARS.psionics.psionicAttackPercentageTable) {
            const [min, max] = entry.range;
            if (intelligenceWisdom >= min && intelligenceWisdom <= max) {
                for (let subEntry of entry.result) {
                    const [subMin, subMax] = subEntry.range;
                    if (roll >= subMin && roll <= subMax) {
                        return subEntry.result;
                    }
                }
            }
        }
        return null; // No match found
    }

    // #endregion
}
