/**
 *
 * Hooks
 *
 */

import { ARSItem } from './item/item.js';
import { ARS } from './config.js';
import { ARSActor } from './actor/actor.js';
import * as chatManager from './chat.js';
import * as utilitiesManager from './utilities.js';
import * as macrosManager from './macros.js';
import * as effectManager from './effect/effects.js';
import { CombatManager } from './combat/combat.js';
import * as dialogManager from './dialog.js';
import * as combatTracker from './combat/combatTracker.js';
import * as initLibrary from './library.js';
import * as encounterManager from './data-models/items/encounter.js';
import * as debug from './debug.js';
import { addPartyTab } from './sidebar/party.js';
import { ARSItemBrowserManager, ARSItemBrowser } from './apps/item-browser.js';
import { ARSActorBrowserManager, ARSActorBrowser } from './apps/actor-browser.js';
import { addTradeButton } from './apps/item-trade.js';
import { ARSCombatHUD } from './apps/combat-hud.js';
import { migrationChecks, changeLogChecks } from './system/migration.js';
import { ImportManager } from './apps/import-tools.js';
import { SocketManager } from './sockets.js';
import { Enrichers } from './text-enrichers.js';

export default function () {
    /**
     * Hook to set default values for new actors
     */

    console.log('hooks.js DEFAULT()');

    /**
     *
     * Hook run on items created.
     *
     * We do some work here so we can see if the item is owned
     *
     */
    Hooks.on('createItem', (item, options, userId) => {
        if (!game.user.isDM) return;
        // console.log("hooks.js createItem", { item, options, userId });
        // if (item.type === 'spell') initLibrary.refreshWorldSpellsList();
        item.postCreateItem();
    });

    Hooks.on('applyActiveEffect', (actor, effectData, change, value, changes) => {
        if (!game.user.isDM) return;
        // console.log("hooks.js applyActiveEffect", { actor, effectData, change, value, changes });

        if (['character', 'npc'].includes(actor.type)) {
            // if the duration.seconds has a value and startTIme is 0, set it.
            if (
                !effectData.effect.disabled &&
                !effectData.effect.isSuppressed &&
                effectData.effect.duration.seconds &&
                !effectData.effect.duration.startTime
            ) {
                effectData.effect.update({
                    'duration.startTime': game.time.worldTime,
                });
            }
        }
    });

    // intercept messages and show/hide buttons based on owner/isGM
    Hooks.on('renderChatMessage', (app, html, data) => {
        // console.log("hooks.js renderChatMessage", { app, html, data });
        chatManager.renderChatMessages(app, html, data);
    });

    /**
     * Do anything after initialization but before "ready"
     */
    Hooks.once('setup', function () {});

    /**
     *
     * Hook to capture socket requests to run action as GM
     *
     */
    Hooks.on('ready', async () => {
        console.log('hooks.js Hook:ready!!!!!');

        await initLibrary.default();
        // pre-create the browser so it doesn't take long time to load
        // when they click it later.
        // if (!game.ars.ui?.itembrowser) {
        game.ars.ui = {
            itembrowser: new ARSItemBrowser(),
            actorbrowser: game.user.isGM ? new ARSActorBrowser() : undefined,
        };
        // }

        game.socket.on('system.ars', (context) => {
            console.log('hooks.js game.socket.on', { context });
            // process commands only GM can perform
            if (context.type === 'runAsGM') {
                if (game.user.isGM) utilitiesManager.processGMCommand(context);
            } else {
                // throw messages around between all players
                // used for request trade mostly right now
                SocketManager.socketedCommunication(context);
            }
        });

        // last thing we do is initiallize the party-tracker bits

        if (game.user.isDM) {
            migrationChecks();

            game.party.initializePartyTracker();

            changeLogChecks();
        }

        // on ready - set important data to body based on settings/configuration
        const variant = game.settings.get('ars', 'systemVariant');
        const usePsionics = game.settings.get('ars', 'usePsionics'); // #psionic
        const usePsionicsV1 = game.settings.get('ars', 'usePsionicsV1');
        const useExperimental = game.settings.get('ars', 'useExperimental');
        const isGM = game.user.isGM;
        document.body.setAttribute('data-user', isGM ? 'gm' : 'player');
        document.body.setAttribute('data-system-variant', variant);
        document.body.classList.toggle('psionics-enabled', usePsionics);
        document.body.classList.toggle('psionics-enabled-v1', usePsionicsV1);
        document.body.classList.toggle('experimental-enabled', useExperimental);
        // addTradeButton();
    });

    // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
    Hooks.on('hotbarDrop', (bar, data, slot) => {
        // console.log("hooks.js hotbarDrop", { bar, data, slot });
        macrosManager.createARSMacro(data, slot);
        return false;
    });

    Hooks.on('updateWorldTime', async (worldTime, advanceTime) => {
        console.log('hooks.js Hook: updateWorldTime', {
            worldTime,
            advanceTime,
        });
        if (game.user.isDM) {
            await effectManager.manageEffectExpirations(worldTime);
        }
    });

    /**
     * arsUpdateToken hook from actor.js
     */
    Hooks.on('arsUpdateToken', (actor, token, data, delay = 300) => {
        console.log('hooks.js Hook: arsUpdateToken', { actor, token, data });
        utilitiesManager.delayExecution(() => token.update(data), delay);
        // const _timeout1 = setTimeout(async () => await token.update(data), delay);
    });
    /**
     * hook to apply actor updates via hook
     */
    Hooks.on('arsUpdateActor', (actor, data) => {
        console.log('hooks.js Hook: arsUpdateActor', { actor, data });
        if (actor.isOwner) {
            // const _timeout1 = setTimeout(async () => await actor.update(data), 300);
            utilitiesManager.delayExecution(() => actor.update(data), 300);
        }
    });

    /**
     * When something dropped on canvas
     */
    Hooks.on('dropCanvasData', async function (canvas, object) {
        console.log('Hooks:dropCanvasData', { canvas, object });
        const dropTarget = canvas.tokens.placeables.find((token) => {
            const maximumX = token.x + (token.hitArea?.right ?? 0);
            const maximumY = token.y + (token.hitArea?.bottom ?? 0);
            return object.x >= token.x && object.y >= token.y && object.x <= maximumX && object.y <= maximumY;
        });
        /**
         * Helper Function
         *
         * Spawn a lootable at a specified location when an item is dropped.
         *
         * @param {Object} coords - The coordinates where the token will be placed.
         * @param {number} coords.x - The x-coordinate for the token.
         * @param {number} coords.y - The y-coordinate for the token.
         * @param {Actor} sourceActor - The actor from which the item originates.
         * @param {Item} sourceItem - The item being dropped.
         */
        async function dropLootableItem(coords, sourceActor, sourceItem) {
            // Ensure proper input validation
            if (!coords?.x || !coords?.y || !sourceItem) {
                ui.notifications.error('Drop Item on Map: Invalid arguments passed to dropLootableItem function.');
                return;
            }

            if (!sourceActor && !game.user.isGM) {
                ui.notifications.error('Drop Item on Map: Not a GM and no actor for item dropped.');
                return;
            }

            const gameScene = game.scenes.current;
            await utilitiesManager.gmDropItemOnMap(gameScene, coords, sourceActor, sourceItem);
        }

        /**
         *
         * Handle item/coin drag/drops on tokens
         * Handle encounter drops on map
         *
         */
        function _processActorDrop(obj) {
            const actor = dropTarget?.actor;
            if (actor && actor.isOwner) {
                if (['character', 'npc', 'lootable', 'merchant'].includes(actor.type) && actor.sheet) {
                    actor.sheet._handleOnDrop({}, obj);
                }
            }
        }

        switch (object.type) {
            case 'Item':
                // items from lootables have data
                if (object.hasOwnProperty('data')) {
                    _processActorDrop(object);
                    return false;
                } else {
                    // items from the world/compendium do not have .data, just itemid
                    // const item = await utilitiesManager.getItem(object.id);
                    const item = await fromUuid(object.uuid);
                    if (item instanceof ARSItem) {
                        if (item.type === 'encounter') {
                            encounterManager.encounterDrop(item, object);
                            return false;
                        } else if (dropTarget) {
                            _processActorDrop(object);
                            return false;
                        } else {
                            const actor = item.parent instanceof ARSActor ? item.parent : undefined;
                            if (actor) {
                                if (
                                    await dialogManager.confirm(
                                        `Drop ${item.name}${item.system.quantity > 1 ? 's' : ''} (${item.system.quantity})?`,
                                        `Confirm Drop Item`
                                    )
                                )
                                    dropLootableItem(object, actor, item);
                            } else if (game.user.isGM) {
                                dropLootableItem(object, undefined, item);
                            }
                        }
                    }
                    // else {
                    //     // no a encounter item, we allow rest to fall through as normal drops to actor sheet
                    //     _processActorDrop(object);
                    //     return false;
                    // }
                }
                break;

            case 'Coin':
                _processActorDrop(object);
                return false;
                break;
        }

        return true;
    });

    // dont use this but I tested it out.
    // Hooks.on("getChatLogEntryContext", chat.addChatMessageContextOptions);

    /**
     * Hook for when a token hud is rendered
     */
    // Hooks.on('renderTokenHUD', async (hud, html, token) => {
    // });

    /** hook triggers when user clicks on token. */
    Hooks.on('controlToken', async (token, selected) => {
        // console.log('hooks.js controlToken', { token, selected });
        await ARSCombatHUD.activateCombatHud(token, selected);
        // if (game.settings.get('ars', 'floatingHudAllowPlayer')) {
        //     if (token?.actor?.type !== 'lootable') {
        //         if (selected) {
        //             new ARSCombatHUD(token).createHud();
        //         } else {
        //             if (game.user.isGM && token.combatHud) {
        //                 token.combatHud.close();
        //             } else {
        //                 console.log(`hooks.js NOTHING TO CLOSE Name=${token.actor.name} ID: ${token.id}`);
        //             }
        //         }
        //     }
        // }
    });

    Hooks.on('renderSidebar', (app, html) => {
        // console.log("hooks.js renderSidebar", { app, html })
        // add the partyTab
        addPartyTab(app, html);
    });

    Hooks.on('renderSidebarTab', (app, html) => {
        // console.log("hooks.js renderSidebarTab", { app, html })
        ARSItemBrowserManager.addItemBrowserButton(app, html);
        ARSActorBrowserManager.addActorBrowserButton(app, html);
        ImportManager.addImportActorButton(app, html);
        ImportManager.addImportTableButton(app, html);
    });

    Hooks.on('renderPlayerList', (app, html) => {
        // console.log("hooks.js renderPlayerList", { app, html })
        if (!document.getElementById('trade-request-button')) addTradeButton();
    });

    // Hooks.on('renderActorSheet', (app, html, data) => {
    //     console.log('hooks.js renderActorSheet', { app, html, data });
    // });

    /**
     * capture command line options
     * /award
     * /loadscene uuid
     */
    Hooks.on('chatMessage', (app, message, data) => {
        // ......applications.Award.chatMessage(message)
        console.log('hooks.js chatMessage', { app, message, data });

        // parse loadscene UUID and activate
        const loadSceneRegex = /\/loadscene\s([a-zA-Z0-9]{16})/;
        if (loadSceneRegex.test(message)) {
            if (game.user.isGM) {
                try {
                    const match = message.match(loadSceneRegex);
                    const sceneId = match[1];
                    Enrichers.loadScene(sceneId);
                    return false;
                } catch (err) {
                    ui.notifications.error(`Error in hooks.js chatMessage ${err}`);
                    return false;
                }
            }
        }

        return true;
    });

    // hook tags for token vision updates to use for updating CT views.
    // lightingRefreshm, sightRefresh
    Hooks.on('sightRefresh', (sightLayer) => {
        // console.log("hooks.js sightRefresh", { sightLayer })

        // dont need this, siteRefresh only triggers for the user, not everyone
        // sightLayer.sources.forEach((tokenObject) => {
        //     console.log("hooks.js sightRefresh", { tokenObject }, tokenObject.object.actor.id, game.user.data.character)
        //     if (game.user.data.character === tokenObject.object.actor.id) {
        //         ui.combat.render();
        //     }
        // });
        if (!game.user.isDM) {
            if (game.ars.config.settings.ctShowOnlyVisible) {
                ui.combat.render();
            }
        }
    });

    // updateSetting
    Hooks.on('updateSetting', (setting, value, diff, id) => {
        // console.log("hooks.js updateSetting", { setting, value, diff, id })
        // initLibrary.buildPackItemList();
        const variant = game.settings.get('ars', 'systemVariant');
        if (setting.key === 'ars.systemVariant') {
            document.body.setAttribute('data-system-variant', variant);
        }
    });

    Hooks.on('postRollAbility', (target, success, type, sourceActor) => {
        // console.log("hooks.js postAbilityRoll", { target, success, type, sourceActor });
    });

    Hooks.on('postRollSave', (target, success, type, sourceActor) => {
        // console.log("hooks.js postSaveRoll", { target, success, type, sourceActor });
    });

    Hooks.on('postRollAttack', (target, success, sourceActor, sourceItem, sourceAction) => {
        // console.log("hooks.js postAttackRoll", { target, success, sourceActor, sourceItem, sourceAction });
    });

    Hooks.on('postRollSkill', (target, success, type) => {
        // console.log("hooks.js postSkillRoll", { target, success, type });
    });
} // end hooks initializations
