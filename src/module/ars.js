// Import Modules
import { ARS } from './config.js';
import { registerSystemSettings } from './settings.js';
import { preloadTemplates } from './preloadTemplates.js';
import { Enrichers } from './text-enrichers.js';
import { AuthoringToolbox } from './authoring_toolbox/authoring-toolbox.js';

// Overrides
import { ARSActor } from './actor/actor.js';
import { ARSItem } from './item/item.js';
import * as dataModels from './data-models/_models.js';
import { ARSAction, ARSActionGroup } from './action/action.js';
import { ARSCombatTracker, ARSCombat } from './combat/combatTracker.js';
import { CombatManager } from './combat/combat.js';
import { ARSPsionics } from './psionics/psionics.js';
import { PartySidebar } from './sidebar/party.js';
import { ARSToken, ARSTokenDocument, ARSTokenLayer } from './token/token.js';

import {
    ARSRollTable,
    ARSCombatant,
    ARSPermissionControl,
    ARSJournalDirectory,
    ARSRollTableDirectory,
    ARSItemDirectory,
    ARSPlaylistDirectory,
    ARSCompendiumDirectory,
    ARSTokenConfig,
    ARSRollTableConfig,
    ARSChatLog,
    ARSChatMessage,
    ARSUser,
    ARSTileHUD,
    ARSTileDocument,
    ARSTile,
} from './overrides.js';

import { ARSActiveEffect, ARSActiveEffectConfig } from './effect/effects.js';

import {
    ARSRollBase,
    ARSRoll,
    ARSRollAttack,
    ARSRollCombat,
    ARSRollDamage,
    ARSRollInitiative,
    ARSRollSave,
    ARSRollSkill,
    ARSRollPower,
    ARSRollAbilityCheck,
    ARSRollGeneric,
} from './dice/rolls.js';

import { ARSDamage } from './dice/damage.js';
import { ARSDice } from './dice/dice.js';

// Applications
import { ARSActorSheet } from './actor/actor-sheet.js';
import { ARSLootableSheet } from './actor/actor-sheet-lootable.js';
import { ARSNPCSheet } from './actor/actor-sheet-npc.js';
import { ARSCharacterSheet } from './actor/actor-sheet-character.js';
import { ARSMerchantSheet } from './actor/actor-sheet-merchant.js';

import { ARSJournalSheet } from './journal/journal-sheet.js';
import { ARSItemSheet } from './item/item-sheet.js';
import { ARSItemBrowser } from './apps/item-browser.js';
import { ARSActorBrowser } from './apps/actor-browser.js';
import { ARSCombatHUD } from './apps/combat-hud.js';
import { ImportSheet, ImportManager } from './apps/import-tools.js';
// Import Helpers
// import * as chat from "./chat.js";
import * as debug from './debug.js';
import * as effectManager from './effect/effects.js';
import * as chatManager from './chat.js';
import * as utilitiesManager from './utilities.js';
import * as macrosManager from './macros.js';
import * as dialogManager from './dialog.js';
import * as migrationManager from './system/migration.js';
import * as initHooks from './hooks.js';
import * as initHandlebars from './handlebars.js';
import * as actionsManager from './action/action.js';

Hooks.once('init', async function () {
    console.log('ars | Initializing ARS.System');

    // DEBUG hooks
    // CONFIG.debug.hooks = true;

    game.ars = {
        // Variable we use to make sure we don't run the same request if we have multiple GMs.
        runAsGMRequestIds: [],
        lootables: new Map(),
        applications: {
            ARSCharacterSheet,
            ARSCombatHUD,
            ARSNPCSheet,
            ARSLootableSheet,
            ARSCombatant,
            ARSPermissionControl,
            // ARSFolder,
            ARSActiveEffectConfig,
            ARSJournalSheet,
            ARSJournalDirectory,
            ARSRollTableDirectory,
            ARSRollTableConfig,
            ARSChatLog,
            ARSItemDirectory,
            ARSPlaylistDirectory,
            ARSCompendiumDirectory,
            ARSItemBrowser,
            ARSActorBrowser,
            ImportSheet,
        },
        ARSUser,
        ARSActor,
        ARSItem,
        ARSActorSheet,
        ARSJournalSheet,
        ARSTokenDocument,
        ARSDice,
        ARSDamage,
        ARSRoll,
        ARSRollTable,
        ARSAction,
        ARSActionGroup,
        config: ARS,
        const: CONST,
        macrosManager: macrosManager,
        chatManager: chatManager,
        effectManager: effectManager,
        actionsManager: actionsManager,
        ARSPsionics: ARSPsionics,
        utilitiesManager: utilitiesManager,
        dialogManager: dialogManager,
        combatManager: CombatManager,
        migrationManager: migrationManager,
        importManager: ImportManager,
        AuthoringToolbox,
        library: {},
        //    chat: chat,
        // rollItemMacro: macrosManager.rollItemMacro,
    };

    // ars config
    CONFIG.ARS = ARS;

    // some templates remain for ranks, actions that will be around for awhile
    const ars_template = await fetch('/systems/ars/module/system/template.json');
    game.ars.templates = await ars_template.json();
    //

    //
    /**
     * Turn off legacy ActiveEffects (.parent now .target on effects)
     *
     */
    CONFIG.ActiveEffect.legacyTransferral = false;
    //

    // default initiative
    CONFIG.Combat.initiative = {
        formula: '1d10',
        decimals: 2,
    };

    if (CONFIG.Combat?.skipDefeated === undefined) CONFIG.Combat.skipDefeated = true;

    // round/turn time (in seconds)
    CONFIG.time.roundTime = 60;
    CONFIG.time.turnTime = 600;
    CONFIG.time.hourTime = 3600;
    CONFIG.time.dayTime = 86400; // 24 hours
    CONFIG.time.weekTime = CONFIG.time.dayTime * 7;
    CONFIG.time.monthTime = CONFIG.time.dayTime * 30;

    // Define custom Entity classes
    // CONFIG.ChatMessage.documentClass = ARSChatMessage;
    CONFIG.Dice.rolls = [
        ARSRollBase,
        ARSRoll,
        ARSRollCombat,
        ARSRollAttack,
        ARSRollDamage,
        ARSRollSave,
        ARSRollSkill,
        ARSRollPower,
        ARSRollAbilityCheck,
        ARSRollInitiative,
        ARSRollGeneric,
    ];
    CONFIG.Actor.documentClass = ARSActor;
    CONFIG.Actor.dataModels = {
        character: dataModels.character,
        npc: dataModels.npc,
        lootable: dataModels.lootable,
        merchant: dataModels.merchant,
    };
    CONFIG.Item.documentClass = ARSItem;
    CONFIG.Item.dataModels = {
        item: dataModels.itemData,
        bundle: dataModels.bundle,
        ability: dataModels.ability,
        race: dataModels.race,
        proficiency: dataModels.proficiency,
        currency: dataModels.currency,
        container: dataModels.container,
        potion: dataModels.potion,
        background: dataModels.background,
        encounter: dataModels.encounter,
        skill: dataModels.skill,
        spell: dataModels.spell,
        power: dataModels.power,
        weapon: dataModels.weapon,
        armor: dataModels.armor,
        class: dataModels.class,
    };
    CONFIG.User.documentClass = ARSUser;
    CONFIG.Combat.documentClass = ARSCombat;
    CONFIG.Combatant.documentClass = ARSCombatant;
    CONFIG.RollTable.documentClass = ARSRollTable;
    CONFIG.ActiveEffect.documentClass = ARSActiveEffect;
    // CONFIG.ActiveEffect.dataModels.custom = dataModels.activeEffectData;
    CONFIG.Token.documentClass = ARSTokenDocument;
    CONFIG.Token.objectClass = ARSToken;
    CONFIG.Token.prototypeSheetClass = ARSTokenConfig;

    // CONFIG.Token.documentClass = ARSFormApplication;

    // /**
    //  * Configuration for the ChatMessage document
    //  *
    //  * needed to tweak the chat-message template
    //  */
    CONFIG.ChatMessage = {
        documentClass: ARSChatMessage,
        collection: Messages,
        template: 'systems/ars/templates/chat/chat-message.hbs',
        sidebarIcon: 'fas fa-comments',
        batchSize: 100,
    };

    console.log('CONFIG===============', CONFIG);
    // CONFIG.ChatMessage.template
    CONFIG.Canvas.layers.tokens.layerClass = ARSTokenLayer;
    CONFIG.ui.chat = ARSChatLog;
    CONFIG.ui.combat = ARSCombatTracker;
    CONFIG.ui.journal = ARSJournalDirectory;
    CONFIG.ui.items = ARSItemDirectory;
    CONFIG.ui.tables = ARSRollTableDirectory;
    CONFIG.ui.playlists = ARSPlaylistDirectory;
    CONFIG.ui.compendium = ARSCompendiumDirectory;
    CONFIG.ui.party = PartySidebar;
    CONFIG.Tile.objectClass = ARSTile;
    CONFIG.Tile.documentClass = ARSTileDocument;
    CONFIG.Tile.hudClass = ARSTileHUD;

    // CONFIG.Folder.documentClass = ARSFolder;
    // this overrides/extends the PermissionControl app class, we
    // do subfolder permission settings with this.
    DocumentOwnershipConfig = ARSPermissionControl;

    // Register System Settings
    registerSystemSettings();

    CONFIG.Combat.initiative.formula = game.settings.get('ars', 'initiativeFormula');
    CONFIG.ARS.settings = {
        autohitfail: game.settings.get('ars', 'useAutoHitFailDice'),
        automateLighting: game.settings.get('ars', 'automateLighting'),
        automateVision: game.settings.get('ars', 'automateVision'),
        npcLootable: game.settings.get('ars', 'npcLootable'),
        debugMode: game.settings.get('ars', 'debugMode'),
        ctShowOnlyVisible: game.settings.get('ars', 'ctShowOnlyVisible'),
        encumbranceIncludeCoin: game.settings.get('ars', 'encumbranceIncludeCoin'),
        identificationActor: game.settings.get('ars', 'identificationActor'),
        identificationItem: game.settings.get('ars', 'identificationItem'),
        systemVariant: game.settings.get('ars', 'systemVariant'),
        useArmorDamage: game.settings.get('ars', 'useArmorDamage'),
        initiativeUseSpeed: game.settings.get('ars', 'initiativeUseSpeed'),
        automateEncumbrance: game.settings.get('ars', 'automateEncumbrance'),
        usePsionics: game.settings.get('ars', 'usePsionics'), // #psionic
        usePsionicsV1: game.settings.get('ars', 'usePsionicsV1'), // #psionic
        combineStack: game.settings.get('ars', 'combineStack'),
        macroDontCloneOnDrop: game.settings.get('ars', 'macroDontCloneOnDrop'),
        useInfoHelper: game.settings.get('ars', 'useInfoHelper'),
        useExperimental: game.settings.get('ars', 'useExperimental'),
        useAscendingAC: game.settings.get('ars', 'useAscendingAC'),
        useComeliness: game.settings.get('ars', 'useComeliness'),
        useHonor: game.settings.get('ars', 'useHonor'),
    };
    // set this setting dependant on variant type
    // game.settings.settings.get("ars.useArmorDamage").config = CONFIG.ARS.settings.systemVariant == '2';

    // hook debug
    CONFIG.debug.hooks = CONFIG.ARS.settings.debugMode;

    // Register sheet application classes
    Actors.unregisterSheet('core', ActorSheet);
    // Actors.registerSheet("ars", ARSActorSheet, { makeDefault: true });
    Actors.registerSheet('ars', ARSCharacterSheet, {
        types: ['character'],
        makeDefault: true,
        label: 'ARS.sheet.actor.character',
    });
    Actors.registerSheet('ars', ARSNPCSheet, {
        types: ['npc'],
        makeDefault: true,
        label: 'ARS.sheet.actor.npc',
    });
    Actors.registerSheet('ars', ARSLootableSheet, {
        types: ['lootable'],
        makeDefault: true,
        label: 'ARS.sheet.actor.lootable',
    });
    Actors.registerSheet('ars', ARSMerchantSheet, {
        types: ['merchant'],
        makeDefault: true,
        label: 'ARS.sheet.actor.merchant',
    });

    Items.unregisterSheet('core', ItemSheet);
    Items.registerSheet('ars', ARSItemSheet, { makeDefault: true });

    DocumentSheetConfig.registerSheet(TokenDocument, 'ars', ARSTokenConfig, { makeDefault: true });
    DocumentSheetConfig.registerSheet(ActiveEffect, 'ars', ARSActiveEffectConfig, { makeDefault: true });
    DocumentSheetConfig.registerSheet(RollTable, 'ars', ARSRollTableConfig, { makeDefault: true });

    // DocumentSheetConfig.registerSheet(ActiveEffect, "ars", ARSActiveEffect, {
    //   makeDefault: true,
    //   label: "ARS.ActiveEffect"
    // });

    DocumentSheetConfig.unregisterSheet(JournalEntry, 'core', JournalSheet);
    DocumentSheetConfig.registerSheet(JournalEntry, 'ars', ARSJournalSheet, { makeDefault: true });

    //include ruleset specific statusEffects
    // variant specific status effects
    ARS.statusEncumbranceEffects[CONFIG.ARS.settings.systemVariant].forEach((status) => {
        CONFIG.statusEffects.push(status);
    });
    // default status effects
    ARS.defaultStatusEffects.forEach((status) => {
        CONFIG.statusEffects.push(status);
    });

    // set defeated icon to something special
    CONFIG.controlIcons.defeated = ARS.icons.general.combat.effects.defeated;
    const deadIndex = CONFIG.statusEffects.findIndex((entry) => {
        return entry.id === 'dead';
    });
    if (deadIndex > -1) CONFIG.statusEffects[deadIndex].img = ARS.icons.general.combat.effects.defeated;

    // set some global consts to be used

    initHandlebars.default();

    await preloadTemplates();

    Enrichers.registerCustomEnrichers();
});

initHooks.default();
