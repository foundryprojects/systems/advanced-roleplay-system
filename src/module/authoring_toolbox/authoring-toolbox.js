import { ARS } from '../config.js';
import * as effectManager from '../effect/effects.js';
import { ARSActionGroup, createActionForNPCToken } from '../action/action.js';
import * as initLibrary from '../library.js';
import * as dialogManager from '../dialog.js';
import { ARSCharacterSheet } from '../actor/actor-sheet-character.js';
import { ARSNPCSheet } from '../actor/actor-sheet-npc.js';

import * as debug from '../debug.js';
import { CombatManager } from '../combat/combat.js';
import { ARSPsionics } from '../psionics/psionics.js';

export class AuthoringToolbox {}

// authoring scripts to help with content
window.authoringHelpers = function () {
    // Function to build Everything search string for a specific folder with formatted terms
    function createEverythingSearchString(folderPath) {
        const searchTerms = [];

        // Loop through each actor in the world to gather image file names
        game.actors.forEach((actor) => {
            // Decode URI components, get file name, remove the extension
            let actorImageFileName = decodeURIComponent(actor.img.split('/').pop().split('.').slice(0, -1).join('.'));
            let tokenImageFileName = decodeURIComponent(
                actor.prototypeToken.texture.src.split('/').pop().split('.').slice(0, -1).join('.')
            );

            // Remove whitespace, special characters, convert to lowercase, and add wildcards for partial matching
            actorImageFileName = `*${actorImageFileName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase()}*`;
            tokenImageFileName = `*${tokenImageFileName.replace(/[^a-zA-Z0-9]/g, '').toLowerCase()}*`;

            // Add to search terms
            searchTerms.push(actorImageFileName, tokenImageFileName);
        });

        // Remove duplicates by converting to a Set and back to an array
        const uniqueImages = [...new Set(searchTerms)];

        // Join the terms with " | " for OR search
        const combinedTerms = uniqueImages.join(' | ');

        // Apply the folder path restriction once, for the entire search string
        return `path:"${folderPath}" (${combinedTerms})`;
    }

    // output all compendium pack names - useful for getting the correct path
    function outputPackNames() {
        game.packs.forEach((pack) => {
            console.log(`Compendium Name: ${pack.metadata.label} (ID: ${pack.collection})`);
        });
    }

    // skeletonn - modify for what you need -
    async function pageDataFixer(
        journalId,
        compendiumName = '2e-complete-psionics-handbook.2e-complete-psionics-journals', // Use the compendium name instead of a direct journal ID
        startPageId = '',
        endPageId = ''
    ) {
        // Load the compendium
        const compendium = game.packs.get(compendiumName);
        if (!compendium) {
            ui.notifications.error(`Compendium with name ${compendiumName} not found.`);
            return;
        }

        // Load the Journal Entry from the compendium
        const journalEntry = await compendium.getDocument(journalId);
        if (!journalEntry) {
            ui.notifications.error(`Journal Entry with ID ${journalId} not found in the compendium.`);
            return;
        }

        const hasStartPageId = startPageId !== '';
        const hasEndPageId = endPageId !== '';

        let startProcessing = !hasStartPageId; // Start immediately if no start page ID is given
        let endProcessing = false;

        // Loop through each page of the journal
        for (let page of journalEntry.pages.contents) {
            // Skip pages until we reach the start page if startPageId is provided
            if (hasStartPageId && page.id === startPageId) {
                startProcessing = true;
            }

            // Process only if we are in the valid range
            if (startProcessing && !endProcessing) {
                const pageContent = page.text.content.trim(); // Content of the page

                // Create a DOM parser to turn the HTML into a traversable document
                const parser = new DOMParser();
                const doc = parser.parseFromString(pageContent, 'text/html');

                // Parse the content of the page
                let currentElement = doc.body.firstChild;

                // Loop through all elements of the page content
                while (currentElement) {
                    if (currentElement.tagName === 'P') {
                        const content = currentElement.textContent.trim();
                        // do things with content here
                    }

                    // Move to the next sibling
                    currentElement = currentElement.nextSibling;
                }

                // Mark the end of processing if this is the end page
                if (hasEndPageId && page.id === endPageId) {
                    endProcessing = true;
                }
            }
        }
    }

    // update an item in a compendium - modify as needed to anything
    async function updateSomethingInCompendium(
        compendiumName = `2e-complete-psionics-handbook.2e-complete-psionics-items`,
        type
    ) {
        // Get the compendium
        const compendium = game.packs.get(compendiumName);
        if (!compendium) {
            ui.notifications.error(`Compendium with name '${compendiumName}' not found.`);
            return;
        }

        // Unlock the compendium to make it editable
        if (compendium.locked) {
            await compendium.configure({ locked: false });
        }

        // Load all documents in the compendium
        const content = await compendium.getDocuments();

        // Filter items that are of type
        const obj = content.filter((item) => item.type === type);

        // If there are no powers, exit the function
        if (obj.length === 0) {
            console.log(`No items found in compendium '${compendiumName}'.`);
            return;
        }

        for (let power of obj) {
        }
    }

    // find token match by name - best match
    async function setActorTokenImages(
        imageDirectoryPath = `modules/adnd2e-mc-all/images/tokens/`,
        folder = null,
        ignoreWordsPattern = ''
    ) {
        const result = await FilePicker.browse('data', imageDirectoryPath);

        const findClosestMatch = (files, actorName, imageDirectoryPath) => {
            let bestMatch = null;
            let highestSimilarity = 0;

            files.forEach((file) => {
                let fileName = file
                    .replace(imageDirectoryPath, '') // Remove root directory path
                    .replace(/\.[^/.]+$/, '') // Remove file extension
                    .replace(ignoreWordsPattern, '') // Remove ignored words
                    .replace(/[^a-zA-Z]/g, '') // Remove non-letter characters
                    .toLowerCase(); // Convert to lowercase

                const normalizedActorName = actorName
                    .replace(ignoreWordsPattern, '')
                    .replace(/[^a-zA-Z]/g, '')
                    .toLowerCase();

                const similarity = longestSubstringMatchScore(normalizedActorName, fileName);

                if (similarity > highestSimilarity) {
                    highestSimilarity = similarity;
                    bestMatch = file;
                }
            });

            return bestMatch;
        };

        const longestSubstringMatchScore = (str1, str2) => {
            let longestMatchLength = 0;
            for (let i = 0; i < str1.length; i++) {
                for (let j = i + 1; j <= str1.length; j++) {
                    const substring = str1.slice(i, j);
                    if (str2.includes(substring) && substring.length > longestMatchLength) {
                        longestMatchLength = substring.length;
                    }
                }
            }
            return longestMatchLength;
        };

        if (result.files.length === 0) {
            ui.notifications.warn(`No images found in directory: ${imageDirectoryPath}`);
            return;
        }

        for (let actor of game.actors.contents) {
            if (folder && actor.folder?.name !== folder) {
                console.log(`Skipping actor ${actor.name} (not in folder ${folder})`);
                continue;
            }

            const actorName = actor.name;
            const bestMatch = findClosestMatch(result.files, actorName, imageDirectoryPath);

            if (bestMatch) {
                await actor.prototypeToken.update({
                    'texture.src': bestMatch,
                });

                console.log(`Setting image for ${actorName} to ${bestMatch}`);
            } else {
                console.warn(`No suitable image found for actor: ${actorName}`);
            }
        }
    }

    // Update properties of all actors in world
    async function updateActorProperties(optionalActorUpdates = {}) {
        // Loop through all actors
        for (let actor of game.actors.contents) {
            const img = actor.img; // Get the current portrait image

            // Default updates for the actor
            let actorUpdates = {
                img: img,
            };

            // Merge optionalActorUpdates only with actor updates
            actorUpdates = { ...actorUpdates, ...optionalActorUpdates };

            // Default update for the prototype token image
            let prototypeTokenUpdates = {
                'texture.src': img, // Use texture.src to update the token image
            };

            // Update the actor's portrait image and system details
            await actor.update(actorUpdates);

            // Update the actor's prototype token image
            await actor.prototypeToken.update(prototypeTokenUpdates);
        }
    }

    // delete everything in a specific compendium!!
    async function compendiumDeletion(compendiumName = '') {
        // Get the compendium pack
        const compendium = game.packs.get(compendiumName);
        if (!compendium) {
            return ui.notifications.error(`Compendium "${compendiumName}" not found.`);
        }

        // Load all documents in the compendium
        const documents = await compendium.getDocuments();

        for (const document of documents) {
            await document.delete();
            console.log(`Deleted document: ${document.name}`);
        }

        ui.notifications.info(`Deleted ${documents.length} documents from compendium ${compendiumName}.`);
    }

    // wrapper dialog for power builder
    function openPowersBuilderDialog() {
        new Dialog({
            title: 'Create Powers from Journal',
            content: `
                <form>
                    <div class="form-group">
                        <label>Journal ID:</label>
                        <input type="text" name="journalId" value="agAQF6hmObr1tkw2" />
                    </div>
                    <div class="form-group">
                        <label>Folder Name:</label>
                        <input type="text" name="folderName" value="Clairsentience" />
                    </div>
                    <div class="form-group">
                        <label>Discipline:</label>
                        <input type="text" name="discipline" value="clairsentience" /> <!-- Default value for discipline -->
                    </div>
                    <div class="form-group">
                        <label>Compendium ID:</label>
                        <input type="text" name="compendiumId" value="2e-complete-psionics-handbook.2e-complete-psionics-journals" placeholder="moduleName.packName" /> <!-- Compendium ID field -->
                    </div>
                    <div class="form-group">
                        <label>Start Page ID (Optional):</label>
                        <input type="text" name="startPageId" value="gtOOkI0UlwchA9KC" />
                    </div>
                    <div class="form-group">
                        <label>End Page ID (Optional):</label>
                        <input type="text" name="endPageId" value="0OINJX2ceOB3Jfnv" />
                    </div>
                </form>
            `,
            buttons: {
                create: {
                    label: 'Create Powers',
                    callback: (html) => {
                        const journalId = html.find('[name="journalId"]').val();
                        const folderName = html.find('[name="folderName"]').val();
                        const discipline = html.find('[name="discipline"]').val(); // Get the discipline value
                        const compendiumId = html.find('[name="compendiumId"]').val(); // Get the compendium ID value
                        const startPageId = html.find('[name="startPageId"]').val();
                        const endPageId = html.find('[name="endPageId"]').val();
                        createPowersFromJournal(journalId, folderName, discipline, compendiumId, startPageId, endPageId); // Pass compendiumId as a new parameter
                    },
                },
                cancel: {
                    label: 'Cancel',
                },
            },
            default: 'create',
        }).render(true);
    }

    // scan journal and create powers
    async function createPowersFromJournal(
        journalId,
        folderName = 'testing',
        discipline = 'psychometabolism',
        compendiumName = '2e-complete-psionics-handbook.2e-complete-psionics-journals', // Use the compendium name instead of a direct journal ID
        startPageId = '',
        endPageId = ''
    ) {
        // Get the folder where the items will be stored, or create it if it doesn't exist
        let folder = game.folders.find((f) => f.name === folderName && f.type === 'Item');
        if (!folder) {
            folder = await Folder.create({
                name: folderName,
                type: 'Item',
                parent: null,
            });
        }

        // see powers/scratchpad.js for data
        const lookup = window.lookup || {};

        console.log(
            '--------------------------------------------- create powers ---------------------------------------------'
        );

        // Load the compendium
        const compendium = game.packs.get(compendiumName);
        if (!compendium) {
            ui.notifications.error(`Compendium with name ${compendiumName} not found.`);
            return;
        }

        // Load the Journal Entry from the compendium
        const journalEntry = await compendium.getDocument(journalId);
        // const journalEntry = game.journal.get(journalId // world journal version
        if (!journalEntry) {
            ui.notifications.error(`Journal Entry with ID ${journalId} not found in the compendium.`);
            return;
        }

        const hasStartPageId = startPageId !== '';
        const hasEndPageId = endPageId !== '';

        let startProcessing = !hasStartPageId; // Start immediately if no start page ID is given
        let endProcessing = false;

        // Loop through each page of the journal
        for (let page of journalEntry.pages.contents) {
            // Skip pages until we reach the start page if startPageId is provided
            if (hasStartPageId && page.id === startPageId) {
                startProcessing = true;
            }

            // Process only if we are in the valid range
            if (startProcessing && !endProcessing) {
                const powerName = page.name.trim(); // Use the page title as the power name
                const pageContent = page.text.content.trim(); // Content of the page

                // Create a DOM parser to turn the HTML into a traversable document
                const parser = new DOMParser();
                const doc = parser.parseFromString(pageContent, 'text/html');

                const defaultData = {
                    discipline,
                };

                const powerData = {
                    name: powerName,
                    type: 'power',
                    img: 'icons/magic/perception/third-eye-blue-red.webp', // Default image path
                    data: defaultData,
                };

                // Check if the power name exists in the lookup and assign the image if found
                if (lookup[powerName]) {
                    powerData.img = lookup[powerName].img;
                }

                // Search for an existing world item and update icon/action groups if found
                const existingPower = game.items.find((i) => i.name === powerName && i.type === 'power');
                if (existingPower) {
                    // Copy the properties to powerData if needed
                    powerData.img = existingPower.img;
                    powerData.data.actionGroups = existingPower.system.actionGroups;
                    console.log(`Found existing power: ${powerName}. Using img: ${powerData.img}`);
                }

                let captureDescription = false; // Flag to start capturing description after 'Type' field

                // Parse the content of the page
                let currentElement = doc.body.firstChild;
                let descriptionHTML = ''; // For storing the description data
                let hasMaintain = false;

                // Loop through all elements of the page content
                while (currentElement) {
                    if (currentElement.tagName === 'P') {
                        const content = currentElement.textContent.trim();

                        if (!captureDescription) {
                            if (content.startsWith('Power Score:') && !powerData.data.ability) {
                                // Updated regex to handle space between sign and number (e.g., "- 5")
                                // Updated regex to handle both "-" and "–" (en dash), and any space between the sign and number
                                const match = content.match(/Power Score:\s*(\w+)\s*[–-]?\s*(\d+)?/i);

                                if (match) {
                                    powerData.data.ability = match[1].toLowerCase();

                                    // Clean up any spaces in the ability modifier (e.g., "– 5" or "- 5" -> "-5")
                                    if (match[2]) {
                                        powerData.data.abilityMod = parseInt(`-${match[2].replace(/\s+/g, '')}`, 10);
                                    } else {
                                        powerData.data.abilityMod = 0;
                                    }
                                }
                            }

                            if (content.startsWith('Initial Cost:') && !powerData.data.powercost) {
                                powerData.data.powercost = parseInt(content.replace('Initial Cost:', '').trim());
                                if (isNaN(powerData.data.powercost)) powerData.data.powercost = 0;
                            }

                            if (content.startsWith('Maintenance Cost:') && !powerData.data.maintenance) {
                                const maintenance = content.replace('Maintenance Cost:', '').trim().toLowerCase();
                                powerData.data.maintenance = maintenance;
                                if (!/^(0|na|n\/a|none)$/.test(maintenance)) {
                                    hasMaintain = true;
                                }
                            }

                            if (content.startsWith('Range:') && !powerData.data.range) {
                                powerData.data.range = content.replace('Range:', '').trim().toLowerCase();
                            }

                            if (content.startsWith('Preparation Time:') && !powerData.data.prepTime) {
                                powerData.data.prepTime = content.replace('Preparation Time:', '').trim().toLowerCase();
                            }

                            if (content.startsWith('Area of Effect:') && !powerData.data.areaOfEffect) {
                                powerData.data.areaOfEffect = content.replace('Area of Effect:', '').trim().toLowerCase();
                            }

                            if (content.startsWith('Prerequisite:') && !powerData.data.prerequisites) {
                                powerData.data.prerequisites = content.replace('Prerequisite:', '').trim().toLowerCase();
                            }

                            if (content.startsWith('Type:') && !powerData.data.powerCategory) {
                                powerData.data.powerCategory = content.replace('Type:', '').trim().toLowerCase();
                                // Once 'Type' is found, start capturing the description
                                captureDescription = true;
                            }
                        }
                    }

                    if (captureDescription) {
                        // Append all remaining content as part of the description
                        descriptionHTML += currentElement.outerHTML;
                    }

                    // Move to the next sibling
                    currentElement = currentElement.nextSibling;
                }

                // Use the current page to construct the @Embed reference dynamically
                const embedCode = `@Embed[Compendium.${compendiumName}.JournalEntry.${journalId}.JournalEntryPage.${page.id}]{ }`;

                // Set the description to be the @Embed reference
                powerData.data.description = embedCode;

                let changes = [];
                changes.push({
                    key: 'special.status',
                    mode: 0,
                    value: powerData.name,
                });

                if (hasMaintain) {
                    let maintenance = powerData.data.maintenance;

                    // Extract number from maintenance (if any)
                    const diceMatch = maintenance.match(/\b\d+d\d+\b/);
                    const numberMatch = maintenance.match(/\d+/);
                    let formula = numberMatch ? parseInt(numberMatch[0], 10) : 0;
                    if (diceMatch) formula = diceMatch;

                    formula = formula + '';

                    // Set cycle based on keywords in the maintenance string
                    let cycle = 'round';
                    if (maintenance.includes('day')) {
                        cycle = 'day';
                    } else if (maintenance.includes('hour')) {
                        cycle = 'hour';
                    } else if (maintenance.includes('round')) {
                        cycle = 'round';
                    } else if (maintenance.includes('turn')) {
                        cycle = 'turn';
                    }

                    changes.push({
                        key: 'special.ongoing',
                        mode: 0,
                        value: JSON.stringify({
                            type: 'pspmaintain',
                            rate: '1',
                            cycle: cycle,
                            dmgType: '',
                            formula: formula,
                        }),
                    });
                }

                if (!powerData.data.actionGroups) {
                    powerData.data.actionGroups = [
                        {
                            name: powerData.name,
                            description: '',
                            actions: [
                                {
                                    sort: 0,
                                    name: powerData.name,
                                    type: 'cast',
                                    saveCheck: {
                                        formula: '',
                                        type: 'none',
                                    },
                                    successAction: 'none',
                                },
                                {
                                    sort: 1,
                                    name: powerData.name,
                                    type: 'effect',
                                    targeting: '',
                                    successAction: '',
                                    formula: '',
                                    otherdmg: [],
                                    effectList: [],
                                    effect: {
                                        duration: {
                                            formula: '',
                                            type: 'round',
                                        },
                                        changes: changes,
                                        system: {
                                            applyself: true,
                                            visibility: 'default',
                                        },
                                    },
                                },
                            ],
                        },
                    ];
                }

                // Create the item in Foundry VTT inside the folder
                const name = powerData.name.toLowerCase();

                if (!/devotions|sciences|chapter/.test(name)) {
                    const attackModes = ['ego whip', 'id insinuation', 'mind thrust', 'psionic blast', 'psychic crush'];
                    const defenseModes = [
                        'intellect fortress',
                        'mental barrier',
                        'mind blank',
                        'thought shield',
                        'tower of iron will',
                    ];

                    const attackMatch = attackModes.find((mode) => name.includes(mode));
                    const defenseMatch = defenseModes.find((mode) => name.includes(mode));

                    if (attackMatch) {
                        powerData.data.isAttackMode = true;
                        powerData.data.atkDefType = attackMatch.replace(/\s+/g, ''); // Set to the matched attack mode
                    } else if (defenseMatch) {
                        powerData.data.isDefenseMode = true;
                        powerData.data.atkDefType = defenseMatch.replace(/\s+/g, ''); // Set to the matched defense mode
                    }

                    const newPower = await Item.create({
                        name: powerData.name,
                        type: 'power',
                        img: powerData.img, // Set image based on matching item or default
                        hasDescription: true,
                        system: powerData.data,
                        folder: folder.id, // Store the item inside the folder
                    });
                }

                // Mark the end of processing if this is the end page
                if (hasEndPageId && page.id === endPageId) {
                    endProcessing = true;
                }
            }
        }

        // console.log(lookup);
    }

    // update all powers for the complete psionics
    function updateAllPowers() {
        window.createPowersFromJournal(
            'agAQF6hmObr1tkw2',
            'Clairsentience',
            'clairsentience',
            '2e-complete-psionics-handbook.2e-complete-psionics-journals',
            'gtOOkI0UlwchA9KC',
            '0OINJX2ceOB3Jfnv'
        );

        window.createPowersFromJournal(
            'agAQF6hmObr1tkw2',
            'Psychokinesis',
            'psychokinesis',
            '2e-complete-psionics-handbook.2e-complete-psionics-journals',
            '0OINJX2ceOB3Jfnv',
            '7uVcA3JiEzVEUacp'
        );

        window.createPowersFromJournal(
            'agAQF6hmObr1tkw2',
            'Psychometabolism',
            'psychometabolism',
            '2e-complete-psionics-handbook.2e-complete-psionics-journals',
            '7uVcA3JiEzVEUacp',
            'eUkH8RoalU7G7dot'
        );

        window.createPowersFromJournal(
            'agAQF6hmObr1tkw2',
            'Psychoportation',
            'psychoportation',
            '2e-complete-psionics-handbook.2e-complete-psionics-journals',
            'eUkH8RoalU7G7dot',
            'Sk4SGkbR9mI2mSwg'
        );

        window.createPowersFromJournal(
            'agAQF6hmObr1tkw2',
            'Telepathy',
            'telepathy',
            '2e-complete-psionics-handbook.2e-complete-psionics-journals',
            'Sk4SGkbR9mI2mSwg',
            'z1mRmyEg7HDcTl6F'
        );

        window.createPowersFromJournal(
            'agAQF6hmObr1tkw2',
            'Metapsionics',
            'metapsionics',
            '2e-complete-psionics-handbook.2e-complete-psionics-journals',
            'z1mRmyEg7HDcTl6F',
            'Z11NCUXzkmgJmSo3'
        );
    }

    // used for cph - generates a beautiful summary table for all powers
    async function generatePowerSummary() {
        window.powerSummaryHtmlOutput = ''; // Initialize the global variable to store the HTML output

        // Load the compendium
        let compendium = game.packs.get('2e-complete-psionics-handbook.2e-complete-psionics-items'); // Use the actual compendium name
        if (!compendium) {
            ui.notifications.error('Compendium not found');
            return;
        }

        // Load all items from the compendium
        let items = await compendium.getDocuments();

        // Filter items that are of type 'power'
        let powers = items.filter((item) => item.type === 'power');

        // Group powers by discipline and by sciences/devotions
        let groupedPowers = powers.reduce((grouped, power) => {
            let discipline = power.system.discipline || 'Unknown Discipline'; // Replace with your field name
            let category = power.system.powerCategory || 'devotion'; // Assume devotions if not specified

            // Initialize the discipline object if it doesn't exist
            if (!grouped[discipline]) {
                grouped[discipline] = { sciences: [], devotions: [] };
            }

            // Add power to the appropriate category
            if (category === 'science') {
                grouped[discipline].sciences.push(power);
            } else {
                grouped[discipline].devotions.push(power);
            }

            return grouped;
        }, {});

        // Generate HTML structure, sorting disciplines alphabetically
        let html = '';

        Object.keys(groupedPowers)
            .sort() // Sort disciplines alphabetically
            .forEach((discipline) => {
                // Add discipline title
                html += `<br><h2>${discipline.toUpperCase()}</h2>`;

                // Sort sciences and devotions by name within each discipline
                if (groupedPowers[discipline].sciences.length > 0) {
                    groupedPowers[discipline].sciences.sort((a, b) => a.name.localeCompare(b.name));
                    html += `<h3>Sciences</h3>`;
                    html += buildTable(groupedPowers[discipline].sciences);
                }

                if (groupedPowers[discipline].devotions.length > 0) {
                    groupedPowers[discipline].devotions.sort((a, b) => a.name.localeCompare(b.name));
                    html += `<h3>Devotions</h3>`;
                    html += buildTable(groupedPowers[discipline].devotions);
                }
            });

        // Store the complete HTML in the global variable for external use
        window.powerSummaryHtmlOutput = `${html}`;

        // Send the HTML to chat
        ChatMessage.create({
            content: window.powerSummaryHtmlOutput,
            speaker: ChatMessage.getSpeaker(),
        });

        // Helper function to build a table for a group of powers
        function buildTable(powers) {
            let tableHTML = `<table border="1">
                 <tr>
                     <th>Power</th>
                     <th>Score</th>
                     <th>Cost</th>
                     <th>Maintain</th>
                     <th>Range</th>
                     <th>Prep</th>
                     <th>Area of Effect</th>
                 </tr>`;

            powers.forEach((power) => {
                const powerScore = `${power.system.ability} ${power.system.abilityMod}`;
                tableHTML += `<tr>
                     <td>${power.name}</td>
                     <td>${powerScore}</td>
                     <td>${power.system.powercost}</td>
                     <td>${power.system.maintenance || 'n/a'}</td>
                     <td>${power.system.range || 'n/a'}</td>
                     <td>${power.system.prepTime || 'n/a'}</td>
                     <td>${power.system.areaOfEffect || 'n/a'}</td>
                 </tr>`;
            });

            tableHTML += `</table>`;

            // Append the generated table to the global HTML output
            window.powerSummaryHtmlOutput += tableHTML;

            return tableHTML;
        }
    }

    // dialog wrapper for page builder
    function openCompendiumBuilder() {
        new Dialog({
            title: 'Format Compendium',
            content: `
          <form>
            <div class="form-group">
              <label for="journal-id">Enter Journal ID:</label>
              <input type="text" id="journal-id" name="journal-id" placeholder="Enter Journal Entry ID">
            </div>
            <div class="form-group">
              <label for="start-page-id">Enter Page ID (Optional):</label>
              <input type="text" id="start-page-id" name="start-page-id" placeholder="Enter Page ID">
            </div>
            <div class="form-group">
              <label for="only-current-page">Process Only This Page:</label>
              <input type="checkbox" id="only-current-page" name="only-current-page">
            </div>
            <div class="form-group">
              <label for="delete-original-page">Delete Original Page After Processing:</label>
              <input type="checkbox" id="delete-original-page" name="delete-original-page">
            </div>
          </form>
        `,
            buttons: {
                ok: {
                    label: 'Format Compendium',
                    callback: async (html) => {
                        // Get the entered journal ID, starting page ID, and checkbox values
                        let journalId = html.find('[name="journal-id"]').val().trim();
                        let startPageId = html.find('[name="start-page-id"]').val().trim();
                        let onlyCurrentPage = html.find('[name="only-current-page"]').is(':checked');
                        let deleteOriginalPage = html.find('[name="delete-original-page"]').is(':checked');

                        // Call the method to process the journal
                        createPagesFromJournal(journalId, startPageId, onlyCurrentPage, deleteOriginalPage);
                    },
                },
                cancel: {
                    label: 'Cancel',
                },
            },
            default: 'ok',
        }).render(true);
    }

    // make individual pages from a standard journal header style - supports ranges to scan/create and other feaures
    async function createPagesFromJournal(journalId, startPageId = '', onlyCurrentPage = false, deleteOriginalPage = false) {
        // Get the journal entry by its ID
        let journal = game.journal.get(journalId);

        if (!journal) {
            ui.notifications.error(`Journal Entry with ID ${journalId} not found.`);
            return;
        }

        const hasPageId = startPageId !== '';
        let startProcessing = hasPageId ? false : true; // Start immediately if no start page ID is given

        // Initialize sort increment
        let sortIncrement = 100; // Adjust as needed for spacing between pages
        let currentSort = journal.pages.contents[journal.pages.contents.length - 1]?.sort || 0; // Start after the last page if no starting sort is provided

        // Loop over each existing page in the journal
        for (let page of journal.pages.contents) {
            if (hasPageId && page.id === startPageId) {
                startProcessing = true;
            }

            // Skip processing until we reach the start page
            if (!startProcessing) continue;

            // Increment sort for the new parent page
            currentSort += sortIncrement;

            // Create a new page for the current journal page with level 1 title
            const currentPageTitle = page.name;
            const newParentPage = await journal.createEmbeddedDocuments('JournalEntryPage', [
                {
                    name: currentPageTitle,
                    title: { show: true, level: 1 },
                    text: { content: '' }, // No content for the main chapter page, subpages will hold the content
                    type: 'text',
                    sort: currentSort, // Assign a sort value
                },
            ]);

            console.log(`Created new parent page: ${currentPageTitle} (Level 1)`);

            // Get the content of the page
            let content = page.text.content || '';

            // Regex to find all H1, H2, H3 and the content until the next tag
            let sections = content.split(/(<h1[^>]*>.*?<\/h1>|<h2[^>]*>.*?<\/h2>)/g);

            let currentLevel = 2; // Start creating subpages at level 2 (for H2)

            // Iterate over the sections found by the split
            for (let i = 0; i < sections.length; i++) {
                let section = sections[i];

                // Check if the section is an H1, H2, or H3
                if (section.startsWith('<h1') || section.startsWith('<h2') || section.startsWith('<h3')) {
                    // Extract the title from the H1/H2/H3 tag, stripping out any attributes or inline styles
                    let title = section.replace(/<\/?h[123][^>]*>/g, '').trim();

                    // Remove any remaining HTML tags from the title
                    title = title.replace(/<[^>]+>/g, '').trim();

                    // Set the level based on whether it's an H1, H2, or H3
                    if (section.startsWith('<h1')) {
                        currentLevel = 2; // Reset to level 2 when an H1 is encountered
                    } else if (section.startsWith('<h2')) {
                        currentLevel = 2; // Level 2 for H2
                    } else if (section.startsWith('<h3')) {
                        currentLevel = 3; // Level 3 for H3
                    }

                    // The next section will be the content for this title (i+1)
                    let pageContent = sections[i + 1] || ''; // Get the content after the heading or empty if none

                    // Increment sort for the subpage
                    currentSort += sortIncrement;

                    // Create a new subpage with title and content at the correct level (2 for H2, 3 for H3)
                    await journal.createEmbeddedDocuments('JournalEntryPage', [
                        {
                            name: title,
                            title: { show: true, level: currentLevel },
                            text: { content: pageContent },
                            type: 'text',
                            parent: newParentPage[0].id, // Assign this subpage to the parent page
                            sort: currentSort, // Assign a sort value
                        },
                    ]);

                    console.log(`Created subpage with title: ${title} (Level ${currentLevel})`);
                }
            }

            // If "Only Process Current Page" is checked, break after processing the current page
            if (onlyCurrentPage) {
                if (deleteOriginalPage) {
                    await page.delete(); // Delete the original page
                    console.log(`Deleted original page: ${page.name}`);
                }
                break;
            }

            // If "Delete Original Page After Processing" is checked, delete the original page after processing
            if (deleteOriginalPage) {
                await page.delete(); // Delete the original page
                console.log(`Deleted original page: ${page.name}`);
            }
        }
    }

    // Open a dialog to collect inputs for the folder path, journal ID, actor folder name, and compendium names
    function npcImporterDialog() {
        new Dialog({
            title: 'Import Monsters',
            content: `
        <form>
            <div class="form-group">
                <label>Module Name:</label>
                <input type="text" name="moduleName" placeholder="Enter module name" value="adnd2e-mc-all">
            </div>
            <div class="form-group">
                <label>Folder Path (e.g., modules/your-module-name/monsters):</label>
                <input type="text" name="path" placeholder="Enter folder path" value="modules/adnd2e-mc-all/monsters">
            </div>
            <div class="form-group">
                <label>Journal Compendium Name:</label>
                <input type="text" name="journalCompendium" placeholder="Enter Journal Compendium Name" value="adnd2e-mc-all.journals">
            </div>
            <div class="form-group">
                <label>Journal Entry ID (optional):</label>
                <input type="text" name="journalId" placeholder="Enter Journal ID">
            </div>
            <div class="form-group">
                <label>Actor Compendium Name:</label>
                <input type="text" name="actorCompendium" placeholder="Enter Actor Compendium Name" value="adnd2e-mc-all.actors">
            </div>
            <div class="form-group">
                <label>Folder Name for Actors:</label>
                <input type="text" name="folderName" placeholder="Enter Actor Folder Name" value="Actors Folder from Automation">
            </div>
        </form>
        `,
            buttons: {
                import: {
                    label: 'Import',
                    callback: async (html) => {
                        const moduleName = html.find('input[name="moduleName"]').val();
                        const folderPath = html.find('input[name="path"]').val();
                        const journalCompendium = html.find('input[name="journalCompendium"]').val();
                        const journalId = html.find('input[name="journalId"]').val();
                        const actorCompendium = html.find('input[name="actorCompendium"]').val();
                        const folderName = html.find('input[name="folderName"]').val();

                        if (!moduleName || !folderPath || !journalCompendium || !actorCompendium || !folderName) {
                            return ui.notifications.warn('All fields are required.');
                        }

                        // Start the import process with the provided folder name
                        await importMonsters(folderPath, folderName, journalCompendium, journalId, actorCompendium, moduleName);
                        ui.notifications.info(`*** Import completed ***`, { permanent: true });
                    },
                },
                cancel: { label: 'Cancel' },
            },
        }).render(true);
    }

    // Recursive function to browse subdirectories and collect HTML files
    async function browseSubdirectories(basePath) {
        const htmlFiles = [];
        const directory = await FilePicker.browse('data', basePath);

        for (const entry of directory.dirs) {
            const subFiles = await browseSubdirectories(entry);
            htmlFiles.push(...subFiles);
        }

        const filesInDir = directory.files.filter((file) => file.endsWith('.html'));
        htmlFiles.push(...filesInDir);

        return htmlFiles;
    }

    // Main function to import monsters
    async function importMonsters(basePath, folderName, journalCompendium, journalId, actorCompendium, moduleName) {
        function extractAndFormatCompendiumName(input) {
            const parts = input.split('/'); // Split by "/"

            // Ensure there are at least 4 parts in the path
            if (parts.length < 4) return 'Imported-Monsters';

            // Get 3rd and 4th from the end
            let first = parts.at(-4).toUpperCase(); // Convert to uppercase
            let second = parts.at(-3); // Keep as-is

            return `${first}-${second}`;
        }

        const htmlFiles = await browseSubdirectories(basePath);

        if (htmlFiles.length === 0) {
            return ui.notifications.warn('No HTML files found in the specified folder structure.');
        }

        ui.notifications.info('Scanning for index.html files...');
        const npcArray = [];
        for (const file of htmlFiles) {
            //get the name of the parent folder this MC is located to use later.
            const actorSourceCompendiumName = extractAndFormatCompendiumName(file);
            // file.split('/').slice(-4, -3)[0] || 'Imported Monsters';
            const response = await fetch(file);
            const htmlContent = await response.text();
            const parsedMonsters = extractMonsters(htmlContent, moduleName, actorSourceCompendiumName); // Pass moduleName for image path setup
            npcArray.push(...parsedMonsters);
        }

        // Call the NPC creation function with parsed NPC array and target compendiums
        ui.notifications.info('Processing data found...');
        await createNPCsFromObjects(npcArray, folderName, journalCompendium, journalId, actorCompendium, moduleName);
    }

    // Process HTML content with specified modifications
    function processHtmlSection(htmlContent, moduleName) {
        const $content = $('<div>').html(htmlContent);

        const startElement = $content.find(`div[class*="monsterpage-module--top-header-"]`).first();
        const mainImage = $content.find('[class*="monsterpage-module--monster-img-frame"]');
        $content.find('[class]').removeAttr('class');

        startElement.addClass('npc-source-banner');
        mainImage.addClass('mc-npc-main-img');
        startElement.find('h1').remove();

        // Update image paths to point to the module's asset directory
        $content.find('img').each((_, img) => {
            const src = $(img).attr('src');
            const fileName = src.split('/').pop();
            const newSrc = `modules/${moduleName}/images/${fileName}`;
            $(img).attr('src', newSrc);
        });

        // Remove unnecessary elements and attributes
        $content.find('a > img').unwrap();
        $content.find('a, hr').remove();
        $content.find('[name], [id]').removeAttr('name id');
        $content.find('th').css('text-align', 'left');
        $content.find('h1, h2, h3, h4, h5, h6').before('<p></p>');
        $content.find('table').addClass('mc-statblock-table');

        // Apply custom classes to table rows based on header content
        $content.find('table tr').each((_, row) => {
            const label = $(row)
                .find('th')
                .text()
                .toLowerCase()
                .replace(/[^a-z]/g, '');
            if (label) $(row).addClass(label);
        });

        return $content[0].outerHTML;
    }

    // Parse stats from the table; handle single or multiple columns
    function parseStatsTable(tableHtml, actorName, portrait) {
        const $table = $('<div>').html(tableHtml).find('table');
        const headers = $table
            .find('tr:first-child th')
            .slice(1)
            .map((_, th) => $(th).text().trim())
            .get();
        const isMultiColumn = headers.length > 0;
        $table.addClass('mc-statblock-table');
        const statsArray = [];
        if (isMultiColumn) {
            headers.forEach((headerName, index) => {
                const stats = { name: headerName, portrait: portrait };
                $table.find('tr').each((_, row) => {
                    const statLabel = $(row).find('th').text().trim();
                    const statValue = $(row).find('td').eq(index).text().trim();
                    if (statLabel && statValue) {
                        stats[statLabel.toLowerCase().replace(/[^a-z]/g, '')] = statValue;
                    }
                });
                statsArray.push(stats);
            });
        } else {
            const stats = { portrait: portrait };
            $table.find('tr').each((_, row) => {
                const statLabel = $(row).find('th').text().trim();
                const statValue = $(row).find('td').text().trim();
                if (statLabel && statValue) {
                    stats[statLabel.toLowerCase().replace(/[^a-z]/g, '')] = statValue;
                }
            });
            statsArray.push(stats);
        }

        return [{ name: actorName, stats: statsArray }];
    }

    // Extract monster information from HTML content
    function extractMonsters(htmlContent, moduleName, actorSourceCompendiumName) {
        const $content = $('<div>').html(htmlContent);
        const startElement = $content.find(`div[class*="monsterpage-module--top-header-"]`).first();
        const mainImage = $content.find('[class*="monsterpage-module--monster-img-frame"]');
        const actorName = startElement.find('h1').text().trim() || 'Unknown Actor';

        // if (actorName === 'Unknown Actor') debugger;

        // Get the HTML content from the specified start element to the end
        const extractedHtmlElements = startElement.nextUntil(`div[class*="monsterpage-module--source-list-"]`).addBack();
        const extractedHtml = extractedHtmlElements
            .map((_, el) => $(el).prop('outerHTML'))
            .get()
            .join('');

        const processedHtml = processHtmlSection(extractedHtml, moduleName);
        const table = $(processedHtml).find('table').first();
        let portrait =
            $(processedHtml).find('.mc-npc-main-img img').attr('src') || $(processedHtml).find('img').eq(1).attr('src');

        if (portrait?.includes('.html')) portrait = $(processedHtml).find('img:not(.mc-npc-main-img img)').attr('src');

        // replace .gif with a .webp of the file.
        portrait = portrait?.replace(/\.gif$/, '.webp') ?? portrait;

        // Parse the table or create a single NPC object if no table is found
        const npcArray = table.length
            ? parseStatsTable(table.prop('outerHTML'), actorName, portrait)
            : [{ name: actorName, page: processedHtml, stats: [{}] }];

        // Replace dice formulas with [[/roll N]] pattern
        const diceFormulaRegex = /\b(\d+d\d+(\+\d+)?)\b/g;
        let page = $(processedHtml);
        // replace image hrefs with .gif with .webp
        page.html((_, html) => html.replace(/\.gif/g, '.webp'));
        page.html((_, html) => html.replace(diceFormulaRegex, '[[/roll $1]]'));
        page = page[0].outerHTML;

        return npcArray.map((npc) => ({
            actorname: npc.name,
            page: page,
            stats: npc.stats,
            actorSourceCompendiumName,
        }));
    }

    // Modify the `createNPCsFromObjects` function to target compendiums with folder organization
    async function createNPCsFromObjects(npcArray, folderName, journalCompendium, journalId, actorCompendium, moduleName) {
        function getAlignmentShorthand(alignmentString) {
            const alignments = {
                'lawful good': 'lg',
                'lawful neutral': 'ln',
                'lawful evil': 'le',
                'neutral good': 'ng',
                'neutral evil': 'ne',
                neutral: 'n',
                'chaotic good': 'cg',
                'chaotic neutral': 'cn',
                'chaotic evil': 'ce',
            };
            alignmentString = alignmentString?.toLowerCase();
            for (const [alignment, shorthand] of Object.entries(alignments)) {
                if (alignmentString?.includes(alignment)) return shorthand;
            }
            return 'n';
        }

        function getSize(sizenote = '') {
            let size = 'medium';
            if (sizenote.includes('T (')) {
                size = 'tiny';
            } else if (sizenote.includes('S (')) {
                size = 'small';
            } else if (sizenote.includes('M (')) {
                size = 'medium';
            } else if (sizenote.includes('L (')) {
                size = 'large';
            } else if (sizenote.includes('H (')) {
                size = 'huge';
            } else if (sizenote.includes('G (')) {
                size = 'gargantuan';
            }
            return size;
        }

        /**
         *
         * find a compendium pack by name, make it if it doesnt exist, return it
         *
         * @param {*} moduleName
         * @param {*} compendiumName
         * @param {*} myType
         * @returns
         */
        async function getComp(moduleName, compendiumName, myType) {
            const label = `${compendiumName.toUpperCase()}-${myType.toUpperCase()}`;
            let pack = game.packs.find((p) => p.metadata.name === label);
            if (!pack) {
                console.warn(`Creating pack ${label}`);
                pack = await CompendiumCollection.createCompendium({
                    label,
                    name: label,
                    package: moduleName,
                    type: myType,
                });
            }
            return pack;
        }
        /**
         *
         * find a journal Entry for this pack, or make it and return it
         *
         * @param {*} journalPack
         * @returns
         */
        async function getJournalEntry(mcName, journalPack) {
            // Attempt to get the Journal Entry from the compendium
            const name = `${mcName} Creatures`;
            let journalEntry = journalPack.index.getName(name);

            if (!journalEntry) {
                // Create the Journal Entry if it doesn't exist
                journalEntry = await JournalEntry.create({ name: name }, { pack: journalPack.metadata.id });

                // Since `create` returns a full document, we should re-index it
                await journalPack.getIndex();
            } else {
                // Load the full document from the compendium
                journalEntry = await journalPack.getDocument(journalEntry._id);
            }

            // Return the found or created Journal Entry
            return journalEntry;
        }

        // const journalPack = game.packs.get(journalCompendium);
        // const actorPack = game.packs.get(actorCompendium);

        // if (!journalPack) {
        //     ui.notifications.error(`Journal Compendium "${journalCompendium}" not found.`);
        //     return;
        // }
        // if (!actorPack) {
        //     ui.notifications.error(`Actor Compendium "${actorCompendium}" not found.`);
        //     return;
        // }

        // Try to get the existing Journal Entry by ID
        // let journalEntry;
        // if (journalId) {
        //     journalEntry = await journalPack.getDocument(journalId);
        // }

        // If Journal Entry doesn't exist, create a new one in the compendium
        // if (!journalEntry) {
        //     journalEntry = await JournalEntry.create({ name: 'Imported Monsters' }, { pack: journalCompendium });
        // }

        // let folder = game.folders.find((f) => f.name === folderName && f.type === 'Actor');
        // if (!folder) {
        //     folder = await Folder.create({ name: folderName, type: 'Actor', parent: null });
        // }

        // Sort by the bundle name so we can add a prompt to maybe deal with OOM
        npcArray.sort((a, b) =>
            a.actorSourceCompendiumName.toLowerCase().localeCompare(b.actorSourceCompendiumName.toLowerCase())
        );
        let lastBundleName = '';
        for (const npcObject of npcArray) {
            const { actorname, page, stats = [] } = npcObject;

            const newOne = npcObject.actorSourceCompendiumName.toLowerCase().split('-')[0];
            if (newOne != lastBundleName) {
                // await dialogManager.confirm(`Now saving ${newOne}`, 'Processing...');
                lastBundleName = newOne;
                ui.notifications.info(`Importing entries for ${newOne}...`);
            }
            // use a specific pack for the folder ID imported
            const journalPack = await getComp(moduleName, npcObject.actorSourceCompendiumName, 'JournalEntry');
            // retrieve and existing journalEntry if one exists or make it
            const journalEntry = await getJournalEntry(npcObject.actorSourceCompendiumName, journalPack);

            //
            const createdPage = await JournalEntryPage.create(
                [
                    {
                        name: actorname,
                        text: { content: page, format: 1 }, // Format 1 = HTML
                        type: 'text', // Default page type
                    },
                ],
                { parent: journalEntry } // ✅ Correctly setting the parent
            );
            // // Create a Journal Page in the compendium
            // let newPage = await journalEntry.createEmbeddedDocuments('JournalEntryPage', [
            //     { name: actorname, type: 'text', text: { content: page, format: 1 } },
            // ]);
            let newPage = createdPage[0];

            for (const stat of stats) {
                const alignment = getAlignmentShorthand(stat.alignment || '');
                const size = getSize(stat.size);
                const name = stat.name ? `${actorname}, ${stat.name}` : `${actorname}`;

                const nameSanityCheck = name.toLowerCase();
                if (nameSanityCheck.includes('general information') || nameSanityCheck.includes('unknown')) continue;

                // Create a description embed
                const embedCode = `@Embed[Compendium.${journalPack.collection}.JournalEntry.${journalEntry.id}.JournalEntryPage.${newPage.id}]{ }`;

                const npcData = {
                    name: name,
                    type: 'npc',
                    img: stat.portrait,
                    // folder: folder.id,
                    system: {
                        armorClass: { value: parseInt(stat.armorclass) || 10 },
                        attributes: {
                            ac: { value: parseInt(stat.armorclass) || 10 },
                            hp: { max: 0, value: 0 },
                            movement: { text: stat.movement },
                            thaco: { value: parseInt(stat.thac) || 20 },
                            size: size,
                        },
                        climate: stat.climateterrain || 'Unknown',
                        frequency: stat.frequency || 'Unknown',
                        organization: stat.organization || 'Unknown',
                        activity: stat.activitycycle || 'Unknown',
                        diet: stat.diet || 'Unknown',
                        intelligence: stat.intelligence || 'Unknown',
                        treasureType: stat.treasure || 'None',
                        numberAppearing: stat.noappearing || '1',
                        numberAttacks: parseInt(stat.noofattacks) || 1,
                        damage: stat.damageattack || '1d6',
                        specialAttacks: stat.specialattacks || 'None',
                        specialDefenses: stat.specialdefenses || 'None',
                        sizenote: stat.size || '',
                        magicresist: stat.magicresistance || 'None',
                        morale: stat.morale || 'Steady',
                        movement: stat.movement,
                        xp: { value: parseInt(stat.xpvalue?.replace(/,/g, '')) || 0 },
                        hitdice: stat.hitdice || '1',
                        hpCalculation: '',
                        details: {
                            alignment: alignment,
                            source: '',
                            biography: { value: embedCode, public: true },
                        },
                    },
                };

                // Save the NPC Actor in the specified compendium pack with the folder name
                // console.log(`Adding to compendium:${npcObject.actorSourceCompendiumName}`);
                const actorPack = await getComp(moduleName, npcObject.actorSourceCompendiumName, 'Actor');
                await Actor.create(npcData, { pack: actorPack.metadata.id });
            }
        }

        await setActorTokenImages(
            `modules/${moduleName}/images/tokens/`,
            'Actors Folder from Automation',
            /\b(Greater|Lesser|Black|Red|Blue|Tanar'ri|True|Yugoloth|Aasimon|Baatezu|Least|Minor|Major|True)\b/gi
        );
    }

    return {
        createEverythingSearchString,
        outputPackNames,
        pageDataFixer,
        updateSomethingInCompendium,
        setActorTokenImages,
        updateActorProperties,
        compendiumDeletion,
        generatePowerSummary,
        openPowersBuilderDialog,
        createPowersFromJournal,
        updateAllPowers,
        openCompendiumBuilder,
        createPagesFromJournal,
        npcImporterDialog,
    };
};
