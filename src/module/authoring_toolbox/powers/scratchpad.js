
// power icon dic. use with 
const lookup = {
    'Aura Sight': {
        img: 'icons/magic/control/silhouette-hold-change-blue.webp',
    },
    Clairaudience: {
        img: 'icons/magic/sonic/projectile-sound-rings-wave.webp',
    },
    Clairvoyance: {
        img: 'icons/magic/perception/third-eye-blue-red.webp',
    },
    'Object Reading': {
        img: 'icons/sundries/gaming/dice-runed-brown.webp',
    },
    Precognition: {
        img: 'icons/magic/time/arrows-circling-pink.webp',
    },
    'Sensitivity to Psychic Impressions': {
        img: 'icons/magic/time/day-night-sunset-sunrise.webp',
    },
    'All-Round Vision': {
        img: 'icons/creatures/eyes/humanoid-single-yellow.webp',
    },
    'Combat Mind': {
        img: 'icons/skills/melee/swords-triple-orange.webp',
    },
    'Danger Sense': {
        img: 'icons/magic/death/skeleton-worn-skull-tan.webp',
    },
    'Feel Light': {
        img: 'icons/magic/light/hand-sparks-smoke-teal.webp',
    },
    'Feel Sound': {
        img: 'icons/magic/sonic/explosion-shock-sound-wave.webp',
    },
    'Hear Light': {
        img: 'icons/magic/light/explosion-impact-purple.webp',
    },
    'Know Direction': {
        img: 'icons/tools/navigation/compass-chain-blue.webp',
    },
    'Know Location': {
        img: 'icons/tools/navigation/map-marked-green.webp',
    },
    'Poison Sense': {
        img: 'icons/consumables/potions/potion-labeled-poison-white.webp',
    },
    'Radial Navigation': {
        img: 'icons/tools/navigation/watch-simple-blue.webp',
    },
    'See Sound': {
        img: 'icons/magic/sonic/bell-alarm-red-purple.webp',
    },
    'Spirit Sense': {
        img: 'icons/magic/death/undead-ghost-scream-teal.webp',
    },
    'Create Object': {
        img: 'icons/magic/earth/projectile-stone-boulder-blue.webp',
    },
    Detonate: {
        img: 'icons/magic/fire/explosion-fireball-large-blue.webp',
    },
    Disintegrate: {
        img: 'icons/magic/unholy/beam-impact-purple.webp',
    },
    'Molecular Rearrangement': {
        img: 'icons/magic/water/orb-water-bubbles-blue.webp',
    },
    'Project Force': {
        img: 'icons/magic/sonic/projectile-shock-wave-blue.webp',
    },
    Telekinesis: {
        img: 'icons/magic/earth/projectile-boulder-debris.webp',
    },
    'Animate Object': {
        img: 'icons/containers/kitchenware/vase-clay-painted-brown-white.webp',
    },
    'Animate Shadow': {
        img: 'icons/magic/unholy/silhouette-evil-horned-giant.webp',
    },
    'Ballistic Attack': {
        img: 'icons/magic/earth/projectile-stone-bullet-pink.webp',
    },
    'Control Body': {
        img: 'icons/magic/control/control-influence-puppet.webp',
    },
    'Control Flames': {
        img: 'icons/magic/fire/elemental-creature-horse.webp',
    },
    'Control Light': {
        img: 'icons/magic/light/beam-rays-yellow-blue-small.webp',
    },
    'Control Sound': {
        img: 'icons/magic/sonic/explosion-impact-shock-wave.webp',
    },
    'Control Wind': {
        img: 'icons/magic/air/air-pressure-shield-blue.webp',
    },
    'Create Sound': {
        img: 'icons/magic/sonic/scream-wail-shout-teal.webp',
    },
    'Inertial Barrier': {
        img: 'icons/magic/defensive/barrier-shield-dome-blue-purple.webp',
    },
    Levitation: {
        img: 'icons/skills/movement/arrows-up-trio-red.webp',
    },
    'Molecular Agitation': {
        img: 'icons/magic/unholy/projectiles-binary-pink.webp',
    },
    'Molecular Manipulation': {
        img: 'icons/magic/unholy/projectiles-blood-salvo-red.webp',
    },
    Soften: {
        img: 'icons/commodities/materials/slime-thick-blue.webp',
    },
    'Animal Affinity': {
        img: 'icons/magic/nature/wolf-paw-glow-large-teal-blue.webp',
    },
    'Complete Healing': {
        img: 'icons/magic/life/cross-beam-green.webp',
    },
    'Death Field': {
        img: 'icons/magic/unholy/orb-glowing-purple.webp',
    },
    'Energy Containment': {
        img: 'icons/magic/light/explosion-star-glow-silhouette.webp',
    },
    'Life Draining': {
        img: 'icons/magic/unholy/energy-smoke-pink.webp',
    },
    Metamorphosis: {
        img: 'icons/creatures/abilities/lion-roar-yellow.webp',
    },
    'Shadow-form': {
        img: 'icons/magic/perception/silhouette-stealth-shadow.webp',
    },
    'Absorb Disease': {
        img: 'icons/skills/wounds/blood-cells-vessel-blue.webp',
    },
    'Adrenaline Control': {
        img: 'icons/magic/control/orb-web-hold.webp',
    },
    Aging: {
        img: 'icons/magic/death/hand-withered-gray.webp',
    },
    Biofeedback: {
        img: 'icons/commodities/biological/organ-heart-red.webp',
    },
    'Body Control': {
        img: 'icons/magic/control/buff-strength-muscle-damage-orange.webp',
    },
    'Body Equilibrium': {
        img: 'icons/magic/movement/abstract-ribbons-red-orange.webp',
    },
    'Body Weaponry': {
        img: 'icons/magic/unholy/hand-fire-skeleton-pink.webp',
    },
    Catfall: {
        img: 'icons/magic/movement/chevrons-down-yellow.webp',
    },
    'Cause Decay': {
        img: 'icons/magic/nature/plant-undersea-seaweed-glow-green.webp',
    },
    'Cell Adjustment': {
        img: 'icons/skills/wounds/blood-cells-vessel-red.webp',
    },
    'Chameleon Power': {
        img: 'icons/magic/nature/leaf-armor-scale-worn-green.webp',
    },
    'Chemical Simulation': {
        img: 'icons/skills/wounds/blood-cells-disease-green.webp',
    },
    Displacement: {
        img: 'icons/magic/defensive/illusion-evasion-echo-purple.webp',
    },
    'Double Pain': {
        img: 'icons/skills/wounds/injury-face-impact-orange.webp',
    },
    'Ectoplasmic Form': {
        img: 'icons/magic/death/undead-ghost-scream-teal.webp',
    },
    'Enhanced Strength': {
        img: 'icons/magic/control/debuff-chains-shackle-movement-red.webp',
    },
    Expansion: {
        img: 'icons/magic/control/encase-creature-monster-hold.webp',
    },
    'Flesh Armor': {
        img: 'icons/magic/defensive/armor-stone-skin.webp',
    },
    'Graft Weapon': {
        img: 'icons/skills/melee/strike-weapon-polearm-ice-blue.webp',
    },
    'Heightened Senses': {
        img: 'icons/magic/perception/eye-ringed-glow-angry-small-teal.webp',
    },
    Immovability: {
        img: 'icons/magic/defensive/shield-barrier-deflect-teal.webp',
    },
    'Lend Health': {
        img: 'icons/magic/life/heart-hand-gold-green-light.webp',
    },
    'Mind Over Body': {
        img: 'icons/magic/symbols/chevron-elipse-circle-blue.webp',
    },
    Reduction: {
        img: 'icons/magic/control/encase-creature-humanoid-hold.webp',
    },
    'Share Strength': {
        img: 'icons/magic/control/buff-flight-wings-runes-red-yellow.webp',
    },
    'Suspend Animation': {
        img: 'icons/magic/water/orb-ice-glow.webp',
    },
    Banishment: {
        img: 'icons/magic/unholy/orb-swirling-teal.webp',
    },
    'Probability Travel': {
        img: 'icons/magic/sonic/explosion-shock-wave-teal.webp',
    },
    'Summon Planar Creature': {
        img: 'icons/magic/unholy/strike-body-explode-disintegrate.webp',
    },
    Teleport: {
        img: 'icons/magic/light/explosion-star-glow-silhouette.webp',
    },
    'Teleport Other': {
        img: 'icons/magic/light/explosion-star-glow-silhouette.webp',
    },
    'Astral Projection': {
        img: 'icons/magic/symbols/triangle-glow-purple.webp',
    },
    'Dimensional Door': {
        img: 'icons/magic/light/beam-deflect-path-yellow.webp',
    },
    'Dimension Walk': {
        img: 'icons/magic/air/wind-vortex-swirl-purple.webp',
    },
    'Dream Travel': {
        img: 'icons/magic/perception/orb-crystal-ball-scrying-blue.webp',
    },
    'Teleport Trigger': {
        img: 'icons/magic/light/explosion-star-glow-silhouette.webp',
    },
    'Time Shift': {
        img: 'icons/magic/time/clock-spinning-gold-pink.webp',
    },
    'Time/Space Anchor': {
        img: 'icons/magic/time/clock-stopwatch-white-blue.webp',
    },
    Domination: {
        img: 'icons/magic/control/silhouette-hold-change-green.webp',
    },
    Ejection: {
        img: 'icons/magic/light/circle-window-clock-blue.webp',
    },
    'Fate Link': {
        img: 'icons/magic/time/arrows-circling-green.webp',
    },
    'Mass Domination': {
        img: 'icons/magic/control/hypnosis-mesmerism-pendulum.webp',
    },
    Mindlink: {
        img: 'icons/magic/perception/hand-eye-fire-blue.webp',
    },
    Mindwipe: {
        img: 'icons/magic/lightning/orb-ball-blue.webp',
    },
    Probe: {
        img: 'icons/magic/perception/mask-stone-eyes-orange.webp',
    },
    'Psionic Blast': {
        img: 'icons/magic/lightning/bolt-strike-beam-pink.webp',
    },
    'Superior Invisibility': {
        img: 'icons/magic/perception/shadow-stealth-eyes-purple.webp',
    },
    'Switch Personality': {
        img: 'icons/skills/social/diplomacy-handshake.webp',
    },
    'Tower of Iron Will': {
        img: 'icons/magic/lightning/barrier-shield-crackling-orb-pink.webp',
    },
    Attraction: {
        img: 'icons/magic/life/heart-glowing-red.webp',
    },
    Aversion: {
        img: 'icons/magic/death/mouth-teeth-fangs-vampire.webp',
    },
    Awe: {
        img: 'icons/magic/control/fear-fright-white.webp',
    },
    'Conceal Thoughts': {
        img: 'icons/magic/defensive/barrier-shield-dome-deflect-teal.webp',
    },
    Contact: {
        img: 'icons/magic/lightning/bolt-strike-explosion-blue.webp',
    },
    Daydream: {
        img: 'icons/magic/light/hand-sparks-glow-yellow.webp',
    },
    'Ego Whip': {
        img: 'icons/magic/light/beam-rays-magenta-large.webp',
    },
    Empathy: {
        img: 'icons/magic/light/beam-rays-orange-small.webp',
    },
    ESP: {
        img: 'icons/magic/perception/third-eye-blue-red.webp',
    },
    'False Sensory Input': {
        img: 'icons/magic/perception/hand-eye-pink.webp',
    },
    'Id Insinuation': {
        img: 'icons/magic/light/beams-rays-orange-purple.webp',
    },
    'Identity Penetration': {
        img: 'icons/magic/perception/eye-slit-red-orange.webp',
    },
    'Incarnation Awareness': {
        img: 'icons/magic/perception/eye-tendrils-web-purple.webp',
    },
    'Inflict Pain': {
        img: 'icons/magic/death/hand-undead-skeleton-fire-pink.webp',
    },
    'Intellect Fortress': {
        img: 'icons/magic/lightning/barrier-shield-orb-pink.webp',
    },
    'Invincible Foes': {
        img: 'icons/magic/perception/silhouette-stealth-shadow.webp',
    },
    Invisibility: {
        img: 'icons/magic/perception/shadow-stealth-eyes-purple.webp',
    },
    'Life Detection': {
        img: 'icons/magic/life/cross-explosion-burst-green.webp',
    },
    'Mental Barrier': {
        img: 'icons/magic/lightning/orb-ball-purple.webp',
    },
    'Mind Bar': {
        img: 'icons/magic/lightning/orb-ball-spiral-blue.webp',
    },
    'Mind Blank': {
        img: 'icons/magic/lightning/projectile-tendrils-teal.webp',
    },
    'Mind Thrust': {
        img: 'icons/magic/lightning/projectiles-tendril-salvo-pink.webp',
    },
    'Phobia Amplification': {
        img: 'icons/magic/death/skull-energy-light-purple.webp',
    },
    'Post-Hypnotic Suggestion': {
        img: 'icons/magic/perception/eye-ringed-glow-angry-teal.webp',
    },
    'Psychic Crush': {
        img: 'icons/magic/lightning/bolt-strike-embers-teal.webp',
    },
    'Psychic Impersonation': {
        img: 'icons/magic/air/air-smoke-casting.webp',
    },
    'Psychic Messenger': {
        img: 'icons/magic/sonic/projectile-sound-rings-wave.webp',
    },
    Repugnance: {
        img: 'icons/magic/death/projectile-skull-fire-orange-red.webp',
    },
    'Send Thoughts': {
        img: 'icons/magic/perception/orb-eye-scrying.webp',
    },
    'Sight Link': {
        img: 'icons/magic/perception/eye-ringed-green.webp',
    },
    'Sound Link': {
        img: 'icons/magic/sonic/explosion-impact-shock-wave.webp',
    },
    'Synaptic Static': {
        img: 'icons/magic/light/explosion-impact-purple.webp',
    },
    'Taste Link': {
        img: 'icons/consumables/food/soup-broth-bowl-wooden-yellow.webp',
    },
    'Telempathic Projection': {
        img: 'icons/magic/perception/orb-crystal-ball-scrying-blue.webp',
    },
    'Thought Shield': {
        img: 'icons/magic/lightning/bolt-strike-blue-white.webp',
    },
    Truthear: {
        img: 'icons/magic/perception/eye-slit-pink.webp',
    },
    Appraise: {
        img: 'icons/magic/perception/eye-slit-orange.webp',
    },
    'Aura Alteration': {
        img: 'icons/magic/control/sihouette-hold-beam-green.webp',
    },
    Empower: {
        img: 'icons/weapons/swords/sword-gold-holy.webp',
    },
    'Psychic Clone': {
        img: 'icons/magic/control/energy-stream-link-blue.webp',
    },
    'Psychic Surgery': {
        img: 'icons/skills/wounds/anatomy-organ-brain-pink-red.webp',
    },
    'Split Personality': {
        img: 'icons/magic/control/mouth-smile-deception-purple.webp',
    },
    Ultrablast: {
        img: 'icons/magic/sonic/explosion-shock-sound-wave.webp',
    },
    Cannibalize: {
        img: 'icons/magic/death/hand-withered-gray.webp',
    },
    Convergence: {
        img: 'icons/magic/perception/third-eye-blue-red.webp',
    },
    Enhancement: {
        img: 'icons/magic/light/orbs-smoke-pink.webp',
    },
    Gird: {
        img: 'icons/magic/symbols/ring-circle-smoke-blue.webp',
    },
    Intensify: {
        img: 'icons/magic/lightning/barrier-shield-crackling-orb-pink.webp',
    },
    Magnify: {
        img: 'icons/magic/lightning/orb-ball-spiral-blue.webp',
    },
    'Martial Trance': {
        img: 'icons/magic/light/orb-shadow-blue.webp',
    },
    Prolong: {
        img: 'icons/magic/time/hourglass-brown-orange.webp',
    },
    'Psionic Inflation': {
        img: 'icons/magic/unholy/beams-impact-pink.webp',
    },
    'Psionic Sense': {
        img: 'icons/magic/perception/orb-crystal-ball-scrying-blue.webp',
    },
    'Psychic Drain': {
        img: 'icons/magic/control/energy-stream-link-spiral-teal.webp',
    },
    Receptacle: {
        img: 'icons/sundries/misc/key-ring-long-silver.webp',
    },
    Retrospection: {
        img: 'icons/magic/perception/orb-crystal-ball-scrying.webp',
    },
    Splice: {
        img: 'icons/magic/time/arrows-circling-pink.webp',
    },
    'Stasis Field': {
        img: 'icons/magic/defensive/barrier-shield-dome-blue-purple.webp',
    },
    Wrench: {
        img: 'icons/magic/control/silhouette-grow-shrink-blue.webp',
    },
};
