/**
 *
 * Functions to manage version migration/updates
 *
 */
import * as debug from '../debug.js';
import { ARS } from '../config.js';
import { ARSActionGroup } from '../action/action.js';

export async function migrationChecks() {
    if (!game.user.isDM) return;
    const totalDocuments = game.actors.size + game.scenes.size + game.items.size;

    // previous '2024.04.31';
    const _MIGRATE_VERSION = '2024.06.12';
    const systemCurrentVersion = game.settings.get('ars', 'systemCurrentVersion') || game.system.version;
    const needsMigration = foundry.utils.isNewerVersion(_MIGRATE_VERSION, systemCurrentVersion) && totalDocuments !== 0;
    // const needsMigration = false;

    console.log(
        'migration.js migrationChecks',
        { _MIGRATE_VERSION, systemCurrentVersion, needsMigration },
        game.system.version
    );
    if (needsMigration) {
        console.log('migration.js migrationChecks UPDATING');
        await migrateWorld();
    }

    // check for datamodel migrate flag on actor/item
    const needActorModelMigration = game.actors.some((actor) => {
        // Check if the actor itself needs migration
        if (actor.system.migrate === true) {
            return true;
        }
        // Check if any of the actor's items need migration
        return actor.items && actor.items.some((item) => item.system.migrate === true);
    });
    // check if any items in world need migration
    const needItemModelMigration = game.items.some((item) => item.system.migrate === true);
    if (needActorModelMigration) migrateActorModel();
    if (needItemModelMigration) migrateItemModel(game.items, 'World');

    //game.packs.get('my-data-module.items').locked
    // end model migration

    return game.settings.set('ars', 'systemCurrentVersion', game.system.version);
}

/**
 * Migrate migrationData() fixes to database for actors
 */
async function migrateActorModel() {
    const label = 'Migrating datamodel actors';
    ui.notifications.info('Starting migrateActorModel...', { permanent: true });
    let loopTotal = game.actors.size;
    let loopCount = 0;
    let count = 0;
    for (const actor of game.actors) {
        if (actor.system.migrate) {
            count++;
            console.log(`migrateActorModel: Updating ${actor.name}...`);
            await actor.updateSource({ 'system.migrate': false });
            await actor.update(actor.toObject(), { diff: false, recursive: false });
        }
        if (actor.items && actor.items.contents.some((item) => item.system.migrate === true))
            migrateItemModel(actor.items, actor.name);
        SceneNavigation.displayProgressBar({
            label: label,
            pct: Math.round((loopCount * 100) / loopTotal),
        });
    }
    SceneNavigation.displayProgressBar({
        label: label,
        pct: 100,
    });
    ui.notifications.info(`...Completed ${count} migrateActorModel`, { permanent: true });
}
/**
 * Migrate migrationData() fixes to database for items
 */
async function migrateItemModel(itemsList, name = '') {
    const label = 'Migrating datamodel items';
    ui.notifications.info(`Starting migrateItemModel ${name ? name : ''}...`, { permanent: name == 'World' });
    let loopTotal = itemsList.length;
    let loopCount = 0;
    let count = 0;
    for (const item of itemsList) {
        loopCount++;
        if (item.system.migrate) {
            count++;
            console.log(`migrateItemModel: Updating ${item.name}...`);
            await item.updateSource({ 'system.migrate': false });
            await item.update(item.toObject(), { diff: false, recursive: false });
        }
        SceneNavigation.displayProgressBar({
            label: label,
            pct: Math.round((loopCount * 100) / loopTotal),
        });
    }
    SceneNavigation.displayProgressBar({ label: b, pct: 100 });
    ui.notifications.info(`...Completed ${count} migrateItemModel`, { permanent: name == 'World' });
}
/**
 *
 * Migrate all world data
 *
 */
export async function migrateWorld() {
    ui.notifications.info(
        `Applying System Migration for version ${game.system.version}. Please be patient and do not close your game or shut down your server.`,
        { permanent: true }
    );

    // const migrationData = await getMigrationData();
    let migrationData;

    // Migrate World Actors
    let loopTotal = game.actors.size;
    let loopCount = 0;
    console.groupCollapsed('ARS | Actor Migration');
    for (let a of game.actors) {
        loopCount++;
        try {
            const updateData = migrateActorData(a.toObject(), migrationData, a);
            if (!foundry.utils.isEmpty(updateData)) {
                console.log(`Migrating Actor document ${a.name}`);
                await a.update(updateData, { enforceTypes: false });
                ARSActionGroup.loadAll(a);
            }
            //CONFIG.ActiveEffect.legacyTransferral changes
            _migrateLegacyEffects(a);
            // const deleteIds = _duplicatedEffects(a);
            // if (deleteIds.size) await a.deleteEmbeddedDocuments('ActiveEffect', Array.from(deleteIds));
        } catch (err) {
            err.message = `Failed system migration for Actor ${a.name}: ${err.message}`;
            console.error(err);
        }
        SceneNavigation.displayProgressBar({ label: 'Migrating actors', pct: Math.round((loopCount * 100) / loopTotal) });
    }
    SceneNavigation.displayProgressBar({ label: 'Migrating actors', pct: 100 });
    console.groupEnd();

    // Migrate World Items
    loopTotal = game.items.size;
    loopCount = 0;
    console.groupCollapsed('ARS | Item Migration');
    for (let i of game.items) {
        loopCount++;
        try {
            const updateData = migrateItemData(i.toObject(), migrationData, i);
            if (!foundry.utils.isEmpty(updateData)) {
                console.log(`Migrating Item document ${i.name}`);
                await i.update(updateData, { enforceTypes: false });
                ARSActionGroup.loadAll(i);
            }
        } catch (err) {
            err.message = `Failed system migration for Item ${i.name}: ${err.message}`;
            console.error(err);
        }
        SceneNavigation.displayProgressBar({ label: 'Migrating items', pct: Math.round((loopCount * 100) / loopTotal) });
    }
    SceneNavigation.displayProgressBar({ label: 'Migrating items', pct: 100 });
    console.groupEnd();
    // // Migrate World Macros
    // for (const m of game.macros) {
    //     try {
    //         const updateData = migrateMacroData(m.toObject(), migrationData);
    //         if (!foundry.utils.isEmpty(updateData)) {
    //             console.log(`Migrating Macro document ${m.name}`);
    //             await m.update(updateData, { enforceTypes: false });
    //         }
    //     } catch (err) {
    //         err.message = `Failed dnd5e system migration for Macro ${m.name}: ${err.message}`;
    //         console.error(err);
    //     }
    // }

    // Migrate Actor Override Tokens
    loopTotal = game.items.size;
    loopCount = 0;
    console.groupCollapsed('ARS | Scene Migration');
    for (let s of game.scenes) {
        loopCount++;
        try {
            const updateData = migrateSceneData(s, migrationData);
            if (!foundry.utils.isEmpty(updateData)) {
                console.log(`Migrating Scene document ${s.name}`);
                await s.update(updateData, { enforceTypes: false });
                // If we do not do this, then synthetic token actors remain in cache
                // with the un-updated actorData.
                s.tokens.forEach((t) => (t._actor = null));
            }
        } catch (err) {
            err.message = `Failed system migration for Scene ${s.name}: ${err.message}`;
            console.error(err);
        }
        SceneNavigation.displayProgressBar({ label: 'Migrating scenes', pct: Math.round((loopCount * 100) / loopTotal) });
    }
    SceneNavigation.displayProgressBar({ label: 'Migrating scenes', pct: 100 });
    console.groupEnd();
    // Migrate World Compendium Packs
    loopTotal = game.items.size;
    loopCount = 0;
    console.groupCollapsed('ARS | Pack Migration');
    for (let p of game.packs) {
        loopCount++;
        if (p.metadata.packageType !== 'world') continue;
        if (!['Actor', 'Item', 'Scene'].includes(p.documentName)) continue;
        await migrateCompendium(p);
        SceneNavigation.displayProgressBar({ label: 'Migrating world packs', pct: Math.round((loopCount * 100) / loopTotal) });
    }
    SceneNavigation.displayProgressBar({ label: 'Migrating world packs', pct: 100 });
    console.groupEnd();
    // Set the migration as complete
    console.log('migration.js migrationChecks UPDATING COMPLETE');
    game.settings.set('ars', 'systemCurrentVersion', game.system.version);
    ui.notifications.info(
        `System Migration to version ${game.system.version} completed! It is recommended to reload now (PRESS F5)`,
        { permanent: true }
    );
}

/**
 *
 * Migrate compendium data
 *
 * @param {*} pack
 * @returns
 */
export const migrateCompendium = async function (pack) {
    const documentName = pack.documentName;
    if (!['Actor', 'Item', 'Scene'].includes(documentName)) return;

    // const migrationData = await getMigrationData();
    let migrationData; // currently not used

    // Unlock the pack for editing
    const wasLocked = pack.locked;
    await pack.configure({ locked: false });

    // Begin by requesting server-side data model migration and get the migrated content
    await pack.migrate();
    const documents = await pack.getDocuments();

    // Iterate over compendium entries - applying fine-tuned migration functions
    console.log('migrateCompendium documents', { documents });
    for (let doc of documents) {
        console.log('migrateCompendium', { doc });
        let updateData = {};
        try {
            const source = doc.toObject();
            switch (documentName) {
                case 'Actor':
                    updateData = migrateActorData(source, migrationData, doc);
                    // cleanup for legacyTransfer change
                    _migrateLegacyEffects(doc);
                    // if (source.effects && source.items) {
                    //     const deleteIds = _duplicatedEffects(source);
                    //     if (deleteIds.size) await doc.deleteEmbeddedDocuments('ActiveEffect', Array.from(deleteIds));
                    // }
                    break;
                case 'Item':
                    updateData = migrateItemData(source, migrationData, doc);
                    break;
                case 'Scene':
                    updateData = migrateSceneData(doc.data, migrationData);
                    break;
            }

            // Save the entry, if data was changed
            if (foundry.utils.isEmpty(updateData)) continue;
            await doc.update(updateData);
            ARSActionGroup.loadAll(doc);
            console.log(`Migrated ${documentName} document ${doc.name} in Compendium ${pack.collection}`);
        } catch (err) {
            // Handle migration failures
            err.message = `Failed system migration for document ${doc.name} in pack ${pack.collection}: ${err.message}`;
            console.error(err);
        }
    }

    // Apply the original locked status for the pack
    await pack.configure({ locked: wasLocked });
    console.log(`Migrated all ${documentName} documents from Compendium ${pack.collection}`);
};

/**
 *
 * Migrate actor data
 *
 * @param {*} actor
 * @param {*} migrationData
 * @returns
 */
export const migrateActorData = function (actor, migrationData, actorProper = undefined) {
    const systemCurrentVersion = game.settings.get('ars', 'systemCurrentVersion') || game.system.version;
    const updateData = {};

    // Actor Data Updates
    if (actor.system) {
        // _migrateActorMovement(actor, updateData);
        // _migrateActorActions(actor, updateData);
        _migrateActorEffects(actor, updateData);
        if (foundry.utils.isNewerVersion('2023.08.17', systemCurrentVersion)) _migrateActorCurrency(actor, updateData);

        if (actorProper.uuid) {
            // for this update we store uuid into the source for the action
            const actorUuid = actor;
            actorUuid.uuid = actorProper.uuid;
            _migrateActions(actorUuid, updateData);
        }
    }

    // Migrate Owned Items
    if (!actor.items) return updateData;
    const items = actor.items.reduce((arr, i) => {
        // Migrate the Owned Item
        const itemData = i instanceof CONFIG.Item.documentClass ? i.toObject() : i;
        const itemProper = actorProper ? actorProper.getEmbeddedDocument('Item', i._id) : undefined;
        let itemUpdate = migrateItemData(itemData, migrationData, itemProper);

        // Update the Owned Item
        if (!foundry.utils.isEmpty(itemUpdate)) {
            itemUpdate._id = itemData._id;
            arr.push(expandObject(itemUpdate));
        }
        return arr;
    }, []);
    if (items.length > 0) updateData.items = items;
    return updateData;
};

/**
 *
 * migrate item data changes
 *
 * @param {*} item
 * @param {*} migrationData
 * @returns
 */
export const migrateItemData = function (item, migrateData, itemProper = undefined) {
    const updateData = {};

    // _migrateCost(item, updateData);
    // _migrateItemClass(item, updateData);
    // _migrateItemActions(item, updateData)
    // _migrateItemEffects(item, updateData)
    if (itemProper.uuid) {
        // for this update we store uuid into the source for the action
        const itemUuid = item;
        itemUuid.uuid = itemProper.uuid;
        _migrateActions(itemUuid, updateData);
    }

    // console.log(`migration.js migrateItemData ${item.name}`, { updateData })
    return updateData;
};

/**
 * Migrate a single Scene document to incorporate changes to the data model of it's actor data overrides
 * Return an Object of updateData to be applied
 * @param {object} scene            The Scene data to Update
 * @param {object} [migrationData]  Additional data to perform the migration
 * @returns {object}                The updateData to apply
 */
export const migrateSceneData = function (scene, migrationData) {
    const tokens = scene.tokens.map((token) => {
        const t = token instanceof foundry.abstract.DataModel ? token.toObject() : token;
        const update = {};

        // _migrateTokenImage(t, update);
        console.log('Migrating scene====>', scene);
        _migrateTokenObjects(t, update, token);

        if (Object.keys(update).length) foundry.utils.mergeObject(t, update);
        if (!t.actorId || t.actorLink) {
            t.delta = {};
        } else if (!game.actors.has(t.actorId)) {
            t.actorId = null;
            t.delta = {};
        } else if (!t.actorLink) {
            const actorData = foundry.utils.duplicate(t.delta);
            actorData.type = token.actor?.type;
            const update = migrateActorData(actorData, migrationData);
            ['items', 'effects'].forEach((embeddedName) => {
                if (!update[embeddedName]?.length) return;
                const updates = new Map(update[embeddedName].map((u) => [u._id, u]));
                t.delta[embeddedName].forEach((original) => {
                    const update = updates.get(original._id);
                    if (update) foundry.utils.mergeObject(original, update);
                });
                delete update[embeddedName];
            });

            foundry.utils.mergeObject(t.delta, update);
        }
        return t;
    });
    return { tokens };
};

/**
 *
 * This checks tokens for action/effects not on their synthetic parent and updates
 *
 * @param {*} token
 * @param {*} updateData
 * @returns
 */
function _migrateTokenObjects(tokenObject, updateData, token) {
    const systemCurrentVersion = game.settings.get('ars', 'systemCurrentVersion') || game.system.version;
    console.log(`migration.js checking _migrateTokenObjects ${tokenObject.name}`, { tokenObject, token, updateData });
    if (foundry.utils.isNewerVersion('2023.08.17', systemCurrentVersion)) {
        _migrateNPCCurrency(token, updateData);
    }

    return updateData;
}

/**
 *
 * NPC currency is a bit different, we only migrate if they are on the map
 * since currency can be a formula created when dropped
 *
 * @param {*} token
 * @param {*} updateData
 * @returns
 */
function _migrateNPCCurrency(token, updateData) {
    console.log(`migration.js _migrateNPCCurrency ${token.name}`, { token, updateData });
    // this should be an evergreen update since we moved from int[array] for coins.
    // get rid of this 24.12.01

    if (token.actor.type !== 'character' && (token.uuid.includes('Scene') || token.uuid.includes('Token'))) {
        const actualActor = token.actor;
        if (actualActor.system.currency) {
            for (const coin in actualActor.system.currency) {
                const count = parseInt(actualActor.system.currency[coin]) || 0;
                if (count > 0) {
                    console.warn(`NPC: Migrating coin type ${coin} for ${actualActor.name} to item style.`);
                    try {
                        actualActor.giveSpecificCurrency(count, coin);
                        actualActor.update({ [`system.currency.${coin}`]: 0 });
                    } catch (err) {
                        console.error(err);
                    }
                }
            }
        }
    }
}

/* -------------------------------------------- */
// /**
//  *
//  * Convert from original "100 gp" to new value/currency
//  *
//  * @param {*} item
//  * @param {*} updateData
//  * @returns
//  */
// function _migrateCost(item, updateData) {
//     console.log("migration.js _migrateCost", { item, updateData })
//     if (['item', 'weapon', 'armor', 'potion'].includes(item.type)) {
//         const originalCost = item.cost;
//         if (originalCost) {
//             if (originalCost.match(/\d+ \w+/)) {
//                 const [itemCost, itemCurrency] = originalCost.split(" ");
//                 const newCost = { value: parseInt(itemCost) || 0, currency: itemCurrency };
//                 updateData["cost"] = newCost;
//             } else {
//                 updateData["-=cost"] = null;
//             }
//         }
//     }
//     return updateData;
// }

function _migrateActorCurrency(actor, updateData) {
    console.log('migration.js _migrateActorCurrency', { actor, updateData });

    // this should be an evergreen update since we moved from int[array] for coins.
    // get rid of this 24.12.01
    if (actor.type === 'character') {
        const actualActor = game.actors.get(actor._id);
        console.log('_migrateActorCurrency actualActor', actualActor);
        if (actualActor.system.currency) {
            for (const coin in actualActor.system.currency) {
                const count = parseInt(actualActor.system.currency[coin]) || 0;
                if (count > 0) {
                    console.warn(`Character: Migrating coin type ${coin} for ${actualActor.name} to item style.`);
                    try {
                        actualActor.giveSpecificCurrency(count, coin);
                        actualActor.update({ [`system.currency.${coin}`]: 0 });
                    } catch (err) {
                        console.error(err);
                    }
                }
            }
        }
    }
}

/**
 *
 * This checks for actions on an actor and updates them
 *
 * @param {*} actor
 * @param {*} updateData
 */
function _migrateActorActions(actor, updateData) {
    // console.log("migration.js _migrateActorActions", { actor, updateData })
    if (actor.system.actions)
        for (const action of actor.system.actions) {
            if (action.effect.changes.length) _migrateAction(actor, action, updateData);
        }
}
/**
 *
 * This checks for actions on items and updates them
 *
 * @param {*} item
 * @param {*} updateData
 */
function _migrateItemActions(item, updateData) {
    // console.log("migration.js _migrateItemActions", { item, updateData })
    if (item.system.actions)
        for (const action of item.system.actions) {
            if (action.effect.changes.length) _migrateAction(item, action, updateData);
        }
}

/**
 *
 * Migrate actions to actionGroups
 *
 * @param {*} object
 * @param {*} updateData
 */
function _migrateActions(object, updateData) {
    const systemCurrentVersion = game.settings.get('ars', 'systemCurrentVersion') || game.system.version;

    if (foundry.utils.isNewerVersion('2024.06.12', systemCurrentVersion)) {
        const actionGroups = game.ars.ARSAction.convertFromActionBundle(object, object.system.actions);
        const serialized = game.ars.ARSActionGroup.serializeGroups(actionGroups);
        // object.update({ 'system.actionGroups': serialized });
        updateData['system.actionGroups'] = serialized;
    }
}

/**
 *
 * This fixes actions that apply effects
 *
 * @param {*} object
 * @param {*} action
 * @param {*} updateData
 */
function _migrateAction(object, action, updateData) {
    // console.log(`migration.js _migrateAction ${object.name}`, { object, action, updateData })
    let actionBundle = foundry.utils.deepClone(foundry.utils.getProperty(object, 'system.actions') || []);
    actionBundle = Object.values(actionBundle);
    for (const [index, change] of action.effect.changes.entries()) {
        // test if the action needs to be updated!!!
        // console.log("migration.js _migrateAction", { index, change })
        // _migrateEffectChange_230401(object, action.effect, change, updateData);
        // actionBundle[action.index].effect.changes[index].key = change.key;
        // actionBundle[action.index].effect.changes[index].value = change.value;
    }
    // set this so it persists through looping over actions
    object.system.actions = actionBundle;
    //
    updateData['system.actions'] = actionBundle;
    // console.log(`migration.js _migrateAction updateData for ${object.name}`, { actionBundle, updateData })
}

/**
 *
 * This checks for effects on actors and updates
 *
 * @param {*} actor
 * @param {*} updateData
 */
async function _migrateActorEffects(actor, updateData) {
    if (actor.effects) {
        // let effectsBundle = foundry.utils.duplicate(actor.effects);
        // if (effectsBundle.length) {
        // for (const effect of effectsBundle) {
        // for (const change of effect.changes) {
        // _migrateEffectChange_230401(actor, effect, change, updateData);
        // }
        // }
        // actor.update({ 'effects': effectsBundle });
        // updateData['effects'] = effectsBundle;
        // }
    }
}
/**
 *
 * This checks for effects on items and updates
 *
 * @param {*} item
 * @param {*} updateData
 */
function _migrateItemEffects(item, updateData) {
    let effectsBundle = foundry.utils.duplicate(item.effects);
    if (effectsBundle.length) {
        for (const effect of effectsBundle) {
            for (const change of effect.changes) {
                _migrateEffectChange_230401(item, effect, change, updateData);
            }
        }
        // item.update({ 'effects': effectsBundle });
        updateData['effects'] = effectsBundle;
    }
}

/**
 * Migrate legacy duplicate effects
 *
 * @param {*} source
 */
async function _migrateLegacyEffects(source) {
    const systemCurrentVersion = game.settings.get('ars', 'systemCurrentVersion') || game.system.version;
    if (foundry.utils.isNewerVersion('2024.04.31', systemCurrentVersion)) {
        if (source.effects && source.items) {
            const deleteIds = _duplicatedEffects(source);
            if (deleteIds.size) await source.deleteEmbeddedDocuments('ActiveEffect', Array.from(deleteIds));
        }
    }
}
/**
 * Identify effects that might have been duplicated when legacyTransferral was disabled.
 * @param {object} parent   Data of the actor being migrated.
 * @returns {Set<string>}   IDs of effects to delete from the actor.
 * @private
 */
function _duplicatedEffects(parent) {
    const deleteIds = new Set();
    for (const item of parent.items) {
        for (const effect of item.effects ?? []) {
            if (!effect.transfer) continue;
            const match = parent.effects.find((t) => {
                const diff = foundry.utils.diffObject(t, effect);
                return t.origin?.endsWith(`Item.${item._id}`) && !('changes' in diff) && !deleteIds.has(t._id);
            });
            if (match) deleteIds.add(match._id);
        }
    }
    return deleteIds;
}

/**
 *
 * This will update the effect.changes change entries for the 23.04.1 effect mass changes
 *
 * @param {*} object
 * @param {*} effect
 * @param {*} change
 * @param {*} updateData
 */
function _migrateEffectChange_230401(object, effect, change, updateData) {
    const systemCurrentVersion = game.settings.get('ars', 'systemCurrentVersion') || game.system.version;
    if (foundry.utils.isNewerVersion('23.04.01', systemCurrentVersion)) {
        let detailsCheck = undefined;
        try {
            detailsCheck = JSON.parse(change.value);
        } catch {}
        const migratedAlready =
            detailsCheck &&
            (foundry.utils.hasProperty(detailsCheck, 'formula') || foundry.utils.hasProperty(detailsCheck, 'properties'));

        if (change.key.toLowerCase() === 'light') {
            console.log(`migration.js _migrationEffects LIGHT ${object.name}`, { object, effect, change });
            const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
            const details = {
                color: opts[0],
                dim: opts[1],
                bright: opts[2] || opts[1],
                angle: opts[3] || 360,
                animation: opts[4] || undefined,
                alpha: opts[5] || undefined,
            };
            change.key = 'special.light';
            change.mode = 0;
            change.value = JSON.stringify(details);
        } else if (change.key.toLowerCase() === 'vision') {
            console.log(`migration.js _migrationEffects VISION ${object.name}`, { object, effect, change });
            // { name: "special.vision", mode: 0, value: '{"range": 10, "angle": 360, "mode": "basic"}' }
            const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
            const details = {
                range: parseInt(opts[0]) || 10,
                angle: opts[1] || 360,
                mode: opts[2] || 'basic',
            };
            change.key = 'special.vision';
            change.mode = 0;
            change.value = JSON.stringify(details);
        } else if (change.key.toLowerCase() === 'absorb') {
            console.log(`migration.js _migrationEffects ABSORB ${object.name}`, { object, effect, change });
            // { name: "special.absorb", mode: 0, value: '{"amount":10, "damageType":"fire"}' },
            const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
            const details = {
                amount: parseInt(opts[0]) || 10,
                damageType: opts[1] || 'fire',
            };
            change.key = 'special.absorb';
            change.mode = 0;
            change.value = JSON.stringify(details);
        } else if (change.key.toLowerCase() === 'aura') {
            console.log(`migration.js _migrationEffects AURA ${object.name}`, { object, effect, change });
            // { name: "special.aura", mode: 0, value: '{"distance": 10, "color": "red", "faction": "friendly", "opacity": "0.50", "shape": "round"}' },
            const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
            const details = {
                distance: parseInt(opts[0]) || 10,
                color: opts[1] || 'red',
                faction: opts[2] || 'friendly',
                opacity: parseFloat(opts[3]) || 0.5,
                shape: opts[4] || 'round',
            };
            change.key = 'special.aura';
            change.mode = 0;
            change.value = JSON.stringify(details);
        } else if (change.key.toLowerCase() === 'system.mods.heal.regen') {
            console.log(`migration.js _migrationEffects system.mods.heal.regen ${object.name}`, { object, effect, change });
            const details = {
                type: 'heal',
                rate: 1,
                cycle: 'round',
                dmgType: '',
                formula: parseInt(change.value) || 1,
            };
            change.key = 'special.ongoing';
            change.mode = 0;
            change.value = JSON.stringify(details);
        } else if (change.key.toLowerCase().startsWith('ongoing ')) {
            console.log(`migration.js _migrationEffects ONGOING ${object.name}`, { object, effect, change });
            // { name: "special.ongoing", mode: 0, value: '{"type":"heal", "rate":"1", "cycle": "round", "dmgType": "", "formula": "1d4"}' },
            const opts = change.key.split(' ').map((text) => text.trim().toLowerCase());
            const valueOpts = change.value.split(' ').map((text) => text.trim().toLowerCase());
            const details = {
                type: opts[1] || 'heal',
                rate: parseInt(opts[3]) || 1,
                cycle: opts[4] || 'round',
                dmgType: valueOpts[1] || '',
                formula: valueOpts[0] || 1,
            };
            change.key = 'special.ongoing';
            change.mode = 0;
            change.value = JSON.stringify(details);
        } else if (change.key.toLowerCase() === 'mirrorimage') {
            console.log(`migration.js _migrationEffects MIRRORIMAGE ${object.name}`, { object, effect, change });
            // { name: "special.mirrorimage", mode: 0, value: 5 },
            change.key = 'special.mirrorimage';
            change.mode = 0;
            change.value = change.value;
        } else if (change.key.toLowerCase() === 'stoneskin') {
            console.log(`migration.js _migrationEffects STONESKIN ${object.name}`, { object, effect, change });
            // { name: "special.stoneskin", mode: 0, value: 5 },
            change.key = 'special.stoneskin';
            change.mode = 0;
            change.value = change.value;
        } else if (change.key.toLowerCase() === 'status') {
            console.log(`migration.js _migrationEffects STATUS ${object.name}`, { object, effect, change });
            // { name: "special.status", mode: 0, value: 'blind' },
            change.key = 'special.status';
            change.mode = 0;
            change.value = change.value;
        } else if (!migratedAlready) {
            //check detailsCheck to make sure we only do these once.

            if (change.key.toLowerCase().startsWith('target.')) {
                console.log(`migration.js _migrationEffects TARGET. ${object.name}`, { object, effect, change });
                // { name: "target.alignment", mode: 0, value: '{"trigger": "ne,ce,le", "properties": "", "type": "attack", "formula": "1d6"}' },
                const opts1 = change.key.split(':').map((text) => text.trim().toLowerCase());
                const opts2 = opts1[0].split('.').map((text) => text.trim().toLowerCase());
                const triggerType = opts2[1];
                let saveType = '';
                if (opts1[1]?.startsWith('save')) {
                    // get the saveTypes
                    //save,spell,breath,poison
                    //remove save
                    const arr = opts1[1].split(',');
                    // put everything else in saveType
                    saveType = arr.slice(1).join(',');
                }
                const details = {
                    trigger: opts2[2],
                    type: saveType ? 'save' : opts1[1] || 'attack',
                    saveType: saveType ? saveType : undefined,
                    properties: '',
                    formula: change.value,
                };
                change.key = `target.${triggerType}`;
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase().startsWith('attacker.')) {
                console.log(`migration.js _migrationEffects ATTACKER. ${object.name}`, { object, effect, change });
                // { name: "attacker.alignment", mode: 0, value: '{"trigger": "ne,ce,le", "properties": "", "type": "attack", "formula": "1d6"}' },
                const opts1 = change.key.split(':').map((text) => text.trim().toLowerCase());
                const opts2 = opts1[0].split('.').map((text) => text.trim().toLowerCase());
                const triggerType = opts2[1];
                const details = {
                    trigger: opts2[2],
                    type: opts1[1] || 'attack',
                    properties: '',
                    formula: change.value,
                };
                change.key = `attacker.${triggerType}`;
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.paralyzation') {
                console.log(`migration.js _migrationEffects PARA ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.paralyzation", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.paralyzation';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.all') {
                console.log(`migration.js _migrationEffects ALL ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.poison", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.all';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.poison') {
                console.log(`migration.js _migrationEffects POISON ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.poison", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.poison';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.death') {
                console.log(`migration.js _migrationEffects DEATH ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.death", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.death';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.rod') {
                console.log(`migration.js _migrationEffects ROD ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.rod", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.rod';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.staff') {
                console.log(`migration.js _migrationEffects STAFF ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.staff", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.staff';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.wand') {
                console.log(`migration.js _migrationEffects WAND ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.wand", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.wand';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.petrification') {
                console.log(`migration.js _migrationEffects PETRI ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.petrification", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.petrification';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.polymorph') {
                console.log(`migration.js _migrationEffects POLY ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.polymorph", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.polymorph';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.breath') {
                console.log(`migration.js _migrationEffects BREATH ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.breath", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.breath';
                change.mode = 0;
                change.value = JSON.stringify(details);
            } else if (change.key.toLowerCase() === 'system.mods.saves.spell') {
                console.log(`migration.js _migrationEffects SPELL ${object.name}`, { object, effect, change });
                // { name: "system.mods.saves.spell", mode: 0, value: '{"formula": "1", "properties": ""}' },
                const opts = change.value.split(' ').map((text) => text.trim().toLowerCase());
                const details = {
                    formula: opts[0] || 1,
                    properties: opts[1] || '',
                };
                change.key = 'system.mods.saves.spell';
                change.mode = 0;
                change.value = JSON.stringify(details);
            }
        }
    }
}
/**
 *
 * Remove the old @abilities.con.hp
 *
 * @param {*} item
 * @param {*} updateData
 */
function _migrateItemClass(item, updateData) {
    if (['class'].includes(item.type)) {
        if (item.system.features.hpConFormula === '@abilities.con.hp') {
            console.log(`Updating ${item.name} with ${item.system.features.hpConFormula}`);
            // console.log("", { updateData })
            foundry.utils.mergeObject(updateData, {
                system: {
                    features: {
                        hpConFormula: '',
                    },
                },
            });
        }
    }
}

/** ------------------- ChangeLog ----------------------- */
class ARSChangeLogView extends Application {
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            resizable: true,
            minimizable: true,
            id: 'changelog-view-sheet',
            classes: ['ars', 'changelog-view'],
            title: 'Change Log View',
            template: 'systems/ars/templates/apps/changelog-view.hbs',
            width: 700,
            height: 600,
            // scrollY: [".filter-list", ".item-list"],
        });
    }

    async getData() {
        const data = await super.getData();

        data.gameSystem = game.system;
        data.ARS = ARS;

        return data;
    }
}

export function changeLogLoadView() {
    if (!game.ars.ui?.changelogView) {
        game.ars.ui = {
            changelogView: new ARSChangeLogView(),
        };
        game.ars.ui.changelogView.render(true);
    }
}
/**
 * This checks for updates and then displays details. Not really a change log but gives details if changeed.
 */
export function changeLogChecks() {
    const logVersion = game.settings.get('ars', 'systemChangeLogVersion');
    const needsReview = !logVersion || logVersion != game.system.version; //foundry.utils.isNewerVersion('2023.01.01', logVersion);

    console.log('migration.js changeLogChecks', { logVersion, needsReview }, game.system.version);

    if (needsReview) {
        console.log('migration.js changeLogChecks REVIEW!');
        game.settings.set('ars', 'systemChangeLogVersion', game.system.version);
        changeLogLoadView();
    }
}

/**
 * @summary: because psionics modified the class data with psionic ranks - we need to backfill the existing classes so that something exists for the u.i.
 */
async function _backfillPsionicRankData() {
    // Define the psionic data to be added
    const psionicData = {
        disciplines: 0,
        sciences: 0,
        devotions: 0,
        defenseModes: 0,
        psp: 0,
    };

    // Helper function to update ranks for a given item
    const updateRanksWithPsionics = async (classItem) => {
        let ranks = classItem.system.ranks;
        let psionicLevel = 1;

        let updatedRanks = ranks.map((rank) => {
            let newRank = { ...rank };

            // Add psionic data if missing
            if (!newRank.psionic) {
                newRank.psionic = psionicData;
            }

            // Check if casterlevel.psionic is missing or null, and if so, assign and increment
            if (newRank.casterlevel && newRank.casterlevel.psionic == null) {
                newRank.casterlevel.psionic = psionicLevel;
                psionicLevel++;
            }

            return newRank;
        });

        // Update the class item with the new ranks
        await classItem.update({ 'system.ranks': updatedRanks });
    };

    // Get all class items from actors and global items
    const allActors = game.actors;
    const allItems = game.items.filter((item) => item.type === 'class');

    // Update class items for all actors
    for (let actor of allActors) {
        let classItems = actor.items.filter((item) => item.type === 'class');
        for (let classItem of classItems) {
            await updateRanksWithPsionics(classItem);
        }
    }

    // Update global class items
    for (let classItem of allItems) {
        await updateRanksWithPsionics(classItem);
    }
}
