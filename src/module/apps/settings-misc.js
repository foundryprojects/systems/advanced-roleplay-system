import { ARS } from '../config.js';
import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
import * as debug from '../debug.js';

export class ARSSettingsMisc extends FormApplication {
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            id: 'ars-settings-misc-form',
            title: 'Configure Misc Settings',
            template: 'systems/ars/templates/apps/settings-misc.hbs',
            width: 600,
            // height: 860,
            resizable: true,
            height: 'auto',
            closeOnSubmit: true,
        });
    }

    async getData() {
        let data = super.getData();
        foundry.utils.mergeObject(data, {
            usePsionics: await game.settings.get('ars', 'usePsionics'),
            usePsionicsV1: await game.settings.get('ars', 'usePsionicsV1'),
            useInfoHelper: await game.settings.get('ars', 'useInfoHelper'),
            combineStack: await game.settings.get('ars', 'combineStack'),
            macroDontCloneOnDrop: await game.settings.get('ars', 'macroDontCloneOnDrop'),
            useExperimental: await game.settings.get('ars', 'useExperimental'),
            useAscendingAC: await game.settings.get('ars', 'useAscendingAC'),
            useComeliness: await game.settings.get('ars', 'useComeliness'),
            useHonor: await game.settings.get('ars', 'useHonor'),
        });

        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);
    }

    async _updateObject(event, formData) {
        for (let setting in formData) {
            game.settings.set('ars', setting, formData[setting]);
        }
        window.location.reload();
    }
}
